#include "dvmh_data.h"

#include <cassert>
#include <cstring>
#include <algorithm>
#include <set>
#include <cmath>

#include "util.h"
#include "dvmh_device.h"
#include "dvmh_copying.h"
#include "cuda_transform.h"
#include "region.h"
#include "dvmh_rts.h"
#include "loop.h"
#include "dvmh_stat.h"

namespace libdvmh {

// Intervals

void Intervals::append(const Interval &inter) {
    if (!inter.empty()) {
        checkInternal2(intervals.empty() || intervals.back()[1] + 1 < inter[0], "Use unite instead");
        intervals.push_back(inter);
    }
}

void Intervals::unite(const Interval &inter) {
    if (inter.empty()) {
        // Nothing to do here
    } else if (intervals.empty() || intervals.back()[1] + 1 < inter[0]) {
        // Append new to the end
        intervals.push_back(inter);
    } else if (inter[0] >= intervals.back()[0] && inter[0] <= intervals.back()[1] + 1) {
        // Extend last
        intervals.back()[1] = inter[1];
    } else {
        // Common case
        Interval toReplace = Interval::create(upperIndex(&intervals[0], intervals.size(), inter[0]), lowerIndex(&intervals[0], intervals.size(), inter[1]));
        if (toReplace.empty()) {
            // Insert new
            int where = toReplace[0];
            intervals.resize(intervals.size() + 1);
            for (int i = (int)intervals.size() - 1; i > where; i--)
                intervals[i] = intervals[i - 1];
            intervals[where] = inter;
        } else {
            // Replace several with one
            int howManyRemove = (int)toReplace.size() - 1;
            int where = toReplace[0];
            if (howManyRemove > 0) {
                intervals[where][1] = intervals[toReplace[1]][1];
                for (int i = where + 1; i < (int)intervals.size() - howManyRemove; i++)
                    intervals[i] = intervals[i + howManyRemove];
                intervals.resize(intervals.size() - howManyRemove);
            }
            intervals[where].encloseInplace(inter);
        }
    }
}

void Intervals::intersect(const Interval &inter) {
    if (empty()) {
        // Nothing to do here
    } else if (inter.empty()) {
        clear();
    } else if (inter[0] <= intervals.front()[0] && inter[1] >= intervals.back()[1]) {
        // Nothing to do here
    } else if (inter[0] <= intervals.front()[1] && inter[1] >= intervals.back()[0]) {
        // Modifications only to edge intervals
        intervals.front()[0] = inter[0];
        intervals.front()[1] = inter[1];
    } else {
        // Common case
        Interval toLeave = Interval::create(upperIndex(&intervals[0], intervals.size(), inter[0]), lowerIndex(&intervals[0], intervals.size(), inter[1]));
        if (toLeave.empty()) {
            clear();
        } else {
            int removeLeft = toLeave[0];
            int removeRight = (int)intervals.size() - 1 - toLeave[1];
            if (removeLeft > 0)
                for (int i = 0; i < (int)intervals.size() - removeLeft - removeRight; i++)
                    intervals[i] = intervals[i + removeLeft];
            if (removeLeft + removeRight > 0)
                intervals.resize((int)intervals.size() - removeLeft - removeRight);
            intervals.front().intersectInplace(inter);
            intervals.back().intersectInplace(inter);
        }
    }
}

DvmhPieces *Intervals::toPieces() const {
    DvmhPieces *res = new DvmhPieces(1);
    for (int i = 0; i < (int)intervals.size(); i++)
        res->appendOne(&intervals[i]);
    return res;
}

Interval Intervals::getBoundInterval() const {
    if (intervals.size() < 1)
        return Interval::createEmpty();
    return Interval::create(intervals[0][0], intervals.back()[1]);
}

// MPSAxis

void MPSAxis::setCoordWeights(double wgts[], bool copyFlag) {
    if (copyFlag) {
        coordWeights = new double[procCount];
        safeMemcpy(coordWeights, wgts, procCount);
    } else {
        coordWeights = wgts;
    }
    double sum = 0;
    for (int i = 0; i < procCount; i++)
        sum += coordWeights[i];
    double coef = procCount / sum;
    for (int i = 0; i < procCount; i++)
        coordWeights[i] *= coef;
    weightedFlag = false;
    for (int i = 0; i < procCount; i++)
        if (fabs(coordWeights[i] - 1.0) > 1e-2)
            weightedFlag = true;
}

void MPSAxis::freeAxisComm() {
    if (ownComm)
        delete axisComm;
    axisComm = 0;
    ownComm = false;
}

// DvmhCommunicator

DvmhCommunicator::DvmhCommunicator(MPI_Comm aComm, int aRank, const int sizes[], bool fortranOrder) {
    rank = aRank;
    assert(rank >= 0);
    while (rank > 0 && sizes[rank - 1] == 1)
        rank--;
    axes = new MPSAxis[rank];
    comm = aComm;
    ownComm = false;
    if (comm == MPI_COMM_NULL) {
        commRank = -1;
    } else {
        int tmpInt;
        checkInternalMPI(MPI_Comm_rank(comm, &tmpInt));
        commRank = tmpInt;
    }
    commSize = 1;
    for (int i = 0; i < rank; i++) {
        assert(sizes[i] > 0);
        axes[i].procCount = sizes[i];
        commSize *= sizes[i];
    }
    if (commRank >= 0) {
        int tmpInt;
        checkInternalMPI(MPI_Comm_size(comm, &tmpInt));
        checkInternal(tmpInt == commSize);
    }
    inverseOrder = fortranOrder;
    int accumSize = 1;
    for (int i = 0; i < rank; i++) {
        int idx = inverseOrder ? i : rank - 1 - i;
        int pc = axes[idx].procCount;
        axes[idx].commStep = accumSize;
        if (commRank >= 0)
            axes[idx].ourProc = commRank / accumSize % pc;
        else
            axes[idx].ourProc = -1;
        accumSize *= pc;
    }
    if (commRank >= 0) {
        for (int i = 0; i < rank; i++) {
            if (sizes[i] == 1) {
                axes[i].axisComm = selfComm;
            } else if (sizes[i] == commSize) {
                axes[i].axisComm = this;
            } else {
                int color = getCommRankAxisOffset(i + 1, -axes[i].ourProc);
                MPI_Comm axisComm;
                checkInternalMPI(MPI_Comm_split(comm, color, commRank, &axisComm));
                axes[i].axisComm = new DvmhCommunicator(axisComm, 1, sizes + i, fortranOrder);
                axes[i].axisComm->ownComm = true;
                axes[i].ownComm = true;
            }
        }
    }
    resetNextAsyncTag();
}

DvmhCommunicator &DvmhCommunicator::operator=(const DvmhCommunicator &other) {
    clear();
    rank = other.rank;
    axes = new MPSAxis[rank];
    if (other.comm == MPI_COMM_SELF || other.comm == MPI_COMM_NULL) {
        comm = other.comm;
        ownComm = false;
    } else {
        checkInternalMPI(MPI_Comm_dup(other.comm, &comm));
        ownComm = true;
    }
    commSize = other.commSize;
    inverseOrder = other.inverseOrder;
    for (int i = 0; i < rank; i++) {
        axes[i].procCount = other.axes[i].procCount;
        axes[i].commStep = other.axes[i].commStep;
        axes[i].ourProc = other.axes[i].ourProc;
        if (other.axes[i].axisComm) {
            if (other.axes[i].axisComm == &other) {
                axes[i].axisComm = this;
            } else if (!other.axes[i].ownComm) {
                axes[i].axisComm = other.axes[i].axisComm;
            } else {
                axes[i].axisComm = new DvmhCommunicator(*other.axes[i].axisComm);
                axes[i].ownComm = true;
            }
        }
    }
    commRank = other.commRank;
    return *this;
}

int DvmhCommunicator::getCommRank(int aRank, const int indexes[]) const {
    int res = 0;
    for (int i = 0; i < aRank; i++) {
        MPSAxis ax = getAxis(i + 1);
        assert(indexes[i] >= 0 && indexes[i] < ax.procCount);
        res += indexes[i] * ax.commStep;
    }
    return res;
}

int DvmhCommunicator::getCommRankAxisOffset(int axisNum, int offs, bool periodic) const {
    checkInternal(commRank >= 0);
    MPSAxis ax = getAxis(axisNum);
    if (!periodic) {
        assert(ax.ourProc + offs >= 0 && ax.ourProc + offs < ax.procCount);
        return commRank + offs * ax.commStep;
    } else {
        return commRank + ((ax.ourProc + offs % ax.procCount + ax.procCount) % ax.procCount - ax.ourProc) * ax.commStep;
    }
}

int DvmhCommunicator::getAxisIndex(int aCommRank, int axisNum) const {
    assert(aCommRank >= 0 && aCommRank < commSize);
    MPSAxis ax = getAxis(axisNum);
    return aCommRank / ax.commStep % ax.procCount;
}

void DvmhCommunicator::fillAxisIndexes(int aCommRank, int aRank, int indexes[]) const {
    assert(aCommRank >= 0 && aCommRank < commSize);
    for (int i = 0; i < aRank; i++) {
        MPSAxis ax = getAxis(i + 1);
        indexes[i] = aCommRank / ax.commStep % ax.procCount;
    }
}

UDvmType DvmhCommunicator::bcast(int root, void *buf, UDvmType size) const {
    resetNextAsyncTag();
    UDvmType rest = size;
    while (rest > 0) {
        int curSize;
        if (rest > INT_MAX)
            curSize = INT_MAX;
        else
            curSize = rest;
        checkInternalMPI(MPI_Bcast((char *)buf + (size - rest), curSize, MPI_BYTE, root, comm));
        rest -= curSize;
    }
    return size;
}

UDvmType DvmhCommunicator::bcast(int root, char **pBuf, UDvmType *pSize) const {
    resetNextAsyncTag();
    char fastBuffer[1024];
    UDvmType fastBufSize = sizeof(fastBuffer);
    UDvmType &size = *pSize;
    char *&buf = *pBuf;
    if (commSize == 1)
        return size;
    UDvmType fastBufDataSize = fastBufSize - sizeof(UDvmType);
    if (commRank == root) {
        BufferWalker fbw(fastBuffer, fastBufSize);
        fbw.putValue(size);
        fbw.putData(buf, std::min(size, fastBufDataSize));
    }
    bcast(root, fastBuffer, fastBufSize);
    if (commRank != root) {
        BufferWalker fbw(fastBuffer, fastBufSize);
        fbw.extractValue(size);
        buf = new char[size];
        fbw.extractData(buf, std::min(size, fastBufDataSize));
    }
    if (size > fastBufDataSize)
        bcast(root, buf + fastBufDataSize, size - fastBufDataSize);
    return size;
}

UDvmType DvmhCommunicator::send(int dest, const void *buf, UDvmType size) const {
    UDvmType rest = size;
    while (rest > 0) {
        int curSize;
        if (rest > INT_MAX)
            curSize = INT_MAX;
        else
            curSize = rest;
        checkInternalMPI(MPI_Send((char *)buf + (size - rest), curSize, MPI_BYTE, dest, 0, comm));
        rest -= curSize;
    }
    return size;
}

UDvmType DvmhCommunicator::recv(int src, void *buf, UDvmType size) const {
    UDvmType rest = size;
    while (rest > 0) {
        int curSize;
        if (rest > INT_MAX)
            curSize = INT_MAX;
        else
            curSize = rest;
        MPI_Status status;
        checkInternalMPI(MPI_Recv((char *)buf + (size - rest), curSize, MPI_BYTE, src, 0, comm, &status));
        int receivedCount = 0;
        checkInternalMPI(MPI_Get_count(&status, MPI_BYTE, &receivedCount));
        assert(receivedCount == curSize);
        rest -= curSize;
    }
    return size;
}

UDvmType DvmhCommunicator::allgather(void *buf, UDvmType elemSize) const {
    resetNextAsyncTag();
    checkInternal(elemSize <= (UDvmType)INT_MAX);
    checkInternalMPI(MPI_Allgather(MPI_IN_PLACE, elemSize, MPI_BYTE, buf, elemSize, MPI_BYTE, comm));
    return elemSize * commSize;
}

UDvmType DvmhCommunicator::allgatherv(void *buf, const UDvmType sizes[]) const {
    resetNextAsyncTag();
    int *recvcounts = new int[commSize];
    int *displs = new int[commSize + 1];
    for (int i = 0; i < commSize; i++) {
        recvcounts[i] = 0;
        displs[i] = 0;
    }
    int procDone = 0;
    UDvmType bytesDone = 0;
    for (int i = 0; i < commSize; i++) {
        if (sizes[i] > INT_MAX) {
            if (procDone < i) {
                checkInternalMPI(MPI_Allgatherv(MPI_IN_PLACE, recvcounts[commRank], MPI_BYTE, (char *)buf + bytesDone, recvcounts, displs, MPI_BYTE, comm));
                bytesDone += (UDvmType)displs[i - 1] + (UDvmType)recvcounts[i - 1];
                for (int j = procDone; j < i; j++) {
                    recvcounts[j] = 0;
                    displs[j] = 0;
                }
                procDone = i;
            }
            bcast(i, (char *)buf + bytesDone, sizes[i]);
            bytesDone += sizes[i];
            procDone = i + 1;
        } else {
            recvcounts[i] = sizes[i];
            if ((UDvmType)displs[i] + sizes[i] > INT_MAX) {
                checkInternalMPI(MPI_Allgatherv(MPI_IN_PLACE, recvcounts[commRank], MPI_BYTE, (char *)buf + bytesDone, recvcounts, displs, MPI_BYTE, comm));
                bytesDone += (UDvmType)displs[i] + (UDvmType)recvcounts[i];
                for (int j = procDone; j <= i; j++) {
                    recvcounts[j] = 0;
                    displs[j] = 0;
                }
                procDone = i + 1;
            }
            displs[i + 1] = displs[i] + recvcounts[i];
        }
    }
    if (procDone < commSize) {
        checkInternalMPI(MPI_Allgatherv(MPI_IN_PLACE, recvcounts[commRank], MPI_BYTE, (char *)buf + bytesDone, recvcounts, displs, MPI_BYTE, comm));
        bytesDone += (UDvmType)displs[commSize - 1] + (UDvmType)recvcounts[commSize - 1];
        procDone = commSize;
    }
    delete[] recvcounts;
    delete[] displs;
    return bytesDone;
}

UDvmType DvmhCommunicator::alltoall(const void *sendBuffer, UDvmType elemSize, void *recvBuffer) const {
    resetNextAsyncTag();
    checkInternal(elemSize <= INT_MAX);
    checkInternalMPI(MPI_Alltoall((void *)sendBuffer, elemSize, MPI_BYTE, recvBuffer, elemSize, MPI_BYTE, comm));
    return elemSize * commSize;
}

UDvmType DvmhCommunicator::alltoallv1(const UDvmType sendSizes[], char *sendBuffers[], UDvmType recvSizes[], char *recvBuffers[]) const {
    alltoall(sendSizes, recvSizes);
    for (int p = 0; p < commSize; p++)
        recvBuffers[p] = recvSizes[p] > 0 ? new char[recvSizes[p]] : 0;
    return alltoallv2(sendSizes, sendBuffers, recvSizes, recvBuffers);
}

UDvmType DvmhCommunicator::alltoallv2(const UDvmType sendSizes[], char *sendBuffers[], const UDvmType recvSizes[], char *recvBuffers[]) const {
    dvmh_log(TRACE, "I am processor %d of %d", commRank, commSize);
    for (int p = 0; p < commSize; p++)
        dvmh_log(TRACE, "I send to %d processor " UDTFMT " bytes", p, sendSizes[p]);
    for (int p = 0; p < commSize; p++)
        dvmh_log(TRACE, "I receive from %d processor " UDTFMT " bytes", p, recvSizes[p]);
    dvmh_log(TRACE, "Current next async tag is %d", nextAsyncTag);
    assert(recvSizes[commRank] == sendSizes[commRank]);
    int sendCount = 0;
    int recvCount = 0;
    for (int p = 0; p < commSize; p++) {
        if (recvSizes[p] > 0)
            assert(recvBuffers[p]);
        if (sendSizes[p] > 0 && p != commRank)
            sendCount++;
        if (recvSizes[p] > 0 && p != commRank)
            recvCount++;
    }
    dvmh_log(TRACE, "Really I send to %d processors and receive from %d processors (excluded myself)", sendCount, recvCount);
    std::vector<MPI_Request> reqs;
    reqs.reserve(sendCount);
    for (int i = 0; i < commSize; i++) {
        int p = (commRank + i) % commSize;
        if (sendSizes[p] > 0 && p != commRank) {
            UDvmType rest = sendSizes[p];
            int tag = nextAsyncTag;
            while (rest > 0) {
                int curSize;
                if (rest > INT_MAX)
                    curSize = INT_MAX;
                else
                    curSize = rest;
                MPI_Request req;
                checkInternalMPI(MPI_Isend(sendBuffers[p] + (sendSizes[p] - rest), curSize, MPI_BYTE, p, tag, comm, &req));
                dvmh_log(TRACE, "Sent to %d processor %d bytes", p, curSize);
                reqs.push_back(req);
                rest -= curSize;
            }
        }
    }
    UDvmType bytesDone = 0;
    if (recvSizes[commRank] > 0) {
        if (recvBuffers[commRank] != sendBuffers[commRank])
            memcpy(recvBuffers[commRank], sendBuffers[commRank], recvSizes[commRank]);
        bytesDone += recvSizes[commRank];
    }
    int receivedCount = 0;
    while (receivedCount < recvCount) {
        int tag = nextAsyncTag;
        MPI_Status st;
        checkInternalMPI(MPI_Probe(MPI_ANY_SOURCE, tag, comm, &st));
        int p = st.MPI_SOURCE;
        assert(recvSizes[p] > 0);
        UDvmType rest = recvSizes[p];
        while (rest > 0) {
            int curSize;
            if (rest > INT_MAX)
                curSize = INT_MAX;
            else
                curSize = rest;
            checkInternalMPI(MPI_Recv(recvBuffers[p] + (recvSizes[p] - rest), curSize, MPI_BYTE, p, tag, comm, &st));
            int receivedSize = 0;
            checkInternalMPI(MPI_Get_count(&st, MPI_BYTE, &receivedSize));
            dvmh_log(TRACE, "Received from %d processor %d bytes", p, receivedSize);
            assert(receivedSize == curSize);
            rest -= curSize;
        }
        receivedCount++;
        bytesDone += recvSizes[p];
    }
    checkInternalMPI(MPI_Waitall(reqs.size(), &reqs[0], MPI_STATUSES_IGNORE));
    advanceNextAsyncTag();
    return bytesDone;
}

UDvmType DvmhCommunicator::alltoallv3(int sendProcCount, const int sendProcs[], const UDvmType sendSizes[], char *sendBuffers[], int recvProcCount,
        const int recvProcs[], const UDvmType recvSizes[], char *recvBuffers[]) const {
    dvmh_log(TRACE, "I am processor %d of %d", commRank, commSize);
    for (int pi = 0; pi < sendProcCount; pi++)
        dvmh_log(TRACE, "I send to %d processor " UDTFMT " bytes", sendProcs[pi], sendSizes[pi]);
    for (int pi = 0; pi < recvProcCount; pi++)
        dvmh_log(TRACE, "I receive from %d processor " UDTFMT " bytes", recvProcs[pi], recvSizes[pi]);
    dvmh_log(TRACE, "Current next async tag is %d", nextAsyncTag);
    int sendCount = 0;
    int recvCount = 0;
    int selfSendIndex = -1;
    int selfRecvIndex = -1;
    bool recvProcsOrdered = true;
    for (int pi = 0; pi < sendProcCount; pi++) {
        bool itsMe = sendProcs[pi] == commRank;
        if (sendSizes[pi] > 0 && !itsMe)
            sendCount++;
        if (itsMe)
            selfSendIndex = pi;
    }
    for (int pi = 0; pi < recvProcCount; pi++) {
        bool itsMe = recvProcs[pi] == commRank;
        if (recvSizes[pi] > 0 && !itsMe)
            recvCount++;
        if (itsMe)
            selfRecvIndex = pi;
        recvProcsOrdered = recvProcsOrdered && (pi == 0 || recvProcs[pi] > recvProcs[pi - 1]);
    }
    assert((selfSendIndex >= 0) == (selfRecvIndex >= 0));
    if (selfSendIndex >= 0)
        assert(sendSizes[selfSendIndex] == recvSizes[selfRecvIndex]);
    dvmh_log(TRACE, "Really I send to %d processors and receive from %d processors (excluded myself)", sendCount, recvCount);
    std::vector<MPI_Request> reqs;
    reqs.reserve(sendCount);
    for (int pi = 0; pi < sendProcCount; pi++) {
        int p = sendProcs[pi];
        if (sendSizes[pi] > 0 && p != commRank) {
            UDvmType rest = sendSizes[pi];
            int tag = nextAsyncTag;
            while (rest > 0) {
                int curSize;
                if (rest > INT_MAX)
                    curSize = INT_MAX;
                else
                    curSize = rest;
                MPI_Request req;
                checkInternalMPI(MPI_Isend(sendBuffers[pi] + (sendSizes[pi] - rest), curSize, MPI_BYTE, p, tag, comm, &req));
                dvmh_log(TRACE, "Sent to %d processor %d bytes", p, curSize);
                reqs.push_back(req);
                rest -= curSize;
            }
        }
    }
    UDvmType bytesDone = 0;
    if (selfRecvIndex >= 0 && recvSizes[selfRecvIndex] > 0) {
        if (recvBuffers[selfRecvIndex] != sendBuffers[selfSendIndex])
            memcpy(recvBuffers[selfRecvIndex], sendBuffers[selfSendIndex], recvSizes[selfRecvIndex]);
        bytesDone += recvSizes[selfRecvIndex];
    }
    int receivedCount = 0;
    std::vector<std::pair<int, int> > recvIndexSearch;
    if (!recvProcsOrdered) {
        recvIndexSearch.reserve(recvProcCount);
        for (int pi = 0; pi < recvProcCount; pi++)
            recvIndexSearch.push_back(std::make_pair(recvProcs[pi], pi));
        std::sort(recvIndexSearch.begin(), recvIndexSearch.end());
    }
    while (receivedCount < recvCount) {
        int tag = nextAsyncTag;
        MPI_Status st;
        checkInternalMPI(MPI_Probe(MPI_ANY_SOURCE, tag, comm, &st));
        int p = st.MPI_SOURCE;
        int pi;
        if (recvProcsOrdered) {
            pi = exactIndex(recvProcs, recvProcCount, p);
        } else {
            int idx = upperIndex(&recvIndexSearch[0], recvProcCount, std::make_pair(p, -1));
            assert(idx >= 0 && idx < recvProcCount);
            assert(recvIndexSearch[idx].first == p);
            pi = recvIndexSearch[idx].second;
        }
        assert(pi >= 0 && pi < recvProcCount);
        assert(recvSizes[pi] > 0);
        UDvmType rest = recvSizes[pi];
        while (rest > 0) {
            int curSize;
            if (rest > INT_MAX)
                curSize = INT_MAX;
            else
                curSize = rest;
            checkInternalMPI(MPI_Recv(recvBuffers[pi] + (recvSizes[pi] - rest), curSize, MPI_BYTE, p, tag, comm, &st));
            int receivedSize = 0;
            checkInternalMPI(MPI_Get_count(&st, MPI_BYTE, &receivedSize));
            dvmh_log(TRACE, "Received from %d processor %d bytes", p, receivedSize);
            assert(receivedSize == curSize);
            rest -= curSize;
        }
        receivedCount++;
        bytesDone += recvSizes[pi];
    }
    checkInternalMPI(MPI_Waitall(reqs.size(), &reqs[0], MPI_STATUSES_IGNORE));
    advanceNextAsyncTag();
    return bytesDone;
}

void DvmhCommunicator::barrier() const {
    resetNextAsyncTag();
    checkInternalMPI(MPI_Barrier(comm));
}

static void allReduceMPI(void *addr, MPI_Datatype dt, MPI_Op op, MPI_Comm comm, UDvmType count = 1) {
    UDvmType rest = count;
    MPI_Aint lb, extent;
    MPI_Type_get_extent(dt, &lb, &extent);
    while (rest > 0) {
        int curCount;
        if (rest > INT_MAX)
            curCount = INT_MAX;
        else
            curCount = rest;
        checkInternalMPI(MPI_Allreduce(MPI_IN_PLACE, (char *)addr + (count - rest) * extent, curCount, dt, op, comm));
        rest -= curCount;
    }
}

static MPI_Op dvmRedFuncToMPI(int redFunc, bool *pSuccess = 0) {
    if (pSuccess)
        *pSuccess = true;
    switch (redFunc) {
        case rf_SUM: return MPI_SUM;
        case rf_PROD: return MPI_PROD;
        case rf_MAX: return MPI_MAX;
        case rf_MIN: return MPI_MIN;
        case rf_AND: return MPI_BAND;
        case rf_OR: return MPI_BOR;
        case rf_XOR: return MPI_BXOR;
        case rf_NE: return MPI_BXOR;
        case rf_MAXLOC: return MPI_MAXLOC;
        case rf_MINLOC: return MPI_MINLOC;
        default: checkInternal(pSuccess);
    }
    *pSuccess = false;
    return MPI_SUM;
}

static MPI_Datatype dvmTypeToMPI(DvmhData::DataType dt, bool *pSuccess) {
    if (pSuccess)
        *pSuccess = true;
    switch (dt) {
        case DvmhData::dtChar: return MPI_CHAR;
        case DvmhData::dtUChar: return MPI_UNSIGNED_CHAR;
        case DvmhData::dtShort: return MPI_SHORT;
        case DvmhData::dtUShort: return MPI_UNSIGNED_SHORT;
        case DvmhData::dtInt: return MPI_INT;
        case DvmhData::dtUInt: return MPI_UNSIGNED;
        case DvmhData::dtLong: return MPI_LONG;
        case DvmhData::dtULong: return MPI_UNSIGNED_LONG;
        case DvmhData::dtLongLong: return MPI_LONG_LONG;
        case DvmhData::dtULongLong: return MPI_UNSIGNED_LONG_LONG;
        case DvmhData::dtFloat: return MPI_FLOAT;
        case DvmhData::dtDouble: return MPI_DOUBLE;
        case DvmhData::dtFloatComplex: return MPI_C_FLOAT_COMPLEX;
        case DvmhData::dtDoubleComplex: return MPI_C_DOUBLE_COMPLEX;
        case DvmhData::dtLogical: return MPI_INT;
        default: checkInternal(pSuccess);
    }
    *pSuccess = false;
    return MPI_BYTE;
}

template <>
void DvmhCommunicator::allreduce(bool &var, int redFunc) const {
    resetNextAsyncTag();
    unsigned char tmp = var ? 1 : 0;
    allReduceMPI(&tmp, MPI_UNSIGNED_CHAR, dvmRedFuncToMPI(redFunc), comm);
    var = tmp ? true : false;
}

template <>
void DvmhCommunicator::allreduce(unsigned char &var, int redFunc) const {
    resetNextAsyncTag();
    allReduceMPI(&var, MPI_UNSIGNED_CHAR, dvmRedFuncToMPI(redFunc), comm);
}

template <>
void DvmhCommunicator::allreduce(int &var, int redFunc) const {
    resetNextAsyncTag();
    allReduceMPI(&var, MPI_INT, dvmRedFuncToMPI(redFunc), comm);
}

template <>
void DvmhCommunicator::allreduce(UDvmType &var, int redFunc) const {
    resetNextAsyncTag();
    allReduceMPI(&var, (sizeof(UDvmType) == sizeof(long) ? MPI_UNSIGNED_LONG : MPI_UNSIGNED_LONG_LONG), dvmRedFuncToMPI(redFunc), comm);
}

template <>
void DvmhCommunicator::allreduce(std::pair<int, int> &var, int redFunc) const {
    resetNextAsyncTag();
    allReduceMPI(&var, MPI_2INT, dvmRedFuncToMPI(redFunc), comm);
}

template <>
void DvmhCommunicator::allreduce(long long &var, int redFunc) const {
    resetNextAsyncTag();
    allReduceMPI(&var, MPI_LONG_LONG, dvmRedFuncToMPI(redFunc), comm);
}

template <>
void DvmhCommunicator::allreduce(UDvmType arr[], int redFunc, UDvmType count) const {
    resetNextAsyncTag();
    allReduceMPI(arr, (sizeof(UDvmType) == sizeof(long) ? MPI_UNSIGNED_LONG : MPI_UNSIGNED_LONG_LONG), dvmRedFuncToMPI(redFunc), comm, count);
}

static THREAD_LOCAL const DvmhReduction *currentRed = 0;

static void dvmhReductionMPIOp(char *in, char *inout, int *len, MPI_Datatype *datatype) {
    UDvmType pitch = currentRed->elemCount * (currentRed->elemSize + currentRed->locSize);
    UDvmType locOffset = currentRed->elemCount * currentRed->elemSize;
    for (int j = 0; j < *len; j++) {
        currentRed->performOperation(inout, inout + locOffset, in, in + locOffset);
        in += pitch;
        inout += pitch;
    }
}

void DvmhCommunicator::allreduce(DvmhReduction *red) const {
    resetNextAsyncTag();
    MPI_Datatype dt;
    MPI_Op op;
    // Try to use a shortcut if reduction is 'simple'
    if (!red->isLoc()) {
        bool funcOk;
        op = dvmRedFuncToMPI(red->redFunc, &funcOk);
        if (funcOk) {
            bool typeOk;
            dt = dvmTypeToMPI(red->arrayElementType, &typeOk);
            if (typeOk) {
                allReduceMPI(red->arrayAddr, dt, op, comm, red->elemCount);
                return;
            }
        }
    }
    // Common case
    MPI_Op_create((MPI_User_function *)dvmhReductionMPIOp, 1, &op);
    MPI_Type_contiguous(red->elemCount * (red->elemSize + red->locSize), MPI_BYTE, &dt);
    MPI_Type_commit(&dt);
    currentRed = red;
    char *addr = new char[red->elemCount * (red->elemSize + red->locSize)];
    memcpy(addr, red->arrayAddr, red->elemCount * red->elemSize);
    if (red->isLoc())
        memcpy(addr + red->elemCount * red->elemSize, red->locAddr, red->elemCount * red->locSize);
    allReduceMPI(addr, dt, op, comm);
    memcpy(red->arrayAddr, addr, red->elemCount * red->elemSize);
    if (red->isLoc())
        memcpy(red->locAddr, addr + red->elemCount * red->elemSize, red->elemCount * red->locSize);
    delete[] addr;
    MPI_Type_free(&dt);
    MPI_Op_free(&op);
    currentRed = 0;
}

void DvmhCommunicator::initDefault() {
    rank = 0;
    axes = new MPSAxis[rank];
    comm = MPI_COMM_SELF;
    ownComm = false;
    commSize = 1;
    inverseOrder = false;
    commRank = 0;
    resetNextAsyncTag();
}

void DvmhCommunicator::clear() {
    for (int i = 0; i < rank; i++)
        axes[i].freeAxisComm();
    delete[] axes;
    if (ownComm)
        checkInternalMPI(MPI_Comm_free(&comm));
    rank = -1;
    axes = 0;
    comm = MPI_COMM_NULL;
    ownComm = false;
    commSize = 0;
    inverseOrder = false;
    commRank = -1;
    resetNextAsyncTag();
}

void DvmhCommunicator::resetNextAsyncTag() const {
    nextAsyncTag = 1;
}

void DvmhCommunicator::advanceNextAsyncTag() const {
    if (nextAsyncTag < INT_MAX)
        nextAsyncTag++;
    else
        barrier();
}

DvmhCommunicator *DvmhCommunicator::selfComm = new DvmhCommunicator();

// MultiprocessorSystem

MultiprocessorSystem::MultiprocessorSystem(MPI_Comm aComm, int aRank, const int sizes[], bool fortranOrder, int IOProc):
            DvmhCommunicator(aComm, aRank, sizes, fortranOrder) {
    parentMPS = 0;
    parentToOur = 0;
    ourToParent = 0;
    procWeights = 0;
    ioProc = IOProc;
    assert(ioProc >= 0 && ioProc < commSize);
}

MultiprocessorSystem::MultiprocessorSystem() {
    parentMPS = 0;
    parentToOur = 0;
    ourToParent = 0;
    procWeights = 0;
    ioProc = 0;
}

int MultiprocessorSystem::getChildCommRank(const MultiprocessorSystem *child, int aCommRank) const {
    if (child == this) {
        return aCommRank;
    } else {
        checkInternal(child->parentMPS);
        int parentCommRank = getChildCommRank(child->parentMPS, aCommRank);
        return (parentCommRank >= 0 ? child->parentToOur[parentCommRank] : -1);
    }
}

int MultiprocessorSystem::getParentCommRank(const MultiprocessorSystem *parent, int aCommRank) const {
    if (parent == this) {
        return aCommRank;
    } else {
        checkInternal(parentMPS);
        return parentMPS->getParentCommRank(parent, ourToParent[aCommRank]);
    }
}

int MultiprocessorSystem::getOtherCommRank(const MultiprocessorSystem *other, int aCommRank) const {
    if (other->isSubsystemOf(this))
        return getChildCommRank(other, aCommRank);
    else if (this->isSubsystemOf(other))
        return getParentCommRank(other, aCommRank);
    else
        return -2;
}

void MultiprocessorSystem::setWeights(const double wgts[], int len) {
    delete[] procWeights;
    procWeights = new double[commSize];
    if (len <= 0)
        len = commSize;
    if (len >= commSize) {
        safeMemcpy(procWeights, wgts, commSize);
    } else {
        for (int i = 0; i < commSize; i++)
            procWeights[i] = wgts[i % len];
    }
    double sum = 0;
    for (int i = 0; i < commSize; i++)
        sum += procWeights[i];
    checkInternal(sum > 0);
    double coef = commSize / sum;
    for (int i = 0; i < commSize; i++)
        procWeights[i] *= coef;
    for (int ax = 0; ax < rank; ax++) {
        axes[ax].freeCoordWeights();
        int pc = axes[ax].procCount;
        int step = axes[ax].commStep;
        if (pc > 1) {
            double *coordWeights = new double[pc];
            for (int i = 0; i < pc; i++)
                coordWeights[i] = 0;
            for (int i = 0; i < commSize; i++)
                coordWeights[i / step % pc] += procWeights[i];
            bool weighted = false;
            double etalon = coordWeights[0];
            double sum = etalon;
            for (int i = 1; i < pc; i++) {
                double val = coordWeights[i];
                if (!((etalon < 1e-6 && val < 1e-6) || (etalon > 1e-6 && fabs(1.0 - val / etalon) < 1e-2)))
                    weighted = true;
                sum += val;
            }
            if (sum < 1e-6)
                weighted = false;
            if (weighted)
                axes[ax].setCoordWeights(coordWeights);
            else
                delete[] coordWeights;
        }
    }
}

void MultiprocessorSystem::attachChildMPS(MultiprocessorSystem *child) {
    assert(child);
    assert(children.find(child) == children.end());
    assert(child->parentMPS == 0);
    child->parentMPS = this;
    child->parentToOur = new int[commSize];
    child->parentToOur[commRank] = child->getCommRank();
    allgather(child->parentToOur);
    child->ourToParent = new int[child->commSize];
    for (int i = 0; i < commSize; i++)
        if (child->parentToOur[i] >= 0)
            child->ourToParent[child->parentToOur[i]] = i;
}

bool MultiprocessorSystem::isSubsystemOf(const MultiprocessorSystem *otherMPS) const {
    assert(otherMPS);
    const MultiprocessorSystem *curMPS = this;
    while (curMPS && curMPS != otherMPS)
        curMPS = curMPS->parentMPS;
    return (curMPS == otherMPS);
}

int MultiprocessorSystem::newFile(DvmhFile *stream) {
    assert(stream);
    int fn = -1;
    for (int i = 0; i < (int)myFiles.size(); i++)
        if (!myFiles[i]) {
            myFiles[i] = stream;
            fn = i;
            break;
        }
    if (fn < 0) {
        myFiles.push_back(stream);
        fn = myFiles.size() - 1;
    }
    return fn;
}

void MultiprocessorSystem::deleteFile(int fn) {
    checkInternal(fn >= 0 && fn < (int)myFiles.size() && myFiles[fn]);
    myFiles[fn] = 0;
    while (!myFiles.empty() && !myFiles.back())
        myFiles.pop_back();
}

static bool areSimilar(MPI_Comm comm1, MPI_Comm comm2) {
    bool res = false;
    if (comm1 == comm2)
        res = true;
    if (!res) {
        int result = MPI_UNEQUAL;
        checkInternalMPI(MPI_Comm_compare(comm1, comm2, &result));
        if (result != MPI_UNEQUAL)
            res = true;
    }
    return res;
}

bool MultiprocessorSystem::isSimilarTo(const MultiprocessorSystem *other) const {
    // TODO: If current processor does not belong to this MPS, then comm is MPI_COMM_NULL and it should be compared differently (perharps through parent)
    return this == other || areSimilar(comm, other->comm);
}

MultiprocessorSystem::~MultiprocessorSystem() {
    bool similarToParent = parentMPS ? isSimilarTo(parentMPS) : false;
    checkInternal(children.empty() || similarToParent);
    while (!children.empty()) {
        MultiprocessorSystem *child = *children.begin();
        detachChildMPS(child);
        parentMPS->attachChildMPS(child);
    }
    checkInternal(myFiles.empty());
    if (parentMPS)
        parentMPS->detachChildMPS(this);
    assert(parentToOur == 0);
    assert(ourToParent == 0);
    for (int i = 0; i < rank; i++)
        axes[i].freeCoordWeights();
    delete[] procWeights;
}

void MultiprocessorSystem::detachChildMPS(MultiprocessorSystem *child) {
    assert(children.find(child) != children.end());
    assert(child->parentMPS == this);
    child->parentMPS = 0;
    delete[] child->parentToOur;
    child->parentToOur = 0;
    delete[] child->ourToParent;
    child->ourToParent = 0;
    children.erase(child);
}

// DvmhAxisDistribRule

ReplicatedAxisDistribRule *DvmhAxisDistribRule::createReplicated(MultiprocessorSystem *mps, const Interval &spaceDim) {
    return new ReplicatedAxisDistribRule(mps, spaceDim);
}

BlockAxisDistribRule *DvmhAxisDistribRule::createBlock(MultiprocessorSystem *mps, int mpsAxis, const Interval &spaceDim) {
    return new BlockAxisDistribRule(dtBlock, mps, mpsAxis, spaceDim, 1);
}

BlockAxisDistribRule *DvmhAxisDistribRule::createWeightBlock(MultiprocessorSystem *mps, int mpsAxis, const Interval &spaceDim, DvmhData *weights) {
    return new BlockAxisDistribRule(mps, mpsAxis, spaceDim, weights);
}

BlockAxisDistribRule *DvmhAxisDistribRule::createGenBlock(MultiprocessorSystem *mps, int mpsAxis, const Interval &spaceDim, DvmhData *givenGbl) {
    return new BlockAxisDistribRule(mps, mpsAxis, givenGbl, spaceDim);
}

BlockAxisDistribRule *DvmhAxisDistribRule::createMultBlock(MultiprocessorSystem *mps, int mpsAxis, const Interval &spaceDim, UDvmType multQuant) {
    return new BlockAxisDistribRule(dtMultBlock, mps, mpsAxis, spaceDim, multQuant);
}

IndirectAxisDistribRule *DvmhAxisDistribRule::createIndirect(MultiprocessorSystem *mps, int mpsAxis, DvmhData *givenMap) {
    return new IndirectAxisDistribRule(mps, mpsAxis, givenMap);
}

IndirectAxisDistribRule *DvmhAxisDistribRule::createDerived(MultiprocessorSystem *mps, int mpsAxis, const Interval &spaceDim, DvmType derivedBuf[],
        UDvmType derivedCount) {
    return new IndirectAxisDistribRule(mps, mpsAxis, spaceDim, derivedBuf, derivedCount);
}

// DistributedAxisDistribRule

int DistributedAxisDistribRule::getProcIndex(DvmType spaceIndex) const {
    return (std::upper_bound(sumGenBlock, sumGenBlock + mps->getAxis(mpsAxis).procCount + 1, spaceIndex) - sumGenBlock) - 1;
}

// BlockAxisDistribRule

BlockAxisDistribRule::BlockAxisDistribRule(DistribType dt, MultiprocessorSystem *aMps, int aMpsAxis, const Interval &aSpaceDim, UDvmType aMultQuant):
        DistributedAxisDistribRule(dt, aMps, aMpsAxis) {
    // BLOCK and MULT_BLOCK
    assert(dt == dtBlock || dt == dtMultBlock);
    spaceDim = aSpaceDim;
    // TODO: Prohibit =0 case, when DvmhDistribSpace no longer could be constructed from s_AMView
    assert(spaceDim.size() >= 0);
    multQuant = aMultQuant;
    assert(dt == dtMultBlock || multQuant == 1);
    assert(multQuant > 0 && spaceDim.size() % multQuant == 0);
    int procCount = mps->getAxis(mpsAxis).procCount;
    sumGenBlock[0] = spaceDim[0];
    UDvmType qCount = spaceDim.size() / multQuant;
    if (!mps->getAxis(mpsAxis).isWeighted()) {
        if (qCount >= (UDvmType)procCount) {
            UDvmType minBlock = qCount / procCount;
            UDvmType elemsToAdd = qCount % procCount;
            UDvmType curRemainder = 0;
            for (int p = 0; p < procCount; p++) {
                curRemainder = (curRemainder + elemsToAdd) % procCount;
                UDvmType curBlock = multQuant * (minBlock + (curRemainder < elemsToAdd));
                sumGenBlock[p + 1] = sumGenBlock[p] + curBlock;
            }
        } else {
            int offs = 0;
            if (dvmhSettings.useGenblock)
                offs = (procCount - (int)qCount) / 2;
            for (int p = 0; p < procCount; p++) {
                UDvmType curBlock = multQuant * (p >= offs && p - offs < (int)qCount);
                sumGenBlock[p + 1] = sumGenBlock[p] + curBlock;
            }
        }
    } else {
        MPSAxis axis = mps->getAxis(mpsAxis);
        double sumCoordWeight = 0;
        UDvmType prevEnd = 0;
        for (int p = 0; p < procCount; p++) {
            sumCoordWeight += axis.getCoordWeight(p);
            UDvmType curEnd = (UDvmType)(sumCoordWeight / procCount * qCount);
            if (prevEnd > 0 && curEnd <= prevEnd)
                curEnd = prevEnd + 1;
            if (p == procCount - 1)
                curEnd = qCount;
            UDvmType curBlock = multQuant * (curEnd - prevEnd);
            sumGenBlock[p + 1] = sumGenBlock[p] + curBlock;
            prevEnd = curEnd;
        }
    }
    wgtBlockPart.push_back(1);
    haveWgtPart = spaceDim;
    finishInit();
}

BlockAxisDistribRule::BlockAxisDistribRule(MultiprocessorSystem *aMps, int aMpsAxis, const Interval &aSpaceDim, DvmhData *weights):
        DistributedAxisDistribRule(dtWgtBlock, aMps, aMpsAxis) {
    // WGT_BLOCK
    spaceDim = aSpaceDim;
    assert(spaceDim.size() > 0);
    assert(weights->getDataType() == DvmhData::dtFloat || weights->getDataType() == DvmhData::dtDouble);
    assert(weights->getRank() == 1);
    multQuant = 1;
    int procCount = mps->getAxis(mpsAxis).procCount;
    int ourProc = mps->getAxis(mpsAxis).ourProc;
    double wgtSum = 0;
    // TODO: Add support for distributed weights array. maybe
    assert(!weights->isDistributed());
    DvmType weightsLen = weights->getAxisSpace(1).size();
    for (DvmType j = 0; j < weightsLen; j++) {
        double blockWgt;
        if (weights->getDataType() == DvmhData::dtFloat)
            blockWgt = weights->getRepr(0)->getElement<float>(j);
        else
            blockWgt = weights->getRepr(0)->getElement<double>(j);
        wgtSum += blockWgt;
    }
    double wgtPerProc = wgtSum / procCount;
    double curSumWgt = 0;
    DvmType lastWgtBlockIdx = -1;
    UDvmType dimSize = spaceDim.size();
    UDvmType *resGenBlock = new UDvmType[procCount];
    int p = 0;
    resGenBlock[p] = 0;
    double sumCoordWeight = mps->getAxis(mpsAxis).getCoordWeight(p);
    for (DvmType j = 0; j < weightsLen; j++) {
        double blockWgt;
        if (weights->getDataType() == DvmhData::dtFloat)
            blockWgt = weights->getRepr(0)->getElement<float>(j);
        else
            blockWgt = weights->getRepr(0)->getElement<double>(j);
        DvmType blockStart = spaceDim[0] + j * (dimSize / weightsLen) + std::min(j, DvmType(dimSize % weightsLen));
        DvmType blockEnd = spaceDim[0] + (j + 1) * (dimSize / weightsLen) + std::min(j + 1, DvmType(dimSize % weightsLen)); //non-inclusive
        DvmType curStart = blockStart;
        DvmType curEnd = blockEnd;
        while (curEnd > curStart) {
            DvmType selectedCount;
            if (blockWgt > 0) {
                double selectedPart = std::max(0.0, std::min(1.0, (sumCoordWeight * wgtPerProc - curSumWgt) / blockWgt));
                selectedCount = std::max(DvmType(1), std::min(curEnd - curStart, DvmType((blockEnd - blockStart) * selectedPart)));
            } else
                selectedCount = curEnd - curStart;
            if (p == ourProc && j > lastWgtBlockIdx) {
                wgtBlockPart.push_back(blockWgt);
                if (wgtBlockPart.size() == 1)
                    haveWgtPart[0] = blockStart;
                haveWgtPart[1] = blockEnd - 1;
                lastWgtBlockIdx = j;
            }
            resGenBlock[p] += selectedCount;
            curSumWgt += blockWgt * double(selectedCount) / double(blockEnd - blockStart);
            curStart += selectedCount;
            if (p + 1 < procCount && curSumWgt >= sumCoordWeight * wgtPerProc) {
                p++;
                resGenBlock[p] = 0;
                sumCoordWeight += mps->getAxis(mpsAxis).getCoordWeight(p);
            }
        }
    }
    p++;
    for (; p < procCount; p++)
        resGenBlock[p] = 0;
    checkInternal(resGenBlock[ourProc] == 0 || wgtBlockPart.size() > 0);
    sumGenBlock[0] = spaceDim[0];
    for (int p = 0; p < procCount; p++)
        sumGenBlock[p + 1] = sumGenBlock[p] + resGenBlock[p];
    delete[] resGenBlock;
    finishInit();
}

BlockAxisDistribRule::BlockAxisDistribRule(MultiprocessorSystem *aMps, int aMpsAxis, DvmhData *givenGbl, const Interval &aSpaceDim):
        DistributedAxisDistribRule(dtGenBlock, aMps, aMpsAxis) {
    multQuant = 1;
    spaceDim = aSpaceDim;
    assert(spaceDim.size() > 0);
    assert(givenGbl->getDataType() == DvmhData::dtInt || givenGbl->getDataType() == DvmhData::dtLong || (givenGbl->getTypeType() == DvmhData::ttInteger &&
            givenGbl->getTypeSize() == sizeof(UDvmType)));
    int procCount = mps->getAxis(mpsAxis).procCount;
    sumGenBlock[0] = spaceDim[0];
    UDvmType dimSize = 0;
    for (int p = 0; p < procCount; p++) {
        UDvmType curBlock;
        if (givenGbl->getDataType() == DvmhData::dtInt)
            curBlock = givenGbl->getRepr(0)->getElement<int>(p);
        else if (givenGbl->getDataType() == DvmhData::dtLong)
            curBlock = givenGbl->getRepr(0)->getElement<long>(p);
        else
            curBlock = givenGbl->getRepr(0)->getElement<UDvmType>(p);
        sumGenBlock[p + 1] = sumGenBlock[p] + curBlock;
        dimSize += curBlock;
    }
    assert(dimSize == spaceDim.size());
    wgtBlockPart.push_back(1);
    haveWgtPart = spaceDim;
    finishInit();
}

int BlockAxisDistribRule::genSubparts(const double distribPoints[], Interval parts[], int count) const {
    assert(count >= 0);
    int res = 0;
    if (localElems.empty()) {
        std::fill(parts, parts + count, localElems);
    } else if (distribType == dtGenBlock) {
        int maxWgtIdx = 0;
        for (int i = 1; i < count; i++) {
            if (distribPoints[i + 1] - distribPoints[i] > distribPoints[maxWgtIdx + 1] - distribPoints[maxWgtIdx])
                maxWgtIdx = i;
        }
        for (int i = 0; i < count; i++) {
            if (i < maxWgtIdx)
                parts[i] = Interval::create(localElems[0], localElems[0] - 1);
            else if (i == maxWgtIdx)
                parts[i] = localElems;
            else
                parts[i] = Interval::create(localElems[1] + 1, localElems[1]);
        }
        res = 1;
    } else {
        // TODO: Take into account wgtBlockPart
        DvmType initIdx = localElems[0];
        DvmType endIdx = localElems[1];
        UDvmType wholeSize = localElems.size();
        DvmType prevIdx = initIdx - 1;
        for (int i = 0; i < count; i++) {
            parts[i][0] = prevIdx + 1;
            if (distribPoints[i + 1] == 1 || i == count - 1)
                parts[i][1] = endIdx;
            else
                parts[i][1] = std::max(prevIdx, std::min(endIdx + 1, initIdx + (DvmType)roundDownU((UDvmType)(distribPoints[i + 1] * wholeSize + 1e-8),
                        multQuant)) - 1);
            if (!parts[i].empty())
                res++;
            prevIdx = parts[i][1];
        }
    }
    return res;
}

void BlockAxisDistribRule::finishInit() {
    int ourProc = mps->getAxis(mpsAxis).ourProc;
    localElems = Interval::create(sumGenBlock[ourProc], sumGenBlock[ourProc + 1] - 1);
    if (distribType == dtGenBlock)
        maxSubparts = localElems.empty() ? 0 : 1;
    else
        maxSubparts = localElems.size() / multQuant;
}

// ExchangeMap

void ExchangeMap::init() {
    sendProcs = recvProcs = 0;
    sendStarts = recvStarts = 0;
    sendIndices = recvIndices = 0;
    bestOrder = 0;
}

void ExchangeMap::clear() {
    delete[] sendProcs; delete[] sendStarts; delete[] sendIndices;
    delete[] recvProcs; delete[] recvStarts; delete[] recvIndices;
    delete[] bestOrder;
    init();
}

static void fillSendRecvFlags(int procCount, int commProcCount, const int procList[], const UDvmType indexStarts[], bool flags[]) {
    if (!procList) {
        for (int i = 0; i < commProcCount; i++)
            flags[i] = indexStarts[i + 1] - indexStarts[i] > 0;
        for (int i = commProcCount; i < procCount; i++)
            flags[i] = false;
    } else {
        for (int i = 0; i < procCount; i++)
            flags[i] = false;
        for (int i = 0; i < commProcCount; i++)
            flags[procList[i]] = indexStarts[i + 1] - indexStarts[i] > 0;
    }
}

bool ExchangeMap::checkConsistency(DvmhCommunicator *axisComm) const {
    // Checks that for each send/recv operation in send/recv list there is a corresponding recv/send operation on other process
    // TODO: Add check for number of elements as well, not only the fact of communication. Unite communications (two at once).
    int ourProc = axisComm->getCommRank();
    int procCount = axisComm->getCommSize();

    bool *sendFlags = new bool[procCount];
    bool *recvFlags = new bool[procCount];
    bool *sendFlagsCheck = new bool[procCount];
    bool *recvFlagsCheck = new bool[procCount];

    fillSendRecvFlags(procCount, sendProcCount, sendProcs, sendStarts, sendFlags);
    fillSendRecvFlags(procCount, recvProcCount, recvProcs, recvStarts, recvFlags);

    for (int otherProc = 0; otherProc < procCount; otherProc++) {
        if (otherProc > ourProc) {
            axisComm->send(otherProc, sendFlags[otherProc]);
            axisComm->recv(otherProc, sendFlagsCheck[otherProc]);
            axisComm->send(otherProc, recvFlags[otherProc]);
            axisComm->recv(otherProc, recvFlagsCheck[otherProc]);
        } else if (otherProc < ourProc) {
            axisComm->recv(otherProc, recvFlagsCheck[otherProc]);
            axisComm->send(otherProc, recvFlags[otherProc]);
            axisComm->recv(otherProc, sendFlagsCheck[otherProc]);
            axisComm->send(otherProc, sendFlags[otherProc]);
        }
    }
    recvFlagsCheck[ourProc] = false;
    sendFlagsCheck[ourProc] = false;

    bool isConsistent = true;
    for (int i = 0; i < procCount; i++)
        isConsistent = isConsistent && recvFlagsCheck[i] == recvFlags[i] && sendFlagsCheck[i] == sendFlags[i];

    delete[] sendFlags;
    delete[] recvFlags;
    delete[] sendFlagsCheck;
    delete[] recvFlagsCheck;

    axisComm->allreduce(isConsistent, rf_MIN);

    return isConsistent;
}

static void fillProcPointers(int procCount, int commProcCount, const int procList[], int procPointers[], int multiplier = 1) {
    if (!procList) {
        for (int i = 0; i < commProcCount; i++)
            procPointers[i] = multiplier * (i + 1);
        for (int i = commProcCount; i < procCount; i++)
            procPointers[i] = 0;
    } else {
        for (int i = 0; i < procCount; i++)
            procPointers[i] = 0;
        for (int i = 0; i < commProcCount; i++)
            procPointers[procList[i]] = multiplier * (i + 1);
    }
}

template <typename T>
static int getNextAvailableWave(T selfMask[], const T receivedMask[], int maskArrayLength, int &maskArrayIndex) {
    T currentMask = 0;
    for (; maskArrayIndex < maskArrayLength; maskArrayIndex++) {
        currentMask = receivedMask[maskArrayIndex] & selfMask[maskArrayIndex];
        if (currentMask)
            break;
    }
    checkInternal(currentMask);
    int waveOffset = ilogN(currentMask, 1);
    selfMask[maskArrayIndex] &= ~((T)1 << waveOffset);
    return maskArrayIndex * sizeof(T) * CHAR_BIT + waveOffset;
}

void ExchangeMap::fillBestOrder(DvmhCommunicator *axisComm) {
    // If send/recv lists are not consistent (see checkConsistency), function will hang.

    int ourProc = axisComm->getCommRank();
    int procCount = axisComm->getCommSize();

    bool *sendFlags = new bool[procCount];
    bool *recvFlags = new bool[procCount];
    int *sendPointers = new int[procCount];
    int *recvPointers = new int[procCount];

    // Setting flags for sending
    fillSendRecvFlags(procCount, sendProcCount, sendProcs, sendStarts, sendFlags);
    fillSendRecvFlags(procCount, recvProcCount, recvProcs, recvStarts, recvFlags);
    fillProcPointers(procCount, sendProcCount, sendProcs, sendPointers);
    fillProcPointers(procCount, recvProcCount, recvProcs, recvPointers, -1);

    dvmh_log(DEBUG, "Started ordering...");

    int maxWaves = procCount * 2;
    int *tempBestOrder = new int[maxWaves];
    // Masks show availability. 1 in a specific position of the mask means that this process can do
    // communication on this specific "wave", and 0 means that process is already busy at that "wave".
    int maskElemBits = sizeof(unsigned long) * CHAR_BIT;
    int maskArrayLength = divUpS(maxWaves, maskElemBits);
    int maskArraySize = maskArrayLength * sizeof(unsigned long);
    unsigned long *selfAvailabilityMask = new unsigned long[maskArrayLength];
    unsigned long *receivedAvailabilityMask = new unsigned long[maskArrayLength];
    for (int i = 0; i < maxWaves; i++)
        tempBestOrder[i] = 0;
    for (int i = 0; i < maskArrayLength; i++)
        selfAvailabilityMask[i] = ~0UL;

    // step is used for setting fixed preliminary order
    int step = std::max((procCount - 1) / 2, 1);
    // finding correct step closest to procCount / 2 for efficiency on most common communication patterns
    int offs = 0;
    while (gcd(procCount, step + offs) != 1) {
        if (offs <= 0)
            offs = - offs + 1;
        else
            offs = - offs;
    }
    step = step + offs;

    int otherProc = (step - ourProc + procCount) % procCount;
    for (int i = 0; i < procCount; i++) {
        otherProc = (otherProc + step) % procCount;
        if (otherProc == ourProc)
            continue;
        if (!recvFlags[otherProc] && !sendFlags[otherProc])
            continue;
        // When 2 processes communicate, first sends it's mask to second one, which calculates "waves", when they will
        // conduct exchange in an optimized order. Then second one sends back numbers for those "waves".
        if (otherProc > ourProc) {
            int recvWaveNum[2];
            axisComm->send(otherProc, selfAvailabilityMask, maskArraySize);
            axisComm->recv(otherProc, recvWaveNum);
            // recvWaveNum[0] contains wave number, when this process should send a message to otherProc
            // recvWaveNum[1] contains wave number, when this process should receive a message from otherProc
            // -1 in any of them means that this type of message exchange is not needed
            assert(sendFlags[otherProc] == (recvWaveNum[0] >= 0));
            assert(recvFlags[otherProc] == (recvWaveNum[1] >= 0));
            if (recvWaveNum[0] >= 0) {
                int waveNum = recvWaveNum[0];
                assert(waveNum < maxWaves);
                int maskArrayIndex = waveNum / maskElemBits;
                int maskBitIndex = waveNum % maskElemBits;
                selfAvailabilityMask[maskArrayIndex] &= ~(1UL << maskBitIndex);
                tempBestOrder[waveNum] = otherProc + 1;
            }
            if (recvWaveNum[1] >= 0) {
                int waveNum = recvWaveNum[1];
                assert(waveNum < maxWaves);
                int maskArrayIndex = waveNum / maskElemBits;
                int maskBitIndex = waveNum % maskElemBits;
                selfAvailabilityMask[maskArrayIndex] &= ~(1UL << maskBitIndex);
                tempBestOrder[waveNum] = - (otherProc + 1);
            }
        } else {
            axisComm->recv(otherProc, receivedAvailabilityMask, maskArraySize);
            // second process finds 2 (or 1, if only one operation between processes is needed) earliest waves, where both
            // processes are available, and sets their exchange on those waves

            int sendWaveNum[2] = {-1, -1};
            int maskArrayIndex = 0;
            if (sendFlags[otherProc]) {
                int waveNum = getNextAvailableWave(selfAvailabilityMask, receivedAvailabilityMask, maskArrayLength, maskArrayIndex);
                checkInternal(waveNum < maxWaves);
                sendWaveNum[1] = waveNum;
                tempBestOrder[waveNum] = otherProc + 1;
            }
            if (recvFlags[otherProc]) {
                int waveNum = getNextAvailableWave(selfAvailabilityMask, receivedAvailabilityMask, maskArrayLength, maskArrayIndex);
                checkInternal(waveNum < maxWaves);
                sendWaveNum[0] = waveNum;
                tempBestOrder[waveNum] = - (otherProc + 1);
            }
            axisComm->send(otherProc, sendWaveNum);
        }
    }

    delete[] selfAvailabilityMask;
    delete[] receivedAvailabilityMask;
    delete[] sendFlags;
    delete[] recvFlags;

    // Filling the actual bestOrder with links to send/recv arrays. tempBestOrder has only ranks of processes by now.
    delete[] bestOrder;
    bestOrder = new int[sendProcCount + recvProcCount];
    int currentPosition = 0;
    for (int i = 0; i < maxWaves; i++) {
        if (tempBestOrder[i] > 0) {
            checkInternal(currentPosition < sendProcCount + recvProcCount);
            bestOrder[currentPosition] = sendPointers[tempBestOrder[i] - 1];
            currentPosition++;
        } else if (tempBestOrder[i] < 0) {
            checkInternal(currentPosition < sendProcCount + recvProcCount);
            bestOrder[currentPosition] = recvPointers[ - (tempBestOrder[i] + 1)];
            currentPosition++;
        }
    }
    for (; currentPosition < sendProcCount + recvProcCount; currentPosition++)
        bestOrder[currentPosition] = 0;

    dvmh_log(DEBUG, "Successfully filled bestOrder.");

    delete[] sendPointers;
    delete[] recvPointers;
    delete[] tempBestOrder;
}

bool ExchangeMap::checkBestOrder(DvmhCommunicator *axisComm) const {
    // This is slow and should be used only for debug.

    bool isBestOrderEmpty = false;
    if (!bestOrder)
        isBestOrderEmpty = true;

    axisComm->allreduce(isBestOrderEmpty, rf_MIN);

    if (isBestOrderEmpty)
        return true; // No bestOrder is a good bestOrder

    DvmhTimer tm(true);

    int ourProc = axisComm->getCommRank();
    int procCount = axisComm->getCommSize();

    dvmh_log(DEBUG, "ExchangeMap's bestOrder debug start");

    // The first half of debug checks. Makes sure that every operation required is present in bestOrder and only once.

    bool debugStatus = true;

    bool *sendDebug = new bool[sendProcCount];
    bool *recvDebug = new bool[recvProcCount];
    for (int i = 0; i < sendProcCount; i++)
        sendDebug[i] = false;
    for (int i = 0; i < recvProcCount; i++)
        recvDebug[i] = false;
    for (int i = 0; i < sendProcCount + recvProcCount; i++) {
        int procIdx = std::abs(bestOrder[i]) - 1;
        if (bestOrder[i] > 0) {
            assert(procIdx < sendProcCount);
            debugStatus = debugStatus && !sendDebug[procIdx];
            sendDebug[procIdx] = true;
        } else if (bestOrder[i] < 0) {
            assert(procIdx < recvProcCount);
            debugStatus = debugStatus && !recvDebug[procIdx];
            recvDebug[procIdx] = true;
        }
    }
    for (int i = 0; i < sendProcCount; i++) {
        if (!sendDebug[i])
            debugStatus = debugStatus && sendStarts[i + 1] - sendStarts[i] == 0;
    }
    for (int i = 0; i < recvProcCount; i++) {
        if (!recvDebug[i])
            debugStatus = debugStatus && recvStarts[i + 1] - recvStarts[i] == 0;
    }
    delete[] sendDebug;
    delete[] recvDebug;

    axisComm->allreduce(debugStatus, rf_MIN);

    dvmh_log(DEBUG, "Time to do the first half of debug check for bestOrder: %g", tm.lap());
    if (!debugStatus) {
        dvmh_log(DEBUG, "First half of debug failed, bestOrder was not correct.");
        return false;
    }
    dvmh_log(DEBUG, "First half of debug check succeeded.");

    // The second half of debug checks. Makes sure that there are no deadlocks.

    if (ourProc == 0) {
        int *processIndexes = new int[procCount];
        int myPosition = 0;

        processIndexes[0] = getNextPartnerAndDirection(myPosition);
        for (int i = 1; i < procCount; i++)
            axisComm->recv(i, processIndexes[i]);

        bool zeroFlag = false;
        while (debugStatus && !zeroFlag) {
            bool activeFlag = false;
            zeroFlag = true;
            for (int i = 0; i < procCount; i++) {
                if (processIndexes[i] == 0)
                    continue;
                zeroFlag = false;
                int partnerProc = std::abs(processIndexes[i]) - 1;
                if (std::abs(processIndexes[partnerProc]) - 1 == i && (sign(processIndexes[i]) * sign(processIndexes[partnerProc]) < 0)) {
                    if (i != 0)
                        axisComm->recv(i, processIndexes[i]);
                    if (partnerProc != 0)
                        axisComm->recv(partnerProc, processIndexes[partnerProc]);
                    if (i == 0 || partnerProc == 0)
                        processIndexes[0] = getNextPartnerAndDirection(myPosition);
                    activeFlag = true;
                }
            }
            debugStatus = debugStatus && (activeFlag || zeroFlag);
        }
        for (int i = 1; i < procCount; i++) {
            while (processIndexes[i] != 0)
                axisComm->recv(i, processIndexes[i]);
        }
        delete[] processIndexes;
    } else {
        int position = 0;
        while (int partnerAndDirection = getNextPartnerAndDirection(position))
            axisComm->send(0, partnerAndDirection);
        int endFlag = 0;
        axisComm->send(0, endFlag);
    }

    axisComm->bcast(0, debugStatus);

    dvmh_log(DEBUG, "Time to do the second half of debug check for bestOrder: %g", tm.lap());
    if (!debugStatus) {
        dvmh_log(DEBUG, "Second half of debug check failed. Deadlock was found.");
        return false;
    }
    dvmh_log(DEBUG, "Second half of debug check succeeded.");

    return true;
}

int ExchangeMap::getNextPartnerAndDirection(int &position) const {
    int res = 0;
    while (position < sendProcCount + recvProcCount && bestOrder[position] == 0)
        position++;
    if (position < sendProcCount + recvProcCount) {
        assert(bestOrder[position] != 0);
        int procIdx = std::abs(bestOrder[position]) - 1;
        if (bestOrder[position] > 0)
            res = (sendProcs ? sendProcs[procIdx] : procIdx) + 1;
        else
            res = - (recvProcs ? recvProcs[procIdx] : procIdx) - 1;
        position++;
    }
    return res;
}

// IndirectAxisDistribRule

IndirectAxisDistribRule::IndirectAxisDistribRule(MultiprocessorSystem *aMps, int aMpsAxis, DvmhData *givenMap):
        DistributedAxisDistribRule(dtIndirect, aMps, aMpsAxis) {
    shdWidth = ShdWidth::createEmpty();
    DvmhTimer tm(true);
    assert(givenMap->getDataType() == DvmhData::dtInt || givenMap->getDataType() == DvmhData::dtLong);
    assert(givenMap->getRank() == 1);
    assert(givenMap->isAligned());
    spaceDim = givenMap->getAxisSpace(1);
    assert(spaceDim.size() > 0);
    int procCount = mps->getAxis(mpsAxis).procCount;
    int ourProc = mps->getAxis(mpsAxis).ourProc;
    DvmhCommunicator *axisComm = mps->getAxis(mpsAxis).axisComm;
    createGlobalToLocal();
    // TODO: Do not demand it, allow any (block?) distribution
    checkError2(!givenMap->isDistributed() ||
            (!givenMap->getAlignRule()->getDspace()->getDistribRule()->hasIndirect() && givenMap->getAxisLocalPart(1) == globalToLocal->getAxisLocalPart(1)),
            "Map array for the indirect distribution can only be a regular array, a fully replicated array or a BLOCK-distributed array");
    givenMap->syncWriteAccesses();
    givenMap->getActual(givenMap->getLocalPlusShadow(), true);
    // XXX: It is not correct in some cases
    bool givenRepl = givenMap->getAxisSpace(1) == givenMap->getAxisLocalPart(1);
    UDvmType *fakeGenBlock = new UDvmType[procCount];
    memset(fakeGenBlock, 0, sizeof(UDvmType) * procCount);
    dvmh_log(DEBUG, "Time to prepare: %g", tm.lap());
    DvmhBuffer *mapArr = (givenMap->hasLocal() ? givenMap->getRepr(0) : 0);
    DvmhBuffer *myGlobalToLocal = (globalToLocal->hasLocal() ? globalToLocal->getRepr(0) : 0);
    Interval part;
    UDvmType *gblPart = 0;
    part = givenMap->getAxisLocalPart(1);
    for (DvmType gi = part[0]; gi <= part[1]; gi++) {
        int curProc = (givenMap->getDataType() == DvmhData::dtInt ? mapArr->getElement<int>(gi) : (int)(mapArr->getElement<long>(gi)));
        checkError3(curProc >= 0, "Invalid value encountered in mapping array at index " DTFMT, gi);
        curProc = curProc % procCount;
        fakeGenBlock[curProc]++;
    }
    if (!givenRepl) {
        gblPart = new UDvmType[procCount];
        safeMemcpy(gblPart, fakeGenBlock, procCount);
        axisComm->allreduce(fakeGenBlock, rf_SUM, procCount);
    }
    // fakeGenBlock is done
    dvmh_log(DEBUG, "Time to fill fakeGenBlock: %g", tm.lap());
    fillGenblock(fakeGenBlock);
    delete[] fakeGenBlock;
    createLocal2ToGlobal();
    dvmh_log(DEBUG, "Time to fill gbl and create ltg: %g", tm.lap());
    tm.push();
    if (givenRepl) {
        UDvmType filledCount = 0;
        UDvmType *filledByProc = new UDvmType[procCount];
        memset(filledByProc, 0, sizeof(UDvmType) * procCount);
        part = givenMap->getAxisLocalPart(1);
        Interval myPart = globalToLocal->getAxisLocalPart(1);
        for (DvmType gi = part[0]; gi <= part[1]; gi++) {
            int curProc = (givenMap->getDataType() == DvmhData::dtInt ? mapArr->getElement<int>(gi) : (int)(mapArr->getElement<long>(gi)));
            curProc = curProc % procCount;
            if (curProc == ourProc)
                local2ToGlobal[filledCount++] = gi;
            if (myPart.contains(gi))
                myGlobalToLocal->getElement<DvmType>(gi) = sumGenBlock[curProc] + (DvmType)filledByProc[curProc];
            filledByProc[curProc]++;
        }
        assert(filledCount == localElems.size());
        delete[] filledByProc;
        // local2ToGlobal and globalToLocal are done
    } else {
        assert(gblPart);
        UDvmType *sendSizes = new UDvmType[procCount];
        DvmType **sendBuffers = new DvmType *[procCount];
        UDvmType *sendFilled = new UDvmType[procCount];
        for (int p = 0; p < procCount; p++) {
            sendSizes[p] = gblPart[p] * sizeof(DvmType);
            sendBuffers[p] = gblPart[p] > 0 ? new DvmType[gblPart[p]] : 0;
            sendFilled[p] = 0;
        }
        dvmh_log(DEBUG, "Time to prepare send buffers: %g", tm.lap());
        part = givenMap->getAxisLocalPart(1);
        for (DvmType gi = part[0]; gi <= part[1]; gi++) {
            int curProc = (givenMap->getDataType() == DvmhData::dtInt ? mapArr->getElement<int>(gi) : (int)(mapArr->getElement<long>(gi)));
            curProc = curProc % procCount;
            sendBuffers[curProc][sendFilled[curProc]++] = gi;
        }
        delete[] sendFilled;
        dvmh_log(DEBUG, "Time to fill send buffers: %g", tm.lap());
        UDvmType *recvSizes = new UDvmType[procCount];
        char **recvBuffers = new char *[procCount];
        axisComm->alltoallv1(sendSizes, (char **)sendBuffers, recvSizes, recvBuffers);
        for (int p = 0; p < procCount; p++)
            delete[] sendBuffers[p];
        delete[] sendSizes;
        delete[] sendBuffers;
        dvmh_log(DEBUG, "Time to alltoallv and delete[]: %g", tm.lap());
        UDvmType filledCount = 0;
        for (int p = 0; p < procCount; p++) {
            assert(recvSizes[p] % sizeof(DvmType) == 0);
            memcpy(&local2ToGlobal[filledCount], recvBuffers[p], recvSizes[p]);
            filledCount += recvSizes[p] / sizeof(DvmType);
            delete[] recvBuffers[p];
        }
        assert(filledCount == localElems.size());
        delete[] recvSizes;
        delete[] recvBuffers;
        dvmh_log(DEBUG, "Time to copy received buffers: %g", tm.lap());
        std::sort(local2ToGlobal, local2ToGlobal + localElems.size());
        dvmh_log(DEBUG, "Time to sort local2ToGlobal: %g", tm.lap());
        delete[] gblPart;
        // local2ToGlobal is done
        // TODO: Work also with other-type distributed mapping array, maybe
        part = globalToLocal->getAxisLocalPart(1);
        for (DvmType gi = part[0]; gi <= part[1]; gi++) {
            int curProc = (givenMap->getDataType() == DvmhData::dtInt ? mapArr->getElement<int>(gi) : (int)(mapArr->getElement<long>(gi)));
            curProc = curProc % procCount;
            myGlobalToLocal->getElement<DvmType>(gi) = curProc;
        }
        // globalToLocal is filled with processor indexes
        dvmh_log(DEBUG, "Time to prefill globalToLocal: %g", tm.lap());
        finishGlobalToLocal();
        dvmh_log(DEBUG, "Time to finish globalToLocal: %g", tm.lap());
        // globalToLocal is done
    }
    tm.pop();
    dvmh_log(DEBUG, "Time to fill local2ToGlobal and globalToLocal: %g", tm.lap());
    dvmh_log(DEBUG, "Indirect localElems=" DTFMT ".." DTFMT, localElems[0], localElems[1]);
    if (0) {
        for (int i = 0; i < (int)localElems.size(); i++)
            dvmh_log(TRACE, "local2ToGlobal[" DTFMT "]=" DTFMT, localElems[0] + i, local2ToGlobal[i]);
        for (DvmType gi = globalToLocal->getAxisLocalPart(1)[0]; gi <= globalToLocal->getAxisLocalPart(1)[1]; gi++)
            dvmh_log(TRACE, "globalToLocal[" DTFMT "]=" DTFMT, gi, globalToLocal->getRepr(0)->getElement<DvmType>(gi));
    }
    shdElements = 0;
    dvmh_log(DEBUG, "Total time for IndirectAxis creation: %g", tm.total());
}

static void sortAndTrim(DvmType buf[], UDvmType bufSize, Interval space, UDvmType *pToSkip, UDvmType *pToTruncate) {
    DvmhTimer tm(true);
    std::sort(buf, buf + bufSize);
    dvmh_log(DEBUG, "Time to sort: %g", tm.lap());
    UDvmType toSkip = 0;
    while (toSkip < bufSize && buf[toSkip] < space.begin())
        toSkip++;
    UDvmType toTruncate = 0;
    while (toSkip + toTruncate < bufSize && buf[bufSize - 1 - toTruncate] > space.end())
        toTruncate++;
    dvmh_log(DEBUG, "Time to trim: %g", tm.lap());
    *pToSkip = toSkip;
    *pToTruncate = toTruncate;
}

static void sortTrimAndFillUnique(DvmType buf[], UDvmType bufSize, Interval space, UDvmType *pUniqueCount, UDvmType **pUniqueStart) {
    UDvmType toSkip, toTruncate;
    sortAndTrim(buf, bufSize, space, &toSkip, &toTruncate);
    DvmhTimer tm(true);
    UDvmType &uniqueCount = *pUniqueCount;
    uniqueCount = bufSize > toSkip + toTruncate ? 1 : 0;
    for (UDvmType i = toSkip + 1; i < bufSize - toTruncate; i++) {
        if (buf[i - 1] != buf[i])
            uniqueCount++;
    }
    UDvmType *&uniqueStart = *pUniqueStart;
    uniqueStart = new UDvmType[uniqueCount + 1];
    uniqueStart[0] = toSkip;
    UDvmType uniqueIndex = 1;
    for (UDvmType i = toSkip + 1; i < bufSize - toTruncate; i++) {
        if (buf[i - 1] != buf[i])
            uniqueStart[uniqueIndex++] = i;
    }
    uniqueStart[uniqueCount] = bufSize - toTruncate;
    dvmh_log(DEBUG, "Time to fill unique: %g", tm.lap());
}

static UDvmType sortTrimAndMakeUnique(DvmType buf[], UDvmType bufSize, Interval space) {
    UDvmType toSkip, toTruncate;
    sortAndTrim(buf, bufSize, space, &toSkip, &toTruncate);
    DvmhTimer tm(true);
    UDvmType uniqueCount = 0;
    if (bufSize > toSkip + toTruncate) {
        if (uniqueCount != toSkip)
            buf[uniqueCount] = buf[toSkip];
        uniqueCount++;
    }
    for (UDvmType i = toSkip + 1; i < bufSize - toTruncate; i++) {
        if (buf[i - 1] != buf[i]) {
            if (uniqueCount != i)
                buf[uniqueCount] = buf[i];
            uniqueCount++;
        }
    }
    dvmh_log(DEBUG, "Time to make unique: %g", tm.lap());
    return uniqueCount;
}

IndirectAxisDistribRule::IndirectAxisDistribRule(MultiprocessorSystem *aMps, int aMpsAxis, const Interval &aSpaceDim, DvmType derivedBuf[],
        UDvmType derivedCount):
        DistributedAxisDistribRule(dtDerived, aMps, aMpsAxis) {
    DvmhTimer tm(true);
    shdWidth = ShdWidth::createEmpty();
    shdElements = 0;
    spaceDim = aSpaceDim;
    assert(spaceDim.size() > 0);
    createGlobalToLocal();
    int procCount = mps->getAxis(mpsAxis).procCount;
    int ourProc = mps->getAxis(mpsAxis).ourProc;
    DvmhCommunicator *axisComm = mps->getAxis(mpsAxis).axisComm;
    UDvmType uniqueCount = 0;
    UDvmType *uniqueStart = 0;
    sortTrimAndFillUnique(derivedBuf, derivedCount, spaceDim, &uniqueCount, &uniqueStart);
    assert(uniqueStart);
    dvmh_log(DEBUG, "Time to sort,trim,fill unique: %g", tm.lap());
    UDvmType *sendSizes = new UDvmType[procCount];
    char **sendBuffers = new char *[procCount];
    const BlockAxisDistribRule *axRule = globalToLocal->getAlignRule()->getDspace()->getAxisDistribRule(1)->asBlockDistributed();
    UDvmType nextConsider = 0;
    DvmType nextEl = derivedBuf[uniqueStart[nextConsider]];
    for (int p = 0; p < procCount; p++) {
        Interval part = axRule->getLocalElems(p);
        UDvmType partStart = uniqueCount;
        UDvmType elemsToSend = 0;
        while (nextConsider < uniqueCount && nextEl <= part.end()) {
            elemsToSend++;
            if (elemsToSend == 1)
                partStart = nextConsider;
            nextConsider++;
            nextEl = derivedBuf[uniqueStart[nextConsider]];
        }
        sendSizes[p] = (sizeof(DvmType) + 1) * elemsToSend;
        char *buf = elemsToSend > 0 ? new char[(sizeof(DvmType) + 1) * elemsToSend] : 0;
        DvmType *elemBuf = (DvmType *)buf;
        unsigned char *priorityBuf = (unsigned char *)(buf + sizeof(DvmType) * elemsToSend);
        for (UDvmType eli = 0; eli < elemsToSend; eli++) {
            UDvmType idx = uniqueStart[partStart + eli];
            elemBuf[eli] = derivedBuf[idx];
            priorityBuf[eli] = std::min((UDvmType)255, uniqueStart[partStart + eli + 1] - idx);
        }
        sendBuffers[p] = buf;
    }
    dvmh_log(DEBUG, "Time to fill send buffers: %g", tm.lap());
    UDvmType *recvSizes = new UDvmType[procCount];
    char **recvBuffers = new char *[procCount];
    axisComm->alltoallv1(sendSizes, sendBuffers, recvSizes, recvBuffers);
    for (int p = 0; p < procCount; p++)
        delete[] sendBuffers[p];
    dvmh_log(DEBUG, "Time to alltoallv and delete[]: %g", tm.lap());
    unsigned char *myPriorities = new unsigned char[globalToLocal->getLocalElemCount()];
    memset(myPriorities, 0, globalToLocal->getLocalElemCount());
    std::vector<DvmType> *rejectedElems = new std::vector<DvmType>[procCount];
    for (int p = 0; p < procCount; p++) {
        assert(recvSizes[p] % (sizeof(DvmType) + 1) == 0);
        UDvmType count = recvSizes[p] / (sizeof(DvmType) + 1);
        DvmType *elemBuf = (DvmType *)recvBuffers[p];
        unsigned char *priorityBuf = (unsigned char *)(recvBuffers[p] + sizeof(DvmType) * count);
        for (UDvmType eli = 0; eli < count; eli++) {
            DvmType curEl = elemBuf[eli];
            unsigned char curPriority = priorityBuf[eli];
            DvmType localEli = curEl - globalToLocal->getAxisLocalPart(1).begin();
            dvmh_log(DONT_LOG, "Received element " DTFMT " with priority %d from %d", curEl, (int)curPriority, p);
            int curProc = p;
            if (curPriority > myPriorities[localEli]) {
                DvmType &resProc = globalToLocal->getRepr(0)->getElement<DvmType>(curEl);
                if (myPriorities[localEli] > 0)
                    rejectedElems[resProc].push_back(curEl);
                resProc = curProc;
                myPriorities[localEli] = curPriority;
            } else {
                rejectedElems[curProc].push_back(curEl);
            }
        }
        delete[] recvBuffers[p];
    }
    for (UDvmType i = 0; i < globalToLocal->getLocalElemCount(); i++) {
        if (myPriorities[i] == 0)
            checkError3(0, "No processor has claimed an ownership of the element at index " DTFMT, globalToLocal->getAxisLocalPart(1).begin() + (DvmType)i);
    }
    delete[] myPriorities;
    dvmh_log(DEBUG, "Time to fill my globalToLocal part and rejectedElems: %g", tm.lap());
    for (int p = 0; p < procCount; p++) {
        sendSizes[p] = sizeof(DvmType) * rejectedElems[p].size();
        sendBuffers[p] = (char *)(!rejectedElems[p].empty() ? &rejectedElems[p][0] : 0);
    }
    UDvmType overlaySize = axisComm->alltoallv1(sendSizes, sendBuffers, recvSizes, recvBuffers) / sizeof(DvmType);
    delete[] rejectedElems;
    dvmh_log(DEBUG, "Time to alltoallv overlay: %g", tm.lap());
    DvmType *overlayElems = new DvmType[overlaySize];
    UDvmType filledCount = 0;
    for (int p = 0; p < procCount; p++) {
        assert(recvSizes[p] % sizeof(DvmType) == 0);
        UDvmType curCount = recvSizes[p] / sizeof(DvmType);
        for (UDvmType eli = 0; eli < curCount; eli++) {
            overlayElems[filledCount + eli] = ((DvmType *)recvBuffers[p])[eli];
            dvmh_log(TRACE, "Adding to overlay element with global index " DTFMT, overlayElems[filledCount + eli]);
        }
        filledCount += curCount;
        delete[] recvBuffers[p];
    }
    assert(filledCount == overlaySize);
    dvmh_log(DEBUG, "Time to fill overlayElems: %g", tm.lap());
    std::sort(overlayElems, overlayElems + overlaySize);
    dvmh_log(DEBUG, "Time to sort overlayElems: %g", tm.lap());
    for (UDvmType i = 1; i < overlaySize; i++)
        assert(overlayElems[i] > overlayElems[i - 1]);
    delete[] sendSizes;
    delete[] sendBuffers;
    delete[] recvSizes;
    delete[] recvBuffers;
    assert(overlaySize <= uniqueCount);
    UDvmType myLocalCount = uniqueCount - overlaySize;
    UDvmType *fakeGenBlock = new UDvmType[procCount];
    fakeGenBlock[ourProc] = myLocalCount;
    axisComm->allgather(fakeGenBlock);
    dvmh_log(DEBUG, "Time to allgather fakeGenBlock: %g", tm.lap());
    fillGenblock(fakeGenBlock);
    delete[] fakeGenBlock;
    createLocal2ToGlobal();
    UDvmType li = 0;
    UDvmType si = 0;
    for (UDvmType ui = 0; ui < uniqueCount; ui++) {
        DvmType curEl = derivedBuf[uniqueStart[ui]];
        while (si < overlaySize && overlayElems[si] < curEl)
            si++;
        if (si >= overlaySize || overlayElems[si] != curEl)
            local2ToGlobal[li++] = curEl;
    }
    assert(li == myLocalCount);
    delete[] uniqueStart;
    dvmh_log(DEBUG, "Time to fill local part of local2ToGlobal: %g", tm.lap());
    addShadow(overlayElems, overlaySize, "overlay");
    delete[] overlayElems;
    dvmh_log(DEBUG, "Time to add 'overlay' shadow edge: %g", tm.lap());
    dvmh_log(DEBUG, "Total time for IndirectAxis creation: %g", tm.total());
}

bool IndirectAxisDistribRule::fillPart(Intervals &res, const Interval &globalSpacePart, DvmType regInterval) const {
    res.clear();
    if (localElems.empty())
        return false;
    UDvmType myLocalSize = localElems.size();
    DvmType *myLocalToGlobal = local2ToGlobal + shdWidth[0];
    if (regInterval == 1 && globalSpacePart[0] <= myLocalToGlobal[0] && globalSpacePart[1] >= myLocalToGlobal[myLocalSize - 1]) {
        res.append(localElems);
    } else if (regInterval == 1) {
        res.append(Interval::create(upperIndex(myLocalToGlobal, myLocalSize, globalSpacePart[0]), lowerIndex(myLocalToGlobal, myLocalSize, globalSpacePart[1]))
                + localElems[0]);
    } else {
        dvmh_log(TRACE, "Bad case for IndirectAxisDistribRule::fillPart encountered");
        Interval enclosingInter = Interval::create(upperIndex(myLocalToGlobal, myLocalSize, globalSpacePart[0]), lowerIndex(myLocalToGlobal, myLocalSize,
                globalSpacePart[1]));
        Interval accumInter = Interval::createEmpty();
        for (DvmType li = enclosingInter[0]; li <= enclosingInter[1]; li++) {
            bool toTake = (myLocalToGlobal[li] - globalSpacePart[0]) % regInterval == 0;
            if (toTake) {
                if (accumInter.empty())
                    accumInter = Interval::create(li, li);
                else
                    accumInter[1]++;
            } else {
                if (!accumInter.empty()) {
                    res.append(accumInter + localElems[0]);
                    accumInter = Interval::createEmpty();
                }
            }
        }
        if (!accumInter.empty()) {
            res.append(accumInter + localElems[0]);
            accumInter = Interval::createEmpty();
        }
    }
    return !res.empty();
}

DvmType IndirectAxisDistribRule::globalToLocalOwn(DvmType ind, bool *pLocalElem) const {
    bool isLocal = false;
    DvmType res = localElems[0] + exactIndex(local2ToGlobal + shdWidth[0], localElems.size(), ind);
    if (localElems.contains(res))
        isLocal = true;
    if (pLocalElem)
        *pLocalElem = isLocal;
    return res;
}

DvmType IndirectAxisDistribRule::globalToLocal2(DvmType ind, bool *pLocalElem, bool *pShadowElem) const {
    bool isLocal = false, isShadow = false;
    DvmType res = globalToLocalOwn(ind, &isLocal);
    if (!isLocal) {
        DvmType shdInd = exactIndex(shdElements, shdWidth.size(), ShadowElementInfo::wrap(ind));
        if (shdInd >= 0 && shdInd < (DvmType)shdWidth.size()) {
            res = shdElements[shdInd].local2Index;
            isShadow = true;
        }
    }
    if (pLocalElem)
        *pLocalElem = isLocal;
    if (pShadowElem)
        *pShadowElem = isShadow;
    return res;
}

int IndirectAxisDistribRule::genSubparts(const double distribPoints[], Interval parts[], int count) const {
    assert(count >= 0);
    int res = 0;
    if (localElems.empty()) {
        std::fill(parts, parts + count, localElems);
    } else if (count > 0) {
        int maxWgtIdx = 0;
        for (int i = 1; i < count; i++) {
            if (distribPoints[i + 1] - distribPoints[i] > distribPoints[maxWgtIdx + 1] - distribPoints[maxWgtIdx])
                maxWgtIdx = i;
        }
        for (int i = 0; i < count; i++) {
            if (i < maxWgtIdx)
                parts[i] = Interval::create(localElems[0], localElems[0] - 1);
            else if (i == maxWgtIdx)
                parts[i] = localElems;
            else
                parts[i] = Interval::create(localElems[1] + 1, localElems[1]);
        }
        res = 1;
    }
    return res;
}

template <typename T>
static void growArray(T *&arr, UDvmType prevSize, UDvmType toLeft, UDvmType toRight) {
    T *newArr = new T[toLeft + prevSize + toRight];
    if (arr) {
        safeMemcpy(newArr + toLeft, arr, prevSize);
        delete[] arr;
    }
    arr = newArr;
}

void IndirectAxisDistribRule::addShadow(DvmType elemBuf[], UDvmType elemCount, const std::string &name) {
    DvmhTimer tm(true);
    dvmh_log(DEBUG, "Making shadow edge '%s'", name.c_str());
    int myShadowNumber = -1;
    for (int i = 0; i < (int)shadows.size(); i++) {
        if (shadows[i])
            checkError3(shadows[i]->name != name, "Shadow edge with name '%s' already exists", name.c_str());
        else if (myShadowNumber < 0)
            myShadowNumber = i;
    }
    if (myShadowNumber < 0) {
        myShadowNumber = shadows.size();
        shadows.push_back(0);
    }
    UDvmType uniqueCount = sortTrimAndMakeUnique(elemBuf, elemCount, spaceDim);
    dvmh_log(DEBUG, "Time to sort,trim,make unique: %g", tm.lap());
    // Subtract local part
    elemCount = std::set_difference(elemBuf, elemBuf + uniqueCount, local2ToGlobal + shdWidth[0], local2ToGlobal + shdWidth[0] + localElems.size(), elemBuf)
            - elemBuf;
    // Now we have no out-of-space indexes, no duplicates, no local-part indexes
    dvmh_log(DEBUG, "Time to subtract local part: %g", tm.lap());
    // Calculate new elements count
    UDvmType newElemCount = 0;
    UDvmType si = 0;
    for (UDvmType eli = 0; eli < elemCount; eli++) {
        DvmType curEl = elemBuf[eli];
        while (si < shdWidth.size() && shdElements[si].globalIndex < curEl)
            si++;
        if (si >= shdWidth.size()) {
            newElemCount += elemCount - eli;
            break;
        }
        if (shdElements[si].globalIndex != curEl)
            newElemCount++;
    }
    dvmh_log(DEBUG, "Time to calculate new elements count: %g", tm.lap());
    tm.push();
    if (newElemCount < elemCount) {
        // Compactify old elements
        DvmType *l2Convert = new DvmType[shdWidth.size()];
        for (UDvmType si = 0; si < shdWidth.size(); si++)
            l2Convert[si] = si;
        bool isIdentical = true; // Flag if there is no need to convert indices
        int shadowBlockCount = shadowBlocks.size();
        for (int i = 0; i < shadowBlockCount; i++) {
            Interval curBlock = shadowBlocks[i].getBlock();
            std::vector<bool> foundElems(curBlock.size(), false);
            UDvmType foundCount = 0;
            UDvmType si = 0;
            for (UDvmType eli = 0; eli < elemCount; eli++) {
                DvmType curEl = elemBuf[eli];
                while (si < shdWidth.size() && shdElements[si].globalIndex < curEl)
                    si++;
                if (si < shdWidth.size() && shdElements[si].globalIndex == curEl && curBlock.contains(shdElements[si].local2Index)) {
                    foundCount++;
                    foundElems[shdElements[si].local2Index - curBlock.begin()] = true;
                }
            }
            if (foundCount == curBlock.size()) {
                // Add the whole block to our new shadow edge
                shadowBlocks[i].includeIn(myShadowNumber);
            } else if (foundCount > 0) {
                bool done = false;
                UDvmType count = 0;
                for (UDvmType j = 0; j < curBlock.size(); j++) {
                    if (foundElems[j])
                        count++;
                    else
                        break;
                }
                if (count == foundCount) {
                    // Split the block and add its first part to our new shadow edge
                    shadowBlocks.push_back(shadowBlocks[i].extractOnLeft(foundCount, myShadowNumber));
                    done = true;
                } else if (count == 0) {
                    UDvmType count = 0;
                    for (UDvmType j = 0; j < curBlock.size(); j++) {
                        if (foundElems[foundElems.size() - 1 - j])
                            count++;
                        else
                            break;
                    }
                    if (count == foundCount) {
                        // Split the block and add its second part to our new shadow edge
                        shadowBlocks.push_back(shadowBlocks[i].extractOnRight(foundCount, myShadowNumber));
                        done = true;
                    }
                }
                if (!done) {
                    // It is not an one piece at either edge. Reorder the block's elements so that first part belongs to our new shadow edge.
                    isIdentical = false;
                    DvmType ni = 0;
                    DvmType offs = shdWidth[0] + (curBlock[0] < localElems[0] ? -localElems[0] : -(localElems[1] + 1));
                    for (UDvmType j = 0; j < curBlock.size(); j++) {
                        if (foundElems[j]) {
                            l2Convert[curBlock[0] + j + offs] = curBlock[0] + ni + offs;
                            ni++;
                        }
                    }
                    for (UDvmType j = 0; j < curBlock.size(); j++) {
                        if (!foundElems[j]) {
                            l2Convert[curBlock[0] + j + offs] = curBlock[0] + ni + offs;
                            ni++;
                        }
                    }
                    // Split the block and add its first part to our new shadow edge
                    shadowBlocks.push_back(shadowBlocks[i].extractOnLeft(foundCount, myShadowNumber));
                }
            }
        }
        dvmh_log(DEBUG, "Time to modify shadowBlocks and fill l2Convert: %g", tm.lap());
        // shadowBlocks is done
        // l2Convert is ready
        if (!isIdentical) {
            // Converting L2 indices in all our structures.
            for (UDvmType i = 0; i < shdWidth.size(); i++) {
                DvmType prevL2 = shdElements[i].local2Index;
                DvmType prevOffs = shdWidth[0] + (prevL2 < localElems[0] ? -localElems[0] : -(localElems[1] + 1));
                DvmType prevL2Compact = prevL2 + prevOffs;
                DvmType newL2Compact = l2Convert[prevL2Compact];
                if (newL2Compact != prevL2Compact) {
                    DvmType newOffs = shdWidth[0] + (newL2Compact < shdWidth[0] ? -localElems[0] : -(localElems[1] + 1));
                    DvmType newL2 = newL2Compact - newOffs;
                    shdElements[i].local2Index = newL2;
                    local2ToGlobal[newL2 + shdWidth[0] - localElems[0]] = shdElements[i].globalIndex;
                }
            }
            for (int i = 0; i < (int)shadows.size(); i++) {
                if (shadows[i]) {
                    ExchangeMap &xchgMap = shadows[i]->exchangeMap;
                    for (UDvmType j = 0; j < xchgMap.recvStarts[xchgMap.recvProcCount]; j++) {
                        DvmType prevL2 = xchgMap.recvIndices[j];
                        DvmType prevOffs = shdWidth[0] + (prevL2 < localElems[0] ? -localElems[0] : -(localElems[1] + 1));
                        DvmType prevL2Compact = prevL2 + prevOffs;
                        DvmType newL2Compact = l2Convert[prevL2Compact];
                        if (newL2Compact != prevL2Compact) {
                            DvmType newOffs = shdWidth[0] + (newL2Compact < shdWidth[0] ? -localElems[0] : -(localElems[1] + 1));
                            DvmType newL2 = newL2Compact - newOffs;
                            xchgMap.recvIndices[j] = newL2;
                        }
                    }
                }
            }
            // Conversion is done
            // TODO: Need to move data in arrays, maybe. Or declare that shadow adding is a disruptive operation regarding values in shadow edges.
        }
        dvmh_log(DEBUG, "Time to perform shadow edge indexes conversion: %g", tm.lap());
    }
    tm.pop();
    dvmh_log(DEBUG, "Time to compactify old elements: %g", tm.lap());
    tm.push();
    if (newElemCount > 0) {
        // Add new elements
        ShdWidth myWidth;
        myWidth[1] = std::min(spaceDim[1] - (localElems[1] + shdWidth[1]), (DvmType)newElemCount);
        myWidth[0] = newElemCount - myWidth[1];
        growArray(shdElements, shdWidth.size(), 0, myWidth.size());
        growArray(local2ToGlobal, localElems.size() + shdWidth.size(), myWidth[0], myWidth[1]);
        dvmh_log(DEBUG, "Time to grow indexing arrays: %g", tm.lap());
        DvmType newElemIdx = 0;
        si = 0;
        for (UDvmType eli = 0; eli < elemCount; eli++) {
            DvmType curEl = elemBuf[eli];
            while (si < shdWidth.size() && shdElements[si].globalIndex < curEl)
                si++;
            if (si >= shdWidth.size() || shdElements[si].globalIndex != curEl) {
                shdElements[shdWidth.size() + newElemIdx].globalIndex = curEl;
                DvmType l2i;
                if (newElemIdx < myWidth[0])
                    l2i = localElems[0] - shdWidth[0] - myWidth[0] + newElemIdx;
                else
                    l2i = localElems[1] + 1 + shdWidth[1] + (newElemIdx - myWidth[0]);
                shdElements[shdWidth.size() + newElemIdx].local2Index = l2i;
                shdElements[shdWidth.size() + newElemIdx].local1Index = spaceDim[0] - 1;
                shdElements[shdWidth.size() + newElemIdx].ownerProcessorIndex = -1;
                local2ToGlobal[l2i - localElems[0] + shdWidth[0] + myWidth[0]] = curEl;
                newElemIdx++;
            }
        }
        dvmh_log(DEBUG, "Time to add new indexes to indexing arrays: %g", tm.lap());
        assert(newElemIdx == (DvmType)newElemCount);
        shdWidth[0] += myWidth[0];
        shdWidth[1] += myWidth[1];
        if (myWidth[0] > 0) {
            Interval newBlock;
            newBlock[0] = localElems[0] - shdWidth[0];
            newBlock[1] = newBlock[0] + myWidth[0] - 1;
            shadowBlocks.push_back(IndirectShadowBlock(newBlock));
            shadowBlocks.back().includeIn(myShadowNumber);
        }
        if (myWidth[1] > 0) {
            Interval newBlock;
            newBlock[1] = localElems[1] + shdWidth[1];
            newBlock[0] = newBlock[1] - myWidth[1] + 1;
            shadowBlocks.push_back(IndirectShadowBlock(newBlock));
            shadowBlocks.back().includeIn(myShadowNumber);
        }
        std::sort(shdElements, shdElements + shdWidth.size()); // in fact, we can use merge_inplace
        dvmh_log(DEBUG, "Time to sort shdElements: %g", tm.lap());
    }
    tm.pop();
    dvmh_log(DEBUG, "Time to add index information: %g", tm.lap());
    fillShadowLocal1Indexes();
    dvmh_log(DEBUG, "Time to fill local1 indexes for shadow elements: %g", tm.lap());
    IndirectShadow *newShadow = new IndirectShadow();
    assert(myShadowNumber >= 0 && myShadowNumber < (int)shadows.size());
    assert(!shadows[myShadowNumber]);
    shadows[myShadowNumber] = newShadow;
    newShadow->name = name;
    fillExchangeMap(myShadowNumber);
    dvmh_log(DEBUG, "Total time for addShadow: %g", tm.total());
}

bool IndirectAxisDistribRule::fillShadowL2Indexes(Intervals &res, const std::string &name) const {
    int shadowIndex = -1;
    for (int i = 0; i < (int)shadows.size(); i++) {
        if (shadows[i]->name == name) {
            shadowIndex = i;
            break;
        }
    }
    if (shadowIndex < 0)
        return false;
    res.clear();
    for (int i = 0; i < (int)shadowBlocks.size(); i++) {
        if (shadowBlocks[i].isIncludedIn(shadowIndex))
            res.unite(shadowBlocks[i].getBlock());
    }
    return true;
}

DvmType *IndirectAxisDistribRule::getLocal2ToGlobal(DvmType *pFirstIndex) const {
    if (pFirstIndex)
        *pFirstIndex = localElems[0] - shdWidth[0];
    return local2ToGlobal;
}

IndirectAxisDistribRule::~IndirectAxisDistribRule() {
    delete globalToLocal;
    delete[] local2ToGlobal;
    delete[] shdElements;
    for (int i = 0; i < (int)shadows.size(); i++)
        delete shadows[i];
    shadows.clear();
}

void IndirectAxisDistribRule::createGlobalToLocal() {
    globalToLocal = new DvmhData((sizeof(DvmType) == sizeof(long) ? DvmhData::dtLong : DvmhData::dtLongLong), 1, &spaceDim, 0);
    {
        DvmhDistribSpace *dspace = new DvmhDistribSpace(1, &spaceDim);
        DvmhDistribRule *disRule = new DvmhDistribRule(1, mps);
        disRule->setAxisRule(1, DvmhAxisDistribRule::createBlock(mps, mpsAxis, spaceDim));
        dspace->redistribute(disRule);
        DvmhAxisAlignRule axRule;
        axRule.setLinear(1, 1, 0);
        DvmhAlignRule *alRule = new DvmhAlignRule(1, dspace, &axRule);
        globalToLocal->realign(alRule, true);
    }
    dvmh_log(DEBUG, "globalToLocal's localPart is " DTFMT ".." DTFMT, globalToLocal->getAxisLocalPart(1)[0], globalToLocal->getAxisLocalPart(1)[1]);
    if (globalToLocal->hasLocal()) {
        if (!globalToLocal->getRepr(0))
            globalToLocal->createNewRepr(0, globalToLocal->getLocalPart());
        globalToLocal->initActual(0);
    }
}

void IndirectAxisDistribRule::fillGenblock(const UDvmType fakeGenBlock[]) {
    int procCount = mps->getAxis(mpsAxis).procCount;
    int ourProc = mps->getAxis(mpsAxis).ourProc;
    UDvmType totalCount = 0;
    for (int i = 0; i < procCount; i++) {
        dvmh_log(TRACE, "fakeGenBlock[%d]=" UDTFMT, i, fakeGenBlock[i]);
        totalCount += fakeGenBlock[i];
    }
    checkInternal2(totalCount == spaceDim.size(), "Element count mismatch");
    sumGenBlock[0] = spaceDim[0];
    for (int i = 0; i < procCount; i++)
        sumGenBlock[i + 1] = sumGenBlock[i] + fakeGenBlock[i];
    // fake sumGenBlock is done
    localElems = Interval::create(sumGenBlock[ourProc], sumGenBlock[ourProc + 1] - 1);
    maxSubparts = localElems.empty() ? 0 : 1;
}

void IndirectAxisDistribRule::createLocal2ToGlobal() {
    if (!localElems.empty() || !shdWidth.empty())
        local2ToGlobal = new DvmType[localElems.size() + shdWidth.size()];
    else
        local2ToGlobal = 0;
}

void IndirectAxisDistribRule::finishGlobalToLocal() {
    // Converts globalToProc to globalToLocal. Properly filled local2ToGlobal and sumGenBlock needed.
    int procCount = mps->getAxis(mpsAxis).procCount;
    UDvmType *filledByProc = new UDvmType[procCount];
    UDvmType *filledByMe = new UDvmType[procCount];
    memset(filledByMe, 0, sizeof(UDvmType) * procCount);
    const BlockAxisDistribRule *axRule = globalToLocal->getAlignRule()->getDspace()->getAxisDistribRule(1)->asBlockDistributed();
    UDvmType nextConsider = 0;
    DvmType nextEl = local2ToGlobal[shdWidth[0]];
    for (int p = 0; p < procCount - 1; p++) {
        filledByMe[p + 1] = filledByMe[p];
        Interval part = axRule->getLocalElems(p);
        while (nextConsider < localElems.size() && nextEl <= part.end()) {
            filledByMe[p + 1]++;
            nextConsider++;
            nextEl = local2ToGlobal[shdWidth[0] + nextConsider];
        }
    }
    mps->getAxis(mpsAxis).axisComm->alltoall(filledByMe, filledByProc);
    delete[] filledByMe;
    Interval part = globalToLocal->getAxisLocalPart(1);
    for (DvmType gi = part[0]; gi <= part[1]; gi++) {
        int curProc = globalToLocal->getRepr(0)->getElement<DvmType>(gi);
        globalToLocal->getRepr(0)->getElement<DvmType>(gi) = sumGenBlock[curProc] + (DvmType)filledByProc[curProc];
        filledByProc[curProc]++;
    }
    delete[] filledByProc;
}

void IndirectAxisDistribRule::fillShadowLocal1Indexes() {
    DvmhTimer tm(true);
    int procCount = mps->getAxis(mpsAxis).procCount;
    DvmhCommunicator *axisComm = mps->getAxis(mpsAxis).axisComm;
    UDvmType *sendSizes = new UDvmType[procCount];
    for (int p = 0; p < procCount; p++)
        sendSizes[p] = 0;
    std::vector<UDvmType> shdElementsToFill;
    for (UDvmType i = 0; i < shdWidth.size(); i++) {
        if (shdElements[i].local1Index < spaceDim[0]) {
            DvmType gi = shdElements[i].globalIndex;
            if (globalToLocal->getRepr(0)->hasElement(gi)) {
                shdElements[i].ownerProcessorIndex = axisComm->getCommRank();
                shdElements[i].local1Index = globalToLocal->getRepr(0)->getElement<DvmType>(gi);
            } else {
                int p = globalToLocal->getAlignRule()->getDspace()->getAxisDistribRule(1)->asBlockDistributed()->getProcIndex(gi);
                assert(p >= 0 && p < procCount);
                shdElements[i].ownerProcessorIndex = p;
                sendSizes[p] += sizeof(DvmType);
                shdElementsToFill.push_back(i);
            }
        }
    }
    dvmh_log(DEBUG, "Time to fill shdElementsToFill: %g", tm.lap());
    DvmType **sendBuffers = new DvmType *[procCount];
    UDvmType *filledCount = new UDvmType[procCount];
    for (int p = 0; p < procCount; p++) {
        if (sendSizes[p])
            sendBuffers[p] = new DvmType[sendSizes[p] / sizeof(DvmType)];
        else
            sendBuffers[p] = 0;
        filledCount[p] = 0;
    }
    for (UDvmType j = 0; j < shdElementsToFill.size(); j++) {
        UDvmType i = shdElementsToFill[j];
        DvmType gi = shdElements[i].globalIndex;
        int p = shdElements[i].ownerProcessorIndex;
        sendBuffers[p][filledCount[p]] = gi;
        filledCount[p]++;
    }
    dvmh_log(DEBUG, "Time to fill sendBuffers: %g", tm.lap());
    DvmType **recvBuffers = new DvmType *[procCount];
    UDvmType *recvSizes = new UDvmType[procCount];
    axisComm->alltoallv1(sendSizes, (char **)sendBuffers, recvSizes, (char **)recvBuffers);
    dvmh_log(DEBUG, "Time to alltoallv1: %g", tm.lap());
    for (int p = 0; p < procCount; p++) {
        UDvmType elemCount = recvSizes[p] / sizeof(DvmType);
        for (UDvmType j = 0; j < elemCount; j++) {
            DvmType gi = recvBuffers[p][j];
            assert(globalToLocal->getRepr(0)->hasElement(gi));
            recvBuffers[p][j] = globalToLocal->getRepr(0)->getElement<DvmType>(gi);
        }
    }
    dvmh_log(DEBUG, "Time to convert global indexes: %g", tm.lap());
    std::swap(recvSizes, sendSizes);
    std::swap(recvBuffers, sendBuffers);
    UDvmType answersReceived = axisComm->alltoallv2(sendSizes, (char **)sendBuffers, recvSizes, (char **)recvBuffers) / sizeof(DvmType);
    assert(shdElementsToFill.size() == answersReceived);
    dvmh_log(DEBUG, "Time to alltoallv2: %g", tm.lap());
    for (int p = 0; p < procCount; p++)
        filledCount[p] = 0;
    for (UDvmType j = 0; j < shdElementsToFill.size(); j++) {
        UDvmType i = shdElementsToFill[j];
        int p = shdElements[i].ownerProcessorIndex;
        shdElements[i].local1Index = recvBuffers[p][filledCount[p]];
        filledCount[p]++;
    }
    dvmh_log(DEBUG, "Time to fill shdElements[i].local1Index: %g", tm.lap());
    delete[] filledCount;
    delete[] recvSizes;
    delete[] sendSizes;
    for (int p = 0; p < procCount; p++) {
        delete[] recvBuffers[p];
        delete[] (char *)sendBuffers[p];
    }
    delete[] sendBuffers;
    delete[] recvBuffers;
}

void IndirectAxisDistribRule::fillExchangeMap(int shadowIndex) {
    DvmhTimer tm(true);
    ExchangeMap &xchgMap = shadows[shadowIndex]->exchangeMap;
    int procCount = mps->getAxis(mpsAxis).procCount;
    DvmhCommunicator *axisComm = mps->getAxis(mpsAxis).axisComm;
    xchgMap.clear();
    // Firstly, the 'recv' part
    UDvmType totalRecvElements = 0;
    for (int i = 0; i < (int)shadowBlocks.size(); i++) {
        if (shadowBlocks[i].isIncludedIn(shadowIndex))
            totalRecvElements += shadowBlocks[i].getBlock().size();
    }
    UDvmType *shdElementsToRecv = new UDvmType[totalRecvElements];
    DvmType *globalIndexesToRecv = new DvmType[totalRecvElements];
    UDvmType counter = 0;
    for (int i = 0; i < (int)shadowBlocks.size(); i++) {
        if (shadowBlocks[i].isIncludedIn(shadowIndex)) {
            Interval block = shadowBlocks[i].getBlock();
            DvmType startI = block[0] - localElems[0] + shdWidth[0];
            DvmType endI = startI + block.size();
            for (DvmType j = startI; j < endI; j++)
                globalIndexesToRecv[counter++] = local2ToGlobal[j];
        }
    }
    assert(counter == totalRecvElements);
    std::sort(globalIndexesToRecv, globalIndexesToRecv + totalRecvElements);
    UDvmType *fromProcessors = new UDvmType[procCount + 1];
    for (int p = 0; p <= procCount; p++)
        fromProcessors[p] = 0;
    for (UDvmType i = 0, j = 0; i < totalRecvElements; i++) {
        while (shdElements[j].globalIndex < globalIndexesToRecv[i])
            j++;
        shdElementsToRecv[i] = j;
        fromProcessors[shdElements[j].ownerProcessorIndex + 1]++;
    }
    delete[] globalIndexesToRecv;
    for (int p = 1; p <= procCount; p++)
        fromProcessors[p] += fromProcessors[p - 1];
    assert(totalRecvElements == fromProcessors[procCount]);
    int notEmptyCount = 0;
    for (int p = 0; p < procCount; p++) {
        if (fromProcessors[p + 1] - fromProcessors[p] > 0)
            notEmptyCount++;
    }
    xchgMap.recvProcCount = notEmptyCount;
    xchgMap.recvProcs = new int[notEmptyCount];
    xchgMap.recvStarts = new UDvmType[notEmptyCount + 1];
    xchgMap.recvIndices = new DvmType[totalRecvElements];
    for (int p = 0, notEmptyIdx = 0; p < procCount; p++) {
        if (fromProcessors[p + 1] - fromProcessors[p] > 0) {
            xchgMap.recvProcs[notEmptyIdx] = p;
            xchgMap.recvStarts[notEmptyIdx] = fromProcessors[p];
            notEmptyIdx++;
        }
    }
    xchgMap.recvStarts[notEmptyCount] = totalRecvElements;
    for (UDvmType i = 0; i < totalRecvElements; i++) {
        UDvmType j = shdElementsToRecv[i];
        int p = shdElements[j].ownerProcessorIndex;
        xchgMap.recvIndices[fromProcessors[p]] = shdElements[j].local2Index;
        fromProcessors[p]++;
    }
    // The 'recv' part is done
    dvmh_log(DEBUG, "Time to fill the recv part of the ExchangeMap: %g", tm.lap());
    // Working on the 'send' part
    UDvmType *sendSizes = new UDvmType[procCount];
    DvmType **sendBuffers = new DvmType *[procCount];
    for (int p = procCount - 1; p >= 0; p--) {
        UDvmType elemCount = fromProcessors[p] - (p > 0 ? fromProcessors[p - 1] : 0);
        if (elemCount > 0)
            sendBuffers[p] = new DvmType[elemCount];
        else
            sendBuffers[p] = 0;
        sendSizes[p] = elemCount * sizeof(DvmType);
        fromProcessors[p] = 0;
    }
    for (UDvmType i = 0; i < totalRecvElements; i++) {
        UDvmType j = shdElementsToRecv[i];
        int p = shdElements[j].ownerProcessorIndex;
        sendBuffers[p][fromProcessors[p]] = shdElements[j].local1Index;
        fromProcessors[p]++;
    }
    delete[] shdElementsToRecv;
    delete[] fromProcessors;
    UDvmType *recvSizes = new UDvmType[procCount];
    DvmType **recvBuffers = new DvmType *[procCount];
    UDvmType totalSendElems = axisComm->alltoallv1(sendSizes, (char **)sendBuffers, recvSizes, (char **)recvBuffers) / sizeof(DvmType);
    notEmptyCount = 0;
    for (int p = 0; p < procCount; p++) {
        if (recvSizes[p] > 0)
            notEmptyCount++;
    }
    xchgMap.sendProcCount = notEmptyCount;
    xchgMap.sendProcs = new int[notEmptyCount];
    xchgMap.sendStarts = new UDvmType[notEmptyCount + 1];
    xchgMap.sendIndices = new DvmType[totalSendElems];
    UDvmType nextSendElemIndex = 0;
    for (int p = 0, notEmptyIdx = 0; p < procCount; p++) {
        UDvmType elemCount = recvSizes[p] / sizeof(DvmType);
        if (elemCount > 0) {
            xchgMap.sendProcs[notEmptyIdx] = p;
            xchgMap.sendStarts[notEmptyIdx] = nextSendElemIndex;
            for (UDvmType i = 0; i < elemCount; i++)
                xchgMap.sendIndices[nextSendElemIndex++] = recvBuffers[p][i];
            notEmptyIdx++;
        }
        delete[] sendBuffers[p];
        delete[] (char *)recvBuffers[p];
    }
    assert(nextSendElemIndex == totalSendElems);
    xchgMap.sendStarts[notEmptyCount] = totalSendElems;
    delete[] sendSizes;
    delete[] sendBuffers;
    delete[] recvSizes;
    delete[] recvBuffers;
    // The 'send' part is done
    dvmh_log(DEBUG, "Time to fill the send part of the ExchangeMap: %g", tm.lap());
    bool doChecks = true;
    if (doChecks) {
        bool isConsistent = xchgMap.checkConsistency(axisComm);
        checkInternal2(isConsistent, "ExchangeMap consistency check failed");
    }
    dvmh_log(DEBUG, "Time to check consistency of the ExchangeMap: %g", tm.lap());
    xchgMap.fillBestOrder(axisComm);
    dvmh_log(DEBUG, "Time to fill bestOrder of the ExchangeMap: %g", tm.lap());
    if (doChecks) {
        bool isConsistent = xchgMap.checkBestOrder(axisComm);
        checkInternal2(isConsistent, "BestOrder consistency check failed");
    }
    dvmh_log(DEBUG, "Time to check bestOrder of the ExchangeMap: %g", tm.lap());
    dvmh_log(DEBUG, "Total time to fill the ExchangeMap: %g", tm.total());
}

// DvmhDistribRule

DvmhDistribRule::DvmhDistribRule(int aRank, const MultiprocessorSystem *aMPS) {
    rank = aRank;
    MPS = aMPS;
    assert(rank > 0);
    assert(MPS);
    axes = new DvmhAxisDistribRule *[rank];
    for (int i = 0; i < rank; i++)
        axes[i] = 0;
    hasIndirectFlag = false;
    hasLocalFlag = false;
    mpsAxesUsed = 0;
    localPart = new Interval[rank];
    for (int i = 0; i < rank; i++)
        localPart[i] = Interval::createEmpty();
    filledFlag = false;
}

void DvmhDistribRule::setAxisRule(int axis, DvmhAxisDistribRule *rule) {
    delete axes[axis - 1];
    assert(rule);
    axes[axis - 1] = rule;
    hasIndirectFlag = false;
    hasLocalFlag = true;
    filledFlag = true;
    std::set<int> usesMpsAxes;
    mpsAxesUsed = 0;
    for (int i = 0; i < rank; i++) {
        if (axes[i] && axes[i]->isIndirect())
            hasIndirectFlag = true;
        if (!axes[i] || axes[i]->getLocalElems().empty())
            hasLocalFlag = false;
        if (!axes[i])
            filledFlag = false;
        if (axes[i]) {
            assert(axes[i]->getMPS() == MPS);
            localPart[i] = axes[i]->getLocalElems();
            if (!axes[i]->isReplicated()) {
                mpsAxesUsed = std::max(mpsAxesUsed, axes[i]->getMPSAxis());
                bool newOne = usesMpsAxes.insert(axes[i]->getMPSAxis()).second;
                assert(newOne);
            }
        }
    }
}

bool DvmhDistribRule::fillLocalPart(int proc, Interval res[]) const {
    assert(filledFlag);
    if (proc == MPS->getCommRank()) {
        res->blockAssign(rank, localPart);
        return hasLocalFlag;
    }
    bool ret = true;
    for (int i = 0; i < rank; i++) {
        if (axes[i]->isReplicated()) {
            res[i] = axes[i]->getSpaceDim();
        } else {
            int axisProc = MPS->getAxisIndex(proc, axes[i]->getMPSAxis());
            if (axisProc == MPS->getAxis(axes[i]->getMPSAxis()).ourProc)
                res[i] = axes[i]->getLocalElems();
            else
                res[i] = axes[i]->asDistributed()->getLocalElems(axisProc);
        }
        ret = ret && !res[i].empty();
    }
    return ret;
}

DvmhDistribRule::~DvmhDistribRule() {
    for (int i = 0; i < rank; i++)
        delete axes[i];
    delete[] axes;
    delete[] localPart;
}

// DvmhDistribSpace

DvmhDistribSpace::DvmhDistribSpace(int aRank, const Interval aSpace[]) {
    rank = aRank;
    space = new Interval[rank];
    distribRule = 0;
    refCount = 0;
    for (int i = 0; i < rank; i++)
        space[i] = aSpace[i];
}

bool DvmhDistribSpace::isSuitable(const DvmhDistribRule *aDistribRule) const {
    bool res = true;
    res = res && aDistribRule->getRank() == rank;
    for (int i = 0; i < rank; i++)
        res = res && aDistribRule->getAxisRule(i + 1)->getSpaceDim() == space[i];
    return res;
}

void DvmhDistribSpace::redistribute(DvmhDistribRule *aDistribRule) {
    assert(isSuitable(aDistribRule));
    for (std::set<DvmhData *>::iterator it = alignedDatas.begin(); it != alignedDatas.end(); it++)
        (*it)->redistribute(aDistribRule);
    delete distribRule;
    distribRule = aDistribRule;
    assert(distribRule);
}

DvmhDistribSpace::~DvmhDistribSpace() {
    checkError2(alignedDatas.empty(), "Can not delete the template if any distributed arrays aligned with it remain");
    checkInternal(refCount == 0);
    assert(space);
    delete[] space;
    delete distribRule;
}

// DvmhAxisAlignRule

void DvmhAxisAlignRule::setReplicated(DvmType aMultiplier, const Interval &aReplicInterval) {
    if (aReplicInterval.size() == 1) {
        setConstant(aReplicInterval[0]);
    } else {
        axisNumber = -1;
        multiplier = aMultiplier;
        assert(multiplier > 0);
        summand = 0;
        summandLocal = summand;
        replicInterval = aReplicInterval;
    }
}

void DvmhAxisAlignRule::setConstant(DvmType aSummand) {
    axisNumber = 0;
    multiplier = 0;
    summand = aSummand;
    summandLocal = summand;
    replicInterval = Interval::createEmpty();
}

void DvmhAxisAlignRule::setLinear(int aAxisNumber, DvmType aMultiplier, DvmType aSummand) {
    axisNumber = aAxisNumber;
    assert(axisNumber > 0);
    multiplier = aMultiplier;
    assert(multiplier != 0);
    summand = aSummand;
    summandLocal = summand;
    replicInterval = Interval::createEmpty();
}

void DvmhAxisAlignRule::composite(const DvmhAxisAlignRule rules[]) {
    DvmhAxisAlignRule oldRule = *this;
    if (oldRule.axisNumber > 0) {
        int j = oldRule.axisNumber - 1;
        if (rules[j].axisNumber == -1) {
            DvmType mult;
            Interval replicInt;
            if (oldRule.multiplier > 0) {
                mult = oldRule.multiplier * rules[j].multiplier;
                replicInt[0] = rules[j].replicInterval[0] * oldRule.multiplier + oldRule.summand;
                replicInt[1] = rules[j].replicInterval[1] * oldRule.multiplier + oldRule.summand;
            } else {
                mult = (-1) * oldRule.multiplier * rules[j].multiplier;
                replicInt[0] = rules[j].replicInterval[1] * oldRule.multiplier + oldRule.summand;
                replicInt[1] = rules[j].replicInterval[0] * oldRule.multiplier + oldRule.summand;
            }
            setReplicated(mult, replicInt);
        } else if (rules[j].axisNumber == 0) {
            setConstant(rules[j].summand * oldRule.multiplier + oldRule.summand);
        } else {
            setLinear(rules[j].axisNumber, rules[j].multiplier * oldRule.multiplier, rules[j].summand * oldRule.multiplier + oldRule.summand);
        }
    }
}

// DvmhAlignRule

DvmhAlignRule::DvmhAlignRule(int aRank, DvmhDistribSpace *aDspace, const DvmhAxisAlignRule axisRules[]) {
    dspace = aDspace;
    assert(dspace);
    dspace->addRef();
    rank = aRank;
    assert(rank >= 0);
    map = new DvmhAxisAlignRule[dspace->getRank()];
    dspaceAxis = new int[rank];
    for (int i = 0; i < dspace->getRank(); i++)
        map[i] = axisRules[i];
    setDistribRule(dspace->getDistribRule());
}

DvmhAlignRule::DvmhAlignRule(const DvmhAlignRule &other) {
    dspace = other.dspace;
    dspace->addRef();
    rank = other.rank;
    map = new DvmhAxisAlignRule[dspace->getRank()];
    for (int i = 0; i < dspace->getRank(); i++)
        map[i] = other.map[i];
    dspaceAxis = new int[rank];
    for (int i = 0; i < rank; i++)
        dspaceAxis[i] = other.dspaceAxis[i];
    hasIndirectFlag = other.hasIndirectFlag;
    disRule = other.disRule;
}

bool DvmhAlignRule::mapOnPart(const Interval dspacePart[], Interval res[], const DvmType intervals[]) const {
    dvmh_log(TRACE, "Mapping rect to dspace part. Rect:");
    custom_log(TRACE, piecesOutOne, rank, res);
    dvmh_log(TRACE, "Part:");
    custom_log(TRACE, piecesOutOne, dspace->getRank(), dspacePart);
    bool hasLocal = true;
    for (int i = 0; i < dspace->getRank(); i++) {
        const DvmhAxisAlignRule *curRule = getAxisRule(i + 1);
        int ax = curRule->axisNumber;
        if (!disRule->isIndirect(i + 1)) {
            if (ax == -1) {
                bool ok = false;
                if (dspacePart[i][0] <= curRule->replicInterval[0] && dspacePart[i][1] >= curRule->replicInterval[1]) {
                    ok = true;
                } else if (dspacePart[i][0] <= curRule->replicInterval[0] && dspacePart[i][1] >= curRule->replicInterval[0]) {
                    ok = true;
                } else if (dspacePart[i][0] <= curRule->replicInterval[1] && dspacePart[i][1] >= curRule->replicInterval[1]) {
                    ok = true;
                } else {
                    DvmType rounded = curRule->replicInterval[0] + roundDownS(dspacePart[i][1] - curRule->replicInterval[0], curRule->multiplier);
                    if (dspacePart[i][0] <= rounded)
                        ok = true;
                }
                if (!ok)
                    hasLocal = false;
            } else if (ax == 0) {
                if (!(curRule->summand >= dspacePart[i][0] && curRule->summand <= dspacePart[i][1]))
                    hasLocal = false;
            } else {
                DvmType cand;
                if (curRule->multiplier > 0)
                    cand = divUpS(dspacePart[i][0] - curRule->summand, curRule->multiplier);
                else
                    cand = divDownS(dspacePart[i][0] - curRule->summand, curRule->multiplier);
                if (cand > res[ax - 1][0]) {
                    if (intervals)
                        res[ax - 1][0] = res[ax - 1][0] + roundUpS(cand - res[ax - 1][0], intervals[ax - 1]);
                    else
                        res[ax - 1][0] = cand;
                }
                if (curRule->multiplier > 0)
                    cand = divDownS(dspacePart[i][1] - curRule->summand, curRule->multiplier);
                else
                    cand = divUpS(dspacePart[i][1] - curRule->summand, curRule->multiplier);
                if (cand < res[ax - 1][1]) {
                    if (intervals)
                        res[ax - 1][1] = res[ax - 1][1] - roundUpS(res[ax - 1][1] - cand, intervals[ax - 1]);
                    else
                        res[ax - 1][1] = cand;
                }
                if (res[ax - 1][0] > res[ax - 1][1])
                    hasLocal = false;
            }
        } else {
            // If indirect - dspacePart is in local indexes
            const IndirectAxisDistribRule *axRule = disRule->getAxisRule(i + 1)->asIndirect();
            Intervals havePart;
            if (ax == -1) {
                axRule->fillPart(havePart, curRule->replicInterval, curRule->multiplier);
                havePart.intersect(dspacePart[i]);
            } else if (ax == 0) {
                axRule->fillPart(havePart, Interval::create(curRule->summand, curRule->summand));
                havePart.intersect(dspacePart[i]);
            } else {
                // Converting to DistribSpace global space
                DvmType interval = (intervals ? intervals[ax - 1] : 1);
                assert(curRule->multiplier == 1);
                res[ax - 1] += curRule->summand;
                checkError2(interval == 1, "It is not allowed to use step for loop dimension mapped on non-block distributed axis");
                // Converting to DistribSpace local space
                axRule->fillPart(havePart, res[ax - 1], interval);
                havePart.intersect(dspacePart[i]);
                // We do not allow multi-interval results and convert them to local distrib space's indexes
                if (!havePart.empty())
                    res[ax - 1] = havePart.toInterval();
                else
                    res[ax - 1][1] = res[ax - 1][0] - 1;
            }
            if (havePart.empty())
                hasLocal = false;
        }
    }
    if (!hasLocal) {
        for (int i = 0; i < rank; i++) {
            if (getDspaceAxis(i + 1) <= 0)
                res[i][1] = res[i][0] - 1;
        }
    }
    dvmh_log(TRACE, "Result (hasLocal=%d):", (int)hasLocal);
    custom_log(TRACE, piecesOutOne, rank, res);
    return hasLocal;
}

bool DvmhAlignRule::mapOnPart(const Intervals dspacePart[], Intervals res[], const Interval spacePart[], const DvmType intervals[]) const {
    bool hasLocal = true;
    for (int i = 0; i < dspace->getRank(); i++) {
        bool axisHasLocal = false;
        for (int j = 0; j < dspacePart[i].getIntervalCount(); j++) {
            //Interval axisDspacePart = dspacePart[i].getInterval(j);
            // TODO: implement, maybe
            checkInternal2(false, "Not implemented");
        }
        if (!axisHasLocal)
            hasLocal = false;
    }
    return hasLocal;
}

void DvmhAlignRule::setDistribRule(const DvmhDistribRule *newDisRule) {
    disRule = newDisRule;
    assert(disRule->getRank() == dspace->getRank());
    for (int i = 0; i < dspace->getRank(); i++)
        assert(disRule->getAxisRule(i + 1)->getSpaceDim() == dspace->getSpace()[i]);
    for (int i = 0; i < rank; i++)
        dspaceAxis[i] = -1;
    hasIndirectFlag = false;
    for (int i = 0; i < dspace->getRank(); i++) {
        if (disRule->isIndirect(i + 1)) {
            checkError2(map[i].axisNumber == 0 || map[i].multiplier == 1,
                    "It is not allowed to use coefficient for linear rule on non-block distributed template axis");
            if (map[i].axisNumber > 0)
                hasIndirectFlag = true;
        }
        if (map[i].axisNumber > 0)
            dspaceAxis[map[i].axisNumber - 1] = i + 1;
    }
}

DvmhAlignRule::~DvmhAlignRule() {
    assert(map);
    delete[] map;
    assert(dspaceAxis);
    delete[] dspaceAxis;
    assert(dspace);
    dspace->removeRef();
}

// DvmhBuffer

DvmhBuffer::DvmhBuffer(int aRank, UDvmType aTypeSize, int devNum, const Interval aHavePortion[], void *devAddr, bool aOwnMemory) {
    rank = aRank;
    typeSize = aTypeSize;
    deviceNum = devNum;
    totalSize = typeSize;
    if (rank > 0) {
        havePortion = new Interval[rank];
        safeMemcpy(havePortion, aHavePortion, rank);
        for (int i = 0; i < rank; i++) {
            assert(!havePortion[i].empty() || (devAddr && i == 0));
            totalSize *= std::max((UDvmType)1, havePortion[i].size());
        }
    } else
        havePortion = 0;
    assert(totalSize > 0);
    if (!devAddr) {
        deviceAddr = devices[deviceNum]->allocBytes(totalSize, dvmhSettings.pageSize);
        ownMemory = true;
    } else {
        deviceAddr = devAddr;
        ownMemory = aOwnMemory;
    }
    assert(deviceAddr);
    if (rank > 0) {
        axisPerm = new int[rank];
        for (int i = 0; i < rank; i++)
            axisPerm[i] = i + 1;
    } else
        axisPerm = 0;
    diagonalizedState = 0;
}

bool DvmhBuffer::pageLock() const {
    bool res = false;
    if (!isIncomplete() && devices[deviceNum]->getType() == dtHost) {
        bool hasCuda = false;
        for (int i = 0; i < devicesCount; i++)
            if (devices[i]->getType() == dtCuda)
                hasCuda = true;
        if (hasCuda)
            res = ((HostDevice *)devices[deviceNum])->pageLock(deviceAddr, totalSize);
    }
    return res;
}

bool DvmhBuffer::isTransformed() const {
    if (diagonalizedState != 0)
        return true;
    for (int i = 0; i < rank; i++)
        if (axisPerm[i] != i + 1)
            return true;
    return false;
}

void DvmhBuffer::doTransform(const int newAxisPerm[], int perDiagonalState) {
    if (rank <= 1)
        return;
#ifdef NON_CONST_AUTOS
    int mNewAxisPerm[rank], permNoDiag[rank];
#else
    int mNewAxisPerm[MAX_ARRAY_RANK], permNoDiag[MAX_ARRAY_RANK];
#endif
    for (int i = 0; i < rank; i++)
        mNewAxisPerm[i] = (newAxisPerm ? newAxisPerm[i] : i + 1);
    canonicalizeTransformState(mNewAxisPerm, perDiagonalState);
    safeMemcpy(permNoDiag, mNewAxisPerm, rank);
    if (perDiagonalState) {
        int tmp = 0;
        canonicalizeTransformState(permNoDiag, tmp);
    }
    bool same = perDiagonalState == diagonalizedState;
    for (int i = 0; i < rank; i++)
        same = same && mNewAxisPerm[i] == axisPerm[i];
    if (same)
        return;
    if (isDiagonalized()) {
        applyDiagonal(0);
        canonicalizeTransformState();
    }
    assert(!isDiagonalized());
    same = true;
    for (int i = 0; i < rank; i++)
        same = same && permNoDiag[i] == axisPerm[i];
    if (!same) {
#ifdef NON_CONST_AUTOS
        int axisDelta[rank], revAxisPerm[rank];
#else
        int axisDelta[MAX_ARRAY_RANK], revAxisPerm[MAX_ARRAY_RANK];
#endif
        for (int i = 0; i < rank; i++)
            revAxisPerm[axisPerm[i] - 1] = i + 1;
        for (int i = 0; i < rank; i++)
            axisDelta[i] = revAxisPerm[permNoDiag[i] - 1];
        applyPermDelta(axisDelta);
    }
    if (perDiagonalState) {
        safeMemcpy(axisPerm, mNewAxisPerm, rank);
        applyDiagonal(perDiagonalState);
    }
    assert(canonicalizeTransformState() == false);
}

void DvmhBuffer::setTransformState(const int newAxisPerm[], int perDiagonalState) {
    for (int i = 0; i < rank; i++)
        axisPerm[i] = (newAxisPerm ? newAxisPerm[i] : i + 1);
    diagonalizedState = perDiagonalState;
    canonicalizeTransformState();
}

void DvmhBuffer::undoDiagonal() {
    doTransform(axisPerm, 0);
}

void DvmhBuffer::fillHeader(const void *base, DvmType header[], DvmhDiagonalInfo *diagonalInfo, bool zeroDiagonalized) const {
    DvmType collector = 1;
    DvmType offset = 0;
    header[0] = (DvmType)this;
    header[rank + 2] = (DvmType)base;
    checkInternal2(((DvmType)deviceAddr - header[rank + 2]) % typeSize == 0, "Impossible to calculate an offset of the array from the provided base");
    for (int i = rank; i >= 1; i--) {
        int origAx = axisPerm[i - 1];
        if (zeroDiagonalized && i >= rank - 1 && diagonalizedState != 0) {
            header[(origAx - 1) + 1] = 0;
        } else {
            offset += collector * havePortion[origAx - 1][0];
            header[(origAx - 1) + 1] = collector;
        }
        collector *= havePortion[axisPerm[i - 1] - 1].size();
    }
    header[rank + 1] = ((DvmType)deviceAddr - header[rank + 2]) / typeSize - offset;
    if (diagonalInfo) {
        if (diagonalizedState != 0) {
            int xAxis = axisPerm[rank - 1];
            int yAxis = axisPerm[rank - 2];
            diagonalInfo->slashFlag = diagonalizedState == 2;
            diagonalInfo->x_axis = xAxis;
            diagonalInfo->y_axis = yAxis;
            diagonalInfo->x_first = havePortion[xAxis - 1][0];
            diagonalInfo->y_first = havePortion[yAxis - 1][0];
            diagonalInfo->x_length = havePortion[xAxis - 1].size();
            diagonalInfo->y_length = havePortion[yAxis - 1].size();
        } else {
            diagonalInfo->slashFlag = -1;
        }
    }
}

void *DvmhBuffer::getNaturalBase() const {
    DvmType collector = 1;
    DvmType offset = 0;
    for (int i = rank; i >= 1; i--) {
        int origAx = axisPerm[i - 1];
        if (diagonalizedState == 0 || i < rank - 1)
            offset += collector * havePortion[origAx - 1][0];
        collector *= havePortion[origAx - 1].size();
    }
    return (char *)deviceAddr - offset * typeSize;
}

bool DvmhBuffer::hasElement(const DvmType indexes[]) const {
    if (isIncomplete())
        return indexes[0] >= havePortion[0].begin() && (havePortion + 1)->blockContains(rank - 1, indexes + 1);
    else
        return havePortion->blockContains(rank, indexes);
}

void *DvmhBuffer::getElemAddr(const DvmType indexes[]) const {
    DvmType collector = 1;
    DvmType offset = 0;
    for (int i = rank; i >= 1; i--) {
        int origAx = axisPerm[i - 1];
        if (diagonalizedState == 0 || i < rank - 1)
            offset += collector * (indexes[origAx - 1] - havePortion[origAx - 1][0]);
        collector *= havePortion[origAx - 1].size();
    }
    if (diagonalizedState != 0) {
        int xAxis = axisPerm[rank - 1];
        int yAxis = axisPerm[rank - 2];
        offset += dvmhXYToDiagonal(indexes[xAxis - 1] - havePortion[xAxis - 1][0], indexes[yAxis - 1] - havePortion[yAxis - 1][0],
                havePortion[xAxis - 1].size(), havePortion[yAxis - 1].size(), (diagonalizedState == 2 ? true : false));
    }
    return (char *)deviceAddr + offset * typeSize;
}

int DvmhBuffer::getMinIndexTypeSize(const Interval block[]) const {
    UDvmType collector = 1;
    DvmType offset = 0;
    UDvmType maxAbsIdx = 0;
    for (int i = rank; i >= 1; i--) {
        if (diagonalizedState == 0 || i < rank - 1)
            offset += collector * havePortion[axisPerm[i - 1] - 1][0];
        maxAbsIdx += collector * std::max(std::abs(block[axisPerm[i - 1] - 1][0]), std::abs(block[axisPerm[i - 1] - 1][1]));
        collector *= havePortion[axisPerm[i - 1] - 1].size();
    }
    int natBaseBits = valueBits(maxAbsIdx) + 1;
    int factBaseBits = (offset < 0 ? valueBits(maxAbsIdx + (UDvmType)(-offset)) + 1 : natBaseBits);
    return divUpS(std::max(natBaseBits, factBaseBits), CHAR_BIT);
}

DvmhBuffer *DvmhBuffer::undiagCutting(const Interval cutting[]) const {
    assert(isDiagonalized());
#ifdef NON_CONST_AUTOS
    Interval resPortion[rank];
#else
    Interval resPortion[MAX_ARRAY_RANK];
#endif
    safeMemcpy(resPortion, cutting, rank);
    DvmhBuffer *res = new DvmhBuffer(rank, typeSize, deviceNum, resPortion);
    res->setTransformState(axisPerm, 0);
    ((DvmhBuffer *)this)->undiagCuttingInternal(res->getDeviceAddr(), resPortion, false);
    return res;
}

void DvmhBuffer::replaceUndiagedCutting(DvmhBuffer *part) {
    assert(isDiagonalized());
#ifdef NON_CONST_AUTOS
    Interval partPortion[rank];
#else
    Interval partPortion[MAX_ARRAY_RANK];
#endif
    safeMemcpy(partPortion, part->getHavePortion(), rank);
    undiagCuttingInternal(part->getDeviceAddr(), partPortion, true);
}

void DvmhBuffer::copyTo(DvmhBuffer *to, const Interval piece[]) const {
#ifdef NON_CONST_AUTOS
    Interval block[rank];
#else
    Interval block[MAX_ARRAY_RANK];
#endif
    assert(to);
    assert(rank == to->rank);
    assert(typeSize == to->typeSize);
    if (piece)
        safeMemcpy(block, piece, rank);
    else
        havePortion->blockIntersect(rank, to->havePortion, block);
    dvmh_log(TRACE, "Copying block:");
    custom_log(TRACE, piecesOutOne, rank, block);
    dvmh_log(TRACE, "Source on device #%d has:", deviceNum);
    custom_log(TRACE, piecesOutOne, rank, havePortion);
    dvmh_log(TRACE, "Destination on device #%d has:", to->deviceNum);
    custom_log(TRACE, piecesOutOne, rank, to->havePortion);
    assert(havePortion->blockContains(rank, block) && to->havePortion->blockContains(rank, block));
    dvmhCopy(this, to, block);
}

bool DvmhBuffer::canDumpInplace(const Interval piece[]) const {
    return deviceNum == 0 && isBlockConsecutive(piece);
}

DvmhBuffer *DvmhBuffer::dumpPiece(const Interval piece[], bool allowInplace) const {
    DvmhBuffer *res = 0;
    bool inPlace = allowInplace && canDumpInplace(piece);
    if (!inPlace) {
        res = new DvmhBuffer(rank, typeSize, 0, piece);
        res->pageLock();
        copyTo(res);
    } else {
#ifdef NON_CONST_AUTOS
        DvmType beginIndexes[rank];
#else
        DvmType beginIndexes[MAX_ARRAY_RANK];
#endif
        for (int i = 0; i < rank; i++)
            beginIndexes[i] = piece[i].begin();
        res = new DvmhBuffer(rank, typeSize, 0, piece, getElemAddr(beginIndexes));
        dvmh_log(TRACE, "Made new in-place DvmhBuffer at offset " UDTFMT " bytes", (UDvmType)res->deviceAddr - (UDvmType)deviceAddr);
    }
    assert(res);
    return res;
}

bool DvmhBuffer::overlaps(const DvmhBuffer *other) const {
    assert(this != other);
    if (deviceNum == other->deviceNum) {
        char *addr1 = (char *)deviceAddr;
        char *addr2 = (char *)other->deviceAddr;
        return (addr1 <= addr2 && addr1 + totalSize > addr2) || (addr2 <= addr1 && addr2 + other->totalSize > addr1);
    } else {
        return false;
    }
}

DvmhBuffer::~DvmhBuffer() {
    if (devices[deviceNum]->getType() == dtHost)
        ((HostDevice *)devices[deviceNum])->pageUnlock(deviceAddr);
    if (ownMemory && deviceAddr)
        devices[deviceNum]->dispose(deviceAddr);
    delete[] havePortion;
    delete[] axisPerm;
}

bool DvmhBuffer::canonicalizeTransformState(int perm[], int &diag) const {
    bool changed = false;
    if (rank == 0) {
        assert(diag == 0);
    } else if (rank == 1) {
        assert(perm[0] == 1);
        assert(diag == 0);
    } else {
        if (diag != 0) {
            UDvmType Rx = havePortion[perm[rank - 1] - 1].size();
            UDvmType Ry = havePortion[perm[rank - 2] - 1].size();
            bool slashFlag = diag == 2;
            if (!slashFlag && (Rx == 1 || Ry == 1)) {
                diag = 0;
                changed = true;
            } else if (slashFlag && Ry == 1) {
                diag = 0;
                changed = true;
            }
        }
        if (isIncomplete()) {
            for (int i = 0; i < rank; i++) {
                if (perm[i] != 1)
                    assert(havePortion[perm[i] - 1].size() == 1);
                else
                    break;
            }
        }
        int leaveRight = diag ? 2 : 0;
        assert(leaveRight <= rank);
        if (rank > leaveRight) {
            // TODO: change permutation: let all the one-sized dimensions stay at their original place
        }
    }
    return changed;
}

bool DvmhBuffer::isBlockConsecutive(const Interval piece[]) const {
    bool res = true;
    bool needWholeDim = false;
    for (int i = 0; i < rank; i++) {
        if (needWholeDim) {
            res = res && !isDiagonalized() && axisPerm[i] == i + 1 && havePortion[i] == piece[i];
        } else if (piece[i].size() > 1) {
            res = res && !isDiagonalized() && axisPerm[i] == i + 1;
            needWholeDim = true;
        }
    }
    return res;
}

void DvmhBuffer::applyDiagonal(int newDiagonalState) {
    assert((newDiagonalState == 0) ^ (diagonalizedState == 0));
    assert(!isIncomplete());
    dvmh_log(TRACE, "Applying diagonal transformation newDiagonalState = %d", newDiagonalState);
    if (devices[deviceNum]->getType() == dtCuda)
        ((CudaDevice *)devices[deviceNum])->deviceSynchronize();
    DvmhTimer tm(true);
    UDvmType Rx = havePortion[axisPerm[rank - 1] - 1].size();
    UDvmType Ry = havePortion[axisPerm[rank - 2] - 1].size();
    UDvmType Rz = 1;
    for (int i = 0; i < rank - 2; i++)
        Rz *= havePortion[axisPerm[i] - 1].size();
    bool slashFlag = (newDiagonalState + diagonalizedState) == 2;
    bool backFlag = newDiagonalState == 0;
    void *oldMem = deviceAddr;
    void *newMem = devices[deviceNum]->allocBytes(totalSize, dvmhSettings.pageSize);
    checkInternal2(devices[deviceNum]->getType() == dtCuda, "Diagonal transformation is implemented only for CUDA devices");
    dvmhCudaTransformArray((CudaDevice *)devices[deviceNum], oldMem, typeSize, Rx, Ry, Rz, backFlag, slashFlag, newMem);
    if (ownMemory) {
        devices[deviceNum]->dispose(oldMem);
        deviceAddr = newMem;
    } else {
        devices[deviceNum]->memCopy(oldMem, newMem, totalSize);
        devices[deviceNum]->dispose(newMem);
    }
    if (devices[deviceNum]->getType() == dtCuda)
        ((CudaDevice *)devices[deviceNum])->deviceSynchronize();
    double timeNow = tm.total();
    if (devices[deviceNum]->getType() == dtCuda)
        dvmh_stat_add_measurement(((CudaDevice *)devices[deviceNum])->index, DVMH_STAT_METRIC_UTIL_ARRAY_TRANSFORMATION, totalSize, 0.0, timeNow);
    diagonalizedState = newDiagonalState;
}

void DvmhBuffer::applyPermDelta(int permDelta[]) {
    assert(diagonalizedState == 0);
#ifdef MAX_ARRAY_RANK
    SmallVector<int, MAX_ARRAY_RANK> axes;
#else
    HybridVector<int, 10> axes;
#endif
    for (int i = 0; i < rank; i++) {
        if (permDelta[i] != i + 1)
            axes.push_back(i + 1);
    }
    assert(axes.size() > 1);
    assert(!isIncomplete());
    if (devices[deviceNum]->getType() == dtCuda)
        ((CudaDevice *)devices[deviceNum])->deviceSynchronize();
    DvmhTimer tm(true);
#ifdef NON_CONST_AUTOS
    Interval portion[rank];
#else
    Interval portion[MAX_ARRAY_RANK];
#endif
    for (int i = 0; i < rank; i++)
        portion[i] = havePortion[axisPerm[i] - 1];
    UDvmType aggregatedTypeSize = typeSize;
    for (int i = axes.back() + 1; i <= rank; i++)
        aggregatedTypeSize *= portion[i - 1].size();
    UDvmType Rz = 1;
    for (int i = 1; i < axes[0]; i++)
        Rz *= portion[i - 1].size();
    if (axes.size() == 2) {
        // Transposition of 2 dimensions
        UDvmType Rx = portion[axes[1] - 1].size();
        UDvmType Ry = portion[axes[0] - 1].size();
        UDvmType Rbetween = 1;
        for (int i = axes[0] + 1; i < axes[1]; i++)
            Rbetween *= portion[i - 1].size();
        dvmh_log(TRACE, "Applying transposition of (transformed) axes %d and %d. typeSize=" DTFMT ", Rx=" DTFMT ", Rbetween=" DTFMT ", Ry=" DTFMT
                ", Rz=" DTFMT, axes[0], axes[1], aggregatedTypeSize, Rx, Rbetween, Ry, Rz);
        checkInternal2(devices[deviceNum]->getType() == dtCuda, "Dimension transposition is implemented only for CUDA devices");
        bool inPlace = dvmhCudaCanTransposeInplace((CudaDevice *)devices[deviceNum], deviceAddr, aggregatedTypeSize, Rx, Rbetween, Ry, Rz);
        void *oldMem = deviceAddr;
        void *newMem = (inPlace ? deviceAddr : devices[deviceNum]->allocBytes(totalSize, dvmhSettings.pageSize));
        dvmhCudaTransposeArray((CudaDevice *)devices[deviceNum], oldMem, aggregatedTypeSize, Rx, Rbetween, Ry, Rz, newMem);
        if (!inPlace) {
            if (ownMemory) {
                devices[deviceNum]->dispose(oldMem);
                deviceAddr = newMem;
            } else {
                devices[deviceNum]->memCopy(oldMem, newMem, totalSize);
                devices[deviceNum]->dispose(newMem);
            }
        }
    } else {
        // Any permutation
        int toLeft = axes[0] - 1;
        int toRight = rank - axes.back();
        int modRank = rank - toLeft - toRight;
#ifdef NON_CONST_AUTOS
        int modPerm[modRank];
        UDvmType modSizes[modRank];
#else
        int modPerm[MAX_ARRAY_RANK];
        UDvmType modSizes[MAX_ARRAY_RANK];
#endif
        UDvmType Rbetween = 1;
        for (int i = axes[0]; i <= axes.back(); i++) {
            modPerm[i - axes[0]] = permDelta[i - 1] - toLeft;
            modSizes[i - axes[0]] = portion[i - 1].size();
            Rbetween *= portion[i - 1].size();
        }
        dvmh_log(TRACE, "Applying permutation of %d dimensions", (int)axes.size());
        void *oldMem = deviceAddr;
        void *newMem = devices[deviceNum]->allocBytes(totalSize, dvmhSettings.pageSize);
        checkInternal2(devices[deviceNum]->getType() == dtCuda, "Dimension permutation is implemented only for CUDA devices");
        dvmhCudaPermutateArray((CudaDevice *)devices[deviceNum], oldMem, aggregatedTypeSize, modRank, modSizes, modPerm, Rz, newMem);
        if (ownMemory) {
            devices[deviceNum]->dispose(oldMem);
            deviceAddr = newMem;
        } else {
            devices[deviceNum]->memCopy(oldMem, newMem, totalSize);
            devices[deviceNum]->dispose(newMem);
        }
    }
    if (devices[deviceNum]->getType() == dtCuda)
        ((CudaDevice *)devices[deviceNum])->deviceSynchronize();
    double timeNow = tm.total();
    if (devices[deviceNum]->getType() == dtCuda)
        dvmh_stat_add_measurement(((CudaDevice *)devices[deviceNum])->index, DVMH_STAT_METRIC_UTIL_ARRAY_TRANSFORMATION, totalSize, 0.0, timeNow);
#ifdef NON_CONST_AUTOS
    int oldAxisPerm[rank];
#else
    int oldAxisPerm[MAX_ARRAY_RANK];
#endif
    safeMemcpy(oldAxisPerm, axisPerm, rank);
    for (int i = 0; i < rank; i++)
        axisPerm[i] = oldAxisPerm[permDelta[i] - 1];
}

UDvmType DvmhBuffer::undiagCuttingInternal(void *res, Interval resPortion[], bool backFlag) {
    assert(isDiagonalized());
    int zStepAxis = 1;
    for (int i = rank - 2; i >= 1; i--) {
        int origAxis = axisPerm[i - 1];
        if (resPortion[origAxis - 1].size() > 1) {
            zStepAxis = i;
            break;
        }
    }
    for (int i = 1; i < zStepAxis; i++) {
        int origAxis = axisPerm[i - 1];
        if (resPortion[origAxis - 1].size() > 1) {
            Interval sav;
            sav = resPortion[origAxis - 1];
            UDvmType overallWritten = 0;
            for (DvmType curVal = sav[0]; curVal <= sav[1]; curVal++) {
                resPortion[origAxis - 1][0] = curVal;
                resPortion[origAxis - 1][1] = curVal;
                UDvmType written = undiagCuttingInternal((char *)res + overallWritten, resPortion, backFlag);
                overallWritten += written;
            }
            resPortion[origAxis - 1] = sav;
            return overallWritten;
        }
    }
    UDvmType Rx = havePortion[axisPerm[rank - 1] - 1].size();
    UDvmType Ry = havePortion[axisPerm[rank - 2] - 1].size();
    UDvmType Rz = 1;
    if (zStepAxis <= rank - 2)
        Rz *= resPortion[axisPerm[zStepAxis - 1] - 1].size();
    for (int i = zStepAxis + 1; i <= rank - 2; i++)
        Rz *= havePortion[axisPerm[i - 1] - 1].size();
    UDvmType xStart = resPortion[axisPerm[rank - 1] - 1][0] - havePortion[axisPerm[rank - 1] - 1][0];
    UDvmType xStep = 1;
    UDvmType xCount = resPortion[axisPerm[rank - 1] - 1].size();
    UDvmType yStart = resPortion[axisPerm[rank - 2] - 1][0] - havePortion[axisPerm[rank - 2] - 1][0];
    UDvmType yStep = 1;
    UDvmType yCount = resPortion[axisPerm[rank - 2] - 1].size();
    UDvmType zStep = 1;
    for (int i = zStepAxis + 1; i <= rank - 2; i++)
        zStep *= havePortion[axisPerm[i - 1] - 1].size();
    UDvmType offs = 0;
    UDvmType collector = 1;
    for (int i = rank; i >= 1; i--) {
        int origAxis = axisPerm[i - 1];
        if (i <= rank - 2)
            offs += collector * (resPortion[origAxis - 1][0] - havePortion[origAxis - 1][0]);
        collector *= havePortion[origAxis - 1].size();
    }
    bool slashFlag = (diagonalizedState == 2);
    checkInternal2(devices[deviceNum]->getType() == dtCuda, "Diagonal transformation is implemented only for CUDA devices");
    void *oldMem = (char *)deviceAddr + typeSize * offs;
    void *newMem = res;
    if (backFlag)
        std::swap(oldMem, newMem);
    dvmhCudaTransformCutting((CudaDevice *)devices[deviceNum], oldMem, typeSize, Rx, Ry, Rz, xStart, xStep, xCount, yStart, yStep, yCount, zStep, !backFlag,
            slashFlag, newMem);
    return typeSize * xCount * yCount * (Rz / zStep);
}

// DvmhRepresentative

DvmhRepresentative::DvmhRepresentative(DvmhData *aData, int devNum, const Interval aHavePortion[], void *devAddr):
        DvmhBuffer(aData->getRank(), aData->getTypeSize(), devNum, aHavePortion, devAddr) {
    actualState = new DvmhPieces(rank);
    cleanTransformState = false;
    pageLock();
}

void DvmhRepresentative::doTransform(const int newAxisPerm[], int perDiagonalState) {
    if (rank <= 1)
        return;
    // Checking newAxisPerm
    if (newAxisPerm) {
#ifdef NON_CONST_AUTOS
        int hasAxes[rank];
#else
        int hasAxes[MAX_ARRAY_RANK];
#endif
        for (int i = 0; i < rank; i++)
            hasAxes[i] = 0;
        for (int i = 0; i < rank; i++) {
            checkInternal(newAxisPerm[i] >= 1 && newAxisPerm[i] <= rank);
            hasAxes[newAxisPerm[i] - 1] = 1;
        }
        for (int i = 0; i < rank; i++)
            checkInternal(hasAxes[i]);
    }
    checkInternal(perDiagonalState >= 0 && perDiagonalState <= 2);
    {
        char buf[300];
        char *s = buf;
        for (int i = 0; i < rank; i++)
            s += sprintf(s, "%d,", (newAxisPerm ? newAxisPerm[i] : i + 1));
        if (rank > 0)
            s--;
        *s = 0;
        dvmh_log(TRACE, "Requested new transformed state: newAxisPerm = (%s), perDiagonalState = %d", buf, perDiagonalState);
    }
    if (!cleanTransformState)
        DvmhBuffer::doTransform(newAxisPerm, perDiagonalState);
    else
        setTransformState(newAxisPerm, perDiagonalState);
}

void DvmhRepresentative::undoDiagonal() {
    if (diagonalizedState != 0)
        doTransform(axisPerm, 0);
}


DvmhRepresentative::~DvmhRepresentative() {
    delete actualState;
}

// DvmhDataState

bool DvmhDataState::operator<(const DvmhDataState &state) const {
    int prodRes = producer.compareTo(state.producer);
    if (prodRes < 0)
        return true;
    if (prodRes > 0)
        return false;
    if (readers.size() < state.readers.size())
        return true;
    if (readers.size() > state.readers.size())
        return false;
    std::set<DvmhRegVar>::const_iterator it2 = state.readers.begin();
    for (std::set<DvmhRegVar>::const_iterator it = readers.begin(); it != readers.end(); it++) {
        int res = it->compareTo(*it2);
        if (res < 0)
            return true;
        if (res > 0)
            return false;
        it2++;
    }
    return false;
}

// DvmhData

DvmhData::DvmhData(UDvmType aTypeSize, TypeType aTypeType, int aRank, const Interval aSpace[], const ShdWidth shadows[]) {
    typeSize = aTypeSize;
    typeType = aTypeType;
    dataType = dtUnknown;
    for (int idt = 0; idt < DATA_TYPES; idt++) {
        DataType dt = (DataType)idt;
        if (typeType == getTypeType(dt) && typeSize == getTypeSize(dt)) {
            dataType = dt;
            break;
        }
    }
    finishInitialization(aRank, aSpace, shadows);
}

DvmhData::DvmhData(DataType aDataType, int aRank, const Interval aSpace[], const ShdWidth shadows[]) {
    dataType = aDataType;
    typeSize = getTypeSize(dataType);
    typeType = getTypeType(dataType);
    finishInitialization(aRank, aSpace, shadows);
}

DvmhData *DvmhData::fromRegularArray(DataType dt, void *regArr, DvmType len, DvmType baseIdx) {
    Interval space[1];
    space[0][0] = baseIdx;
    space[0][1] = baseIdx + len - 1;
    DvmhData *res = new DvmhData(dt, 1, space, 0);
    res->realign(0);
    res->setHostPortion(regArr, space);
    res->initActual(0);
    return res;
}

void DvmhData::setHostPortion(void *hostAddr, const Interval havePortion[]) {
    checkInternal2(hasLocal(), "Setting host portion for data without local part at all");
    DvmhPieces *saveActual = new DvmhPieces(rank);
    if (representatives[0])
        saveActual->append(representatives[0]->getActualState());
    deleteRepr(0);
    representatives[0] = new DvmhRepresentative(this, 0, havePortion, hostAddr);
    representatives[0]->getActualState()->append(saveActual);
    delete saveActual;
    updateHeader();
}

void DvmhData::setHeader(DvmType aHeader[], const void *base, bool owning) {
    header = aHeader;
    freeBase = base == 0;
    ownHeader = header && owning;
    if (header)
        header[rank + 2] = (DvmType)base;
    updateHeader();
}

void DvmhData::initActual(int dev) {
    assert(dev >= -1 && dev < devicesCount);
    if (dev >= 0)
        assert(representatives[dev]);
    if (rank > 0 && space[0].empty())
        dev = -1;
    initShadowProfile();
    for (int i = 0; i < devicesCount; i++) {
        DvmhRepresentative *repr = representatives[i];
        if (repr) {
            DvmhPieces *aState = repr->getActualState();
            aState->clear();
            if (i == dev) {
                if (rank > 0) {
                    aState->appendOne(localPlusShadow);
                    DvmhPieces *p = applyShadowProfile(aState, localPart);
                    aState->clear();
                    aState->append(p);
                    delete p;
                } else
                    aState->appendOne(repr->getHavePortion());
            }
        }
    }
}

void DvmhData::initActualShadow() {
    initShadowProfile();
    if (hasLocalFlag) {
        DvmhPieces *toIntersect = new DvmhPieces(rank);
        toIntersect->appendOne(localPlusShadow, maxOrder);
        toIntersect->uniteOne(localPart);
        for (int i = 0; i < devicesCount; i++) {
            if (representatives[i]) {
#ifdef NON_CONST_AUTOS
                Interval block[rank];
#else
                Interval block[MAX_ARRAY_RANK];
#endif
                representatives[i]->getActualState()->intersectInplace(toIntersect);
                if (localPlusShadow->blockIntersect(rank, representatives[i]->getHavePortion(), block))
                    representatives[i]->getActualState()->uniteOne(block, maxOrder);
            }
        }
    }
}

DvmhRepresentative *DvmhData::createNewRepr(int dev, const Interval havePortion[]) {
    deleteRepr(dev);
    representatives[dev] = new DvmhRepresentative(this, dev, havePortion);
    if (dev == 0)
        updateHeader();
    return representatives[dev];
}

void DvmhData::deleteRepr(int device) {
    DvmhRepresentative *repr = representatives[device];
    if (repr) {
        dvmh_log(TRACE, "Deleting representative on device %d", device);
        delete repr;
    }
    representatives[device] = 0;
}

void DvmhData::extendBlock(int rank, const Interval block[], const ShdWidth shadow[], const Interval bounds[], Interval result[]) {
    for (int i = 0; i < rank; i++) {
        if (!block[i].empty()) {
            result[i][0] = std::max(block[i][0] - (shadow ? shadow[i][0] : 0), bounds[i][0]);
            result[i][1] = std::min(block[i][1] + (shadow ? shadow[i][1] : 0), bounds[i][1]);
        } else {
            result[i] = block[i];
        }
    }
}

void DvmhData::expandIncomplete(DvmType newHigh) {
    assert(isIncomplete() && newHigh >= space[0][0]);
    assert(!isDistributed());
    assert(hasLocal());
    assert(!header || ownHeader);
    space[0][1] = newHigh;
    localPart[0][1] = newHigh;
    recalcLocalPlusShadow();
    void *addr = representatives[0]->getDeviceAddr();
    syncAllAccesses();
    setHostPortion(addr, space);
    initActual(0);
}

void DvmhData::getActualBase(int dev, DvmhPieces *p, const Interval curLocalPart[], bool addToActual) {
    if (curLocalPart) {
        DvmhPieces *tmp = applyShadowProfile(p, curLocalPart);
        performGetActual(dev, tmp, addToActual);
        delete tmp;
    } else
        performGetActual(dev, p, addToActual);
}

void DvmhData::getActualBaseOne(int dev, const Interval realBlock[], const Interval curLocalPart[], bool addToActual) {
    // realBlock must be not bigger than data->space
    DvmhPieces *p = new DvmhPieces(rank);
    p->appendOne(realBlock);
    getActualBase(dev, p, curLocalPart, addToActual);
    delete p;
}

void DvmhData::getActual(const Interval indexes[], bool addToActual) {
    if (hasLocal()) {
        getActualBaseOne(0, indexes, localPart, addToActual);
        curState.addReader(DvmhRegVar());
    }
}

void DvmhData::getActualEdges(const Interval aLocalPart[], const ShdWidth shdWidths[], bool addToActual) {
    assert(hasLocal());
#ifdef NON_CONST_AUTOS
    Interval realBlock[rank];
#else
    Interval realBlock[MAX_ARRAY_RANK];
#endif
    const Interval *curLocalPart;
    if (aLocalPart)
        curLocalPart = aLocalPart;
    else
        curLocalPart = localPart;
    safeMemcpy(realBlock, curLocalPart, rank);
    for (int j = 0; j < rank; j++) {
        Interval leftPart;
        leftPart[0] = curLocalPart[j][0];
        leftPart[1] = curLocalPart[j][1];
        if (shdWidths[j][1] > 0 && localPart[j][0] > space[j][0]) {
            realBlock[j][0] = curLocalPart[j][0];
            realBlock[j][1] = std::min(realBlock[j][0] + shdWidths[j][1] - 1, localPart[j][1]);
            dvmh_log(TRACE, "actualizing edge");
            custom_log(TRACE, piecesOutOne, rank, realBlock);
            getActualBaseOne(0, realBlock, 0, addToActual);
            leftPart[0] = realBlock[j][1] + 1;
        }

        if (shdWidths[j][0] > 0 && localPart[j][1] < space[j][1]) {
            realBlock[j][1] = curLocalPart[j][1];
            realBlock[j][0] = std::max(realBlock[j][1] - (shdWidths[j][0] - 1), localPart[j][0]);
            if (realBlock[j][0] < leftPart[0])
                realBlock[j][0] = leftPart[0];
            if (realBlock[j][0] <= realBlock[j][1]) {
                dvmh_log(TRACE, "actualizing edge");
                custom_log(TRACE, piecesOutOne, rank, realBlock);
                getActualBaseOne(0, realBlock, 0, addToActual);
            }
            leftPart[1] = realBlock[j][0] - 1;
        }

        if (leftPart[0] > leftPart[1])
            break;
        realBlock[j] = leftPart;
    }
}

template <typename T>
static void doWithEveryShadow(int rank, const Interval localPart[], const Interval bounds[], const ShdWidth shdWidths[], bool cornersFlag, T &f) {
    // Shadow edges with or without corners
#ifdef NON_CONST_AUTOS
    Interval realBlock[rank];
#else
    Interval realBlock[MAX_ARRAY_RANK];
#endif
    if (cornersFlag)
        DvmhData::extendBlock(rank, localPart, shdWidths, bounds, realBlock);
    else
        safeMemcpy(realBlock, localPart, rank);

    for (int j = 0; j < rank; j++) {
        if (shdWidths[j][0] > 0 && localPart[j][0] > bounds[j][0]) {
            realBlock[j][0] = std::max(localPart[j][0] - shdWidths[j][0], bounds[j][0]);
            realBlock[j][1] = localPart[j][0] - 1;
            f(realBlock);
        }

        if (shdWidths[j][1] > 0 && localPart[j][1] < bounds[j][1]) {
            realBlock[j][1] = std::min(localPart[j][1] + shdWidths[j][1], bounds[j][1]);
            realBlock[j][0] = localPart[j][1] + 1;
            f(realBlock);
        }

        realBlock[j] = localPart[j];
    }
}

struct ShadowsAccumulator: private Uncopyable {
    DvmhPieces *p;
    DvmType order;
    explicit ShadowsAccumulator(int rank, DvmType aOrder = ABS_ORDER): order(aOrder) { p = new DvmhPieces(rank); }
    void operator()(const Interval realBlock[]) { p->appendOne(realBlock, order); }
    ~ShadowsAccumulator() { delete p; }
};

void DvmhData::getActualShadow(int dev, const Interval curLocalPart[], bool cornerFlag, const ShdWidth curShdWidths[], bool addToActual) {
    assert(hasLocal());
    dvmh_log(TRACE, "Requested to get actual shadow widths:");
    custom_log(TRACE, piecesOutOne, rank, (Interval *)curShdWidths);
    ShadowsAccumulator shadowsAccumulator(rank);
    doWithEveryShadow(rank, curLocalPart, localPlusShadow, curShdWidths, cornerFlag, shadowsAccumulator);
    getActualBase(dev, shadowsAccumulator.p, curLocalPart, addToActual);
}

void DvmhData::clearActual(DvmhPieces *p, int exceptDev) {
    if (!p->isEmpty()) {
        DvmhPieces *tmp = 0;
        for (int i = 0; i < devicesCount; i++) {
            if (representatives[i] && i != exceptDev) {
                if (!tmp) {
                    tmp = new DvmhPieces(rank);
                    for (int i = 0; i < p->getCount(); i++)
                        tmp->appendOne(p->getPiece(i, 0), maxOrder);
                }
                dvmh_log(TRACE, "Clearing actual from device %d. What to clear:", i);
                custom_log(TRACE, piecesOut, tmp);
                dvmh_log(TRACE, "Was:");
                custom_log(TRACE, piecesOut, representatives[i]->getActualState());
                representatives[i]->getActualState()->subtractInplace(tmp, FROM_ABS);
                dvmh_log(TRACE, "Now:");
                custom_log(TRACE, piecesOut, representatives[i]->getActualState());
            }
        }
        delete tmp;
    }
}

void DvmhData::clearActualOne(const Interval piece[], int exceptDev) {
    DvmhPieces *tmp = 0;
    for (int i = 0; i < devicesCount; i++) {
        if (representatives[i] && i != exceptDev) {
            if (!tmp) {
                tmp = new DvmhPieces(rank);
                tmp->appendOne(piece, maxOrder);
            }
            dvmh_log(TRACE, "Clearing actual from device %d. What to clear:", i);
            custom_log(TRACE, piecesOut, tmp);
            dvmh_log(TRACE, "Was:");
            custom_log(TRACE, piecesOut, representatives[i]->getActualState());
            representatives[i]->getActualState()->subtractInplace(tmp, FROM_ABS);
            dvmh_log(TRACE, "Now:");
            custom_log(TRACE, piecesOut, representatives[i]->getActualState());
        }
    }
    delete tmp;
}

void DvmhData::clearActualShadow(const Interval curLocalPart[], bool cornerFlag, const ShdWidth curShdWidths[], int exceptDev) {
    ShadowsAccumulator shadowsAccumulator(rank);
    doWithEveryShadow(rank, curLocalPart, localPlusShadow, curShdWidths, cornerFlag, shadowsAccumulator);
    clearActual(shadowsAccumulator.p, exceptDev);
}

void DvmhData::performSetActual(int dev, DvmhPieces *p) {
    assert(representatives[dev]);
    clearActual(p, dev);
    representatives[dev]->getActualState()->unite(p);
}

void DvmhData::performSetActualOne(int dev, const Interval realBlock[]) {
    checkInternal((rank == 0) >= (realBlock == 0));
    assert(representatives[dev]);
    clearActualOne(realBlock, dev);
    representatives[dev]->getActualState()->uniteOne(realBlock);
}

void DvmhData::setActual(DvmhPieces *p) {
    if (hasLocal()) {
        performSetActual(0, p);
        curState = DvmhDataState();
    }
}

void DvmhData::setActual(const Interval indexes[]) {
    if (hasLocal()) {
        performSetActualOne(0, indexes);
        curState = DvmhDataState();
    }
}

void DvmhData::setActualShadow(int dev, const Interval curLocalPart[], bool cornerFlag, const ShdWidth curShdWidths[]) {
    ShadowsAccumulator shadowsAccumulator(rank);
    doWithEveryShadow(rank, curLocalPart, localPlusShadow, curShdWidths, cornerFlag, shadowsAccumulator);
    performSetActual(dev, shadowsAccumulator.p);
}

void DvmhData::shadowComputed(int dev, const Interval curLocalPart[], bool cornerFlag, const ShdWidth curShdWidths[]) {
    ShadowsAccumulator shadowsAccumulator(rank, maxOrder + 1);
    doWithEveryShadow(rank, curLocalPart, localPlusShadow, curShdWidths, cornerFlag, shadowsAccumulator);
    representatives[dev]->getActualState()->unite(shadowsAccumulator.p);
}

void DvmhData::updateShadowProfile(bool cornerFlag, ShdWidth curShdWidths[]) {
    maxOrder++;
#ifdef NON_CONST_AUTOS
    Interval curLocalPart[rank], curBounds[rank];
#else
    Interval curLocalPart[MAX_ARRAY_RANK], curBounds[MAX_ARRAY_RANK];
#endif
    for (int i = 0; i < rank; i++) {
        curLocalPart[i][0] = 0;
        curLocalPart[i][1] = 0;
        curBounds[i][0] = -shdWidths[i][0];
        curBounds[i][1] = shdWidths[i][1];
    }
    ShadowsAccumulator shadowsAccumulator(rank, maxOrder);
    doWithEveryShadow(rank, curLocalPart, curBounds, curShdWidths, cornerFlag, shadowsAccumulator);
    shadowProfile->unite(shadowsAccumulator.p);
    dvmh_log(TRACE, "New maxOrder=" DTFMT " and new shadow profile:", maxOrder);
    custom_log(TRACE, piecesOut, shadowProfile);
}

UDvmType DvmhData::getTotalElemCount() const {
    UDvmType elemCount = 1;
    for (int i = 0; i < rank; i++)
        elemCount *= space[i].size();
    return elemCount;
}

UDvmType DvmhData::getLocalElemCount() const {
    UDvmType elemCount = 1;
    for (int i = 0; i < rank; i++)
        elemCount *= localPart[i].size();
    return elemCount;
}

void DvmhData::realign(DvmhAlignRule *newAlignRule, bool ownDistribSpace, bool newValueFlag) {
    if (!newAlignRule) {
        assert(!isDistributed());
        safeMemcpy(localPart, space, rank);
        safeMemcpy(localPlusShadow, space, rank);
        hasLocalFlag = true;
    } else {
        assert(!isAligned() || isDistributed());
        bool realignFlag = isDistributed();
        if (!realignFlag) {
            assert(!isAligned());
            for (int i = 0; i < devicesCount; i++)
                assert(representatives[i] == 0);
            for (int i = 1; i <= rank; i++) {
                if (newAlignRule->isIndirect(i)) {
                    shdWidths[i - 1][0] = 0;
                    shdWidths[i - 1][1] = 0;
                }
            }
            initShadowProfile();
            assert(!alignRule);
            alignRule = newAlignRule;
            safeMemcpy(localPart, space, rank);
            hasLocalFlag = alignRule->mapOnPart(alignRule->getDspace()->getLocalPart(), localPart);
            // mapOnPart returns DistribSpace's local indexes in case of indirect distributions, we adjust them here
            for (int i = 1; i <= rank; i++) {
                if (alignRule->isIndirect(i)) {
                    int dspaceAxis = alignRule->getDspaceAxis(i);
                    assert(dspaceAxis > 0);
                    const IndirectAxisDistribRule *disRule = alignRule->getDspace()->getAxisDistribRule(dspaceAxis)->asIndirect();
                    assert(disRule);
                    if (space[i - 1].size() != disRule->getSpaceDim().size()) {
                        // Need to compactify indexes to fit in the original space
                        MPSAxis mpsAxis = disRule->getMPS()->getAxis(disRule->getMPSAxis());
                        UDvmType *elemCounts = new UDvmType[mpsAxis.procCount];
                        elemCounts[mpsAxis.ourProc] = (hasLocalFlag ? 0 : localPart[i - 1].size());
                        mpsAxis.axisComm->allgather(elemCounts);
                        UDvmType elemsBefore = 0;
                        for (int j = 0; j < mpsAxis.ourProc; j++)
                            elemsBefore += elemCounts[j];
                        delete[] elemCounts;
                        DvmType newLocalStart = space[i - 1][0] + (DvmType)elemsBefore;
                        alignRule->getAxisRule(dspaceAxis)->summandLocal = localPart[i - 1][0] - newLocalStart;
                    }
                    localPart[i - 1] -= alignRule->getAxisRule(dspaceAxis)->summandLocal;
                }
                checkInternal(space[i - 1].contains(localPart[i - 1]));
            }
            recalcLocalPlusShadow();
        } else {
            redistributeCommon(newAlignRule, newValueFlag);
        }
        alignRule->getDspace()->addAlignedData(this);
        ownDspace = ownDistribSpace;
    }
}

void DvmhData::redistribute(const DvmhDistribRule *newDistribRule) {
    assert(isDistributed());
    DvmhAlignRule *newAlignRule = new DvmhAlignRule(*alignRule);
    newAlignRule->setDistribRule(newDistribRule);
    redistributeCommon(newAlignRule);
}

bool DvmhData::hasElement(const DvmType indexes[]) const {
    if (!hasLocal())
        return false;
    bool res = true;
    for (int i = 0; i < rank; i++)
        if (!localPart[i].contains(indexes[i]))
            res = false;
    return res;
}

bool DvmhData::convertGlobalToLocal2(DvmType indexes[], bool onlyFromLocalPart) const {
    bool res = true;
    if (isDistributed() && alignRule->hasIndirect()) {
        int dspaceRank = alignRule->getDspace()->getRank();
        for (int i = 1; i <= dspaceRank; i++) {
            const DvmhAxisAlignRule *alRule = alignRule->getAxisRule(i);
            const IndirectAxisDistribRule *disRule = alignRule->getDspace()->getAxisDistribRule(i)->asIndirect();
            int ax = alRule->axisNumber;
            if (disRule && ax > 0) {
                bool isLocal = false, isShadow = false;
                DvmType localIdx;
                if (onlyFromLocalPart)
                    localIdx = disRule->globalToLocalOwn(indexes[ax - 1] * alRule->multiplier + alRule->summand, &isLocal);
                else
                    localIdx = disRule->globalToLocal2(indexes[ax - 1] * alRule->multiplier + alRule->summand, &isLocal, &isShadow);
                if (isLocal || isShadow) {
                    indexes[ax - 1] = localIdx - alRule->summandLocal;
                } else {
                    res = false;
                }
            }
        }
    }
    return res;
}

void DvmhData::includeIndirectShadow(int axis, const std::string shadowName) {
    checkError2(isDistributed(), "Array must be distributed");
    int dspaceAxis = alignRule->getDspaceAxis(axis);
    IndirectAxisDistribRule *distrRule = dspaceAxis > 0 ? alignRule->getDspace()->getAxisDistribRule(dspaceAxis)->asIndirect() : 0;
    checkError2(distrRule, "Specified distributed array axis is not aligned with indirectly-distributed template axis");
    if (distrRule->getSpaceDim().size() != space[axis - 1].size())
        dvmh_log(WARNING, "Shortened distributed array space is not fully supported for now for indirectly distributed axis with shadow edges");
    Intervals shdIndexes;
    bool legalName = distrRule->fillShadowL2Indexes(shdIndexes, shadowName);
    checkError3(legalName, "Specified shadow edge name '%s' is not found", shadowName.c_str());
    if (!shdIndexes.empty()) {
        const DvmhAxisAlignRule *alRule = alignRule->getAxisRule(dspaceAxis);
        Interval boundInterval = shdIndexes.getBoundInterval();
        boundInterval -= alRule->summandLocal;
        checkError2(space[axis - 1].contains(boundInterval),
                "Failed to add shadow edges. Consider declaring a distributed array of the same size as the template.");
        ShdWidth newWidth;
        newWidth[0] = std::max(shdWidths[axis - 1][0], localPart[axis - 1][0] - boundInterval[0]);
        newWidth[1] = std::max(shdWidths[axis - 1][1], boundInterval[1] - localPart[axis - 1][1]);
        changeShadowWidth(axis, newWidth);
    }
}

void DvmhData::localizeAsReferenceFor(DvmhData *targetData, int targetAxis) {
    checkError2(dataType == dtInt || dataType == dtLong || dataType == dtLongLong, "Only array with data type int, long, or long long can be localized");
    unlocalizeValues();
    localizeValues(ReferenceDesc(targetData, targetAxis));
}

DvmhEvent *DvmhData::enqueueWriteAccess(DvmhEvent *accessEnd, bool owning) {
    AggregateEvent *res = new AggregateEvent;
    res->addEvent(latestWriterEnd);
    res->addEvent(latestReadersEnd);
    if (owning)
        latestWriterEnd = accessEnd;
    else
        latestWriterEnd = accessEnd->dup();
    latestReadersEnd = new AggregateEvent;
    return res;
}

DvmhEvent *DvmhData::enqueueReadAccess(DvmhEvent *accessEnd, bool owning) {
    if (owning)
        latestReadersEnd->addEvent(accessEnd);
    else
        latestReadersEnd->addEvent(accessEnd->dup());
    return latestWriterEnd->dup();
}

bool DvmhData::mpsCanRead(const MultiprocessorSystem *mps) const {
    // TODO: Check distribution to figure out if current MPS is sufficient to access all the data through local parts
    return !isDistributed() || alignRule->getDspace()->getDistribRule()->getMPS()->isSubsystemOf(mps);
}

bool DvmhData::mpsCanWrite(const MultiprocessorSystem *mps) const {
    return !isDistributed() || alignRule->getDspace()->getDistribRule()->getMPS()->isSubsystemOf(mps);
}

bool DvmhData::fillLocalPart(int proc, Interval res[]) const {
    bool ret = false;
    if (!isDistributed()) {
        res->blockAssign(rank, localPart);
        ret = hasLocalFlag;
    } else if (proc == alignRule->getDspace()->getDistribRule()->getMPS()->getCommRank()) {
        res->blockAssign(rank, localPart);
        ret = hasLocalFlag;
    } else {
#ifdef NON_CONST_AUTOS
        Interval dspacePart[alignRule->getDspace()->getRank()];
#else
        Interval dspacePart[MAX_DISTRIB_SPACE_RANK];
#endif
        alignRule->getDspace()->getDistribRule()->fillLocalPart(proc, dspacePart);
        res->blockAssign(rank, space);
        ret = alignRule->mapOnPart(dspacePart, res, 0);
    }
    return ret;
}

DvmhData::~DvmhData() {
    syncAllAccesses();
    for (int i = 0; i < devicesCount; i++)
        deleteRepr(i);
    delete[] representatives;
    if (alignRule) {
        DvmhDistribSpace *dspace = alignRule->getDspace();
        dspace->removeAlignedData(this);
        delete alignRule;
        if (ownDspace)
            delete dspace;
    }
    if (header) {
        header[0] = 0;
        if (ownHeader)
            delete[] header;
    }
    assert(shdWidths);
    delete[] shdWidths;
    assert(space);
    delete[] space;
    assert(localPart);
    delete[] localPart;
    assert(localPlusShadow);
    delete[] localPlusShadow;
    delete shadowProfile;
    for (int i = 0; i < (int)localizationInfo.references.size(); i++)
        localizationInfo.references[i].data->unlocalizeValues();
    localizationInfo.references.clear();
    delete latestWriterEnd;
    delete latestReadersEnd;
}

void DvmhData::finishInitialization(int aRank, const Interval aSpace[], const ShdWidth shadows[]) {
    rank = aRank;
    checkInternal2(rank >= 0, "Incorrect rank");
    space = new Interval[rank];
    shdWidths = new ShdWidth[rank];
    for (int i = 0; i < rank; i++) {
        space[i] = aSpace[i];
        assert(!space[i].empty() || (!shadows && i == 0));
    }
    hasLocalFlag = false;
    localPart = new Interval[rank];
    localPlusShadow = new Interval[rank];
    for (int i = 0; i < rank; i++) {
        localPart[i] = Interval::createEmpty();
        localPlusShadow[i] = Interval::createEmpty();
    }
    alignRule = 0;
    representatives = new DvmhRepresentative *[devicesCount];
    for (int i = 0; i < devicesCount; i++)
        representatives[i] = 0;
    for (int i = 0; i < rank; i++) {
        shdWidths[i][0] = (shadows ? shadows[i][0] : 0);
        shdWidths[i][1] = (shadows ? shadows[i][1] : 0);
    }
    shadowProfile = 0;
    initShadowProfile();
    header = 0;
    ownHeader = false;
    freeBase = false;
    ownDspace = false;
    latestWriterEnd = new AggregateEvent; // Will remain empty
    latestReadersEnd = new AggregateEvent;
}

void DvmhData::initShadowProfile() {
    maxOrder = 1;
#ifdef NON_CONST_AUTOS
    Interval shdRect[rank];
#else
    Interval shdRect[MAX_ARRAY_RANK];
#endif
    for (int i = 0; i < rank; i++) {
        shdRect[i][0] = -shdWidths[i][0];
        shdRect[i][1] = shdWidths[i][1];
    }
    delete shadowProfile;
    shadowProfile = new DvmhPieces(rank);
    shadowProfile->appendOne(shdRect, maxOrder);
    for (int i = 0; i < rank; i++) {
        shdRect[i][0] = 0;
        shdRect[i][1] = 0;
    }
    shadowProfile->subtractOne(shdRect);
}

void DvmhData::recalcLocalPlusShadow() {
    extendBlock(rank, localPart, shdWidths, space, localPlusShadow);
}

DvmhPieces *DvmhData::applyShadowProfile(DvmhPieces *p, const Interval curLocalPart[]) const {
    DvmhPieces *sizedProfile = new DvmhPieces(rank);
    sizedProfile->appendOne(curLocalPart);
    for (int i = 0; i < shadowProfile->getCount(); i++) {
#ifdef NON_CONST_AUTOS
        Interval sizedInterval[rank];
#else
        Interval sizedInterval[MAX_ARRAY_RANK];
#endif
        DvmType order;
        const Interval *origInterval = shadowProfile->getPiece(i, &order);
        for (int j = 0; j < rank; j++) {
            int side = origInterval[j][0] > 0;
            sizedInterval[j][0] = origInterval[j][0] + curLocalPart[j][side];
            side = origInterval[j][1] >= 0;
            sizedInterval[j][1] = origInterval[j][1] + curLocalPart[j][side];
        }
        sizedProfile->appendOne(sizedInterval, order);
    }
    sizedProfile->intersectInplace(p);
    return sizedProfile;
}

void DvmhData::performCopy(int dev1, int dev2, const Interval aCutting[]) const {
    if (dev1 != dev2) {
        DvmhRepresentative *repr1, *repr2;
        repr1 = representatives[dev1];
        repr2 = representatives[dev2];
        assert(repr1 && repr2);
        repr1->copyTo(repr2, aCutting);
        repr2->setCleanTransformState(false);
    } else {
        dvmh_log(DEBUG, "Something strange happened. Called copy to copy self to self. Doing nothing.");
    }
}

static DvmhPieces *tryToRoundPiece(int rank, DvmhRepresentative *repr1, DvmhRepresentative *repr2, const Interval cutting[], Interval mCutting[]) {
    DvmhPieces *res = 0;
    safeMemcpy(mCutting, cutting, rank);
    for (int i = rank - 1; i >= 1; i--) {
        bool flag = true;
        if (cutting[i].extend(4).contains(repr1->getHavePortion()[i]) && repr1->getHavePortion()[i] == repr2->getHavePortion()[i]) {
            if (cutting[i] != repr1->getHavePortion()[i]) {
                mCutting[i] = repr1->getHavePortion()[i];
                DvmhPieces *mcp = new DvmhPieces(rank);
                mcp->appendOne(mCutting);
                DvmhPieces *p1 = repr1->getActualState()->intersect(mcp);
                DvmhPieces *p2 = repr2->getActualState()->intersect(mcp);
                delete mcp;
                p2->subtractInplace(p1);
                if (p2->isEmpty()) {
                    delete res;
                    res = p1;
                } else {
                    delete p1;
                    mCutting[i] = cutting[i];
                    flag = false;
                }
                delete p2;
            }
        } else
            flag = false;
        if (!flag)
            break;
    }
    if (res) {
        dvmh_log(TRACE, "rounded cutting is");
        custom_log(TRACE, piecesOutOne, rank, mCutting);
    }
    return res;
}

void DvmhData::performGetActual(int dev, DvmhPieces *p, bool addToActual) {
    checkInternal(dev >= 0 && dev < devicesCount && representatives[dev]);
    DvmhPieces *pieces2 = p->subtract(representatives[dev]->getActualState());
    pieces2->compactify();
    for (int i = 0; pieces2->getCount() > 0 && i < devicesCount; i++)
        if (representatives[i] && i != dev) {
            dvmh_log(TRACE, "pieces rest to actualize to device #%d", dev);
            custom_log(TRACE, piecesOut, pieces2);
            dvmh_log(TRACE, "pieces has device #%d", i);
            custom_log(TRACE, piecesOut, representatives[i]->getActualState());
            DvmhPieces *pieces1 = pieces2->intersect(representatives[i]->getActualState(), SET_NOT_LESS);
            pieces1->compactify();
            dvmh_log(TRACE, "pieces that can be actualized");
            custom_log(TRACE, piecesOut, pieces1);
            int count = pieces1->getCount();
            DvmhPieces *copiedPcs = new DvmhPieces(rank);
            for (int j = 0; j < count; j++) {
#ifdef NON_CONST_AUTOS
                Interval roundedPiece[rank];
#else
                Interval roundedPiece[MAX_ARRAY_RANK];
#endif
                DvmType order;
                DvmhPieces *toAdd = tryToRoundPiece(rank, representatives[i], representatives[dev], pieces1->getPiece(j, &order),
                        roundedPiece);
                performCopy(i, dev, roundedPiece);
                if (toAdd) {
                    copiedPcs->unite(toAdd);
                    delete toAdd;
                } else
                    copiedPcs->uniteOne(roundedPiece, order);
            }
            pieces2->subtractInplace(copiedPcs);
            if (addToActual)
                representatives[dev]->getActualState()->unite(copiedPcs);
            delete pieces1;
            delete copiedPcs;
        }
    if (pieces2->getCount() > 0)
        dvmh_log(WARNING, "Can not actualize whole requested block to representative on device %d", dev);
    delete pieces2;
}

void DvmhData::redistributeCommon(DvmhAlignRule *newAlignRule, bool newValueFlag) {
    assert(isDistributed());
    assert(newAlignRule);
    assert(newAlignRule != alignRule);
    bool doComm = !newValueFlag;
    if (!noLibdvm && !alignRule->hasIndirect() && !newAlignRule->hasIndirect())
        doComm = false;
    //checkInternal2(!doComm, "Not implemented yet");
    int newDspaceRank = newAlignRule->getDspace()->getRank();
    int oldDspaceRank = alignRule->getDspace()->getRank();
#ifdef NON_CONST_AUTOS
    Interval newLocalPart[rank], newLocalPlusShadow[rank];
    ShdWidth newShdWidths[rank];
#else
    Interval newLocalPart[MAX_ARRAY_RANK], newLocalPlusShadow[MAX_ARRAY_RANK];
    ShdWidth newShdWidths[MAX_ARRAY_RANK];
#endif
    safeMemcpy(newShdWidths, shdWidths, rank);
    for (int i = 1; i <= rank; i++) {
        if (alignRule->isIndirect(i) || newAlignRule->isIndirect(i)) {
            newShdWidths[i - 1][0] = 0;
            newShdWidths[i - 1][1] = 0;
        }
    }
    safeMemcpy(newLocalPart, space, rank);
    bool newHasLocal = newAlignRule->mapOnPart(newAlignRule->getDistribRule()->getLocalPart(), newLocalPart);
    // mapOnPart returns DistribSpace's local indexes in case of indirect distributions, we adjust them here
    for (int i = 1; i <= rank; i++) {
        if (newAlignRule->isIndirect(i)) {
            int dspaceAxis = newAlignRule->getDspaceAxis(i);
            assert(dspaceAxis > 0);
            const IndirectAxisDistribRule *axRule = newAlignRule->getDistribRule()->getAxisRule(dspaceAxis)->asIndirect();
            assert(axRule);
            if (space[i - 1].size() != axRule->getSpaceDim().size()) {
                // Need to compactify indexes to fit in the original space
                MPSAxis mpsAxis = axRule->getMPS()->getAxis(axRule->getMPSAxis());
                UDvmType *elemCounts = new UDvmType[mpsAxis.procCount];
                elemCounts[mpsAxis.ourProc] = (newHasLocal ? 0 : newLocalPart[i - 1].size());
                mpsAxis.axisComm->allgather(elemCounts);
                UDvmType elemsBefore = 0;
                for (int j = 0; j < mpsAxis.ourProc; j++)
                    elemsBefore += elemCounts[j];
                delete[] elemCounts;
                DvmType newLocalStart = space[i - 1][0] + (DvmType)elemsBefore;
                newAlignRule->getAxisRule(dspaceAxis)->summandLocal = newLocalPart[i - 1][0] - newLocalStart;
            }
            newLocalPart[i - 1] -= newAlignRule->getAxisRule(dspaceAxis)->summandLocal;
        }
        checkInternal(space[i - 1].contains(newLocalPart[i - 1]));
    }
    extendBlock(rank, newLocalPart, newShdWidths, space, newLocalPlusShadow);

    if (newValueFlag) {
        for (int i = 0; i < devicesCount; i++)
            deleteRepr(i);
    } else if (!doComm) {
        // LibDVM does all the communications
        if (hasLocalFlag) {
            checkInternal(representatives[0]);
            if (currentMPS->getCommSize() > 1) {
                PushCurrentPurpose purpose(DvmhCopyingPurpose::dcpRedistribute);
                getActualBaseOne(0, localPart, localPart, true);
                representatives[0]->doTransform(0, 0);
            }
            checkInternal(!representatives[0]->ownsMemory()); // It means we can delete the representative without doubt
        }
        if (hasLocalFlag && (!newHasLocal || !localPlusShadow->blockEquals(rank, newLocalPlusShadow) || !localPart->blockEquals(rank, newLocalPart))) {
            for (int i = 0; i < devicesCount; i++)
                deleteRepr(i);
        }
    } else {
        assert(doComm);
        dvmh_log(TRACE, "Transiting from old local part (hasLocal=%d):", hasLocalFlag ? 1 : 0);
        custom_log(TRACE, piecesOutOne, rank, localPart);
        dvmh_log(TRACE, "Transiting to new local part (newHasLocal=%d):", newHasLocal ? 1 : 0);
        custom_log(TRACE, piecesOutOne, rank, newLocalPart);
        checkInternal2(!alignRule->hasIndirect() && !newAlignRule->hasIndirect(), "Realign for indirectly-distributed arrays is not implemented yet");
        const MultiprocessorSystem *oldMPS, *newMPS;
        oldMPS = alignRule->getDistribRule()->getMPS();
        newMPS = newAlignRule->getDistribRule()->getMPS();
        checkError2(oldMPS->isSubsystemOf(currentMPS) && newMPS->isSubsystemOf(currentMPS),
                "The former multiprocessor system or the new one is not a subsystem of the current multiprocessor system");
        MultiprocessorSystem *commMPS = currentMPS; // XXX: Could be chosen shorter communicator
        if (commMPS->getCommRank() >= 0) {
#ifdef NON_CONST_AUTOS
            Interval oldDspacePart[oldDspaceRank], newDspacePart[newDspaceRank], oldProcBlock[rank], newProcBlock[rank], block[rank];
#else
            Interval oldDspacePart[MAX_DISTRIB_SPACE_RANK], newDspacePart[MAX_DISTRIB_SPACE_RANK], oldProcBlock[MAX_ARRAY_RANK], newProcBlock[MAX_ARRAY_RANK],
                    block[MAX_ARRAY_RANK];
#endif
            char *sends = new char[oldMPS->getCommSize()];
            DvmhPieces *accumulator = new DvmhPieces(rank);
            for (int p = 0; p < oldMPS->getCommSize(); p++) {
                sends[p] = 0;
                if (alignRule->getDistribRule()->fillLocalPart(p, oldDspacePart)) {
                    oldProcBlock->blockAssign(rank, space);
                    if (alignRule->mapOnPart(oldDspacePart, oldProcBlock)) {
                        bool haveSmth = false;
                        for (int j = 0; !haveSmth && j < accumulator->getCount(); j++)
                            haveSmth = haveSmth || oldProcBlock->blockIntersect(rank, accumulator->getPiece(j), block);
                        // block must be either empty or equal to oldProcBlock
                        if (haveSmth) {
                            checkInternal(block->blockEquals(rank, oldProcBlock));
                        } else {
                            accumulator->appendOne(oldProcBlock); // Since they do not intersect
                            sends[p] = 1;
                        }
                    }
                }
                dvmh_log(TRACE, "sends[%d]=%d", p, sends[p]);
            }
            delete accumulator;
            bool iSend = oldMPS->getCommRank() >= 0 ? sends[oldMPS->getCommRank()] : false;
            int procCount = commMPS->getCommSize();
            int ourProc = commMPS->getCommRank();
            UDvmType *sendSizes = new UDvmType[procCount];
            char **sendBuffers = new char *[procCount];
            UDvmType *recvSizes = new UDvmType[procCount];
            char **recvBuffers = new char *[procCount];
            for (int i = 0; i < procCount; i++) {
                sendSizes[i] = 0;
                sendBuffers[i] = 0;
                recvSizes[i] = 0;
                recvBuffers[i] = 0;
            }
            std::vector<DvmhBuffer *> deferredCopyingTo;
            std::vector<DvmhBuffer *> deferredCopyingFrom;
            DvmhPieces *toGetActual = new DvmhPieces(rank);
            DvmhPieces *toSetActual = new DvmhPieces(rank);
            for (int p = 0; p < procCount; p++) {
                int oldProc = commMPS->getChildCommRank(oldMPS, p);
                int newProc = commMPS->getChildCommRank(newMPS, p);
                bool oldProcHasLocal = false;
                bool newProcHasLocal = false;
                if (oldProc >= 0 && alignRule->getDistribRule()->fillLocalPart(oldProc, oldDspacePart)) {
                    oldProcBlock->blockAssign(rank, space);
                    oldProcHasLocal = alignRule->mapOnPart(oldDspacePart, oldProcBlock);
                }
                if (newProc >= 0 && newAlignRule->getDistribRule()->fillLocalPart(newProc, newDspacePart)) {
                    newProcBlock->blockAssign(rank, space);
                    newProcHasLocal = newAlignRule->mapOnPart(newDspacePart, newProcBlock);
                }
                if (p != ourProc && newProcHasLocal && iSend && newProcBlock->blockIntersect(rank, localPart, block)) {
                    DvmhPieces *reduced = new DvmhPieces(rank);
                    reduced->appendOne(block);
                    if (oldProcHasLocal)
                        reduced->subtractOne(oldProcBlock);
                    toGetActual->unite(reduced);
                    reduced->compactify();
                    for (int j = 0; j < reduced->getCount(); j++)
                        sendSizes[p] += reduced->getPiece(j)->blockSize(rank) * typeSize;
                    sendBuffers[p] = new char[sendSizes[p]];
                    char *ptr = sendBuffers[p];
                    for (int j = 0; j < reduced->getCount(); j++) {
                        const Interval *curBlock = reduced->getPiece(j);
                        deferredCopyingTo.push_back(new DvmhBuffer(rank, typeSize, 0, curBlock, ptr));
                        ptr += curBlock->blockSize(rank) * typeSize;
                    }
                    delete reduced;
                }
                if (p != ourProc && oldProcHasLocal && sends[oldProc] && oldProcBlock->blockIntersect(rank, newLocalPart, block)) {
                    DvmhPieces *reduced = new DvmhPieces(rank);
                    reduced->appendOne(block);
                    if (hasLocalFlag)
                        reduced->subtractOne(localPart);
                    toSetActual->unite(reduced);
                    reduced->compactify();
                    for (int j = 0; j < reduced->getCount(); j++)
                        recvSizes[p] += reduced->getPiece(j)->blockSize(rank) * typeSize;
                    recvBuffers[p] = new char[recvSizes[p]];
                    char *ptr = recvBuffers[p];
                    for (int j = 0; j < reduced->getCount(); j++) {
                        const Interval *curBlock = reduced->getPiece(j);
                        deferredCopyingFrom.push_back(new DvmhBuffer(rank, typeSize, 0, curBlock, ptr));
                        ptr += curBlock->blockSize(rank) * typeSize;
                    }
                    delete reduced;
                }
            }
            delete[] sends;
            if (hasLocalFlag) {
                if (!representatives[0])
                    createNewRepr(0, localPlusShadow);
                assert(representatives[0]);
                {
                    PushCurrentPurpose purpose(DvmhCopyingPurpose::dcpRedistribute);
                    getActualBase(0, toGetActual, localPart, true);
                }
                toGetActual->clear();
                for (int i = 0; i < (int)deferredCopyingTo.size(); i++) {
                    representatives[0]->copyTo(deferredCopyingTo[i]);
                    delete deferredCopyingTo[i];
                }
                deferredCopyingTo.clear();
            }
            checkInternal(toGetActual->isEmpty() && deferredCopyingTo.empty());
            delete toGetActual;
            commMPS->alltoallv2(sendSizes, sendBuffers, recvSizes, recvBuffers);
            for (int p = 0; p < procCount; p++)
                delete[] sendBuffers[p];
            delete[] sendBuffers;
            delete[] sendSizes;
            if (newHasLocal) {
                if (hasLocalFlag) {
                    if (!localPlusShadow->blockEquals(rank, newLocalPlusShadow) || !localPart->blockEquals(rank, newLocalPart)) {
                        if (localPart->blockIntersect(rank, newLocalPart, block)) {
                            if (!representatives[0])
                                createNewRepr(0, localPlusShadow);
                            {
                                PushCurrentPurpose purpose(DvmhCopyingPurpose::dcpRedistribute);
                                getActualBaseOne(0, block, localPart, true);
                            }
                            deferredCopyingFrom.push_back(representatives[0]->dumpPiece(block));
                            toSetActual->uniteOne(block);
                        }
                        for (int i = 0; i < devicesCount; i++)
                            deleteRepr(i);
                    }
                }
                if (!representatives[0]) {
                    DvmhPieces *p1 = new DvmhPieces(rank);
                    p1->appendOne(newLocalPart);
                    DvmhPieces *p2 = toSetActual->subtract(p1);
                    checkInternal2(p2->isEmpty(), "Too much to set actual");
                    delete p2;
                    p2 = p1->subtract(toSetActual);
                    checkInternal2(p2->isEmpty(), "Not enough to set actual");
                    delete p2;
                    delete p1;
                }
                if (!representatives[0])
                    createNewRepr(0, newLocalPlusShadow);
                for (int i = 0; i < (int)deferredCopyingFrom.size(); i++) {
                    deferredCopyingFrom[i]->copyTo(representatives[0]);
                    delete deferredCopyingFrom[i];
                }
                deferredCopyingFrom.clear();
                clearActual(toSetActual, 0);
                representatives[0]->getActualState()->unite(toSetActual);
                toSetActual->clear();
            } else {
                for (int i = 0; i < devicesCount; i++)
                    deleteRepr(i);
            }
            checkInternal(toSetActual->isEmpty() && deferredCopyingFrom.empty());
            delete toSetActual;
            for (int p = 0; p < procCount; p++)
                delete[] recvBuffers[p];
            delete[] recvBuffers;
            delete[] recvSizes;
        } else {
            checkInternal(!hasLocalFlag && !newHasLocal);
        }
    }
    DvmhDistribSpace *dspace = alignRule->getDspace();
    delete alignRule;
    if (dspace != newAlignRule->getDspace()) {
        dspace->removeAlignedData(this);
        if (ownDspace) {
            checkError2(dspace->getRefCount() == 0, "Realigning distributed array with existing descendants is prohibited");
            delete dspace;
        }
    }
    alignRule = newAlignRule;
    hasLocalFlag = newHasLocal;
    safeMemcpy(localPart, newLocalPart, rank);
    safeMemcpy(localPlusShadow, newLocalPlusShadow, rank);
    safeMemcpy(shdWidths, newShdWidths, rank);
    initActualShadow();
}

void DvmhData::changeShadowWidth(int axis, ShdWidth newWidth) {
    assert(axis >= 1 && axis <= rank);
    ShdWidth oldWidth = shdWidths[axis - 1];
    DvmType newShadowOrder = 1;
#ifdef NON_CONST_AUTOS
    Interval shdRect[rank];
#else
    Interval shdRect[MAX_ARRAY_RANK];
#endif
    for (int i = 0; i < rank; i++) {
        shdRect[i][0] = -shdWidths[i][0];
        shdRect[i][1] = shdWidths[i][1];
    }
    if (newWidth[0] > oldWidth[0]) {
        shdRect[axis - 1][0] = -newWidth[0];
        shdRect[axis - 1][1] = -oldWidth[0] - 1;
        shadowProfile->uniteOne(shdRect, newShadowOrder);
    } else if (newWidth[0] < oldWidth[0]) {
        shdRect[axis - 1][0] = -oldWidth[0];
        shdRect[axis - 1][1] = -newWidth[0] - 1;
        shadowProfile->subtractOne(shdRect);
    }
    if (newWidth[1] > oldWidth[1]) {
        shdRect[axis - 1][0] = oldWidth[1] + 1;
        shdRect[axis - 1][1] = newWidth[1];
        shadowProfile->uniteOne(shdRect, newShadowOrder);
    } else if (newWidth[1] < oldWidth[1]) {
        shdRect[axis - 1][0] = newWidth[1] + 1;
        shdRect[axis - 1][1] = oldWidth[1];
        shadowProfile->subtractOne(shdRect);
    }
    shdWidths[axis - 1] = newWidth;
    recalcLocalPlusShadow();
    changeReprSize(0, localPlusShadow);
}

void DvmhData::changeReprSize(int dev, const Interval havePortion[]) {
    DvmhRepresentative *oldRepr = representatives[dev];
    if (oldRepr) {
        DvmhRepresentative *newRepr = new DvmhRepresentative(this, dev, havePortion);
        oldRepr->copyTo(newRepr);
        newRepr->getActualState()->clear();
        newRepr->getActualState()->appendOne(havePortion);
        newRepr->getActualState()->intersectInplace(oldRepr->getActualState());
        deleteRepr(dev);
        representatives[dev] = newRepr;
        if (dev == 0)
            updateHeader();
    }
}

void DvmhData::updateHeader() {
    if (header) {
        if (representatives[0]) {
            void *base = freeBase ? representatives[0]->getDeviceAddr() : (void *)header[rank + 2];
            representatives[0]->fillHeader(base, header);
        } else {
            void *base = freeBase ? 0 : (void *)header[rank + 2];
            fillHeader(rank, typeSize, base, base, 0, 0, header);
        }
        header[0] = (DvmType)this;
    }
}

template <class T>
static void traversePiece(int rank, void *addr, UDvmType typeSize, const Interval havePortion[], const Interval piece[], T &f) {
#ifdef NON_CONST_AUTOS
    DvmType currentIndex[rank + 1];
    UDvmType partialSize[rank + 1];
    Interval currentPiece[rank];
#else
    DvmType currentIndex[MAX_ARRAY_RANK + 1];
    UDvmType partialSize[MAX_ARRAY_RANK + 1];
    Interval currentPiece[MAX_ARRAY_RANK];
#endif
    int stepRank = rank > 0 ? 1 : 0;
    UDvmType stepSize = 1;
    for (int i = rank - 2; i >= 0; i--) {
        if (piece[i + 1] != havePortion[i + 1])
            break;
        stepRank++;
        stepSize *= piece[i + 1].size();
    }
    if (rank > 0)
        stepSize *= piece[rank - stepRank].size();
    currentIndex[0] = 0;
    partialSize[0] = typeSize;
    for (int i = 0; i < rank; i++) {
        currentIndex[1 + i] = piece[i][0];
        partialSize[1 + i] = partialSize[1 + i - 1] * havePortion[rank - 1 - i].size();
    }
    char *ptr = (char *)addr;
    for (int i = 0; i < rank; i++)
        ptr += partialSize[rank - 1 - i] * (piece[i][0] - havePortion[i][0]);
    for (int i = 0; i < rank; i++) {
        currentPiece[i][0] = piece[i][0];
        if (i < rank - stepRank)
            currentPiece[i][1] = piece[i][0];
        else
            currentPiece[i][1] = piece[i][1];
    }
    while (currentIndex[0] == 0) {
        f(ptr, currentPiece, stepSize);
        if (stepRank > 0) {
            int i = rank - stepRank;
            ptr -= partialSize[rank - 1 - i] * (currentIndex[1 + i] - piece[i][0]);
            currentIndex[1 + i] = piece[i][0];
        }
        int i = rank - stepRank;
        do {
            i--;
            currentIndex[1 + i]++;
            if (i >= 0) {
                ptr += partialSize[rank - 1 - i];
                if (currentIndex[1 + i] > piece[i][1]) {
                    ptr -= partialSize[rank - 1 - i] * (currentIndex[1 + i] - piece[i][0]);
                    currentIndex[1 + i] = piece[i][0];
                }
                currentPiece[i][0] = currentIndex[1 + i];
                currentPiece[i][1] = currentIndex[1 + i];
            }
        } while (i >= 0 && currentIndex[1 + i] == piece[i][0]);
    }
}

template <typename T>
class Unlocalizer {
public:
    explicit Unlocalizer(const IndirectAxisDistribRule *axisRule, const Interval &spaceDim, DvmType summandLocal): spaceDim(spaceDim) {
        local2ToGlobal = axisRule->getLocal2ToGlobal(&totalOffset);
        totalOffset -= summandLocal;
    }
    void operator() (void *ptr, const Interval piece[], UDvmType pieceSize) {
        T *myPtr = (T *)ptr;
        for (UDvmType eli = 0; eli < pieceSize; eli++) {
            myPtr[eli] = convertElement(myPtr[eli]);
        }
    }
protected:
    T convertElement(T oldVal) {
        if (spaceDim.contains(oldVal))
            return local2ToGlobal[(DvmType)oldVal - totalOffset];
        else
            return oldVal;
    }
protected:
    Interval spaceDim;
    DvmType *local2ToGlobal;
    DvmType totalOffset;
};

void DvmhData::unlocalizeValues() {
    if (localizationInfo.target.isValid()) {
        assert(localizationInfo.target.data->isDistributed());
        int dspaceAxis = localizationInfo.target.data->getAlignRule()->getDspaceAxis(localizationInfo.target.axis);
        assert(dspaceAxis > 0);
        const IndirectAxisDistribRule *axisRule = localizationInfo.target.data->getAlignRule()->getDspace()->getAxisDistribRule(dspaceAxis)->asIndirect();
        assert(axisRule);
        DvmhPieces *totalActualState = new DvmhPieces(rank);
        for (int i = 0; i < devicesCount; i++) {
            if (representatives[i])
                totalActualState->unite(representatives[i]->getActualState());
        }
        if (representatives[0]) {
            DvmhRepresentative *repr = representatives[0];
            getActualBase(0, totalActualState, localPart, true);
            for (int i = 1; i < devicesCount; i++) {
                if (representatives[i]) {
                    representatives[i]->getActualState()->clear();
                    representatives[i]->setCleanTransformState();
                }
            }
            repr->doTransform(0, 0);
            repr->getActualState()->compactify();
            Interval spaceDim = localizationInfo.target.data->getAxisSpace(localizationInfo.target.axis);
            DvmType summandLocal = localizationInfo.target.data->getAlignRule()->getAxisRule(dspaceAxis)->summandLocal;
            for (int i = 0; i < repr->getActualState()->getCount(); i++) {
                const Interval *piece = repr->getActualState()->getPiece(i);
                if (dataType == dtInt) {
                    Unlocalizer<int> unlocalizer(axisRule, spaceDim, summandLocal);
                    traversePiece(rank, repr->getDeviceAddr(), repr->getTypeSize(), repr->getHavePortion(), piece, unlocalizer);
                } else if (dataType == dtLong) {
                    Unlocalizer<long> unlocalizer(axisRule, spaceDim, summandLocal);
                    traversePiece(rank, repr->getDeviceAddr(), repr->getTypeSize(), repr->getHavePortion(), piece, unlocalizer);
                } else if (dataType == dtLongLong) {
                    Unlocalizer<long long> unlocalizer(axisRule, spaceDim, summandLocal);
                    traversePiece(rank, repr->getDeviceAddr(), repr->getTypeSize(), repr->getHavePortion(), piece, unlocalizer);
                } else {
                    assert(false);
                }
            }
        } else {
            checkInternal2(totalActualState->isEmpty(), "Host representative needed to hold actual values");
        }
        std::vector<ReferenceDesc> &refVec = localizationInfo.target.data->localizationInfo.references;
        for (int i = 0; i < (int)refVec.size(); i++) {
            if (refVec[i].data == this) {
                assert(refVec[i].axis = localizationInfo.target.axis);
                std::swap(refVec[i], refVec.back());
                refVec.pop_back();
                break;
            }
        }
        localizationInfo.target.clear();
    }
}

template <typename T>
class Localizer {
public:
    explicit Localizer(const IndirectAxisDistribRule *axisRule, const Interval &spaceDim, const Interval &localPlusShadow, const DvmhAxisAlignRule *alRule):
            axisRule(axisRule), spaceDim(spaceDim), localPlusShadow(localPlusShadow), alRule(alRule) {}
    void operator() (void *ptr, const Interval piece[], UDvmType pieceSize) {
        T *myPtr = (T *)ptr;
        for (UDvmType eli = 0; eli < pieceSize; eli++) {
            myPtr[eli] = convertElement(myPtr[eli]);
        }
    }
protected:
    T convertElement(T oldVal) {
        if (spaceDim.contains(oldVal)) {
            bool isLocal = false;
            bool isShadow = false;
            DvmType localIdx = axisRule->globalToLocal2((DvmType)oldVal * alRule->multiplier + alRule->summand, &isLocal, &isShadow);
            checkError3(isLocal || isShadow, "Can not localize element with value " DTFMT, (DvmType)oldVal);
            localIdx -= alRule->summandLocal;
            checkError3(localPlusShadow.contains(localIdx), "Can not localize element with value " DTFMT, (DvmType)oldVal);
            return localIdx;
        } else {
            return oldVal;
        }
    }
protected:
    const IndirectAxisDistribRule *axisRule;
    Interval spaceDim;
    Interval localPlusShadow;
    const DvmhAxisAlignRule *alRule;
};

void DvmhData::localizeValues(ReferenceDesc target) {
    assert(!localizationInfo.target.isValid());
    DvmhData *targetData = target.data;
    int targetAxis = target.axis;
    checkInternal(targetData->isDistributed());
    checkInternal(targetAxis >= 1 && targetAxis <= targetData->getRank());
    if (!targetData->isDistributed())
        return;
    int dspaceAxis = targetData->getAlignRule()->getDspaceAxis(targetAxis);
    if (dspaceAxis <= 0)
        return;
    IndirectAxisDistribRule *axisRule = targetData->getAlignRule()->getDspace()->getAxisDistribRule(dspaceAxis)->asIndirect();
    if (!axisRule)
        return;
    DvmhPieces *totalActualState = new DvmhPieces(rank);
    for (int i = 0; i < devicesCount; i++) {
        if (representatives[i])
            totalActualState->unite(representatives[i]->getActualState());
    }
    if (representatives[0]) {
        DvmhRepresentative *repr = representatives[0];
        getActualBase(0, totalActualState, localPart, true);
        for (int i = 1; i < devicesCount; i++) {
            if (representatives[i]) {
                representatives[i]->getActualState()->clear();
                representatives[i]->setCleanTransformState();
            }
        }
        repr->doTransform(0, 0);
        repr->getActualState()->compactify();
        Interval spaceDim = targetData->getAxisSpace(targetAxis);
        Interval lpsDim = targetData->getLocalPlusShadow()[targetAxis - 1];
        const DvmhAxisAlignRule *alRule = targetData->getAlignRule()->getAxisRule(dspaceAxis);
        for (int i = 0; i < repr->getActualState()->getCount(); i++) {
            const Interval *piece = repr->getActualState()->getPiece(i);
            if (dataType == dtInt) {
                Localizer<int> localizer(axisRule, spaceDim, lpsDim, alRule);
                traversePiece(rank, repr->getDeviceAddr(), repr->getTypeSize(), repr->getHavePortion(), piece, localizer);
            } else if (dataType == dtLong) {
                Localizer<long> localizer(axisRule, spaceDim, lpsDim, alRule);
                traversePiece(rank, repr->getDeviceAddr(), repr->getTypeSize(), repr->getHavePortion(), piece, localizer);
            } else if (dataType == dtLongLong) {
                Localizer<long long> localizer(axisRule, spaceDim, lpsDim, alRule);
                traversePiece(rank, repr->getDeviceAddr(), repr->getTypeSize(), repr->getHavePortion(), piece, localizer);
            } else {
                assert(false);
            }
        }
    } else {
        checkInternal2(totalActualState->isEmpty(), "Host representative needed to hold actual values");
    }
    localizationInfo.target = target;
    targetData->localizationInfo.references.push_back(ReferenceDesc(this, targetAxis));
}

// DvmhShadowData

DvmhShadowData::DvmhShadowData(const DvmhShadowData &sdata): data(sdata.data), shdWidths(0), cornerFlag(sdata.cornerFlag) {
    if (sdata.shdWidths) {
        shdWidths = new ShdWidth[data->getRank()];
        safeMemcpy(shdWidths, sdata.shdWidths, data->getRank());
    }
}

DvmhShadowData &DvmhShadowData::operator=(const DvmhShadowData &sdata) {
    data = sdata.data;
    delete[] shdWidths;
    shdWidths = 0;
    if (sdata.shdWidths) {
        shdWidths = new ShdWidth[data->getRank()];
        safeMemcpy(shdWidths, sdata.shdWidths, data->getRank());
    }
    cornerFlag = sdata.cornerFlag;
    return *this;
}

bool DvmhShadowData::empty() const {
    assert(data && shdWidths);
    for (int i = 0; i <  data->getRank(); i++)
        if (shdWidths[i][0] > 0 || shdWidths[i][1] > 0)
            return false;
    return true;
}

// DvmhShadow

static int calcOneOffset(int i, const int stepCount[], bool noZero) {
    if (i < std::abs(stepCount[0])) {
        if (stepCount[0] >= 0)
            i -= stepCount[0];
        else
            i = -stepCount[0] - i;
    } else {
        i -= std::abs(stepCount[0]);
        if (noZero)
            i++;
        if (stepCount[1] < 0)
            i = -i;
    }
    return i;
}

static void doCoordShift(int i, int rank, bool cornerFlag, const int stepCount[], const int beginCoords[], int resCoords[]) {
    if (cornerFlag) {
        int restProd = 1;
        for (int j = rank - 1; j >= 0; j--){
            int offs = calcOneOffset((i / restProd) % (std::abs(stepCount[2 * j + 0]) + std::abs(stepCount[2 * j + 1]) + 1), stepCount + 2 * j, false);
            resCoords[j] = beginCoords[j] + offs;
            restProd *= std::abs(stepCount[2 * j + 0]) + std::abs(stepCount[2 * j + 1]) + 1;
        }
    } else {
        int k;
        for (k = 0; k < rank; k++) {
            if (i < std::abs(stepCount[2 * k + 0]) + std::abs(stepCount[2 * k + 1]))
                break;
            else
                i -= std::abs(stepCount[2 * k + 0]) + std::abs(stepCount[2 * k + 1]);
        }

        for (int j = 0; j < rank; j++) {
            int offs = 0;
            if (j == k)
                offs = calcOneOffset(i, stepCount + 2 * k, true);
            resCoords[j] = beginCoords[j] + offs;
        }
    }
}

static void doInterprocessShadowRenew(DvmhData *data, bool cornerFlag, const ShdWidth shdWidths[]) {
    checkInternal(data->isDistributed());
    const DvmhAlignRule *alRule = data->getAlignRule();
    const DvmhDistribRule *disRule = alRule->getDistribRule();
    const MultiprocessorSystem *dataMPS = disRule->getMPS();
    if (dataMPS->getCommRank() >= 0 && data->hasLocal()) {
        assert(data->getRepr(0));
        int dataRank = data->getRank();
        int dspaceRank = alRule->getDspace()->getRank();
        int mpsRank = std::max(dataMPS->getRank(), disRule->getMpsAxesUsed());
        int commRank = dataMPS->getCommRank();
        for (int i = 0; i < dataRank; i++) {
            if (alRule->isIndirect(i + 1))
                checkInternal(shdWidths[i].empty() || data->getLocalPart()[i] == data->getSpace()[i]);
        }
#ifdef NON_CONST_AUTOS
        int recvStepCount[2 * mpsRank], sendStepCount[2 * mpsRank];
#else
        int recvStepCount[2 * MAX_MPS_RANK], sendStepCount[2 * MAX_MPS_RANK], myCoords[MAX_MPS_RANK], coords[MAX_MPS_RANK];
#endif
        for (int i = 0; i < 2 * mpsRank; i++)
            recvStepCount[i] = sendStepCount[i] = 0;
        int sendNeighbNum = cornerFlag ? 1 : 0;
        int recvNeighbNum = cornerFlag ? 1 : 0;
        for (int i = 0; i < dataRank; i++) {
            int dspaceAxis = alRule->getDspaceAxis(i + 1);
            if (dspaceAxis > 0 && disRule->getAxisRule(dspaceAxis)->isDistributed()) {
                const DvmhAxisDistribRule *axDisRule = disRule->getAxisRule(dspaceAxis);
                const DvmhAxisAlignRule *axAlRule = alRule->getAxisRule(dspaceAxis);
                int mpsAxis = axDisRule->getMPSAxis();
                int myProc = dataMPS->getAxis(mpsAxis).ourProc;
                ShdWidth shd = shdWidths[i];
                Interval lp = data->getLocalPart()[i];
                Interval lps = data->getLocalPlusShadow()[i];
                Interval sp = data->getSpace()[i];
                if (shd[0] > 0 && lp[0] > sp[0]) {
                    // Need to receive something to the left shadow edge
                    DvmType dataIndex = std::max(lp[0] - shd[0], lps[0]);
                    DvmType dspaceIndex = axAlRule->multiplier * dataIndex + axAlRule->summand;
                    int proc = axDisRule->getProcIndex(dspaceIndex);
                    recvStepCount[2 * (mpsAxis - 1) + 0] = myProc - proc;
                } else {
                    recvStepCount[2 * (mpsAxis - 1) + 0] = 0;
                }
                if (shd[1] > 0 && lp[1] < sp[1]) {
                    // Need to receive something to the right shadow edge
                    DvmType dataIndex = std::min(lp[1] + shd[1], lps[1]);
                    DvmType dspaceIndex = axAlRule->multiplier * dataIndex + axAlRule->summand;
                    int proc = axDisRule->getProcIndex(dspaceIndex);
                    recvStepCount[2 * (mpsAxis - 1) + 1] = proc - myProc;
                } else {
                    recvStepCount[2 * (mpsAxis - 1) + 1] = 0;
                }
                if (shd[1] > 0 && lp[0] > sp[0]) {
                    // Need to send something to the left
                    DvmType dataIndex = std::max(lp[0] - shd[1], sp[0]);
                    DvmType dspaceIndex = axAlRule->multiplier * dataIndex + axAlRule->summand;
                    int proc = axDisRule->getProcIndex(dspaceIndex);
                    sendStepCount[2 * (mpsAxis - 1) + 0] = myProc - proc;
                } else {
                    sendStepCount[2 * (mpsAxis - 1) + 0] = 0;
                }
                if (shd[0] > 0 && lp[1] < sp[1]) {
                    // Need to send something to the right
                    DvmType dataIndex = std::min(lp[1] + shd[0], sp[1]);
                    DvmType dspaceIndex = axAlRule->multiplier * dataIndex + axAlRule->summand;
                    int proc = axDisRule->getProcIndex(dspaceIndex);
                    sendStepCount[2 * (mpsAxis - 1) + 1] = proc - myProc;
                } else {
                    sendStepCount[2 * (mpsAxis - 1) + 1] = 0;
                }
                if (cornerFlag) {
                    recvNeighbNum *= std::abs(recvStepCount[2 * (mpsAxis - 1) + 0]) + std::abs(recvStepCount[2 * (mpsAxis - 1) + 1]) + 1;
                    sendNeighbNum *= std::abs(sendStepCount[2 * (mpsAxis - 1) + 0]) + std::abs(sendStepCount[2 * (mpsAxis - 1) + 1]) + 1;
                } else {
                    recvNeighbNum += std::abs(recvStepCount[2 * (mpsAxis - 1) + 0]) + std::abs(recvStepCount[2 * (mpsAxis - 1) + 1]);
                    sendNeighbNum += std::abs(sendStepCount[2 * (mpsAxis - 1) + 0]) + std::abs(sendStepCount[2 * (mpsAxis - 1) + 1]);
                }
            }
        }
        // Actually, in case of cornerFlag recvNeighbNum and sendNeighbNum are +1 of the real number
        int *sendProcs = new int[sendNeighbNum];
        UDvmType *sendSizes = new UDvmType[sendNeighbNum];
        char **sendBuffers = new char *[sendNeighbNum];
        int *recvProcs = new int[recvNeighbNum];
        UDvmType *recvSizes = new UDvmType[recvNeighbNum];
        char **recvBuffers = new char *[recvNeighbNum];
        for (int i = 0; i < sendNeighbNum; i++) {
            sendProcs[i] = -1;
            sendSizes[i] = 0;
            sendBuffers[i] = 0;
        }
        for (int i = 0; i < recvNeighbNum; i++) {
            recvProcs[i] = -1;
            recvSizes[i] = 0;
            recvBuffers[i] = 0;
        }
        std::vector<DvmhBuffer *> deferredCopyFrom;
        deferredCopyFrom.reserve(recvNeighbNum);
#ifdef NON_CONST_AUTOS
        int myCoords[mpsRank], coords[mpsRank];
        Interval procDspacePart[dspaceRank], procBlock[dataRank], procLocalPlusShadow[dataRank], commonBlock[dataRank];
#else
        int myCoords[MAX_MPS_RANK], coords[MAX_MPS_RANK];
        Interval procDspacePart[MAX_DISTRIB_SPACE_RANK], procBlock[MAX_ARRAY_RANK], procLocalPlusShadow[MAX_ARRAY_RANK], commonBlock[MAX_ARRAY_RANK];
#endif
        dataMPS->fillAxisIndexes(commRank, mpsRank, myCoords);
        for (int i = 0; i < sendNeighbNum; i++) {
            doCoordShift(i, mpsRank, cornerFlag, sendStepCount, myCoords, coords);
            int otherCommRank = dataMPS->getCommRank(mpsRank, coords);
            sendProcs[i] = otherCommRank;
            if (otherCommRank == commRank)
                continue;
            if (disRule->fillLocalPart(otherCommRank, procDspacePart)) {
                procBlock->blockAssign(dataRank, data->getSpace());
                if (alRule->mapOnPart(procDspacePart, procBlock)) {
                    data->extendBlock(procBlock, procLocalPlusShadow);
                    if (data->getLocalPart()->blockIntersect(dataRank, procLocalPlusShadow, commonBlock)) {
                        UDvmType totalSize = data->getTypeSize() * commonBlock->blockSize(dataRank);
                        sendSizes[i] = totalSize;
                        sendBuffers[i] = new char[totalSize];
                        DvmhBuffer buf(dataRank, data->getTypeSize(), 0, commonBlock, sendBuffers[i]);
                        data->getRepr(0)->copyTo(&buf);
                    }
                }
            }
        }
        for (int i = 0; i < recvNeighbNum; i++) {
            doCoordShift(i, mpsRank, cornerFlag, recvStepCount, myCoords, coords);
            int otherCommRank = dataMPS->getCommRank(mpsRank, coords);
            recvProcs[i] = otherCommRank;
            if (otherCommRank == commRank)
                continue;
            if (disRule->fillLocalPart(otherCommRank, procDspacePart)) {
                procBlock->blockAssign(dataRank, data->getSpace());
                if (alRule->mapOnPart(procDspacePart, procBlock)) {
                    if (procBlock->blockIntersect(dataRank, data->getLocalPlusShadow(), commonBlock)) {
                        UDvmType totalSize = data->getTypeSize() * commonBlock->blockSize(dataRank);
                        recvSizes[i] = totalSize;
                        recvBuffers[i] = new char[totalSize];
                        deferredCopyFrom.push_back(new DvmhBuffer(dataRank, data->getTypeSize(), 0, commonBlock, recvBuffers[i]));
                    }
                }
            }
        }

        dataMPS->alltoallv3(sendNeighbNum, sendProcs, sendSizes, sendBuffers, recvNeighbNum, recvProcs, recvSizes, recvBuffers);

        delete[] sendProcs;
        delete[] sendSizes;
        for (int i = 0; i < sendNeighbNum; i++)
            delete[] sendBuffers[i];
        delete[] sendBuffers;

        for (int i = 0; i < (int)deferredCopyFrom.size(); i++) {
            deferredCopyFrom[i]->copyTo(data->getRepr(0));
            delete deferredCopyFrom[i];
        }
        deferredCopyFrom.clear();

        delete[] recvProcs;
        delete[] recvSizes;
        for (int i = 0; i < recvNeighbNum; i++)
            delete[] recvBuffers[i];
        delete[] recvBuffers;
    }
}

void DvmhShadow::renew(DvmhRegion *currentRegion, bool doComm) const {
    PushCurrentPurpose purpose(DvmhCopyingPurpose::dcpShadow);
    for (int i = 0; i < (int)datas.size(); i++) {
        const DvmhShadowData &sdata = datas[i];
        DvmhData *data = sdata.data;
        if (data->hasLocal()) {
            data->getActualEdges(data->getLocalPart(), sdata.shdWidths, (currentRegion ? currentRegion->canAddToActual(data, data->getLocalPart()) : true));
            data->setActualShadow(0, data->getLocalPart(), sdata.cornerFlag, sdata.shdWidths);
        }
        if (doComm)
            doInterprocessShadowRenew(sdata.data, sdata.cornerFlag, sdata.shdWidths);
        data->updateShadowProfile(sdata.cornerFlag, sdata.shdWidths);
        if (data->hasLocal() && currentRegion)
            currentRegion->markToRenew(data);
    }
}

}
