#pragma once

#include "dvmh_types.h"
#include "dvmh_device.h"

namespace libdvmh {

bool dvmhCudaCanDirectCopy(int rank, UDvmType typeSize, CommonDevice *dev1, const DvmType header1[], CommonDevice *dev2, const DvmType header2[]);

bool dvmhCudaDirectCopy(int rank, UDvmType typeSize, CommonDevice *dev1, const DvmType header1[], CommonDevice *dev2, const DvmType header2[],
        const Interval cutting[]);

}
