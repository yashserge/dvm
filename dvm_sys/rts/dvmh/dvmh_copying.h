#pragma once

#include "dvmh_types.h"
#include "dvmh_data.h"

namespace libdvmh {

void dvmhCopy(const DvmhBuffer *from, DvmhBuffer *to, const Interval cutting[]);

}
