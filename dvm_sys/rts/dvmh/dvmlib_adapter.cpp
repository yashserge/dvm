#ifndef NO_DVM

#include "dvmlib_adapter.h"

#include <cassert>

#include "dvmh_log.h"
#include "dvmh_async.h"

namespace libdvmh {

static std::map<DvmhObject *, SysHandle *> objMap;
static std::map<SysHandle *, DvmhObject *> reverseObjMap;
static DvmhSpinLock mapsLock;

SysHandle *getDvm(DvmhObject *obj) {
    SpinLockGuard guard(mapsLock);
    std::map<DvmhObject *, SysHandle *>::iterator it = objMap.find(obj);
    if (it != objMap.end())
        return it->second;
    else
        return 0;
}

DvmhObject *getDvmh(SysHandle *handle) {
    SpinLockGuard guard(mapsLock);
    std::map<SysHandle *, DvmhObject *>::iterator it = reverseObjMap.find(handle);
    if (it != reverseObjMap.end())
        return it->second;
    else
        return 0;
}

DvmhObject *getDvmh(UDvmType handle) {
    return getDvmh((SysHandle *)handle);
}

DvmhObject *getDvmh(DvmType handle) {
    return getDvmh((SysHandle *)handle);
}

void tieDvm(DvmhObject *obj, SysHandle *handle) {
    assert(obj);
    assert(handle);
    SpinLockGuard guard(mapsLock);
    objMap[obj] = handle;
    reverseObjMap[handle] = obj;
}

void tieDvm(DvmhObject *obj, UDvmType handle) {
    tieDvm(obj, (SysHandle *)handle);
}

void tieDvm(DvmhObject *obj, DvmType handle) {
    tieDvm(obj, (SysHandle *)handle);
}

void untieDvm(DvmhObject *obj) {
    SpinLockGuard guard(mapsLock);
    std::map<DvmhObject *, SysHandle *>::iterator it = objMap.find(obj);
    if (it != objMap.end()) {
        reverseObjMap.erase(it->second);
        objMap.erase(it);
    }
}

UDvmType getDvmMapSize() {
    SpinLockGuard guard(mapsLock);
    return objMap.size();
}


void *getAddrFromDvm(s_DISARRAY *dvmHead) {
    assert(dvmHead != 0);
    dvmh_log(DONT_LOG, "DVM-array is allocated on %p (" DTFMT ")", dvmHead->ArrBlock.ALoc.Ptr, (DvmType)dvmHead->ArrBlock.ALoc.Ptr);
    return dvmHead->ArrBlock.ALoc.Ptr;
}

void *getAddrFromDvmDesc(DvmType dvmDesc[]) {
    s_DISARRAY *dvmHead = (s_DISARRAY *)((SysHandle *)dvmDesc[0])->pP;
    return getAddrFromDvm(dvmHead);
}

DvmhDistribSpace *createDistribSpaceFromAMView(s_AMVIEW *amView) {
    static MultiprocessorSystem *selfMPS = 0;
    checkInternal(amView);
    int dspaceRank = amView->Space.Rank;
#ifdef NON_CONST_AUTOS
    Interval space[dspaceRank], localPart[dspaceRank];
#else
    Interval space[MAX_DISTRIB_SPACE_RANK], localPart[MAX_DISTRIB_SPACE_RANK];
#endif
    for (int i = 0; i < dspaceRank; i++) {
        space[i][0] = 0;
        space[i][1] = amView->Space.Size[i] - 1;
        localPart[i][0] = amView->Local.Set[i].Lower;
        localPart[i][1] = amView->Local.Set[i].Upper;
        dvmh_log(DEBUG, "dspaceLow[%d] = " DTFMT " dspaceHigh[%d] = " DTFMT " localPart[%d] = " DTFMT ".." DTFMT, i, space[i][0], i, space[i][1], i,
                localPart[i][0], localPart[i][1]);
    }
    // XXX: Simplified, acting like there is one processor
    // XXX: Space is replaced with fake one
    for (int i = 0; i < dspaceRank; i++) {
        space[i] = (amView->HasLocal ? localPart[i] : Interval::createEmpty());
    }
    DvmhDistribSpace *res = new DvmhDistribSpace(dspaceRank, space);
    if (!selfMPS) {
        int sizes[1] = {1};
        selfMPS = new MultiprocessorSystem(MPI_COMM_SELF, 1, sizes);
    }
    DvmhDistribRule *rule = new DvmhDistribRule(dspaceRank, selfMPS);
    int curMpsAxis = 1;
    for (int i = 0; i < dspaceRank; i++) {
        if (amView->DISTMAP[i].Attr == map_BLOCK) {
            rule->setAxisRule(i + 1, DvmhAxisDistribRule::createBlock(selfMPS, curMpsAxis, res->getAxisSpace(i + 1)));
            curMpsAxis++;
        } else
            rule->setAxisRule(i + 1, DvmhAxisDistribRule::createReplicated(selfMPS, res->getAxisSpace(i + 1)));
    }
    res->redistribute(rule);
    return res;
}

static void translateAlign(s_ALIGN curAl, const Interval &curSpace, DvmType starti, DvmhAxisAlignRule *pMyAlign) {
    // XXX: Assumes zero-based DistribSpace.
    dvmh_log(TRACE, "imported params: Attr=%d Axis=%d A=" DTFMT " B=" DTFMT " Bound=" DTFMT " TAxis=%d", curAl.Attr, curAl.Axis, curAl.A, curAl.B, curAl.Bound,
            curAl.TAxis);
    if (curAl.Attr == align_REPLICATE) {
        pMyAlign->setReplicated(1, curSpace);
    } else if (curAl.Attr == align_BOUNDREPL) {
        if (curAl.A > 0)
            pMyAlign->setReplicated(curAl.A, Interval::create(curAl.B, curAl.B + curAl.A * (curAl.Bound - 1)));
        else
            pMyAlign->setReplicated(-curAl.A, Interval::create(curAl.B + curAl.A * (curAl.Bound - 1), curAl.B));
    } else if (curAl.Attr == align_CONSTANT) {
        pMyAlign->setConstant(curAl.B);
    } else {
        pMyAlign->setLinear(curAl.Axis, curAl.A, curAl.B - starti * curAl.A);
    }
    dvmh_log(TRACE, "starti=" DTFMT " mapping Axis=%d Multiplier=" DTFMT " Summand=" DTFMT " ReplicInterval=" DTFMT ".." DTFMT, starti, pMyAlign->axisNumber,
            pMyAlign->multiplier, pMyAlign->summand, pMyAlign->replicInterval[0], pMyAlign->replicInterval[1]);
}

DvmhData *createDataFromDvmArray(s_DISARRAY *dvmHead, DvmhDistribSpace *dspace, DvmhData::TypeType givenTypeType) {
    DvmhData *data;
    checkInternal(dvmHead != NULL);
    DvmType *dvmheader = (DvmType *)dvmHead->HandlePtr->HeaderPtr;
    checkInternal(dvmheader != NULL);
    if (!dvmHead->HasLocal)
        dvmHead->ArrBlock.ALoc.Ptr = 0;
    int rank = dvmHead->Space.Rank;
#ifdef NON_CONST_AUTOS
    Interval space[rank], localPart[rank], hostPortion[rank];
    ShdWidth shdWidths[rank];
#else
    Interval space[MAX_ARRAY_RANK], localPart[MAX_ARRAY_RANK], hostPortion[MAX_ARRAY_RANK];
    ShdWidth shdWidths[MAX_ARRAY_RANK];
#endif
    for (int i = 0; i < rank; i++) {
        DvmType starti = dvmheader[rank + (rank - 1 - i) + 2];
        if (dvmHead->RegBufSign && dvmHead->HasLocal) {
            dvmh_log(DEBUG, "Using local part (if any) as space for regular buffer array");
            space[i][0] = dvmHead->Block.Set[i].Lower + starti;
            space[i][1] = dvmHead->Block.Set[i].Upper + starti;
        } else {
            space[i][0] = starti;
            space[i][1] = space[i][0] + dvmHead->Space.Size[i] - 1;
        }

        localPart[i][0] = dvmHead->Block.Set[i].Lower + starti;
        localPart[i][1] = dvmHead->Block.Set[i].Upper + starti;

        shdWidths[i][0] = dvmHead->InitLowShdWidth[i];
        shdWidths[i][1] = dvmHead->InitHighShdWidth[i];

        hostPortion[i][0] = dvmHead->ArrBlock.Block.Set[i].Lower + starti;
        hostPortion[i][1] = dvmHead->ArrBlock.Block.Set[i].Upper + starti;
        dvmh_log(DEBUG, "[%d] space " DTFMT ".." DTFMT "; localPart " DTFMT ".." DTFMT "; shdWidths " DTFMT "," DTFMT "; hostPortion " DTFMT ".." DTFMT "", i,
                space[i][0], space[i][1], localPart[i][0], localPart[i][1], shdWidths[i][0], shdWidths[i][1], hostPortion[i][0], hostPortion[i][1]);
    }
    if (dvmHead->Type >= 0)
        data = new DvmhData((DvmhData::DataType)dvmHead->Type, rank, space, shdWidths);
    else
        data = new DvmhData(dvmHead->TLen, givenTypeType, rank, space, shdWidths);
    DvmhAlignRule *alignRule = 0;
    if (dspace != 0) {
#ifdef NON_CONST_AUTOS
        DvmhAxisAlignRule axisRules[dspace->getRank()];
#else
        DvmhAxisAlignRule axisRules[MAX_DISTRIB_SPACE_RANK];
#endif
        for (int i = 0; i < dspace->getRank(); i++) {
            s_ALIGN curAl = dvmHead->Align[data->getRank() + i];
            DvmType starti = curAl.Axis > 0 ? space[curAl.Axis - 1][0] : 0;
            translateAlign(curAl, dspace->getAxisSpace(i + 1), starti, &axisRules[i]);
        }
        alignRule = new DvmhAlignRule(rank, dspace, axisRules);
    } else
        alignRule = 0;
    if (dvmHead->RegBufSign && !dvmHead->HasLocal)
        assert(!data->hasLocal());
    else
        data->realign(alignRule);
    assert(data->hasLocal() == (dvmHead->HasLocal != 0));
    if (data->hasLocal()) {
        for (int i = 0; i < rank; i++) {
            dvmh_log(TRACE, "data->localPart[%d]=" DTFMT ".." DTFMT, i, data->getAxisLocalPart(i + 1)[0], data->getAxisLocalPart(i + 1)[1]);
            dvmh_log(TRACE, "DVM's localPart[%d]=" DTFMT ".." DTFMT, i, localPart[i][0], localPart[i][1]);
        }
        for (int i = 0; i < rank; i++)
            assert(data->getAxisLocalPart(i + 1)[0] == localPart[i][0] && data->getAxisLocalPart(i + 1)[1] == localPart[i][1]);
    }
    dvmh_log(DEBUG, "hasLocal = %d typeSize = " DTFMT " typeType = %d DVM-type = %d", (int)data->hasLocal(), data->getTypeSize(), (int)data->getTypeType(),
            dvmHead->Type);
    if (data->hasLocal()) {
        data->setHostPortion(getAddrFromDvm(dvmHead), hostPortion);
        data->initActual(0);
    }

    return data;
}

DvmhLoop *createLoopFromDvmLoop(s_PARLOOP *dvmLoop, DvmhRegion *region, DvmhDistribSpace *dspace, DvmhLoopPersistentInfo *persInfo) {
    DvmhLoop *loop;
    int rank = dvmLoop->Rank;
#ifdef NON_CONST_AUTOS
    LoopBounds loopBounds[rank];
#else
    LoopBounds loopBounds[MAX_LOOP_RANK];
#endif
    for (int i = 0; i < rank; i++) {
        loopBounds[i] = LoopBounds::create(Interval::create(dvmLoop->Set[i].Lower, dvmLoop->Set[i].Upper),
                dvmLoop->Set[i].Step * (dvmLoop->Invers[i] ? -1 : 1));
        dvmh_log(TRACE, "Loop [%d]: " DTFMT " " DTFMT " " DTFMT, i, loopBounds[i][0], loopBounds[i][1], loopBounds[i][2]);
    }
    assert(dspace != 0);
#ifdef NON_CONST_AUTOS
    DvmhAxisAlignRule axisRules[dspace->getRank()];
#else
    DvmhAxisAlignRule axisRules[MAX_DISTRIB_SPACE_RANK];
#endif
    for (int i = 0; i < dspace->getRank(); i++) {
        s_ALIGN curAl = dvmLoop->Align[rank + i];
        DvmType starti = curAl.Axis > 0 ? dvmLoop->InitIndex[curAl.Axis - 1] : 0;
        translateAlign(curAl, dspace->getAxisSpace(i + 1), starti, &axisRules[i]);
    }
    loop = new DvmhLoop(region, rank, loopBounds, persInfo);
    loop->setAlignRule(new DvmhAlignRule(rank, dspace, axisRules));
    if (dvmLoop->AddBnd) {
        checkInternal(dvmLoop->TempDArr);
        loop->shadowComputeWidths = new ShdWidth[dspace->getRank()];
        for (int i = 0; i < dspace->getRank(); i++) {
            s_ALIGN curAl = dvmLoop->TempDArr->Align[dvmLoop->TempDArr->Space.Rank + i];
            DvmhAxisAlignRule axisRule;
            translateAlign(curAl, dspace->getAxisSpace(i + 1), 0, &axisRule);
            if (axisRule.axisNumber > 0) {
                ShdWidth w;
                if (dvmLoop->AddBnd == 1) {
                    w[0] = dvmLoop->TempDArr->InitLowShdWidth[axisRule.axisNumber - 1];
                    w[1] = dvmLoop->TempDArr->InitHighShdWidth[axisRule.axisNumber - 1];
                } else {
                    w[0] = dvmLoop->LowShd[axisRule.axisNumber - 1];
                    w[1] = dvmLoop->HighShd[axisRule.axisNumber - 1];
                }
                if (axisRule.multiplier > 0) {
                    loop->shadowComputeWidths[i][0] = w[0] * axisRule.multiplier;
                    loop->shadowComputeWidths[i][1] = w[1] * axisRule.multiplier;
                } else {
                    loop->shadowComputeWidths[i][0] = w[1] * (-axisRule.multiplier);
                    loop->shadowComputeWidths[i][1] = w[0] * (-axisRule.multiplier);
                }
            } else {
                loop->shadowComputeWidths[i][0] = 0;
                loop->shadowComputeWidths[i][1] = 0;
            }
        }
    }
    return loop;
}

bool isDvmArrayRegular(DvmType handle) {
    s_DISARRAY *dvmHead = (s_DISARRAY *)((SysHandle *)handle)->pP;
    assert(dvmHead);
    return dvmHead->MemPtr != NULL;
}

#endif

}
