#pragma once

#include <vector>
#include <cassert>
#include <set>

#pragma GCC visibility push(default)
#ifndef _MPI_STUBS_
#include <mpi.h>
#else
#include <mpi_stubs.h>
#endif
#pragma GCC visibility pop

#include "dvmh_pieces.h"
#include "dvmh_device.h"

namespace libdvmh {

class DvmhData;

class Intervals {
public:
    int getIntervalCount() const { return intervals.size(); }
    const Interval &getInterval(int i) const { return intervals[i]; }
    bool empty() const { return intervals.empty(); }
    bool isInterval() const { return intervals.size() <= 1; }
public:
    Intervals() {}
    // Note, that it is left implicit on purpose
    Intervals(const Interval &inter) { append(inter); }
public:
    void clear() { intervals.clear(); }
    void append(const Interval &inter);
    void unite(const Interval &inter);
    void intersect(const Interval &inter);
    Interval toInterval() const {
        assert(intervals.size() <= 1);
        return empty() ? Interval::createEmpty() : intervals[0];
    }
    DvmhPieces *toPieces() const;
    Interval getBoundInterval() const;
protected:
    HybridVector<Interval, 10> intervals;
};

class DvmhCommunicator;

class MPSAxis {
public:
    // Global
    int procCount;
    int commStep;
    // Local
    int ourProc; // -1 means this processor does not belong to this MPS
    DvmhCommunicator *axisComm;
public:
    double getCoordWeight(int p) const { assert(p >= 0 && p < procCount); return (coordWeights ? coordWeights[p] : 1.0); }
    bool isWeighted() const { return weightedFlag; }
public:
    explicit MPSAxis(int aCommStep = 1, int aOurProc = 0, DvmhCommunicator *aAxisComm = 0): procCount(1), commStep(aCommStep), ourProc(aOurProc),
            axisComm(aAxisComm), coordWeights(0), weightedFlag(false), ownComm(false) {}
protected:
    void setCoordWeights(double wgts[], bool copyFlag = false);
    void freeCoordWeights() {
        delete[] coordWeights;
        coordWeights = 0;
        weightedFlag = false;
    }
    void freeAxisComm();
protected:
    double *coordWeights;
    bool weightedFlag;
    bool ownComm;
private:
    friend class DvmhCommunicator;
    friend class MultiprocessorSystem;
};

class DvmhReduction;

class DvmhCommunicator {
public:
    int getRank() const { return rank; }
    MPI_Comm getComm() const { return comm; }
    int getCommRank() const { return commRank; }
    int getCommSize() const { return commSize; }
    MPSAxis getAxis(int axisNum) const {
        assert(axisNum >= 1);
        return (axisNum <= rank ? axes[axisNum - 1] : MPSAxis(commSize, (commRank >= 0 ? 0 : -1), (commRank >= 0 ? selfComm : 0)));
    }
public:
    explicit DvmhCommunicator(MPI_Comm aComm, int aRank, const int sizes[], bool fortranOrder = false);
    explicit DvmhCommunicator() { initDefault(); }
    DvmhCommunicator(const DvmhCommunicator &other) {
        initDefault();
        *this = other;
    }
    DvmhCommunicator &operator=(const DvmhCommunicator &other);
public:
    int getCommRank(int aRank, const int indexes[]) const;
    int getCommRankAxisOffset(int axisNum, int offs, bool periodic = false) const;
    int getAxisIndex(int aCommRank, int axisNum) const;
    void fillAxisIndexes(int aCommRank, int aRank, int indexes[]) const;

    UDvmType bcast(int root, void *buf, UDvmType size) const;
    template <typename T>
    UDvmType bcast(int root, T &var) const {
        return bcast(root, &var, sizeof(T));
    }
    UDvmType bcast(int root, char **pBuf, UDvmType *pSize) const;
    UDvmType send(int dest, const void *buf, UDvmType size) const;
    template <typename T>
    UDvmType send(int dest, const T &val) const {
        return send(dest, &val, sizeof(T));
    }
    UDvmType recv(int src, void *buf, UDvmType size) const;
    template <typename T>
    UDvmType recv(int src, T &var) const {
        return recv(src, &var, sizeof(T));
    }
    UDvmType allgather(void *buf, UDvmType elemSize) const;
    template <typename T>
    UDvmType allgather(T array[]) const {
        return allgather(array, sizeof(T));
    }
    template <typename T>
    UDvmType allgather(std::vector<T> &array) const {
        return allgather(&array[0]);
    }
    UDvmType allgatherv(void *buf, const UDvmType sizes[]) const;
    UDvmType alltoall(const void *sendBuffer, UDvmType elemSize, void *recvBuffer) const;
    template <typename T>
    UDvmType alltoall(const T sendArray[], T recvArray[]) const {
        return alltoall(sendArray, sizeof(T), recvArray);
    }
    UDvmType alltoallv1(const UDvmType sendSizes[], char *sendBuffers[], UDvmType recvSizes[], char *recvBuffers[]) const;
    UDvmType alltoallv2(const UDvmType sendSizes[], char *sendBuffers[], const UDvmType recvSizes[], char *recvBuffers[]) const;
    UDvmType alltoallv3(int sendProcCount, const int sendProcs[], const UDvmType sendSizes[], char *sendBuffers[], int recvProcCount, const int recvProcs[],
            const UDvmType recvSizes[], char *recvBuffers[]) const;
    void barrier() const;
    template <typename T>
    void allreduce(T &var, int redFunc) const;
    template <typename T>
    void allreduce(T arr[], int redFunc, UDvmType count) const;
    void allreduce(DvmhReduction *red) const;
public:
    virtual ~DvmhCommunicator() { clear(); }
protected:
    void initDefault();
    void clear();
    void resetNextAsyncTag() const;
    void advanceNextAsyncTag() const;
protected:
    // Global
    int rank;
    MPSAxis *axes; // Array of MPSAxis (length=rank)
    MPI_Comm comm;
    bool ownComm;
    int commSize;
    bool inverseOrder;
    mutable int nextAsyncTag;
    // Local
    int commRank; // -1 means this processor does not belong to this Communicator
protected:
    static DvmhCommunicator *selfComm;
};

class MultiprocessorSystem: public DvmhCommunicator, private Uncopyable {
public:
    int getIOProc() const { return ioProc; }
    bool isIOProc() const { return commRank == ioProc; }
    int getFileCount() const { return myFiles.size(); }
    DvmhFile *getFile(int i) const { return myFiles[i]; }
public:
    explicit MultiprocessorSystem(MPI_Comm aComm, int aRank, const int sizes[], bool fortranOrder = false, int IOProc = 0);
    explicit MultiprocessorSystem();
public:
    int getChildCommRank(const MultiprocessorSystem *child, int aCommRank) const;
    int getParentCommRank(const MultiprocessorSystem *parent, int aCommRank) const;
    int getOtherCommRank(const MultiprocessorSystem *other, int aCommRank) const;
    void setWeights(const double wgts[], int len = 0);
    void attachChildMPS(MultiprocessorSystem *child);
    bool isSubsystemOf(const MultiprocessorSystem *otherMPS) const;
    UDvmType ioBcast(void *buf, UDvmType size) const {
        return bcast(ioProc, buf, size);
    }
    int newFile(DvmhFile *stream);
    void deleteFile(int fn);
    bool isSimilarTo(const MultiprocessorSystem *other) const;
public:
    ~MultiprocessorSystem();
protected:
    void detachChildMPS(MultiprocessorSystem *child);
protected:
    // Global
    MultiprocessorSystem *parentMPS;
    std::set<MultiprocessorSystem *> children;
    int *parentToOur; // Array of commRanks of this MPS (length=parentMPS->commSize)
    int *ourToParent; // Array of commRanks of parent MPS (length=commSize)
    double *procWeights;
    int ioProc;
    HybridVector<DvmhFile *, 10> myFiles;
};

class ReplicatedAxisDistribRule;
class DistributedAxisDistribRule;
class BlockAxisDistribRule;
class IndirectAxisDistribRule;

class DvmhAxisDistribRule: private Uncopyable {
public:
    const MultiprocessorSystem *getMPS() const { return mps; }
    int getMPSAxis() const { assert(mpsAxis >= 1); return mpsAxis; }
    const Interval &getSpaceDim() const { return spaceDim; }
    const Interval &getLocalElems() const { return localElems; }
    DvmType getMaxSubparts() const { assert(maxSubparts >= 0); return maxSubparts; }
    bool isReplicated() const { return distribType == dtReplicated; }
    bool isDistributed() const { return !isReplicated(); }
    bool isBlockDistributed() const { return distribType >= dtBlock && distribType <= dtMultBlock; }
    bool isIndirect() const { return distribType >= dtIndirect && distribType <= dtDerived; }
    DistributedAxisDistribRule *asDistributed() { return (DistributedAxisDistribRule *)(isDistributed() ? this : 0); }
    const DistributedAxisDistribRule *asDistributed() const { return (const DistributedAxisDistribRule *)(isDistributed() ? this : 0); }
    BlockAxisDistribRule *asBlockDistributed() { return (BlockAxisDistribRule *)(isBlockDistributed() ? this : 0); }
    const BlockAxisDistribRule *asBlockDistributed() const { return (const BlockAxisDistribRule *)(isBlockDistributed() ? this : 0); }
    IndirectAxisDistribRule *asIndirect() { return (IndirectAxisDistribRule *)(isIndirect() ? this : 0); }
    const IndirectAxisDistribRule *asIndirect() const { return (const IndirectAxisDistribRule *)(isIndirect() ? this : 0); }
public:
    static ReplicatedAxisDistribRule *createReplicated(MultiprocessorSystem *mps, const Interval &spaceDim);
    static BlockAxisDistribRule *createBlock(MultiprocessorSystem *mps, int mpsAxis, const Interval &spaceDim);
    static BlockAxisDistribRule *createWeightBlock(MultiprocessorSystem *mps, int mpsAxis, const Interval &spaceDim, DvmhData *weights);
    static BlockAxisDistribRule *createGenBlock(MultiprocessorSystem *mps, int mpsAxis, const Interval &spaceDim, DvmhData *givenGbl);
    static BlockAxisDistribRule *createMultBlock(MultiprocessorSystem *mps, int mpsAxis, const Interval &spaceDim, UDvmType multQuant);
    static IndirectAxisDistribRule *createIndirect(MultiprocessorSystem *mps, int mpsAxis, DvmhData *givenMap);
    static IndirectAxisDistribRule *createDerived(MultiprocessorSystem *mps, int mpsAxis, const Interval &spaceDim, DvmType derivedBuf[],
            UDvmType derivedCount);
public:
    virtual int genSubparts(const double distribPoints[], Interval parts[], int count) const = 0;
    virtual int getProcIndex(DvmType spaceIndex) const = 0;
public:
    virtual ~DvmhAxisDistribRule() {}
protected:
    enum DistribType {dtReplicated, dtBlock, dtWgtBlock, dtGenBlock, dtMultBlock, dtIndirect, dtDerived};
    explicit DvmhAxisDistribRule(DistribType dt, MultiprocessorSystem *aMps, int aMpsAxis = -1): distribType(dt), mps(aMps), mpsAxis(aMpsAxis),
            maxSubparts(-1) {
        assert(mps);
        spaceDim = Interval::createEmpty();
        localElems = Interval::createEmpty();
    }
protected:
    // Global
    DistribType distribType;
    MultiprocessorSystem *mps;
    int mpsAxis;
    Interval spaceDim;
    // Local
    Interval localElems;
    DvmType maxSubparts;
};

class ReplicatedAxisDistribRule: public DvmhAxisDistribRule {
public:
    explicit ReplicatedAxisDistribRule(MultiprocessorSystem *aMps, const Interval &aSpaceDim): DvmhAxisDistribRule(dtReplicated, aMps) {
        spaceDim = aSpaceDim;
        localElems = spaceDim;
        maxSubparts = 0;
    }
public:
    virtual int genSubparts(const double distribPoints[], Interval parts[], int count) const {
        assert(count == 0);
        return 0;
    }
    virtual int getProcIndex(DvmType spaceIndex) const {
        assert(false);
        return 0;
    }
};

class DistributedAxisDistribRule: public DvmhAxisDistribRule {
public:
    Interval getLocalElems(int procIndex) const { return Interval::create(sumGenBlock[procIndex], sumGenBlock[procIndex + 1] - 1); }
public:
    explicit DistributedAxisDistribRule(DistribType dt, MultiprocessorSystem *aMps, int aMpsAxis): DvmhAxisDistribRule(dt, aMps, aMpsAxis) {
        sumGenBlock = new DvmType[mps->getAxis(mpsAxis).procCount + 1];
    }
public:
    virtual int getProcIndex(DvmType spaceIndex) const;
public:
    ~DistributedAxisDistribRule() {
        delete[] sumGenBlock;
    }
protected:
    DvmType *sumGenBlock;
};

class BlockAxisDistribRule: public DistributedAxisDistribRule {
public:
    explicit BlockAxisDistribRule(DistribType dt, MultiprocessorSystem *aMps, int aMpsAxis, const Interval &aSpaceDim, UDvmType aMultQuant);
    explicit BlockAxisDistribRule(MultiprocessorSystem *aMps, int aMpsAxis, const Interval &aSpaceDim, DvmhData *weights);
    explicit BlockAxisDistribRule(MultiprocessorSystem *aMps, int aMpsAxis, DvmhData *givenGbl, const Interval &aSpaceDim);
public:
    virtual int genSubparts(const double distribPoints[], Interval parts[], int count) const;
protected:
    void finishInit();
protected:
    // Global
    UDvmType multQuant;
    // Local
    HybridVector<double, 1> wgtBlockPart;
    Interval haveWgtPart; // ZERO-based
};

class ExchangeMap {
public:
    int sendProcCount; // Size of sendProcs
    int *sendProcs; // If NULL, then all from 0 to (sendProcCount - 1). If non-NULL, then contains the ordered set of processor indexes.
    UDvmType *sendStarts; // CSR-like. Size is (sendProcCount + 1).
    DvmType *sendIndices; // All in compact form (local notation)

    int recvProcCount; // Size of recvProcs
    int *recvProcs; // If NULL, then all from 0 to (recvProcCount - 1). If non-NULL, then contains the ordered set of processor indexes.
    UDvmType *recvStarts; // CSR-like. Size is (recvProcCount + 1).
    DvmType *recvIndices; // All in compact form (local notation of 2nd type)

    int *bestOrder; // An order, in which it is the best to perform the exchange. Contains negative and positive numbers. Negative means receiving, positive means sending. Absolute value is the (processorIndex + 1). NULL means not filled. Size is (sendProcCount + recvProcCount).
public:
    void init();
    void clear();
    bool checkConsistency(DvmhCommunicator *axisComm) const;
    void fillBestOrder(DvmhCommunicator *axisComm);
    bool checkBestOrder(DvmhCommunicator *axisComm) const;
protected:
    int getNextPartnerAndDirection(int &position) const;
};

class IndirectShadow: private Uncopyable {
public:
    std::string name;
    ExchangeMap exchangeMap;
public:
    IndirectShadow() {
        exchangeMap.init();
    }
public:
    ~IndirectShadow() {
        exchangeMap.clear();
    }
};

class IndirectShadowBlock {
public:
    const Interval &getBlock() const { return block;}
    bool isIncludedIn(int shadowIndex) const { return (shadowMask & (1UL << shadowIndex)) != 0; }
    void includeIn(int shadowIndex) { shadowMask |= (1UL << shadowIndex); }
public:
    explicit IndirectShadowBlock(const Interval &aBlock): block(aBlock), shadowMask(0) {}
public:
    IndirectShadowBlock extractOnLeft(DvmType amount, int shadowIndex) {
        IndirectShadowBlock res(*this);
        res.block[1] = res.block[0] + amount - 1;
        res.includeIn(shadowIndex);
        block[0] += amount;
        return res;
    }
    IndirectShadowBlock extractOnRight(DvmType amount, int shadowIndex) {
        IndirectShadowBlock res(*this);
        res.block[0] = res.block[1] - amount + 1;
        res.includeIn(shadowIndex);
        block[1] -= amount;
        return res;
    }
protected:
    Interval block; // Local indexes (2nd type)
    unsigned long shadowMask; // Set of shadow edges it is included in
};

struct ShadowElementInfo {
    DvmType globalIndex;
    DvmType local2Index;
    DvmType local1Index;
    int ownerProcessorIndex;
public:
    bool operator<(const ShadowElementInfo &other) const { return globalIndex < other.globalIndex; }
    static bool orderByLocal2(const ShadowElementInfo &a, const ShadowElementInfo &b) { return a.local2Index < b.local2Index; }
public:
    static ShadowElementInfo wrap(DvmType index) {
        ShadowElementInfo res;
        res.globalIndex = index;
        res.local2Index = index;
        res.local1Index = index;
        res.ownerProcessorIndex = index;
        return res;
    }
};

class IndirectAxisDistribRule: public DistributedAxisDistribRule {
public:
    explicit IndirectAxisDistribRule(MultiprocessorSystem *aMps, int aMpsAxis, DvmhData *givenMap);
    explicit IndirectAxisDistribRule(MultiprocessorSystem *aMps, int aMpsAxis, const Interval &aSpaceDim, DvmType derivedBuf[], UDvmType derivedCount);
public:
    bool fillPart(Intervals &res, const Interval &globalSpacePart, DvmType regInterval = 1) const;
    DvmType globalToLocalOwn(DvmType ind, bool *pLocalElem = 0) const;
    DvmType globalToLocal2(DvmType ind, bool *pLocalElem = 0, bool *pShadowElem = 0) const;
    virtual int genSubparts(const double distribPoints[], Interval parts[], int count) const;
    void addShadow(DvmType elemBuf[], UDvmType elemCount, const std::string &name);
    bool fillShadowL2Indexes(Intervals &res, const std::string &name) const;
    DvmType *getLocal2ToGlobal(DvmType *pFirstIndex = 0) const;
public:
    virtual ~IndirectAxisDistribRule();
protected:
    void createGlobalToLocal();
    void fillGenblock(const UDvmType fakeGenBlock[]);
    void createLocal2ToGlobal();
    void finishGlobalToLocal();
    void fillShadowLocal1Indexes();
    void fillExchangeMap(int shadowIndex);
protected:
    // Global
    DvmhData *globalToLocal; // BLOCK-distributed, type DvmType
    // Local
    DvmType *local2ToGlobal; // Local part + shadow edges. Local part is ordered by global index. Shadow edges are not.
    ShdWidth shdWidth;
    ShadowElementInfo *shdElements; // Ordered by global index array of all the shadow elements (regardless of their positions in local indexing)
    std::vector<IndirectShadow *> shadows;
    std::vector<IndirectShadowBlock> shadowBlocks;
};

class DvmhDistribRule: private Uncopyable {
public:
    int getRank() const { return rank; }
    const MultiprocessorSystem *getMPS() const { return MPS; }
    DvmhAxisDistribRule *getAxisRule(int i) { assert(i >= 1 && i <= rank && axes[i - 1]); return axes[i - 1]; }
    const DvmhAxisDistribRule *getAxisRule(int i) const { assert(i >= 1 && i <= rank && axes[i - 1]); return axes[i - 1]; }
    bool isIndirect(int i) const { return getAxisRule(i)->isIndirect(); }
    int getMpsAxesUsed() const { assert(filledFlag); return mpsAxesUsed; }
    bool hasIndirect() const { assert(filledFlag); return hasIndirectFlag; }
    bool hasLocal() const { assert(filledFlag); return hasLocalFlag; }
    const Interval *getLocalPart() const { assert(filledFlag); return localPart; }
public:
    explicit DvmhDistribRule(int aRank, const MultiprocessorSystem *aMPS);
public:
    void setAxisRule(int axis, DvmhAxisDistribRule *rule);
    bool fillLocalPart(int proc, Interval res[]) const;
public:
    ~DvmhDistribRule();
protected:
    // Global
    int rank;
    DvmhAxisDistribRule **axes; //Array of pointers to DvmhAxisDistribRule (length=rank)
    const MultiprocessorSystem *MPS;
    int mpsAxesUsed;
    bool hasIndirectFlag;
    bool filledFlag;
    // Local
    bool hasLocalFlag;
    Interval *localPart;
};

class DvmhDistribSpace: public DvmhObject, private Uncopyable {
public:
    int getRank() const { return rank; }
    const Interval *getSpace() const { return space; }
    const Interval &getAxisSpace(int i) const { assert(i >= 1 && i <= rank); return space[i - 1]; }
    bool isDistributed() const { return distribRule != 0; }
    bool hasLocal() const { assert(isDistributed()); return distribRule->hasLocal(); }
    const Interval *getLocalPart() const { assert(isDistributed()); return distribRule->getLocalPart(); }
    const Interval &getAxisLocalPart(int i) const { assert(isDistributed() && i >= 1 && i <= rank); return getLocalPart()[i - 1]; }
    const DvmhDistribRule *getDistribRule() const { return distribRule; }
    DvmhAxisDistribRule *getAxisDistribRule(int i) { assert(distribRule); return distribRule->getAxisRule(i); }
    const DvmhAxisDistribRule *getAxisDistribRule(int i) const { assert(distribRule); return distribRule->getAxisRule(i); }
    int getRefCount() const { return refCount; }
    const std::set<DvmhData *> &getAlignedDatas() const { return alignedDatas; }
public:
    explicit DvmhDistribSpace(int aRank, const Interval aSpace[]);
public:
    void addRef() { refCount++; }
    int removeRef() { refCount--; assert(refCount >= 0); return refCount; }
    void addAlignedData(DvmhData *data) { assert(isDistributed()); alignedDatas.insert(data); }
    void removeAlignedData(DvmhData *data) { alignedDatas.erase(data); }
    bool isSuitable(const DvmhDistribRule *aDistribRule) const;
    void redistribute(DvmhDistribRule *aDistribRule);
public:
    ~DvmhDistribSpace();
protected:
    // Global
    // Static info
    int rank;
    Interval *space; //Array of Intervals (length=rank)
    // Dynamic info
    DvmhDistribRule *distribRule;
    std::set<DvmhData *> alignedDatas;
    // TODO: remove, maybe
    int refCount; //Counter of mapped arrays, loops and regions
};

class DvmhAxisAlignRule {
public:
    // Global
    int axisNumber; // -1 means replicate. 0 means mapping to constant. >=1 means linear.
    DvmType multiplier; // For replicate and linear
    DvmType summand; // For constant and linear
    Interval replicInterval; // For replicate
    mutable DvmType summandLocal; // For indirectly-distributed axes and linear rules. Is equal to summand if corresponding space sizes do not differ.
public:
    void setReplicated(DvmType aMultiplier, const Interval &aReplicInterval);
    void setConstant(DvmType aSummand);
    void setLinear(int aAxisNumber, DvmType aMultiplier, DvmType aSummand);
    void composite(const DvmhAxisAlignRule rules[]);
};

class DvmhAlignRule {
public:
    DvmhDistribSpace *getDspace() const { return dspace; }
    const DvmhAxisAlignRule *getAxisRule(int i) const { assert(i >= 1 && i <= dspace->getRank()); return &map[i - 1]; }
    int getDspaceAxis(int i) const { assert(i >= 1 && i <= rank); return dspaceAxis[i - 1]; }
    bool isIndirect(int i) const { assert(i >= 1 && i <= rank); return dspaceAxis[i - 1] > 0 && dspace->getAxisDistribRule(dspaceAxis[i - 1])->isIndirect(); }
    bool hasIndirect() const { return hasIndirectFlag; }
    const DvmhDistribRule *getDistribRule() const { return disRule; }
public:
    explicit DvmhAlignRule(int aRank, DvmhDistribSpace *aDspace, const DvmhAxisAlignRule axisRules[]);
    DvmhAlignRule(const DvmhAlignRule &other);
public:
    bool mapOnPart(const Interval dspacePart[], Interval res[], const DvmType intervals[] = 0) const;
    bool mapOnPart(const Intervals dspacePart[], Intervals res[], const Interval spacePart[], const DvmType intervals[] = 0) const;
    void setDistribRule(const DvmhDistribRule *newDisRule);
public:
    ~DvmhAlignRule();
protected:
    // Global
    int rank;
    bool hasIndirectFlag;
    DvmhDistribSpace *dspace;
    const DvmhDistribRule *disRule;
    DvmhAxisAlignRule *map; //Array of structures (length=dspace->rank)
    int *dspaceAxis;
};

struct DvmhDiagonalInfo {
    int x_axis;
    DvmType x_first;
    DvmType x_length;
    int y_axis;
    DvmType y_first;
    DvmType y_length;
    int slashFlag;
};

class DvmhBuffer: private Uncopyable {
public:
    int getDeviceNum() const { return deviceNum; }
    void *getDeviceAddr() const { return deviceAddr; }
    int getRank() const { return rank; }
    UDvmType getTypeSize() const { return typeSize; }
    const Interval *getHavePortion() const { return havePortion; }
    bool isDiagonalized() const { return diagonalizedState != 0; }
    int getDiagonalizedState() const { return diagonalizedState; }
    const int *getAxisPerm() const { return axisPerm; }
    UDvmType getSize() const { assert(!isIncomplete()); return totalSize; }
    bool ownsMemory() const { return ownMemory; }
    bool isIncomplete() const { return rank >= 1 && havePortion[0].empty(); }
public:
    explicit DvmhBuffer(int aRank, UDvmType aTypeSize, int devNum, const Interval aHavePortion[], void *devAddr = 0, bool aOwnMemory = false);
public:
    bool pageLock() const;
    bool isTransformed() const;
    void doTransform(const int newAxisPerm[], int perDiagonalState);
    void setTransformState(const int newAxisPerm[], int perDiagonalState);
    void undoDiagonal();
    void fillHeader(const void *base, DvmType header[], DvmhDiagonalInfo *diagonalInfo = 0, bool zeroDiagonalized = true) const;
    void fillHeader(DvmType header[]) const { fillHeader(deviceAddr, header); }
    void *getNaturalBase() const;
    bool hasElement(DvmType index) const { assert(rank == 1); return hasElement(&index); }
    bool hasElement(const DvmType indexes[]) const;
    void *getElemAddr(DvmType index) const { assert(rank == 1); return getElemAddr(&index); }
    void *getElemAddr(const DvmType indexes[]) const;
    template <typename T>
    T &getElement(DvmType index) const {
        assert(typeSize == sizeof(T));
        return *(T *)getElemAddr(index);
    }
    template <typename T>
    T &getElement(const DvmType indexes[]) const {
        assert(typeSize == sizeof(T));
        return *(T *)getElemAddr(indexes);
    }
    int getMinIndexTypeSize(const Interval block[]) const;
    DvmhBuffer *undiagCutting(const Interval cutting[]) const;
    void replaceUndiagedCutting(DvmhBuffer *part);
    void copyTo(DvmhBuffer *to, const Interval piece[] = 0) const;
    bool canDumpInplace(const Interval piece[]) const;
    DvmhBuffer *dumpPiece(const Interval piece[], bool allowInplace = false) const;
    bool overlaps(const DvmhBuffer *other) const;
public:
    ~DvmhBuffer();
protected:
    bool canonicalizeTransformState(int perm[], int &diag) const;
    bool canonicalizeTransformState() { return canonicalizeTransformState(axisPerm, diagonalizedState); }
    bool isBlockConsecutive(const Interval piece[]) const;
    void applyDiagonal(int newDiagonalState);
    void applyPermDelta(int permDelta[]);
    UDvmType undiagCuttingInternal(void *res, Interval resPortion[], bool backFlag);
protected:
    int deviceNum;
    void *deviceAddr;
    int rank;
    UDvmType typeSize;
    Interval *havePortion; //Array of Intervals (length=rank)
    int *axisPerm; //Array of Indexes of original array (length=rank) (transformed => original)
    int diagonalizedState; //State of per-diagonal transformation of last 2 dimensions (in case of permutation last 2 dimensions is meant last 2 dimensions in current permutated state). 0 means no per-diagonal transformation. 1 means per-diagonal transformation with slashFlag=0. 2 means per-diagonal transformation with slashFlag=1.
    UDvmType totalSize;
    bool ownMemory;
};

class DvmhRepresentative: public DvmhBuffer {
public:
    DvmhPieces *getActualState() const { return actualState; }
    void setCleanTransformState(bool value = true) { cleanTransformState = value; }
public:
    explicit DvmhRepresentative(DvmhData *aData, int devNum, const Interval aHavePortion[], void *devAddr = 0);
public:
    void doTransform(const int newAxisPerm[], int perDiagonalState);
    void undoDiagonal();
public:
    ~DvmhRepresentative();
protected:
    DvmhPieces *actualState;
    bool cleanTransformState;
};

class DvmhRegionPersistentInfo;

class DvmhRegVar {
public:
    DvmhRegVar(): region(0), varId(0) {}
    explicit DvmhRegVar(DvmhRegionPersistentInfo *aRegion, int aVarId): region(aRegion), varId(aVarId) {}
public:
    int compareTo(const DvmhRegVar &v) const {
        return (*this < v ? -1 : (*this == v ? 0 : 1));
    }
    bool operator<(const DvmhRegVar &v) const {
        return (region < v.region) || (region == v.region && varId < v.varId);
    }
    bool operator==(const DvmhRegVar &v) const {
        return region == v.region && varId == v.varId;
    }
protected:
    // Global
    DvmhRegionPersistentInfo *region; // NULL for get/set actual
    int varId;
};

class DvmhDataState {
public:
    explicit DvmhDataState(const DvmhRegVar &aProducer = DvmhRegVar()): producer(aProducer) {}
    explicit DvmhDataState(DvmhRegionPersistentInfo *region, int varId): producer(region, varId) {}
public:
    bool operator<(const DvmhDataState &state) const;
    bool has(const DvmhRegVar &v) const {
        return producer == v || readers.find(v) != readers.end();
    }
    void addReader(const DvmhRegVar &v) {
        readers.insert(v);
    }
protected:
    // Global
    DvmhRegVar producer;
    std::set<DvmhRegVar> readers;
};

struct ReferenceDesc {
    DvmhData *data;
    int axis;
    ReferenceDesc(): data(0), axis(-1) {}
    explicit ReferenceDesc(DvmhData *data, int axis): data(data), axis(axis) {}
    bool isValid() const { return data && axis > 0; }
    void clear() {
        data = 0;
        axis = -1;
    }
};

struct LocalizationInfo {
    ReferenceDesc target;
    std::vector<ReferenceDesc> references;
};

class DvmhData: public DvmhObject {
public:
    enum DataType {dtUnknown = -1, dtChar = 0, dtInt = 1, dtLong = 2, dtFloat = 3, dtDouble = 4, dtFloatComplex = 5, dtDoubleComplex = 6, dtLogical = 7,
            dtLongLong = 8, dtUChar = 9, dtUInt = 10, dtULong = 11, dtULongLong = 12, dtShort = 13, dtUShort = 14, dtPointer = 15, DATA_TYPES};
    enum TypeType {ttUnknown = -1, ttInteger = 0, ttFloating = 1, ttComplex = 2, TYPE_TYPES};
    inline static TypeType getTypeType(DataType rt) {
        switch (rt) {
            case dtUnknown: return ttUnknown;
            case dtChar: return ttInteger;
            case dtUChar: return ttInteger;
            case dtShort: return ttInteger;
            case dtUShort: return ttInteger;
            case dtInt: return ttInteger;
            case dtUInt: return ttInteger;
            case dtLong: return ttInteger;
            case dtULong: return ttInteger;
            case dtLongLong: return ttInteger;
            case dtULongLong: return ttInteger;
            case dtFloat: return ttFloating;
            case dtDouble: return ttFloating;
            case dtFloatComplex: return ttComplex;
            case dtDoubleComplex: return ttComplex;
            case dtLogical: return ttInteger;
            case dtPointer: return ttUnknown;
            default: assert(false);
        }
        return ttUnknown;
    }
    inline static UDvmType getTypeSize(DataType rt) {
        switch (rt) {
            case dtUnknown: return 0;
            case dtChar: return sizeof(char);
            case dtUChar: return sizeof(char);
            case dtShort: return sizeof(short);
            case dtUShort: return sizeof(short);
            case dtInt: return sizeof(int);
            case dtUInt: return sizeof(int);
            case dtLong: return sizeof(long);
            case dtULong: return sizeof(long);
            case dtLongLong: return sizeof(long long);
            case dtULongLong: return sizeof(long long);
            case dtFloat: return sizeof(float);
            case dtDouble: return sizeof(double);
            case dtFloatComplex: return 2 * sizeof(float);
            case dtDoubleComplex: return 2 * sizeof(double);
            case dtLogical: return sizeof(int);
            case dtPointer: return sizeof(void *);
            default: assert(false);
        }
        return 0;
    }
public:
    UDvmType getTypeSize() const { return typeSize; }
    TypeType getTypeType() const { return typeType; }
    DataType getDataType() const { return dataType; }
    int getRank() const { return rank; }
    const Interval *getSpace() const { return space; }
    const Interval &getAxisSpace(int i) const { assert(i >= 1 && i <= rank); return space[i - 1]; }
    bool isIncomplete() const { return rank >= 1 && space[0].empty(); }
    const ShdWidth &getShdWidth(int i) const { assert(i >= 1 && i <= rank); return shdWidths[i - 1]; }
    bool isAligned() const { return alignRule != 0 || hasLocal(); }
    bool isDistributed() const { return alignRule != 0; }
    const DvmhAlignRule *getAlignRule() const { return alignRule; }
    bool hasOwnDistribSpace() const { return ownDspace; }
    DvmhDataState &getCurState() { return curState; }
    bool hasLocal() const { return hasLocalFlag; }
    const Interval *getLocalPart() const { return localPart; }
    const Interval &getAxisLocalPart(int i) const { assert(i >= 1 && i <= rank); return localPart[i - 1]; }
    const Interval *getLocalPlusShadow() const { return localPlusShadow; }
    DvmType *getHeader() const { return header; }
    DvmhRepresentative *getRepr(int dev) { assert(dev >= 0 && dev < devicesCount); return representatives[dev]; }
public:
    explicit DvmhData(UDvmType aTypeSize, TypeType aTypeType, int aRank, const Interval aSpace[], const ShdWidth shadows[] = 0);
    explicit DvmhData(DataType aDataType, int aRank, const Interval aSpace[], const ShdWidth shadows[] = 0);
    static DvmhData *fromRegularArray(DataType dt, void *regArr, DvmType len, DvmType baseIdx = 0);
public:
    void setHostPortion(void *hostAddr, const Interval havePortion[]);
    void setHeader(DvmType aHeader[], const void *base, bool owning = false);
    void createHeader() { setHeader(new DvmType[rank + 3], 0, true); }
    void initActual(int dev);
    void initActualShadow();
    DvmhRepresentative *createNewRepr(int dev, const Interval havePortion[]);
    void deleteRepr(int device);
    void extendBlock(const Interval block[], Interval result[]) const {
        extendBlock(rank, block, shdWidths, localPlusShadow, result);
    }
    static void extendBlock(int rank, const Interval block[], const ShdWidth shadow[], const Interval bounds[], Interval result[]);
    void expandIncomplete(DvmType newHigh);
    void getActualBase(int dev, DvmhPieces *p, const Interval curLocalPart[], bool addToActual);
    void getActualBaseOne(int dev, const Interval realBlock[], const Interval curLocalPart[], bool addToActual);
    void getActual(const Interval indexes[], bool addToActual);
    void getActualEdges(const Interval aLocalPart[], const ShdWidth shdWidths[], bool addToActual);
    void getActualShadow(int dev, const Interval curLocalPart[], bool cornerFlag, const ShdWidth curShdWidths[], bool addToActual);
    void clearActual(DvmhPieces *p, int exceptDev = -1);
    void clearActualOne(const Interval piece[], int exceptDev = -1);
    void clearActualShadow(const Interval curLocalPart[], bool cornerFlag, const ShdWidth curShdWidths[], int exceptDev = -1);
    void performSetActual(int dev, DvmhPieces *p);
    void performSetActualOne(int dev, const Interval realBlock[]);
    void setActual(DvmhPieces *p);
    void setActual(const Interval indexes[]);
    void setActualShadow(int dev, const Interval curLocalPart[], bool cornerFlag, const ShdWidth curShdWidths[]);
    void shadowComputed(int dev, const Interval curLocalPart[], bool cornerFlag, const ShdWidth curShdWidths[]);
    void updateShadowProfile(bool cornerFlag, ShdWidth curShdWidths[]);
    UDvmType getTotalElemCount() const;
    UDvmType getLocalElemCount() const;
    void realign(DvmhAlignRule *newAlignRule, bool ownDistribSpace = false, bool newValueFlag = false);
    void redistribute(const DvmhDistribRule *newDistribRule);
    bool hasElement(const DvmType indexes[]) const;
    bool convertGlobalToLocal2(DvmType indexes[], bool onlyFromLocalPart = false) const;
    void includeIndirectShadow(int axis, const std::string shadowName);
    void localizeAsReferenceFor(DvmhData *targetData, int targetAxis);
    DvmhEvent *enqueueWriteAccess(DvmhEvent *accessEnd, bool owning = false);
    DvmhEvent *enqueueReadAccess(DvmhEvent *accessEnd, bool owning = false);
    void syncWriteAccesses() {
        latestWriterEnd->wait();
    }
    void syncAllAccesses() {
        latestWriterEnd->wait();
        latestReadersEnd->wait();
    }
    bool mpsCanRead(const MultiprocessorSystem *mps) const;
    bool mpsCanWrite(const MultiprocessorSystem *mps) const;
    bool fillLocalPart(int proc, Interval res[]) const;
public:
    ~DvmhData();
protected:
    void finishInitialization(int aRank, const Interval aSpace[], const ShdWidth shadows[]);
    void initShadowProfile();
    void recalcLocalPlusShadow();
    DvmhPieces *applyShadowProfile(DvmhPieces *p, const Interval curLocalPart[]) const;
    void performCopy(int dev1, int dev2, const Interval aCutting[]) const;
    void performGetActual(int dev, DvmhPieces *p, bool addToActual);
    void redistributeCommon(DvmhAlignRule *newAlignRule, bool newValueFlag = false);
    void changeShadowWidth(int axis, ShdWidth newWidth);
    void changeReprSize(int dev, const Interval havePortion[]);
    void updateHeader();
    void unlocalizeValues();
    void localizeValues(ReferenceDesc target);
    struct ShadowComputedPerformer;
    struct ProfileUpdater;
protected:
    // Global
    // Static info
    UDvmType typeSize;
    TypeType typeType;
    DataType dataType;
    int rank;
    Interval *space; //Array of Intervals (length=rank)
    ShdWidth *shdWidths; //Array of ShdWidths (length=rank)
    // Dynamic info
    DvmhAlignRule *alignRule;
    bool ownDspace;
    DvmhDataState curState; // Current state in terms of which region is producer and which regions are readers
    // Local
    bool hasLocalFlag;
    Interval *localPart; //Array of Intervals (length=rank)
    Interval *localPlusShadow; //Array of Intervals (length=rank)
    DvmType maxOrder;
    DvmhPieces *shadowProfile; // Shadow profile. Dimension is rank. Space is -shdWidths[i][0]..shdWidths[i][1]
    DvmType *header;
    bool ownHeader;
    bool freeBase;
    LocalizationInfo localizationInfo;
    DvmhEvent *latestWriterEnd;
    AggregateEvent *latestReadersEnd;
    // By-device dynamic info
    DvmhRepresentative **representatives; //Array of pointers to DvmhRepresentatives (length=devicesCount)
};

class DvmhShadowData {
public:
    DvmhData *data;
    ShdWidth *shdWidths;
    bool cornerFlag;
public:
    DvmhShadowData(): data(0), shdWidths(0), cornerFlag(false) {}
    DvmhShadowData(const DvmhShadowData &sdata);
public:
    DvmhShadowData &operator=(const DvmhShadowData &sdata);
    bool empty() const;
public:
    ~DvmhShadowData() {
        delete[] shdWidths;
    }
};

class DvmhRegion;

class DvmhShadow {
public:
    int dataCount() const { return datas.size(); }
    bool empty() const { return dataCount() == 0; }
    const DvmhShadowData &getData(int i) const { assert(i >= 0 && i < dataCount()); return datas[i]; }
public:
    void add(const DvmhShadowData &aSdata) {
        datas.push_back(aSdata);
    }
    void renew(DvmhRegion *currentRegion, bool doComm) const;
    void append(const DvmhShadow &others) {
        for (int i = 0; i < (int)others.datas.size(); i++)
            add(others.datas[i]);
    }
protected:
    std::vector<DvmhShadowData> datas;
};

}
