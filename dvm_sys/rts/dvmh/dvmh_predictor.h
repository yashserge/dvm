#pragma once

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <vector>

namespace libdvmh {

class DvmhPredictor
{
    //N - number of data points, K - polinom power
    int N, K;

    std::vector<double> sums;
    std::vector<double> stages;
    std::vector<double> perfs;

    std::vector<double> a;
    std::vector<double> b;

    bool solved;
    double predicted;
    double seqTime;
    bool stable;
    bool globalStable;
protected:
    void computeCoeffsMatrix();
    void checkDiagonal();
    int solve();

public:
    DvmhPredictor(const int N_, const int K_) : N(N_), K(K_), solved(false), predicted(-1), stable(false), globalStable(false) {};
    DvmhPredictor(const int N_) : N(N_), K(N - 1), solved(false), predicted(-1), stable(false), globalStable(false) { };
    DvmhPredictor(const int N_, const int K_, const double seqTime_, std::vector<double>&, std::vector<double>&);
    DvmhPredictor(const int N_, const double seqTime_, std::vector<double>&, std::vector<double>&);

    int predictStage();
    void addStage(const double stage);
    void addPerf(const double perf);
    bool isStable() { return stable; }
    bool isGlobalStable() { return globalStable; }
    void setGlobalStable(bool st) { globalStable = st; }

    // for debug
    void printresult();
};

}
