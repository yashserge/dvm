#pragma once

#include "dvmlib_incs.h"
#include "dvmh_data.h"
#include "loop.h"

namespace libdvmh {

SysHandle *getDvm(DvmhObject *obj);
DvmhObject *getDvmh(SysHandle *handle);
DvmhObject *getDvmh(UDvmType handle);
DvmhObject *getDvmh(DvmType handle);
template <class T>
T *getDvmh(SysHandle *handle) {
    return static_cast<T *>(getDvmh(handle));
}
template <class T>
T *getDvmh(UDvmType handle) {
    return static_cast<T *>(getDvmh(handle));
}
template <class T>
T *getDvmh(DvmType handle) {
    return static_cast<T *>(getDvmh(handle));
}
void tieDvm(DvmhObject *obj, SysHandle *handle);
void tieDvm(DvmhObject *obj, UDvmType handle);
void tieDvm(DvmhObject *obj, DvmType handle);
void untieDvm(DvmhObject *obj);
UDvmType getDvmMapSize();

void *getAddrFromDvm(s_DISARRAY *dvmHead);
void *getAddrFromDvmDesc(DvmType dvmDesc[]);
DvmhDistribSpace *createDistribSpaceFromAMView(s_AMVIEW *amView);
DvmhData *createDataFromDvmArray(s_DISARRAY *dvmHead, DvmhDistribSpace *dspace, DvmhData::TypeType givenTypeType = DvmhData::ttUnknown);
DvmhLoop *createLoopFromDvmLoop(s_PARLOOP *dvmLoop, DvmhRegion *region, DvmhDistribSpace *dspace, DvmhLoopPersistentInfo *persInfo);
bool isDvmArrayRegular(DvmType handle);

}
