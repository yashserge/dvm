#include "dvmh_predictor.h"

namespace libdvmh {

DvmhPredictor::DvmhPredictor(const int N_, const int K_, const double seqTime_, std::vector<double>& stages_, std::vector<double>& perfs_)
: N(N_), K(K_), stages(stages_), perfs(perfs_), solved(false), seqTime(seqTime_), stable(false), globalStable(false)
{
    if (K + 1 > 0)
    {
        sums.resize((K + 1) * (K + 1));
        a.resize(K + 1);
        b.resize(K + 1);
    }
}

DvmhPredictor::DvmhPredictor(const int N_, const double seqTime_, std::vector<double>& stages_, std::vector<double>& perfs_)
: N(N_), K(N - 1), stages(stages_), perfs(perfs_), solved(false), seqTime(seqTime_), stable(false), globalStable(false)
{
    if (K + 1 > 0)
    {
        sums.resize((K + 1) * (K + 1));
        a.resize(K + 1);
        b.resize(K + 1);
    }
}

void DvmhPredictor::addStage(const double stage)
{
    if (stable == false)
    {
        stages.push_back(stage);

        N++;
        K++;

        a.push_back(0.0);
        b.push_back(0.0);
        sums.resize((K + 1) * (K + 1));
    }
    else
        stages[stages.size() - 1] = stage;
}

void DvmhPredictor::addPerf(const double perf)
{
    // do two first iters
    if (perfs.size() < 2)
    {
        seqTime = perf;
        perfs.push_back(1.0);
    }
    else
    {
        if (stable == false)
        {
            double lastPerf = perfs.back();
            double nextPerf = seqTime / perf;

            if (fabs(1.0 - lastPerf / (nextPerf)) < 0.05)
            {
                //printf(" ** last %f, next %f\n", lastPerf, nextPerf);
                stable = true;
            }

            perfs.push_back(nextPerf);
            if (predicted != -1)
            {
                if (fabs(predicted - perf) / predicted > 0.15)
                    solved = false;
            }
            else
                solved = false;
        }
    }
}

void DvmhPredictor::computeCoeffsMatrix()
{
    //init square sums matrix
    for (int i = 0; i < K + 1; i++)
    {
        for (int j = 0; j < K + 1; j++)
        {
            sums[i * (K + 1) + j] = 0.0;
            for (int k = 0; k < N; k++)
                sums[i * (K + 1) + j] += pow(stages[k], i + j);
        }
    }
    //init free coefficients column
    for (int i = 0; i < K + 1; i++)
    {
        b[i] = 0;
        for (int k = 0; k < N; k++)
            b[i] += pow(stages[k], i) * perfs[k];
    }
}

void DvmhPredictor::checkDiagonal()
{
    for (int i = 0; i < K + 1; i++)
    {
        if (sums[i * (K + 1) + i] == 0.0)
        {
            for (int j = 0; j < K + 1; j++)
            {
                if (j == i)
                    continue;
                if (sums[j * (K + 1) + i] != 0.0 && sums[i * (K + 1) + j] != 0.0)
                {
                    double temp = 0;
                    for (int k = 0; k < K + 1; k++)
                    {
                        temp = sums[j * (K + 1) + k];
                        sums[j * (K + 1) + k] = sums[i * (K + 1) + k];
                        sums[i * (K + 1) + k] = temp;
                    }
                    temp = b[j];
                    b[j] = b[i];
                    b[i] = temp;
                    break;
                }
            }
        }
    }
}

int DvmhPredictor::solve()
{
    if (a.size() != b.size())
        //TODO:
        return -1;

    computeCoeffsMatrix();
    //check if there are 0 on main diagonal and exchange rows in that case
    checkDiagonal();

    for (int k = 0; k < K + 1; k++)
    {
        for (int i = k + 1; i < K + 1; i++)
        {
            if (sums[k * (K + 1) + k] == 0)
            {
                //TODO: printf("\nSolution does not exist.\n");
                return -1;
            }
            double M = sums[i * (K + 1) + k] / sums[k * (K + 1) + k];
            for (int j = k; j < K + 1; j++)
                sums[i * (K + 1) + j] -= M * sums[k * (K + 1) + j];
            b[i] -= M * b[k];
        }
    }

    for (int i = (K + 1) - 1; i >= 0; i--)
    {
        double s = 0;
        a[i] = 0.0;
        for (int j = i; j < K + 1; j++)
            s = s + sums[i * (K + 1) + j] * a[j];
        a[i] = (b[i] - s) / sums[i * (K + 1) + i];
    }
    solved = true;
    return 0;
}

int DvmhPredictor::predictStage()
{
    int err = 0;
    if (solved == false)
        err = solve();

    int ret = -1;
    if (err == 0)
    {
        if (stable)
            ret = (int)(stages.back());
        else
        {
            int x = (int)(stages.back()) + 1;
            predicted = a[0];
            int argX = x;
            for (int i = 1; i < K + 1; i++)
            {
                predicted += argX * a[i];
                argX *= x;
            }
            predicted = seqTime / predicted;
            ret = x;
        }
    }
    return ret;
}

// for debug
void DvmhPredictor::printresult()
{
    //print polynom parameters
    printf("\n");
    for (int i = 0; i < K + 1; i++)
        printf("a[%d] = %f\n", i, a[i]);
    printf("predicted %f\n", predicted);
}

}
