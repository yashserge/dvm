#include "project_ctx.h"

#include <cassert>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <algorithm>

#include "cdvmh_log.h"
#include "messages.h"

namespace cdvmh {

// ConverterOptions

void ConverterOptions::init() {
    autoTfm = false;
    oneThread = false;
    noCuda = false;
    noH = false;
    emitBlankHandlers = false;
    lessDvmLines = false;
    savePragmas = false;
    displayWarnings = false;
    verbose = false;
    seqOutput = false;
    doOpenMP = true;
    paralOutside = false;
    enableIndirect = false;
    linearRefs = true;
    useBlank = false;
    useOmpReduction = false;
    perfDbgLvl = 0;
    dvmDebugLvl = 0;
    useDvmhStdio = true;
    useVoidStdio = true;
    includeDirs.clear();
    addDefines.clear();
    removeDefines.clear();
    dvmhLibraryEntry.clear();
    inputFiles.clear();
    outputFiles.clear();
    languages.clear();
}

void ConverterOptions::setFromArgs(int argc, char *argv[]) {
    init();
    std::string outputFile;
    std::string language;
    for (int i = 0; i < argc; i++) {
        if (!strcmp(argv[i], "-autoTfm")) {
            autoTfm = true;
        } else if (!strcmp(argv[i], "-oneThread")) {
            oneThread = true;
        } else if (!strcmp(argv[i], "-noCuda")) {
            noCuda = true;
        } else if (!strcmp(argv[i], "-I")) {
            i++;
            includeDirs.push_back(argv[i]);
        } else if (!strncmp(argv[i], "-I", 2)) {
            includeDirs.push_back(argv[i] + 2);
        } else if (!strcmp(argv[i], "-D")) {
            i++;
            addDefine(argv[i]);
        } else if (!strncmp(argv[i], "-D", 2)) {
            addDefine(argv[i] + 2);
        } else if (!strcmp(argv[i], "-U")) {
            i++;
            removeDefines.push_back(argv[i]);
        } else if (!strncmp(argv[i], "-U", 2)) {
            removeDefines.push_back(argv[i] + 2);
        } else if (!strcmp(argv[i], "-o")) {
            i++;
            outputFile = argv[i];
        } else if (!strcmp(argv[i], "-x")) {
            i++;
            language = argv[i];
            checkUserErrN(language == "c" || language == "cxx", "", 1, 13, language.c_str());
        } else if (!strcmp(argv[i], "-s")) {
            seqOutput = true;
        } else if (!strcmp(argv[i], "-p")) {
            seqOutput = false;
        } else if (!strcmp(argv[i], "-no-omp")) {
            doOpenMP = false;
        } else if (!strcmp(argv[i], "-Opl")) {
            paralOutside = true;
        } else if (!strcmp(argv[i], "-enable-indirect")) {
            enableIndirect = true;
        } else if (!strcmp(argv[i], "-Ohost")) {
            linearRefs = false;
        } else if (!strcmp(argv[i], "-use-blank")) {
            useBlank = true;
        } else if (!strcmp(argv[i], "-omp-reduction")) {
            useOmpReduction = true;
        } else if (!strcmp(argv[i], "-d1")) {
            dvmDebugLvl = 1;
        } else if (!strcmp(argv[i], "-d2")) {
            dvmDebugLvl = 2;
        } else if (!strcmp(argv[i], "-d3")) {
            dvmDebugLvl = 3;
        } else if (!strcmp(argv[i], "-d4")) {
            dvmDebugLvl = 4;
        } else if (!strcmp(argv[i], "-e1")) {
            perfDbgLvl = 1;
        } else if (!strcmp(argv[i], "-e2")) {
            perfDbgLvl = 2;
        } else if (!strcmp(argv[i], "-e3")) {
            perfDbgLvl = 3;
        } else if (!strcmp(argv[i], "-e4")) {
            perfDbgLvl = 4;
        } else if (!strcmp(argv[i], "-v")) {
            verbose = true;
        } else if (!strcmp(argv[i], "-w")) {
            displayWarnings = true;
        } else if (!strcmp(argv[i], "-dvm-stdio")) {
            useDvmhStdio = false;
        } else if (!strcmp(argv[i], "-no-void-stdio")) {
            useVoidStdio = false;
        } else if (!strcmp(argv[i], "-noH")) {
            noH = true;
        } else if (!strcmp(argv[i], "-dvm-entry")) {
            i++;
            dvmhLibraryEntry = argv[i];
        } else if (!strcmp(argv[i], "-emit-blank-handlers")) {
            emitBlankHandlers = true;
        } else if (!strcmp(argv[i], "-less-dvmlines")) {
            lessDvmLines = true;
        } else if (!strcmp(argv[i], "-save-pragmas")) {
            savePragmas = true;
        } else if (!strncmp(argv[i], "-", 1)) {
            userErrN("", 1, 12, argv[i]);
        } else {
            // If no option recognized - it is an input file
            inputFiles.push_back(argv[i]);
            outputFiles.push_back(outputFile);
            languages.push_back(language);
            outputFile.clear();
        }
    }
    if (!outputFile.empty() && !outputFiles.empty())
        outputFiles.back() = outputFile;
    if (seqOutput || (noH && !paralOutside))
        doOpenMP = false;
}

void ConverterOptions::addDefine(std::string def) {
    if (def.find('=') != def.npos)
        addDefines.push_back(std::make_pair(def.substr(0, def.find('=')), def.substr(def.find('=') + 1)));
    else
        addDefines.push_back(std::make_pair(def, ""));
}

// InputFile

InputFile::InputFile(const std::string &aFileName, std::string forcedLanguage, std::string convName) {
    fileName = aFileName;
    canonicalName = getCanonicalFileName(fileName);
    baseName = getBaseName(fileName);
    std::string ext = toLower(getExtension(baseName));
    checkUserErrN(!ext.empty(), "", 1, 14, fileName.c_str());
    shortName = baseName.substr(0, baseName.size() - ext.size() - 1);
    isCompilable = ext != "h" && ext != "hpp";
    if (forcedLanguage.empty())
        CPlusPlus = ext == "cpp" || ext == "cc" || ext == "cxx" || ext == "hpp";
    else
        CPlusPlus = forcedLanguage == "cxx";
    if (convName.empty())
        convName = shortName + ".DVMH." + (isCompilable ? "c" : "h") + (CPlusPlus ? "pp" : "");
    std::string convExt = toLower(getExtension(convName));
    if (isCompilable) {
        if (CPlusPlus)
            checkUserErrN(convExt == "cpp" || convExt == "cc" || convExt == "cxx", "", 1, 15, convExt.c_str());
        else
            checkUserErrN(convExt == "c", "", 1, 16, convExt.c_str());
    } else {
        if (CPlusPlus)
            checkUserErrN(convExt == "h" || convExt == "hpp", "", 1, 17, convExt.c_str());
        else
            checkUserErrN(convExt == "h", "", 1, 18, convExt.c_str());
    }
    convertedFileName = convName;
    canonicalConverted = getCanonicalFileName(convertedFileName);
    if (isCompilable) {
        std::string convShort = convName.substr(0, convName.size() - convExt.size() - 1);
        outCXXName = convShort + "_cxx.cpp";
        outBlankName = convShort + "_blank." + (CPlusPlus ? "cpp" : "c");
        outHostName = convShort + "_host." + (CPlusPlus ? "cpp" : "c");
        outCudaName = convShort + "_cuda.cu";
        outCudaInfoName = convShort + "_cuda_info.c";
    }
}

// ProjectContext

static void addSame(std::map<std::string, std::string> &m, const std::string &name) {
    m[name] = name;
}

static void addPrefix(std::map<std::string, std::string> &m, const std::string &prefix, const std::string &name, bool voidOption = false) {
    m[name] = prefix + name;
    if (voidOption)
        m["void " + name] = prefix + "void_" + name;
}

ProjectContext::ProjectContext(const ConverterOptions &opts) {
    options = opts;
    for (int i = 0; i < (int)opts.inputFiles.size(); i++) {
        std::string fileName(opts.inputFiles[i]);
        checkUserErrN(fileExists(fileName), "", 1, 19, fileName.c_str());
        InputFile file(fileName, opts.languages[i], opts.outputFiles[i]);
        inputFiles.push_back(file);
        nameToIdx[file.canonicalName] = inputFiles.size() - 1;
    }

    addSame(cudaReplacementNames, "fabs");
    addSame(cudaReplacementNames, "sqrt");
    addSame(cudaReplacementNames, "char");
    addSame(cudaReplacementNames, "signed char");
    addSame(cudaReplacementNames, "unsigned char");
    addSame(cudaReplacementNames, "short");
    addSame(cudaReplacementNames, "unsigned short");
    addSame(cudaReplacementNames, "int");
    addSame(cudaReplacementNames, "unsigned int");
    addSame(cudaReplacementNames, "long");
    addSame(cudaReplacementNames, "unsigned long");
    addSame(cudaReplacementNames, "long long");
    addSame(cudaReplacementNames, "unsigned long long");
    addSame(cudaReplacementNames, "float");
    addSame(cudaReplacementNames, "double");
    addSame(cudaReplacementNames, "long double");
    addSame(cudaReplacementNames, "size_t");
    addSame(cudaReplacementNames, "bool");
    // TODO: Add more CUDA-available functions and types

    std::string stdioPrefix(options.useDvmhStdio ? "dvmh_" : "dvm_");
    // stdio.h functions
    //  Operations on files
    addPrefix(dvmhReplacementNames, stdioPrefix, "remove");
    addPrefix(dvmhReplacementNames, stdioPrefix, "rename");
    addPrefix(dvmhReplacementNames, stdioPrefix, "tmpfile");
    addPrefix(dvmhReplacementNames, stdioPrefix, "tmpnam");
    //  File access functions
    addPrefix(dvmhReplacementNames, stdioPrefix, "fclose");
    addPrefix(dvmhReplacementNames, stdioPrefix, "fflush");
    addPrefix(dvmhReplacementNames, stdioPrefix, "fopen");
    addPrefix(dvmhReplacementNames, stdioPrefix, "freopen");
    addPrefix(dvmhReplacementNames, stdioPrefix, "setbuf");
    addPrefix(dvmhReplacementNames, stdioPrefix, "setvbuf");
    //  Formatted input/output functions
    addPrefix(dvmhReplacementNames, stdioPrefix, "fprintf", options.useVoidStdio);
    addPrefix(dvmhReplacementNames, stdioPrefix, "fscanf", options.useVoidStdio && options.useDvmhStdio);
    addPrefix(dvmhReplacementNames, stdioPrefix, "printf", options.useVoidStdio);
    addPrefix(dvmhReplacementNames, stdioPrefix, "scanf", options.useVoidStdio && options.useDvmhStdio);
    addPrefix(dvmhReplacementNames, stdioPrefix, "vfprintf", options.useVoidStdio);
    addPrefix(dvmhReplacementNames, stdioPrefix, "vfscanf", options.useVoidStdio && options.useDvmhStdio);
    addPrefix(dvmhReplacementNames, stdioPrefix, "vprintf", options.useVoidStdio);
    addPrefix(dvmhReplacementNames, stdioPrefix, "vscanf", options.useVoidStdio && options.useDvmhStdio);
    //  Character input/output functions
    addPrefix(dvmhReplacementNames, stdioPrefix, "fgetc");
    addPrefix(dvmhReplacementNames, stdioPrefix, "fgets", options.useVoidStdio && options.useDvmhStdio);
    addPrefix(dvmhReplacementNames, stdioPrefix, "fputc", options.useVoidStdio && options.useDvmhStdio);
    addPrefix(dvmhReplacementNames, stdioPrefix, "fputs", options.useVoidStdio && options.useDvmhStdio);
    addPrefix(dvmhReplacementNames, stdioPrefix, "getc");
    addPrefix(dvmhReplacementNames, stdioPrefix, "getchar");
    addPrefix(dvmhReplacementNames, stdioPrefix, "gets", options.useVoidStdio && options.useDvmhStdio);
    addPrefix(dvmhReplacementNames, stdioPrefix, "putc", options.useVoidStdio && options.useDvmhStdio);
    addPrefix(dvmhReplacementNames, stdioPrefix, "putchar", options.useVoidStdio && options.useDvmhStdio);
    addPrefix(dvmhReplacementNames, stdioPrefix, "puts", options.useVoidStdio && options.useDvmhStdio);
    addPrefix(dvmhReplacementNames, stdioPrefix, "ungetc", options.useVoidStdio && options.useDvmhStdio);
    //  Direct input/output functions
    addPrefix(dvmhReplacementNames, stdioPrefix, "fread", options.useVoidStdio && options.useDvmhStdio);
    addPrefix(dvmhReplacementNames, stdioPrefix, "fwrite", options.useVoidStdio && options.useDvmhStdio);
    //  File positioning functions
    addPrefix(dvmhReplacementNames, stdioPrefix, "fgetpos");
    addPrefix(dvmhReplacementNames, stdioPrefix, "fseek", options.useVoidStdio && options.useDvmhStdio); // beware of errno checks, maybe
    addPrefix(dvmhReplacementNames, stdioPrefix, "fsetpos");
    addPrefix(dvmhReplacementNames, stdioPrefix, "ftell");
    addPrefix(dvmhReplacementNames, stdioPrefix, "rewind");
    //  Error-handling functions
    addPrefix(dvmhReplacementNames, stdioPrefix, "clearerr");
    addPrefix(dvmhReplacementNames, stdioPrefix, "feof");
    addPrefix(dvmhReplacementNames, stdioPrefix, "ferror");
    addPrefix(dvmhReplacementNames, stdioPrefix, "perror");
    // stdio.h global variables
    // TODO: Actually, they ain't variables, but macros. Make their replacement on another level - in PPCallbacks.
    if (options.useDvmhStdio) {
        addPrefix(dvmhReplacementNames, stdioPrefix, "stderr");
        addPrefix(dvmhReplacementNames, stdioPrefix, "stdin");
        addPrefix(dvmhReplacementNames, stdioPrefix, "stdout");
    } else {
        dvmhReplacementNames["stderr"] = "DVMSTDERR";
        dvmhReplacementNames["stdin"] = "DVMSTDIN";
        dvmhReplacementNames["stdout"] = "DVMSTDOUT";
    }
    if (!options.useDvmhStdio) {
        // stdio.h types
        dvmhReplacementNames["FILE"] = "DVMFILE";
    }

    // stdlib.h functions
    dvmhReplacementNames["calloc"] = "dvmh_calloc_C";
    dvmhReplacementNames["exit"] = "dvmh_exit_C";
    dvmhReplacementNames["free"] = "dvmh_free_C";
    dvmhReplacementNames["malloc"] = "dvmh_malloc_C";
    dvmhReplacementNames["realloc"] = "dvmh_realloc_C";
    dvmhReplacementNames["strdup"] = "dvmh_strdup_C";

    // XXX: Functions not from C Standard Libraries are not included: open, close, fstat, lseek, read, write, access, unlink, stat
}

std::string ProjectContext::getDvmhReplacement(const std::string &name, bool canBeVoid) const {
    std::map<std::string, std::string>::const_iterator it1 = dvmhReplacementNames.find(name);
    std::map<std::string, std::string>::const_iterator it2 = dvmhReplacementNames.find("void " + name);
    if (canBeVoid && it2 != dvmhReplacementNames.end())
        return it2->second;
    else if (it1 != dvmhReplacementNames.end())
        return it1->second;
    assert(false);
    return "UNKNOWN";
}

}
