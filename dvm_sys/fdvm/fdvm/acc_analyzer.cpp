#include "dvm.h"
#include "acc_analyzer.h"
#include "calls.h"


// local functions
static ControlFlowItem* getControlFlowList(SgStatement*, SgStatement*, ControlFlowItem**, SgStatement**, doLoops*, CallData*);
static ControlFlowItem* processOneStatement(SgStatement** stmt, ControlFlowItem** pred, ControlFlowItem **list, ControlFlowItem* oldcur, doLoops*, CallData*);
static ControlFlowItem* switchItem(SgStatement* stmt, ControlFlowItem* empty, SgStatement** lastAnStmt, doLoops* loops, CallData* calls);
static ControlFlowItem* ifItem(SgStatement*, ControlFlowItem*, SgStatement** lastAnStmt, doLoops* loops, bool ins, CallData*);
static void setLeaders(ControlFlowItem*);
static void clearList(ControlFlowItem*);
static void fillLabelJumps(ControlFlowItem*);
#if ACCAN_DEBUG
static void printControlFlowList(ControlFlowItem*, ControlFlowItem* last = NULL);
#endif

static ControlFlowGraph* GetControlFlowGraphWithCalls(SgStatement*, CallData*);
static void FillCFGSets(ControlFlowGraph*);
static void FillPrivates(ControlFlowGraph*);
static ControlFlowItem* AddFunctionCalls(SgStatement*, CallData*, ControlFlowItem**);

const char* is_correct = NULL;

//interprocedural analysis, called for main procedure
void Private_Vars_Analyzer(SgStatement* start)
{
	if (!options.isOn(PRIVATE_ANALYSIS)) {
		return;
	}
	CallData calls;
	calls.AddHeader(start, false, start->symbol());
	//stage 1: preparing graph data
	ControlFlowGraph* CGraph = GetControlFlowGraphWithCalls(start, &calls);
	calls.AssociateGraphWithHeader(start, CGraph);
	//calls.printControlFlows();
	//stage 2: data flow analysis
	FillCFGSets(CGraph);
	//stage 3: fulfilling loop data
	FillPrivates(CGraph);
}

ControlFlowGraph* GetControlFlowGraphWithCalls(SgStatement* start, CallData* calls)
{
	if (start == NULL) {
		is_correct = "no body for call found";
		return NULL;
	}
	doLoops l;
	ControlFlowItem* funcGraph = getControlFlowList(start, start->lastNodeOfStmt(), NULL, NULL, &l, calls);
	fillLabelJumps(funcGraph);
	setLeaders(funcGraph);
	return new ControlFlowGraph(funcGraph, NULL);
}

static void FillCFGSets(ControlFlowGraph* graph)
{
	graph->privateAnalyzer();
}

static void FillPrivates(ControlFlowGraph* graph)
{
}


// CALL function for PRIVATE analyzing
void Private_Vars_Function_Analyzer(SgStatement* start)
{
	//temporary state
	if (!options.isOn(PRIVATE_ANALYSIS)){
		return;
	}
	if (start->variant() == PROG_HEDR)
		Private_Vars_Analyzer(start);
	/*
    ControlFlowItem* funcGraph = getControlFlowList(start, start->lastNodeOfStmt(), NULL, NULL, new doLoops());
    fillLabelJumps(funcGraph);
    setLeaders(funcGraph);
#if ACCAN_DEBUG
    printControlFlowList(funcGraph);
#endif
    ControlFlowItem* p = funcGraph;
    ControlFlowItem* pl_start = NULL;
    ControlFlowItem* pl_end = NULL;
    ControlFlowGraph* graph = new ControlFlowGraph(funcGraph, NULL);
    graph->privateAnalyzer();
	*/
}
/*
// CALL function for PRIVATE analyzing
void Private_Vars_Analyzer(SgStatement *firstSt, SgStatement *lastSt)
{
    // temporary state
    //return;
    SgExpression* par_des = firstSt->expr(2);
    SgSymbol* l;
    SgForStmt* chk;
    int correct = 1;
    firstSt = firstSt->lexNext();
    while (correct && (par_des != NULL) && (par_des->lhs() != NULL) && ((l = par_des->lhs()->symbol()) != NULL)){
        if (firstSt->variant() == FOR_NODE){
            chk = isSgForStmt(firstSt);
            if (chk->symbol() != l)
                correct = 0;
            firstSt = firstSt->lexNext();
            par_des = par_des->rhs();
        }
        else{
            correct = 0;
        }
    }
    if (correct){
        doLoops* loops = new doLoops();
        ControlFlowItem* cfList = getControlFlowList(firstSt, lastSt, NULL, NULL, loops);
        fillLabelJumps(cfList);
        setLeaders(cfList);
#if ACCAN_DEBUG
        printControlFlowList(cfList);
#endif
        VarSet* priv = ControlFlowGraph(cfList, NULL).getPrivate();
#if ACCAN_DEBUG
        priv->print();
#endif
        clearList(cfList);
    }
}
*/

static void fillLabelJumps(ControlFlowItem* cfList)
{
    if (cfList != NULL){
        ControlFlowItem* temp = cfList;
        ControlFlowItem* temp2;
        unsigned int label_no = 0;
        while (temp != NULL){
            if (temp->getLabel() != NULL)
                label_no++;
            temp = temp->getNext();
        }
        LabelCFI* table = new LabelCFI[label_no + 1];
        unsigned int li = 0;
        for (temp = cfList; temp != NULL; temp = temp->getNext()){
            SgLabel* label;
            if ((label = temp->getLabel()) != NULL){
                table[li].item = temp;
                table[li++].l = label->id();
            }
            temp2 = temp;
        }
        temp = new ControlFlowItem();
        temp2->AddNextItem(temp);
        table[label_no].item = temp2;
        table[label_no].l = -1;
        for (temp = cfList; temp != NULL; temp = temp->getNext()){
            SgLabel* jump = temp->getLabelJump();
            int l;
            if (jump != NULL){
                l = jump->id();
                for (unsigned int i = 0; i < label_no + 1; i++){
                    if (table[i].l == l || i == label_no){
                        temp->initJump(table[i].item);
                        break;
                    }
                }
            }
        }
        delete[] table;
    }
}

static void setLeaders(ControlFlowItem* cfList)
{
    if (cfList != NULL)
        cfList->setLeader();
    while (cfList != NULL)
    {
        if (cfList->getJump() != NULL)
        {
            cfList->getJump()->setLeader();
            if (cfList->getNext() != NULL)
                cfList->getNext()->setLeader();
        }
        cfList = cfList->getNext();
    }
}

static void clearList(ControlFlowItem *list)
{
    if (list != NULL)
    {
        if (list->getNext() != NULL)
            clearList(list->getNext());
        delete list;
    }
}

static ControlFlowItem* ifItem(SgStatement* stmt, ControlFlowItem* empty, SgStatement** lastAnStmt, doLoops* loops, bool ins, CallData* calls)
{
    if (stmt == NULL)
        return empty;
    SgIfStmt* cond;
    if (stmt->variant() == ELSEIF_NODE)
        cond = (SgIfStmt*)stmt;
    if (stmt->variant() == ELSEIF_NODE || (!ins && (cond = isSgIfStmt(stmt)) != NULL))
    {
        SgExpression* c = &(SgNotOp((cond->conditional()->copy())));
        ControlFlowItem *n, *j;
        ControlFlowItem* last;
        if ((n = getControlFlowList(cond->trueBody(), NULL, &last, lastAnStmt, loops, calls)) == NULL)
            return NULL;
        j = ifItem(cond->falseBody(), empty, lastAnStmt, loops, cond->falseBody() != NULL ? cond->falseBody()->variant() == IF_NODE : false, calls);
        ControlFlowItem* gotoEmpty = new ControlFlowItem(NULL, empty, j, NULL);
        if (last != NULL)
            last->AddNextItem(gotoEmpty);
        else
            n = gotoEmpty;
        return new ControlFlowItem(c, j, n, stmt->label());
    }
    else
    {
        ControlFlowItem* last;
        ControlFlowItem* ret;
        if ((ret = getControlFlowList(stmt, NULL, &last, lastAnStmt, loops, calls)) == NULL)
            return NULL;
        last->AddNextItem(empty);
        return ret;
    }
}

static ControlFlowItem* switchItem(SgStatement* stmt, ControlFlowItem* empty, SgStatement** lastAnStmt, doLoops* loops, CallData* calls)
{
    SgSwitchStmt* sw = isSgSwitchStmt(stmt);
    SgExpression* sw_cond = (sw->selector());
    stmt = stmt->lexNext();
    *lastAnStmt = stmt;
    ControlFlowItem* last_sw = NULL;
    ControlFlowItem* first = NULL;
    bool is_def_last = false;
    SgStatement* not_def_last;
    while (stmt->variant() == CASE_NODE || stmt->variant() == DEFAULT_NODE)
    {
        if (stmt->variant() == DEFAULT_NODE){
            while (stmt->variant() != CONTROL_END && stmt->variant() != CASE_NODE)
                stmt = stmt->lexNext();
            if (stmt->variant() == CONTROL_END)
                stmt = stmt->lexNext();
            is_def_last = true;
            continue;
        }
        SgExpression* c = ((SgCaseOptionStmt*)stmt)->caseRange(0);
        SgExpression *lhs = NULL;
        SgExpression *rhs = NULL;
        if (c->variant() == DDOT){
            lhs = c->lhs();
            rhs = c->rhs();
            if (rhs == NULL)
                c = &(*lhs <= *sw_cond);
            else if (lhs == NULL)
                c = &(*sw_cond <= *rhs);
            else
                c = &(*lhs <= *sw_cond && *sw_cond <= *rhs);
        }
        else
            c = &SgNeqOp(*sw_cond, *c);
        ControlFlowItem *n, *j;
        ControlFlowItem* last;
        if ((n = getControlFlowList(stmt->lexNext(), NULL, &last, lastAnStmt, loops, calls)) == NULL)
            return NULL;
        j = new ControlFlowItem();
        ControlFlowItem* gotoEmpty = new ControlFlowItem(NULL, empty, j, NULL);
        if (last != NULL)
            last->AddNextItem(gotoEmpty);
        else
            n = gotoEmpty;
        ControlFlowItem* cond = new ControlFlowItem(c, j, n, stmt->label());
        if (last_sw == NULL)
            first = cond;
        else
            last_sw->AddNextItem(cond);
        last_sw = j;
        is_def_last = false;
        not_def_last = *lastAnStmt;
        stmt = *lastAnStmt;
    }
    SgStatement* def = sw->defOption();
    if (def != NULL){
        ControlFlowItem* last;
        ControlFlowItem* n;
        if ((n = getControlFlowList(def->lexNext(), NULL, &last, lastAnStmt, loops, calls)) == NULL)
            return NULL;
        if (last != NULL)
            last->AddNextItem(empty);
        if (last_sw == NULL)
            first = n;
        else
            last_sw->AddNextItem(n);
        last_sw = last;
    }
    last_sw->AddNextItem(empty);
    if (!is_def_last)
        *lastAnStmt = not_def_last;
    return first;
}

static ControlFlowItem* getControlFlowList(SgStatement *firstSt, SgStatement *lastSt, ControlFlowItem **last, SgStatement **lastAnStmt, doLoops* loops, CallData* calls)
{
    ControlFlowItem *list = new ControlFlowItem();
    ControlFlowItem *cur = list;
    ControlFlowItem *pred = list;
    SgStatement *stmt;
    for (stmt = firstSt; (
        stmt != lastSt
        && stmt->variant() != CONTROL_END
        && (lastSt != NULL || stmt->variant() != ELSEIF_NODE)
        && (lastSt != NULL || stmt->variant() != CASE_NODE)
        && (lastSt != NULL || stmt->variant() != DEFAULT_NODE));
        stmt = stmt->lexNext())
    {
        cur = processOneStatement(&stmt, &pred, &list, cur, loops, calls);
        if (cur == NULL){
            clearList(list);
            return NULL;
        }
    }
    if (cur == NULL){
        cur = list = new ControlFlowItem();
    }
    if (last != NULL)
        *last = cur;
    if (lastAnStmt != NULL)
        *lastAnStmt = stmt;
    return list;
}

AnalysedCallsList* CallData::IsHeaderInList(SgStatement* header)
{
	if (header == NULL)
		return NULL;
	AnalysedCallsList* p = calls_list;
	while (p != NULL) {
		if (p->header == header)
			return p;
		p = p->next;
	}
	return NULL;
}

void CallData::AssociateGraphWithHeader(SgStatement* st, ControlFlowGraph* gr)
{
	AnalysedCallsList* l = calls_list;
	while (l != NULL) {
		if (l->header == st) {
			l->graph = gr;
			return;
		}
		l = l->next;
	}
}

AnalysedCallsList* CallData::AddHeader(SgStatement* st, bool isFun, SgSymbol* name)
{
	AnalysedCallsList* l = new AnalysedCallsList(st, isIntrinsicFunctionName(name->identifier()) && !isUserFunction(name), IsPureProcedure(name), isFun);
	l->next = calls_list;
	calls_list = l;
	return l;
}

AnalysedCallsList* CallData::getLinkToCall(SgExpression* e, SgStatement* s)
{
	SgStatement* header;
	SgSymbol* name;
	bool isFun;
	if (e == NULL) {
		//s - procedure call
		SgCallStmt* f = isSgCallStmt(s);
		header = isSgProcHedrStmt(GRAPHNODE(f->name())->st_header);
		name = f->name();
		isFun = false;
		//intr = isIntrinsicFunctionName(f->name()->identifier()) && !isUserFunction(f->name());
		//IsPureProcedure(f->name());
	}
	else {
		//e - function call
		SgFunctionCallExp* f = isSgFunctionCallExp(e);
		header = isSgFuncHedrStmt(GRAPHNODE(f->funName())->st_header);
		name = f->funName();
		isFun = true;
	}
	AnalysedCallsList* p;
	if ((p = IsHeaderInList(header))) {
		recursion_flag = recursion_flag || p->graph != NULL;
		return p;
	}
	p = AddHeader(header, isFun, name);
	if (!p->isIntristic) {
		ControlFlowGraph* graph = GetControlFlowGraphWithCalls(header, this);
		AssociateGraphWithHeader(header, graph);
	}
	return p;
}

static ControlFlowItem* GetFuncCallsForExpr(SgExpression* e, CallData* calls, ControlFlowItem** last)
{
	if (e == NULL) {
		*last = NULL;
		return NULL;
	}
	SgFunctionCallExp* f = isSgFunctionCallExp(e);
	if (f) {
		ControlFlowItem* head = new ControlFlowItem(NULL, NULL, calls->getLinkToCall(e, NULL));
		ControlFlowItem* curl = head;
		head->setFunctionCall(f);
		ControlFlowItem* l1, *l2, *l3;
		ControlFlowItem* tail1 = GetFuncCallsForExpr(e->lhs(), calls, &l1);
		ControlFlowItem* tail2 = GetFuncCallsForExpr(e->rhs(), calls, &l2);
		*last = head;
		if (tail2 != NULL) {
			l2->AddNextItem(head);
			head = tail2;
		}
		if (tail1 != NULL) {
			l1->AddNextItem(head);
			head = tail1;
		}

		return head;
	}
	f = isSgFunctionCallExp(e->lhs());
	if (f) {
		ControlFlowItem* head = new ControlFlowItem(NULL, NULL, calls->getLinkToCall(e->lhs(), NULL));
		head->setFunctionCall(f);
		ControlFlowItem* l1, *l2, *l3;
		ControlFlowItem* tail1 = GetFuncCallsForExpr(e->lhs()->lhs(), calls, &l1);
		ControlFlowItem* tail2 = GetFuncCallsForExpr(e->lhs()->rhs(), calls, &l2);
		ControlFlowItem* tail3 = GetFuncCallsForExpr(e->rhs(), calls, &l3);
		*last = head;
		if (tail2 != NULL) {
			l2->AddNextItem(head);
			head = tail2;
		}
		if (tail1 != NULL) {
			l1->AddNextItem(head);
			head = l1;
		}
		if (tail3 != NULL) {
			(*last)->AddNextItem(tail3);
			*last = l3;
		}
		return head;
	}
	return GetFuncCallsForExpr(e->rhs(), calls, last);
}

static ControlFlowItem* AddFunctionCalls(SgStatement* st, CallData* calls, ControlFlowItem** last)
{
	ControlFlowItem* retv = GetFuncCallsForExpr(st->expr(0), calls, last);
	ControlFlowItem* l2 = NULL;
	ControlFlowItem* second = GetFuncCallsForExpr(st->expr(1), calls, &l2);
	if (retv == NULL) {
		retv = second;
		*last = l2;
	}
	else if (second != NULL) {
		(*last)->AddNextItem(second);
		*last = l2;
	}
	ControlFlowItem* l3 = NULL;
	ControlFlowItem* third = GetFuncCallsForExpr(st->expr(2), calls, &l3);
	if (retv == NULL) {
		retv = third;
		*last = l3;
	}
	else if (third != NULL) {
		(*last)->AddNextItem(third);
		*last = l3;
	}
	return retv;
}

static ControlFlowItem* processOneStatement(SgStatement** stmt, ControlFlowItem** pred, ControlFlowItem **list, ControlFlowItem* oldcur, doLoops* loops, CallData* calls)
{
	ControlFlowItem* lastf;
	ControlFlowItem* funcs = AddFunctionCalls(*stmt, calls, &lastf);
	if (funcs != NULL) {
		if (*pred != NULL)
			(*pred)->AddNextItem(funcs);
		else
			*list = funcs;
		*pred = lastf;
	}
    switch ((*stmt)->variant())
    {
        case IF_NODE:
        {
            ControlFlowItem* emptyAfterIf = new ControlFlowItem(); //empty item to avoid second pass
            /*
            if ((*stmt)->hasLabel()){
                ControlFlowItem* emptyBeforeIf = new ControlFlowItem();
                emptyBeforeIf->setLabel((*stmt)->label());
                if (*pred != NULL)
                    (*pred)->AddNextItem(emptyBeforeIf);
                else
                    *list = emptyBeforeIf;
                *pred = emptyBeforeIf;
            }
            */
            ControlFlowItem* cur = ifItem(*stmt, emptyAfterIf, stmt, loops, false, calls);
            emptyAfterIf->setLabel((*stmt)->label());
            if (*pred != NULL)
                (*pred)->AddNextItem(cur);
            else
                *list = cur;
            return (*pred = emptyAfterIf);
        }
        case ASSIGN_STAT:
		case PROC_STAT:
        {
            ControlFlowItem* cur = new ControlFlowItem(*stmt, NULL, (*stmt)->variant() == PROC_STAT ? calls->getLinkToCall(NULL, *stmt) : NULL);
            if (*pred != NULL)
                (*pred)->AddNextItem(cur);
            else
                *list = cur;
            return (*pred = loops->checkStatementForLoopEnding(cur->getLabel() ? cur->getLabel()->id() : -1, cur));
        }
        case LOGIF_NODE:
        {
            ControlFlowItem* emptyAfterIf = new ControlFlowItem(); //empty item to avoid second pass
            SgLogIfStmt* cond = isSgLogIfStmt(*stmt);
            SgLabel* lbl = (*stmt)->label();
            SgExpression* c = &(SgNotOp((cond->conditional()->copy())));
            ControlFlowItem* cur = new ControlFlowItem(c, emptyAfterIf, NULL, (*stmt)->label());
            if (*pred != NULL)
                (*pred)->AddNextItem(cur);
            else
                *list = cur;
            *stmt = (*stmt)->lexNext();
            ControlFlowItem* body;
            if ((body = processOneStatement(stmt, &cur, list, cur, loops, calls)) == NULL){
                return NULL;
            }
            body->AddNextItem(emptyAfterIf);
            return (*pred = loops->checkStatementForLoopEnding(lbl ? lbl->id() : -1, emptyAfterIf));
        }
        case WHILE_NODE:
        {
            SgWhileStmt* cond = isSgWhileStmt(*stmt);
            bool isEndDo = (*stmt)->lastNodeOfStmt()->variant() == CONTROL_END;
            SgExpression* c = &(SgNotOp((cond->conditional()->copy())));
            ControlFlowItem* emptyAfterWhile = new ControlFlowItem();
            ControlFlowItem* emptyBeforeBody = new ControlFlowItem();
            ControlFlowItem* cur = new ControlFlowItem(c, emptyAfterWhile, emptyBeforeBody, (*stmt)->label());
            ControlFlowItem* gotoStart = new ControlFlowItem(NULL, cur, emptyAfterWhile, NULL);
            ControlFlowItem* emptyBefore = new ControlFlowItem(NULL, (ControlFlowItem*)NULL, cur, cond->label());
            SgVarRefExp* doName = (isSgVarRefExp((*stmt)->expr(2)));
            int lbl = -1;
            if (!isEndDo){
                SgStatement* end = lastStmtOfDo(cond);
                if (end->controlParent() && end->controlParent()->variant() == LOGIF_NODE)
                    lbl = end->controlParent()->label()->id();
                else
                    lbl = end->label()->id();
            }
            loops->addLoop(lbl, doName ? doName->symbol() : NULL, gotoStart, emptyAfterWhile);
            ControlFlowItem* n, *last;
            if (isEndDo){
                if ((n = getControlFlowList((*stmt)->lexNext(), NULL, &last, stmt, loops, calls)) == NULL)
                    return NULL;
                emptyBeforeBody->AddNextItem(n);
                loops->endLoop(last);
            }
            if (*pred != NULL)
                (*pred)->AddNextItem(emptyBefore);
            else
                *list = emptyBefore;
            if (isEndDo)
                return (*pred = emptyAfterWhile);
            return (*pred = emptyBeforeBody);
        }
        case FOR_NODE:
        {
            SgForStmt* fst = isSgForStmt(*stmt);
            SgStatement* p = (*stmt)->lexPrev();
            bool isParLoop = (p && p->variant() == DVM_PARALLEL_ON_DIR);
            SgExpression* pl = NULL;
            if (isParLoop){
                SgExpression* el = p->expr(1);
                while (el != NULL){
                    SgExpression* e = el->lhs();
                    if (e->variant() == ACC_PRIVATE_OP)
                        pl = e;
                    el = el->rhs();
                }
                //pl->unparsestdout();
            }
            bool isEndDo = fst->isEnddoLoop();
            SgExpression* lh = new SgVarRefExp(fst->symbol());
            SgStatement* fa = new SgAssignStmt(*lh, *fst->start());
            //fa->setLabel(*(*stmt)->label());
            ControlFlowItem* last;
            ControlFlowItem* emptyAfterDo = new ControlFlowItem();
            ControlFlowItem* emptyBeforeDo = new ControlFlowItem();
            ControlFlowItem* stcf = new ControlFlowItem(fa, emptyBeforeDo);
            stcf->setLabel((*stmt)->label());
            SgExpression* rh = new SgExpression(ADD_OP, new SgVarRefExp(fst->symbol()), new SgValueExp(1), NULL);
            SgStatement* add = new SgAssignStmt(*lh, *rh);
            SgExpression* endc = new SgExpression(GT_OP, new SgVarRefExp(fst->symbol()), fst->end(), NULL);
            ControlFlowItem* gotoStart = new ControlFlowItem(NULL, emptyBeforeDo, emptyAfterDo, NULL);
            ControlFlowItem* gotoEnd = new ControlFlowItem(endc, emptyAfterDo, gotoStart, NULL);
            ControlFlowItem* loop_d = new ControlFlowItem(add, gotoEnd);
            ControlFlowItem* loop_emp = new ControlFlowItem(NULL, loop_d);
            if (isParLoop){
                SgExpression* par_des = p->expr(2);
                int k = 0;
                while (par_des != NULL && par_des->lhs() != NULL){
                    k++;
                    par_des = par_des->rhs();
                }
                loops->setParallelDepth(k, pl, p);
            }
            
            if (loops->isLastParallel()){
                SgExpression* ex = loops->getPrivateList();
                emptyBeforeDo->MakeParloopStart();
                emptyBeforeDo->setPrivateList(ex, loops->GetParallelStatement());
                loop_d->MakeParloopEnd();
            }
            SgVarRefExp* doName = (isSgVarRefExp((*stmt)->expr(2)));
            int lbl = -1;
            if (!isEndDo){
                SgStatement* end = lastStmtOfDo(fst);
                if (end->variant() == LOGIF_NODE)
                    lbl = end->controlParent()->label()->id();
                else
                    lbl = end->label()->id();
            }
            loops->addLoop(lbl, doName ? doName->symbol() : NULL, loop_emp, emptyAfterDo);
            if (isEndDo){
                ControlFlowItem* body;
                if ((body = getControlFlowList(fst->body(), NULL, &last, stmt, loops, calls)) == NULL)
                    return NULL;
                emptyBeforeDo->AddNextItem(body);
                loops->endLoop(last);
            }
            if (*pred != NULL)
                (*pred)->AddNextItem(stcf);
            else
                *list = stcf;
            if (isEndDo)
                return (*pred = emptyAfterDo);
            return (*pred = emptyBeforeDo);
        }
        case GOTO_NODE:
        {
            SgGotoStmt* gst = isSgGotoStmt(*stmt);
            ControlFlowItem* gt = new ControlFlowItem(NULL, gst->branchLabel(), NULL, gst->label());
            if (*pred != NULL)
                (*pred)->AddNextItem(gt);
            else
                *list = gt;
            return (*pred = gt);
        }
        case ARITHIF_NODE:
        {
            SgArithIfStmt* arif = (SgArithIfStmt*)(*stmt);
            ControlFlowItem* gt3 = new ControlFlowItem(NULL, ((SgLabelRefExp*)(*stmt)->expr(1)->rhs()->rhs()->lhs())->label(), NULL, NULL);
            ControlFlowItem* gt2 = new ControlFlowItem(&SgEqOp(*(arif->conditional()), *new SgValueExp(0)), ((SgLabelRefExp*)(*stmt)->expr(1)->rhs()->lhs())->label(), gt3, NULL);
            ControlFlowItem* gt1 = new ControlFlowItem(&(*arif->conditional() < *new SgValueExp(0)), ((SgLabelRefExp*)(*stmt)->expr(1)->lhs())->label(), gt2, (*stmt)->label());
            if (*pred != NULL)
                (*pred)->AddNextItem(gt1);
            else
                *list = gt1;
            return (*pred = gt3);
        }
        case COMGOTO_NODE:
        {
            SgComputedGotoStmt* cgt = (SgComputedGotoStmt*)(*stmt);
            SgExpression* label = cgt->labelList();
            int i = 0;
            SgLabel* lbl = ((SgLabelRefExp *)(label->lhs()))->label();
            ControlFlowItem* gt = new ControlFlowItem(&SgEqOp(*(cgt->exp()), *new SgValueExp(++i)), lbl, NULL, cgt->label());
            if (*pred != NULL)
                (*pred)->AddNextItem(gt);
            else
                *list = gt;
            ControlFlowItem* old = gt;
            while ((label = label->rhs()))
            {
                lbl = ((SgLabelRefExp *)(label->lhs()))->label();
                gt = new ControlFlowItem(&SgEqOp(*(cgt->exp()), *new SgValueExp(++i)), lbl, NULL, NULL);
                old->AddNextItem(gt);
                old = gt;
            }
            return (*pred = gt);
        }
        case SWITCH_NODE:
        {
             ControlFlowItem* emptyAfterSwitch = new ControlFlowItem();
             ControlFlowItem* cur = switchItem(*stmt, emptyAfterSwitch, stmt, loops, calls);
             emptyAfterSwitch->setLabel((*stmt)->label());
             if (*pred != NULL)
                 (*pred)->AddNextItem(cur);
             else
                 *list = cur;
             return (*pred = emptyAfterSwitch);
        }
        case CONT_STAT:
        {
             ControlFlowItem* cur = new ControlFlowItem(NULL, (ControlFlowItem*)NULL, NULL, (*stmt)->label());
             if (*pred != NULL)
                 (*pred)->AddNextItem(cur);
             else
                 *list = cur;
             return (*pred = loops->checkStatementForLoopEnding(cur->getLabel() ? cur->getLabel()->id() : -1, cur));
        }
        case CYCLE_STMT:
        {
             SgSymbol* ref = (*stmt)->symbol();
             ControlFlowItem* cur = new ControlFlowItem(NULL, loops->getSourceForCycle(ref), NULL, (*stmt)->label());
             if (*pred != NULL)
                 (*pred)->AddNextItem(cur);
             else
                 *list = cur;
             return (*pred = cur);
        }
        case EXIT_STMT:
        {
             SgSymbol* ref = (*stmt)->symbol();
             ControlFlowItem* cur = new ControlFlowItem(NULL, loops->getSourceForExit(ref), NULL, (*stmt)->label());
             if (*pred != NULL)
                 (*pred)->AddNextItem(cur);
             else
                 *list = cur;
             return (*pred = cur);
        }
        case COMMENT_STAT:
            return *pred;
		case COMM_STAT:
		case USE_STMT:
			is_correct = "common block or use in statement";
			return *pred;
        default:
            return *pred;
            //return NULL;
    }
}

ControlFlowGraph::ControlFlowGraph(ControlFlowItem* list, ControlFlowItem* end) : def(NULL), use(NULL), pri(NULL)
{
    int n = 0;
    ControlFlowItem* orig = list;
    BaseBlock* prev = NULL;
    BaseBlock* start = NULL;
    int stmtNo = 0;
    bool ns = list->isEnumerated();
    if (list != NULL && !ns){
        while (list != NULL && list != end)
        {
            list->setStmtNo(++stmtNo);
            list = list->getNext();
        }
    }
    ControlFlowItem* last_prev = NULL;
    list = orig;
    while (list != NULL && list != end)
    {
        BaseBlock* bb = new BaseBlock(list, ++n, this);
        last = bb;
        bb->setPrev(prev);
        if (prev != NULL){
            prev->setNext(bb);
            if (!last_prev->isUnconditionalJump()){
                bb->addToPrev(prev);
                prev->addToSucc(bb);
            }
        }
        if (start == NULL)
            start = bb;
        prev = bb;
        while (list->getNext() != NULL && list->getNext() != end && !list->getNext()->isLeader()){
            list->setBBno(n);
            list = list->getNext();
        }
        list->setBBno(n);
        last_prev = list;
        list = list->getNext();
    }
    list = orig;
    while (list != NULL && list != end)
    {
        ControlFlowItem* target;
        if ((target = list->getJump()) != NULL)
        {
//            //no back edges
//            if (target->getBBno() > list->getBBno())
//            { 
                BaseBlock* tmp1 = start;
                BaseBlock* tmp2 = start;
                for (int i = 1; i < target->getBBno() || i < list->getBBno(); i++)
                {
                    if (i < list->getBBno())
                        tmp2 = tmp2->getLexNext();
                    if (i < target->getBBno())
                        tmp1 = tmp1->getLexNext();
                }
                tmp1->addToPrev(tmp2);
                tmp2->addToSucc(tmp1);
//            }
        }
        list = list->getNext();
    }
    start->markAsReached();
    first = start;
}

void BaseBlock::markAsReached()
{
    prev_status = 1;
    BaseBlockItem* s = succ;
    while (s != NULL){
        BaseBlock* b = s->block;
        if (b->prev_status == -1)
            b->markAsReached();
        s = s->next;
    }
}

void ControlFlowGraph::privateAnalyzer()
{
    BaseBlock* p = first;
    /*
    printf("GRAPH:\n");
    while (p != NULL){
        printf("block %d: ", p->getNum());
        if (p->containsParloopStart())
            printf("start");
        if (p->containsParloopEnd())
            printf("end");
        p->print();
        p = p->getLexNext();
    }
    */
    p = first;

	//if (is_correct) {
		liveAnalysis();
	//}
    while (1){
        ControlFlowItem* lstart, *lend;
		p->PrivateAnalysisForAllCalls();
        if ((lstart = p->containsParloopStart()) != NULL){
			if (is_correct != NULL) {
				Warning("Private analysis is not conducted for loop: %s", is_correct != NULL ? is_correct : "", PRIVATE_ANALYSIS_NOT_CONDUCTED, lstart->getPrivateListStatement());
			}else{
				while ((lend = p->containsParloopEnd()) == NULL){
					p->PrivateAnalysisForAllCalls();
					p = p->getLexNext();
				}
				BaseBlock* afterParLoop = p->getLexNext()->getLexNext();
				VarSet* l_pri = ControlFlowGraph(lstart, lend).getPrivate();
				if (is_correct != NULL) {
					Warning("Private analysis is not conducted for loop: %s", is_correct != NULL ? is_correct : "", PRIVATE_ANALYSIS_NOT_CONDUCTED, lstart->getPrivateListStatement());
					break;
				}
				VarSet* p_pri = new VarSet();
				SgExpression* ex_p = lstart->getPrivateList();
				if (ex_p != NULL)
					ex_p = ex_p->lhs();
				for (; ex_p != NULL; ex_p = ex_p->rhs()) {
					SgVarRefExp* pr;
					if (pr = isSgVarRefExp(ex_p->lhs())) {
						p_pri->addToSet(pr);
					}
				}
				VarSet* live = afterParLoop->getLiveIn();
				VarSet* pri = new VarSet();
				pri->unite(l_pri);
				pri->minus(live);
				l_pri->minus(pri);
				VarSet* test1 = new VarSet();
				test1->unite(pri);
				VarSet* test2 = new VarSet();
				test2->unite(p_pri);
				test2->minus(pri);
				test1->minus(p_pri);
				int extra = 0, missing = 0;
				while (!test2->isEmpty()) {
					//printf("EXTRA IN PRIVATE LIST: ");
					//test2->print();
					extra = 1;
					SgVarRefExp* var = test2->getFirst();
					test2->remove(var);
					Warning("var %s from private list wasn't classified as private", var->unparse(), PRIVATE_ANALYSIS_REMOVE_VAR, lstart->getPrivateListStatement());
				}
				while (!test1->isEmpty()) {
					//printf("MISSING IN PRIVATE LIST: ");
					//test1->print();
					missing = 1;
					SgVarRefExp* var = test1->getFirst();
					test1->remove(var);
					Warning("var %s should be added to private list", var->unparse(), PRIVATE_ANALYSIS_ADD_VAR, lstart->getPrivateListStatement());
				}
				if (extra == 0 && missing == 0) {
#if ACCAN_DEBUG
					Warning("Correct", "", 0, lstart->getPrivateListStatement());
#endif
				}
				//printf("PRIVATE VARS: ");
				//pri->print();
				//printf("DECLARATION: ");
				//p_pri->print();
				//printf("LAST PRIVATE VARS: ");
				//l_pri->print();          
			}
        }
		if (p == last)
			break;
        p = p->getLexNext();
    }
}

ControlFlowItem* doLoops::checkStatementForLoopEnding(int label, ControlFlowItem* last)
{

    if (current == NULL || label == -1 || label != current->getLabel())
        return last;
    return checkStatementForLoopEnding(label, endLoop(last));
}

doLoopItem* doLoops::findLoop(SgSymbol* s)
{
    doLoopItem* l = first;
    while (l != NULL){
        if (l->getName() == s)
            return l;
        l = l->getNext();
    }
    return NULL;
}

void doLoops::addLoop(int l, SgSymbol* s, ControlFlowItem* i, ControlFlowItem* e)
{
    doLoopItem* nl = new doLoopItem(l, s, i, e);
    if (first == NULL)
        first = current = nl;
    else{
        current->setNext(nl);
        current = nl;
    }
}

ControlFlowItem* doLoops::endLoop(ControlFlowItem* last)
{
    doLoopItem* removed = current;
    if (first == current)
        first = current = NULL;
    else{
        doLoopItem* prev = first;
        while (prev->getNext() != current)
            prev = prev->getNext();
        prev->setNext(NULL);
        current = prev;
    }
    last->AddNextItem(removed->getSourceForCycle());
    ControlFlowItem* empty = removed->getSourceForExit();
    delete removed;
    return empty;
}

VarSet* ControlFlowGraph::getPrivate()
{
	//printControlFlowList(first->getStart(), last->getStart());
    if (pri == NULL)
    {
        bool same = false;
        int it = 0;
        BaseBlock* p = first;
        /*
        printf("GRAPH:\n");
        while (p != NULL){
            printf("block %d: ", p->getNum());
            p->print();
            p = p->getLexNext();
        }
        */
        p = first;
        while (!same){
            p = first;
            same = true;
            while (p != NULL){
                same = p->stepMrdIn() && same;
                same = p->stepMrdOut() && same;
                p = p->getLexNext();
            }
            it++;
            //printf("iters: %d\n", it);
        }
        
        p = first;
        VarSet* res = new VarSet();
        VarSet* loc = new VarSet();
		bool il = false;
        while (p != NULL)
        {
            res->unite(p->getUse());
            loc->unite(p->getDef());
            p = p->getLexNext();
        }
        //printf("USE: ");
        //res->print();
        //printf("LOC: ");
        //loc->print();
        res->unite(loc);
        //printf("GETUSE: ");
        //getUse()->print();
        res->minus(getUse());
        pri = res;
    }
    return pri;
}

void ControlFlowGraph::liveAnalysis()
{
    bool same = false;
    int it = 0;
    BaseBlock* p = first;
    p = first;
    while (!same){
        p = last;
        same = true;
        while (p != NULL){
            same = p->stepLVOut() && same;
            same = p->stepLVIn() && same;
            p = p->getLexPrev();
        }
        it++;
        //printf("iters: %d\n", it);
    }
}

VarSet* ControlFlowGraph::getUse()
{
    if (use == NULL)
    {
        BaseBlock* p = first;
        VarSet* res = new VarSet();
        while (p != NULL)
        {
            VarSet* tmp = p->getUse();
            tmp->minus(p->getMrdIn());
            //printf("BLOCK %d INSTR %d USE: ", p->getNum(), p->getStart()->getStmtNo());
            //tmp->print();
            res->unite(tmp);
            p = p->getLexNext();
        }
        use = res;
    }
    return use;
}

VarSet* ControlFlowGraph::getDef()
{
	if (def == NULL) {
		def = new VarSet();
		def->unite(last->getMrdOut());
	}
	return def;
}

void BaseBlock::PrivateAnalysisForAllCalls()
{
	ControlFlowItem* p = start;
	while (p != NULL && (p == start || !p->isLeader())) {
		AnalysedCallsList* c = p->getCall();
		const char* oic = is_correct;
		is_correct = NULL;
		if (c != NULL && c->header != NULL && !c->hasBeenAnalysed) {
			c->graph->privateAnalyzer();
			c->hasBeenAnalysed = true;
		}
		is_correct = oic;
		p = p->getNext();
	}
	return;
}

ControlFlowItem* BaseBlock::containsParloopEnd()
{
    ControlFlowItem* p = start;
    while (p != NULL && (p == start || !p->isLeader())){
        if (p->IsParloopEnd())
            return p;
        p = p->getNext();
    }
    return NULL;
}

ControlFlowItem* BaseBlock::containsParloopStart()
{
    ControlFlowItem* p = start;
    while (p != NULL && (p == start || !p->isLeader())){
        if (p->IsParloopStart())
            return p;
        p = p->getNext();
    }
    return NULL;
}

void BaseBlock::print()
{
    printf("block %d: prev: ", num);
    BaseBlockItem* p = prev;
    while (p != NULL){
        printf("%d ", p->block->num);
        p = p->next;
    }
    printf("\n");
}

ControlFlowItem* BaseBlock::getStart()
{
    return start;
}

ControlFlowItem* BaseBlock::getEnd()
{
    ControlFlowItem* p = start;
    ControlFlowItem* end = p;
    while (p != NULL && (p == start || !p->isLeader())){
        end = p;
        p = p->getNext();
    }
    return end;
}

VarSet* BaseBlock::getLVOut()
{
    if (lv_out == NULL)
    {
        VarSet* res = new VarSet();
        BaseBlockItem* p = succ;
        bool first = true;
        while (p != NULL)
        {
            BaseBlock* b = p->block;
            if (b != NULL && !b->lv_undef)
            {
                res->unite(b->getLVIn());
            }
            p = p->next;
        }
        lv_out = res;
    }
    return lv_out;
}

VarSet* BaseBlock::getLVIn()
{
    if (lv_in == NULL)
    {
        VarSet* res = new VarSet();
        res->unite(getLVOut());
        res->minus(getDef());
        res->unite(getUse());
        lv_in = res;
    }
    return lv_in;
}

bool BaseBlock::stepLVOut()
{
    old_lv_out = lv_out;
    lv_out = NULL;
    getLVOut();
    lv_undef = false;
    //printf("block %d\n", num);
    //old_mrd_out->print();
    //mrd_out->print();
    return (lv_out->equal(old_lv_out));
    //return true;
}

bool BaseBlock::stepLVIn()
{
    old_lv_in = lv_in;
    lv_in = NULL;
    getLVIn();
    return (lv_in->equal(old_lv_in));
    //return true;
}

VarSet* BaseBlock::getMrdIn()
{
    if (mrd_in == NULL)
    {
        VarSet* res = new VarSet();
        BaseBlockItem* p = prev;
        bool first = true;
        while (p != NULL)
        {
            BaseBlock* b = p->block;
            if (b != NULL && !b->undef && b->hasPrev())
            {
                if (first){
                    res->unite(b->getMrdOut());
                    first = false;
                }
                else
                    res->intersect(b->getMrdOut());
            }
            p = p->next;
        }
        mrd_in = res;
    }
    return mrd_in;
}

bool BaseBlock::hasPrev()
{
    return prev_status == 1;
}

VarSet* BaseBlock::getMrdOut()
{
    if (mrd_out == NULL)
    {
        VarSet* res = new VarSet();
        res->unite(getMrdIn());
        res->unite(getDef());
        mrd_out = res;
        //printf("BLOCK %d INSTR %d MRDOUT: ", num, start->getStmtNo());
        //mrd_out->print();
        //print();
    }
    return mrd_out;
}

bool BaseBlock::stepMrdOut()
{
    old_mrd_out = mrd_out;
    mrd_out = NULL;
    getMrdOut();
    undef = false;
    //printf("block %d\n", num);
    //old_mrd_out->print();
    //mrd_out->print();
    return (mrd_out->equal(old_mrd_out));
    //return true;
}

bool BaseBlock::stepMrdIn()
{
    old_mrd_in = mrd_in;
    mrd_in = NULL;
    getMrdIn();
    return (mrd_in->equal(old_mrd_in));
    //return true;
}

void BaseBlock::addExprToUse(SgExpression* ex)
{
	if (ex != NULL)
	{
		SgFunctionCallExp* f = isSgFunctionCallExp(ex);
		if (!f) {
			addExprToUse(ex->lhs());
			if (!isSgUnaryExp(ex))
				addExprToUse(ex->rhs());
			SgVarRefExp* r;
			if ((r = isSgVarRefExp(ex)))
			{
				if (def == NULL || !def->belongs(r))
					use->addToSet(r);
			}
			return;
		}
		/*
		SgVarRefExp* r;
		//printf(" %s\n", f->funName()->identifier());
		bool intr = isIntrinsicFunctionName(f->funName()->identifier()) && !isUserFunction(f->funName());
		bool pure = IsPureProcedure(f->funName());
		if (!intr && !pure){
			printf("function not intristic or pure: %s\n", f->funName()->identifier());
			is_correct = false;
			return;
		}
		if (intr) {
			ProcessIntristicProcedure(true, f->numberOfArgs(), f);
			return;
		}
		ProcessProcedureHeader(true, isSgProcHedrStmt(GRAPHNODE(f->funName())->st_header), f);
		*/
    }
}

void BaseBlock::ProcessIntristicProcedure(bool isF, int narg, void* f)
{
	SgVarRefExp* r;
	for (int i = 0; i < narg; i++) {
		SgExpression* ar = GetProcedureArgument(isF, f, i);
		addExprToUse(ar);
	}
}

SgSymbol* BaseBlock::GetProcedureName(bool isFunc, void* f)
{
	if (isFunc) {
		SgFunctionCallExp* fc = (SgFunctionCallExp*)f;
		return fc->funName();
	}
	SgCallStmt* pc = (SgCallStmt*)f;
	return pc->name();
}

SgExpression* BaseBlock::GetProcedureArgument(bool isF, void* f, int i)
{
	if (isF) {
		SgFunctionCallExp* fc = (SgFunctionCallExp*)f;
		return fc->arg(i);
	}
	SgCallStmt* pc = (SgCallStmt*)f;
	return pc->arg(i);
}

void BaseBlock::ProcessProcedureHeader(bool isF, SgProcHedrStmt* header, void* f)
{
	SgVarRefExp* r;
	if (!header) {
		is_correct = "no header found";
		return;
	}
	for (int i = 0; i < header->numberOfParameters(); i++) {
		SgSymbol* arg = header->parameter(i);
		if (arg->attributes() & IN_BIT) {
			SgExpression* ar = GetProcedureArgument(isF, f, i);
			addExprToUse(ar);
		}
		else if (arg->attributes() & (OUT_BIT)) {
			SgVarRefExp* re;
			if ((re = isSgVarRefExp(GetProcedureArgument(isF, f, i)))) {
				def->addToSet(re);
			}
		}
		else if (arg->attributes() & (INOUT_BIT)) {
			addExprToUse(GetProcedureArgument(isF, f, i));
			SgVarRefExp* re;
			if ((re = isSgVarRefExp(GetProcedureArgument(isF, f, i))))
				def->addToSet(re);
		}
		else {
			is_correct = "no bitflag set for pure procedure";
			return;
		}
	}
}

bool AnalysedCallsList::isArgIn(int i)
{
	SgProcHedrStmt* h = isSgProcHedrStmt(header);
	VarSet* use = graph->getUse();
	SgSymbol* par = h->parameter(i);
	if (use->belongs(par))
		return true;
	return false;
}

bool AnalysedCallsList::isArgOut(int i)
{
	SgProcHedrStmt* h = isSgProcHedrStmt(header);
	VarSet* def = graph->getDef();
	SgSymbol* par = h->parameter(i);
	if (def->belongs(par))
		return true;
	return false;
}

void BaseBlock::ProcessUserProcedure(bool isFun, void* call, AnalysedCallsList* c)
{
	if (c == NULL || c->graph == NULL) {
		is_correct = "no body found for procedure";
		return;
	}
	c->graph->getPrivate(); //all sets actually
	SgProcHedrStmt* header = isSgProcHedrStmt(c->header);
	SgVarRefExp* r;
	if (!header) {
		is_correct = "no header for procedure";
		return;
	}
	for (int i = 0; i < header->numberOfParameters(); i++) {
		SgExpression* ar = GetProcedureArgument(isFun, call, i);
		if (c->isArgIn(i)) {
			addExprToUse(ar);
		}
		if (c->isArgOut(i)) {
			SgVarRefExp* re;
			if ((re = isSgVarRefExp(GetProcedureArgument(isFun, call, i)))) {
				def->addToSet(re);
			}
		}
	}
}

void BaseBlock::setDefAndUse()
{
    ControlFlowItem* p = start;
    while (p != NULL && (p == start || !p->isLeader()))
    {
        if (p->getJump() == NULL)
        {
            SgStatement* st = p->getStatement();
			SgFunctionCallExp* f = p->getFunctionCall();
			if (f != NULL)
			{
				bool intr = isIntrinsicFunctionName(f->funName()->identifier()) && !isUserFunction(f->funName());
				bool pure = IsPureProcedure(f->funName());
				if (!intr && !pure) {
					AnalysedCallsList* c = p->getCall();
					if (c->header == NULL) {
						is_correct = "no header for procedure";
					}
					else {
						ProcessUserProcedure(true, f, c);
					}
				}
				else if (intr) {
					ProcessIntristicProcedure(true, f->numberOfArgs(), f);
				}else
					ProcessProcedureHeader(true, isSgProcHedrStmt(GRAPHNODE(f->funName())->st_header), f);
			}
            if (st != NULL)
            {
                switch (st->variant())
                {
                case ASSIGN_STAT:
                    {
                        SgAssignStmt* s = isSgAssignStmt(st);
                        SgExpression* l = s->lhs();
                        SgExpression* r = s->rhs();
                        addExprToUse(r);
                        SgVarRefExp* re;
                        if ((re = isSgVarRefExp(l)))
                            def->addToSet(re);
						break;
                    }
				case PROC_STAT:
					{
						SgCallStmt* f = isSgCallStmt(st);
						SgVarRefExp* r;
						bool intr = isIntrinsicFunctionName(f->name()->identifier()) && !isUserFunction(f->name());
						bool pure = IsPureProcedure(f->name());
						if (!intr && !pure) {
							AnalysedCallsList* c = p->getCall();
							ProcessUserProcedure(false, f, c);
							break;
						}
						if (intr) {
							ProcessIntristicProcedure(false, f->numberOfArgs(), f);
							break;
						}
						ProcessProcedureHeader(false, isSgProcHedrStmt(GRAPHNODE(f->name())->st_header), f);
					}
                default:
                    break;
                }
            }
        }
        else
            addExprToUse(p->getExpression());
        p = p->getNext();
    }
}

VarSet* BaseBlock::getDef()
{
    if (def == NULL)
    {
        def = new VarSet();
        use = new VarSet();
        setDefAndUse();
    }
    return def;
}

VarSet* BaseBlock::getUse()
{
    if (use == NULL)
    {
        use = new VarSet();
        def = new VarSet();
        setDefAndUse();
    }
    return use;
}

bool VarSet::belongs(SgSymbol* var)
{
	VarItem* l = list;
	bool ex = false;
	while (l != NULL && !ex)
	{
		ex = (l->var->symbol() == var);
		l = l->next;
	}
	return ex;
}

bool VarSet::belongs(SgVarRefExp* var)
{
	return belongs(var->symbol());
}

bool VarSet::equal(VarSet* p2)
{
    if (p2 == NULL)
        return false;
    VarItem* p = list;
    VarItem* prev = NULL;
    while (p != NULL)
    {
        if (!p2->belongs(p->var))
            return false;
        p = p->next;
    }
    p = p2->list;
    while (p != NULL){
        if (!belongs(p->var))
            return false;
        p = p->next;
    }
    return true;
}

void VarSet::print()
{
    VarItem* l = list;
    while (l != NULL)
    {
        printf("%s, ", l->var->unparse());
        l = l->next;
    }
    putchar('\n');
}

void VarSet::addToSet(SgVarRefExp* var)
{
    if (!belongs(var))
    {
        VarItem* n = new VarItem();
        n->var = var;
        n->next = list;
        list = n;
    }
}

void VarSet::intersect(VarSet* set)
{
    VarItem* p = list;
    VarItem* prev = NULL;
    while (p != NULL)
    {        
        if (!set->belongs(p->var))
        {
            if (prev == NULL)
                list = list->next;
            else
            {
                prev->next = p->next;
                delete(p);
                p = prev;
            }
        }
        else
            prev = p;

        p = p->next;
    }
    
}

SgVarRefExp* VarSet::getFirst()
{
	if (list != NULL)
		return list->var;
	return NULL;
}

void VarSet::remove(SgVarRefExp* var)
{
	VarItem* p = list;
	VarItem* prev = NULL;
	while (p != NULL)
	{
		if (var == (p->var))
		{
			if (prev == NULL) {
				VarItem* t = list;
				list = list->next;
				delete(t);
				p = list;

			}
			else
			{
				prev->next = p->next;
				delete(p);
				p = prev->next;
			}
		}
		else {
			prev = p;
			p = p->next;
		}
	}
}

void VarSet::minus(VarSet* set)
{
    VarItem* p = list;
    VarItem* prev = NULL;
    while (p != NULL)
    {
        if (set->belongs(p->var))
        {
            if (prev == NULL)
                list = list->next;
            else
            {
                prev->next = p->next;
                delete(p);
                p = prev;
            }
        }
        else
            prev = p;

        p = p->next;
    }
}

void VarSet::unite(VarSet* set)
{
    VarItem* arg2 = set->list;
    while (arg2 != NULL)
    {
        if (!belongs(arg2->var))
        {
            VarItem* n = new VarItem();
            n->var = arg2->var;
            n->next = list;
            list = n;
        }

        arg2 = arg2->next;
    }
}



void BaseBlock::addToPrev(BaseBlock* bb)
{
    BaseBlockItem* n = new BaseBlockItem();
    n->block = bb;
    n->next = prev;
    prev = n;
}

void BaseBlock::addToSucc(BaseBlock* bb)
{
    BaseBlockItem* n = new BaseBlockItem();
    n->block = bb;
    n->next = succ;
    succ = n;
}

#if ACCAN_DEBUG

void ControlFlowItem::printDebugInfo()
{
	if (jmp == NULL && stmt == NULL && func != NULL)
		printf("FUNCTION CALL: %s\n", func->unparse());
    if (jmp == NULL)
        if (stmt != NULL)
            if (label != NULL)
                printf("%d: %s %s %s lab %4d %s", stmtNo, this->isLeader() ? "L" : " ", this->IsParloopStart() ? "S" : " ", this->IsParloopEnd() ? "E" : " ", label->id(), stmt->unparse());
            else
                printf("%d: %s %s %s          %s", stmtNo, this->isLeader() ? "L" : " ", this->IsParloopStart() ? "S" : " ", this->IsParloopEnd() ? "E" : " ", stmt->unparse());
        else
            if (label != NULL)
                printf("%d: %s %s %s lab %4d \n", stmtNo, this->isLeader() ? "L" : " ", this->IsParloopStart() ? "S" : " ", this->IsParloopEnd() ? "E" : " ", label->id());
            else
                printf("%d: %s %s %s         \n", stmtNo, this->isLeader() ? "L" : " ", this->IsParloopStart() ? "S" : " ", this->IsParloopEnd() ? "E" : " ");
    else
        if (expr == NULL)
            if (label != NULL)
                printf("%d: %s %s %s lab %4d       goto %d\n", stmtNo, this->isLeader() ? "L" : " ", this->IsParloopStart() ? "S" : " ", this->IsParloopEnd() ? "E" : " ", label->id(), jmp->getStmtNo());
            else
                printf("%d: %s %s %s                goto %d\n", stmtNo, this->isLeader() ? "L" : " ", this->IsParloopStart() ? "S" : " ", this->IsParloopEnd() ? "E" : " ", jmp->getStmtNo());
        else
            if (label != NULL)
                printf("%d: %s %s %s lab %4d       if %s goto %d\n", stmtNo, this->isLeader() ? "L" : " ", this->IsParloopStart() ? "S" : " ", this->IsParloopEnd() ? "E" : " ", label->id(), expr->unparse(), jmp->getStmtNo());
            else
                printf("%d: %s %s %s                if %s goto %d\n", stmtNo, this->isLeader() ? "L" : " ", this->IsParloopStart() ? "S" : " ", this->IsParloopEnd() ? "E" : " ", expr->unparse(), jmp->getStmtNo());
}

static void printControlFlowList(ControlFlowItem* list, ControlFlowItem* last)
{

    printf("DEBUG PRINT START\n");
    unsigned int stmtNo = 0;
    ControlFlowItem* list_copy = list;
    while (list != NULL )
    {
        list->setStmtNo(++stmtNo);
		if (list == last)
			break;
        list = list->getNext();
    }

    list = list_copy;
    while (list != NULL)
    {
        list->printDebugInfo();
		if (list == last)
			break;
        list = list->getNext();
    }
    printf("DEBUG PRINT END\n\n");
}
#endif

void CallData::printControlFlows()
{
#if ACCAN_DEBUG
	AnalysedCallsList* l = calls_list;
	while (l != NULL) {
		if (!l->isIntristic && l->graph != NULL && l->header != NULL) {
			ControlFlowGraph* g = l->graph;
			SgStatement* h = l->header;
			printf("CFI for %s\n\n" ,h->symbol()->identifier());
			if (g != NULL) {
				printControlFlowList(g->getCFI());
			}
			else
				printf("ERROR: DOES NOT HAVE CFI\n");
		}
		l = l->next;
	}
#endif
}