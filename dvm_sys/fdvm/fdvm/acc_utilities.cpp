/*****************************/
/* all general functions     */
/*****************************/

#include "acc_data.h"

// copy input string to another buffer 
char *copyOfUnparse(const char *strUp)
{
    char *str;
    str = new char[strlen(strUp) + 1];
    strcpy(str, strUp);
    return str;
}

// convert "str " to "STR "
char* aks_strupr(const char *str)
{
    char *tmpstr = new char[strlen(str) + 1];
    tmpstr[0] = '\0';
    strcat(tmpstr, str);
    for (size_t i = 0; i < strlen(tmpstr); ++i)
    {
        if (tmpstr[i] <= 'z' && tmpstr[i] >= 'a')
            tmpstr[i] += 'A' - 'a';
    }
    return tmpstr;
}

// convert "STR" to "str"
char* aks_strlowr(const char *str)
{
    char *tmpstr = new char[strlen(str) + 1];
    tmpstr[0] = '\0';
    strcat(tmpstr, str);
    for (size_t i = 0; i < strlen(tmpstr); ++i)
    {
        if (tmpstr[i] <= 'Z' && tmpstr[i] >= 'A')
            tmpstr[i] -= 'A' - 'a';
    }
    return tmpstr;
}

// correct private list after CUDA kernel generation
void correctPrivateList(int flag)
{
    if (newVars.size() != 0)
    {
        if (flag == RESTORE)
        {
            if (private_list)
            {
                for (size_t i = 0; i < newVars.size(); ++i)
                    private_list = private_list->rhs();
            }
        }
        else if (flag == ADD)
        {
            for (size_t i = 0; i < newVars.size(); ++i)
            {
                SgExprListExp *e = new SgExprListExp(*new SgVarRefExp(*newVars[i]));
                e->setRhs(private_list);
                private_list = e;
            }
        }
    }
}

// create kernel call functions from HOST: skernel<<< specs>>>( args)
SgFunctionCallExp *cudaKernelCall(SgSymbol *skernel, SgExpression *specs, SgExpression *args = NULL)
{
    SgExpression *fe = new SgExpression(ACC_CALL_OP);
    fe->setSymbol(*skernel);
    fe->setRhs(*specs);
    if (args)
        fe->setLhs(*args);
    
    return (SgFunctionCallExp *)fe;
}

// create FORTRAN index type in kernel: integer*4 if rt_INT or 
// integer*8 if rt_LONG, rt_LLONG   
static SgType *FortranIndexType(int rtType)
{
    SgType *type = NULL;

    if (rtType == rt_INT)
    {
        SgExpression *le = new SgExpression(LEN_OP);
        le->setLhs(new SgValueExp(4));
        type = new SgType(T_INT, le, SgTypeInt());
    }
    else if (rtType == rt_LONG || rtType == rt_LLONG)
    {
        SgExpression *le = new SgExpression(LEN_OP);
        le->setLhs(new SgValueExp(8));
        type = new SgType(T_INT, le, SgTypeInt());
    }
    return type;
}

// create cuda index type in kernel for FORTRAN and C
SgType *indexTypeInKernel(int rt_Type)
{    
    SgType *ret = NULL;

    if (indexType_int == NULL)
    {
        s_indexType_int = new SgSymbol(TYPE_NAME, "__indexTypeInt", options.isOn(C_CUDA) ? *block_C_Cuda : *mod_gpu);
        s_indexType_int->setType(new SgDescriptType(*SgTypeInt(), BIT_TYPEDEF));
        if (options.isOn(C_CUDA))
            indexType_int = C_Derived_Type(s_indexType_int);
        else
        {
            SgExpression *le = new SgExpression(LEN_OP);
            le->setLhs(new SgValueExp(4));
            indexType_int = new SgType(T_INT, new SgVariableSymb("_int", *FortranIndexType(rt_INT), *mod_gpu), le, SgTypeInt());
        }
    }
    
    if (indexType_long == NULL)
    {
        s_indexType_long = new SgSymbol(TYPE_NAME, "__indexTypeLong", options.isOn(C_CUDA) ? *block_C_Cuda : *mod_gpu);
        s_indexType_long->setType(C_LongType());
        if (options.isOn(C_CUDA))
            indexType_long = C_Derived_Type(s_indexType_long);
        else
        {
            SgExpression *le = new SgExpression(LEN_OP);
            le->setLhs(new SgValueExp(8));
            indexType_long = new SgType(T_INT, new SgVariableSymb("_long", *FortranIndexType(rt_LONG), *mod_gpu), le, SgTypeInt());
        }
    }

    if (indexType_llong == NULL)
    {
        s_indexType_llong = new SgSymbol(TYPE_NAME, "__indexTypeLLong", options.isOn(C_CUDA) ? *block_C_Cuda : *mod_gpu);
        s_indexType_llong->setType(C_LongLongType());
        if (options.isOn(C_CUDA))
            indexType_llong = C_Derived_Type(s_indexType_llong);
        else
        {
            SgExpression *le = new SgExpression(LEN_OP);
            le->setLhs(new SgValueExp(8));
            indexType_llong = new SgType(T_INT, new SgVariableSymb("_llong", *FortranIndexType(rt_LLONG), *mod_gpu), le, SgTypeInt());            
        }
    }
    
    if (rt_Type == rt_INT)
        ret = indexType_int;
    else if (rt_Type == rt_LONG)
        ret = indexType_long;
    else if (rt_Type == rt_LLONG)
        ret = indexType_llong;

    return ret;
}

// declare DO variables of parallel loop nest in kernel by indexType: rt_INT, rt_LONG, rt_LLONG
void DeclareDoVars(SgType *indexType)
{
    SgStatement *st;
    SgExpression *vl, *el;

    // declare do_variables of parallel loop nest
    if (options.isOn(C_CUDA))
    {
        vl = &(dvm_parallel_dir->expr(2))->copy(); // do_variables list copy  
        for (el = vl; el; el = el->rhs())
            (el->lhs())->setSymbol(new SgVariableSymb(el->lhs()->symbol()->identifier(), *indexType, *kernel_st));
        st = Declaration_Statement(vl->lhs()->symbol());   // of CudaIndexType
        st->setExpression(0, *vl);
        kernel_st->insertStmtAfter(*st);
        st->addComment("// Local needs");
    }
    else // Fortran-Cuda
    {
        st = indexType->symbol()->makeVarDeclStmt();   // of CudaIndexType
        kernel_st->insertStmtAfter(*st);
        vl = dvm_parallel_dir->expr(2); // do_variables list
        st->setExpression(0, vl->copy());
        st->addComment("! Local needs\n");
    }
}


// create dvm coefficient:*0001, *0002 by indexType: rt_INT, rt_LONG, rt_LLONG
static SgExpression *dvm_coef(SgSymbol *ar, int i, SgType *indeTypeInKernel)
{
    SgVarRefExp *ret = NULL;
    if (options.isOn(C_CUDA))
    {
        SgSymbol *s_dummy_coef = new SgSymbol(VARIABLE_NAME, AR_COEFFICIENTS(ar)->sc[i]->identifier(), *indeTypeInKernel, *kernel_st);
        ret = new SgVarRefExp(*s_dummy_coef);
    }
    else
        ret = new SgVarRefExp(*(AR_COEFFICIENTS(ar)->sc[i]));
    return ret;
}

// create array list by indexType: rt_INT, rt_LONG, rt_LLONG
SgExpression *CreateArrayDummyList(SgType *indeTypeInKernel)
{
    symb_list *sl;
    SgExpression *ae, *coef_list, *edim;
    int n, d;
    SgExpression *arg_list = NULL;

    edim = new SgExprListExp();  // [] dimension

    for (sl = acc_array_list; sl; sl = sl->next)   // + base_ref + <array_coeffs>
    {
        SgSymbol *s_dummy;
        s_dummy = KernelDummyArray(sl->symb);
        if (options.isOn(C_CUDA))
            ae = new SgArrayRefExp(*s_dummy, *edim);
        else
            ae = new SgArrayRefExp(*s_dummy);
        ae->setType(s_dummy->type());   //for C_Cuda
        ae = new SgExprListExp(*ae);
        
        arg_list = AddListToList(arg_list, ae);
        coef_list = NULL;
        if (Rank(sl->symb) == 0)      //remote_access buffer may be of rank 0   
            continue;
        d = options.isOn(AUTO_TFM) ? 0 : 1;
        for (n = Rank(sl->symb) - d; n>0; n--)
        {
            ae = new SgExprListExp(*dvm_coef(sl->symb, n + 1, indeTypeInKernel));
            coef_list = AddListToList(coef_list, ae);
        }

        arg_list = AddListToList(arg_list, coef_list);
    }
    return(arg_list);

}


// create local parts of array list by indexType: rt_INT, rt_LONG, rt_LLONG
SgSymbol *KernelDummyLocalPart(SgSymbol *s, SgType *indeTypeInKernel)
{
    SgArrayType *typearray;
    SgType *type;

    // for C_Cuda
    typearray = new SgArrayType(*indeTypeInKernel);
    typearray->addDimension(NULL);
    type = typearray;

    return(new SgSymbol(VARIABLE_NAME, s->identifier(), *type, *kernel_st));

}

SgExpression *CreateLocalPartList(SgType *indeTypeInKernel)
{
    local_part_list *pl;
    SgExpression *ae;
    SgExpression *arg_list = NULL;
    for (pl = lpart_list; pl; pl = pl->next) // + <local_part>
    {
        if (options.isOn(C_CUDA))
            ae = new SgExprListExp(*new SgArrayRefExp(*KernelDummyLocalPart(pl->local_part, indeTypeInKernel), 
                                   *new SgExprListExp())); //<local_part>[]
        else
            ae = new SgExprListExp(*new SgArrayRefExp(*pl->local_part));
        arg_list = AddListToList(arg_list, ae);
    }
    return(arg_list);

}

// create two kernel calls (for rt_INT and rt_LLONG) in CUDA_handeler by base kernel function. 
// return if(rt_INT) kernel<<< >>>() else kernel2<<< >>>()
SgStatement* createKernelCallsInCudaHandler(SgFunctionCallExp *baseFunc, SgSymbol *s_loop_ref, SgSymbol *idxTypeInKernel, SgSymbol *s_blocks)
{
    SgStatement *stmt = NULL;
    std::string fcall_INT = baseFunc->symbol()->identifier();
    std::string fcall_LLONG = baseFunc->symbol()->identifier();
    fcall_INT += "_int";
    fcall_LLONG += "_llong";

    SgExpression *args = baseFunc->args();

    SgFunctionCallExp *funcCall_int = cudaKernelCall(new SgSymbol(VARIABLE_NAME, fcall_INT.c_str()), baseFunc->rhs());
    SgFunctionCallExp *funcCall_llong = cudaKernelCall(new SgSymbol(VARIABLE_NAME, fcall_LLONG.c_str()), baseFunc->rhs());
    
    while (args)
    {
        bool flag = false;
        if (args->lhs()->symbol())
        {
            if (strcmp(args->lhs()->symbol()->identifier(), "blocks_info") == 0)
            {
                funcCall_int->addArg(*new SgCastExp(*C_PointerType(indexTypeInKernel(rt_INT)), *args->lhs()));
                funcCall_llong->addArg(*new SgCastExp(*C_PointerType(indexTypeInKernel(rt_LLONG)), *args->lhs()));
                flag = true;
            }

            if (args->lhs()->getAttribute(0) != NULL)
            {
                SgAttribute *att = args->lhs()->getAttribute(0);
                if (att->getAttributeSize() == 777)
                {
                    funcCall_int->addArg(*new SgCastExp(*C_PointerType(indexTypeInKernel(rt_INT)), *args->lhs()));
                    funcCall_llong->addArg(*new SgCastExp(*C_PointerType(indexTypeInKernel(rt_LLONG)), *args->lhs()));
                    flag = true;
                    args->lhs()->deleteAttribute(0);
                }
            }
        }

        if (flag == false)
        {
            funcCall_int->addArg(*args->lhs());
            funcCall_llong->addArg(*args->lhs());
        }
        args = args->rhs();
    }

    if (options.isOn(RTC))
    {
        SgFunctionCallExp *rtc_FCall_INT = new SgFunctionCallExp(*createNewFunctionSymbol("loop_cuda_rtc_launch"));
        rtc_FCall_INT->addArg(*new SgVarRefExp(s_loop_ref));
        rtc_FCall_INT->addArg(*new SgValueExp(fcall_INT.c_str()));
        rtc_FCall_INT->addArg(*new SgVarRefExp(*new SgSymbol(VARIABLE_NAME, fcall_INT.c_str())));
        rtc_FCall_INT->addArg(SgAddrOp(*new SgVarRefExp(s_blocks)));
        rtc_FCall_INT->addArg(*new SgValueExp(baseFunc->numberOfArgs()));

        RTC_FArgs.push_back(baseFunc->args());
        RTC_FCall.push_back(rtc_FCall_INT);

        SgFunctionCallExp *rtc_FCall_LLONG = new SgFunctionCallExp(*createNewFunctionSymbol("loop_cuda_rtc_launch"));
        rtc_FCall_LLONG->addArg(*new SgVarRefExp(s_loop_ref));
        rtc_FCall_LLONG->addArg(*new SgValueExp(fcall_LLONG.c_str()));
        rtc_FCall_LLONG->addArg(*new SgVarRefExp(*new SgSymbol(VARIABLE_NAME, fcall_LLONG.c_str())));
        rtc_FCall_LLONG->addArg(SgAddrOp(*new SgVarRefExp(s_blocks)));
        rtc_FCall_LLONG->addArg(*new SgValueExp(baseFunc->numberOfArgs()));

        RTC_FArgs.push_back(baseFunc->args());
        RTC_FCall.push_back(rtc_FCall_LLONG);
    }

    if (options.isOn(RTC))
        stmt = new SgIfStmt(SgEqOp(*new SgVarRefExp(*idxTypeInKernel), *new SgVarRefExp(*new SgSymbol(VARIABLE_NAME, "rt_INT"))),
        *new SgCExpStmt(*RTC_FCall[RTC_FCall.size() - 2]), *new SgCExpStmt(*RTC_FCall[RTC_FCall.size() - 1]));
    else
        stmt = new SgIfStmt(SgEqOp(*new SgVarRefExp(*idxTypeInKernel), *new SgVarRefExp(*new SgSymbol(VARIABLE_NAME, "rt_INT"))),
        *new SgCExpStmt(*funcCall_int), *new SgCExpStmt(*funcCall_llong));
    return stmt;
}