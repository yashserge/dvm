/**************************************************************\
* Fortran DVM                                                  * 
*                                                              *
*            Input/Output Statements Processing                *
\**************************************************************/

#include "dvm.h"

int TestIOList(SgExpression *iol,SgStatement *stmt)
{SgExpression *el,*e;
int tst=1;
for (el=iol;el;el=el->rhs()) {    
   e = el->lhs();  // list item
   ReplaceFuncCall(e); 
   if(isSgExprListExp(e)) // implicit loop in output list
     e = e->lhs();
   if(isSgIOAccessExp(e)) {
      tst=ImplicitLoopTest(e,stmt) ? tst : 0; 
   }
   else 
      tst=IOitemTest(e,stmt) ? tst : 0;     
 }
return (tst);
}

int ImplicitLoopTest(SgExpression *eim,SgStatement *stmt)
{int tst =1;
 SgExpression *ell, *e;
 if(isSgExprListExp(eim->lhs()))
   for (ell = eim->lhs();ell;ell=ell->rhs()){ //looking through item list of implicit loop
     e = ell->lhs();
     if(isSgExprListExp(e)) // implicit loop in output list
       e = e->lhs();
     if(isSgIOAccessExp(e)){
       tst=ImplicitLoopTest(e,stmt) ? tst : 0; 
     }
     else 
       tst=IOitemTest(e,stmt) ? tst : 0;    
   }
 else
       tst=IOitemTest(eim->lhs(),stmt) ? tst : 0;   
 return(tst);
}

int IOitemTest(SgExpression *e,SgStatement *stmt)
{int tst=1;
 if(!e) return(1);
 if(isSgArrayRefExp(e)){
   if( HEADER(e->symbol())) {
       Error("Illegal I/O list item: %s",e->symbol()->identifier(),192,stmt);
       return (0);
   } else
       return(1);
 }
 if(e->variant()  == ARRAY_OP) //substring
       return(IOitemTest(e->lhs(),stmt));
 if(isSgVarRefExp(e) || isSgValueExp(e))
    return(1);
 tst=IOitemTest(e->lhs(),stmt) ? tst : 0;  
 tst=IOitemTest(e->rhs(),stmt) ? tst : 0;   
 return(tst);
}      

SgStatement *Any_IO_Statement(SgStatement *stmt) //, int flag_linenum
{ SgStatement *last;
            ReplaceContext(stmt); 
            if(!IN_COMPUTE_REGION)
               LINE_NUMBER_BEFORE(stmt,stmt);
              	    
            if(perf_analysis){
               InsertNewStatementBefore(St_Biof(),stmt);
               InsertNewStatementAfter ((last = St_Eiof()),stmt,stmt->controlParent());
               cur_st = stmt;
               return(last); 
            } 
            return(stmt);
}

void OpenClose_Statement(SgStatement *stmt, int error_msg)
{SgExpression *ioc[30]; 
 int io_err;
            send = 0;
            io_err=control_list_open(stmt->expr(1),ioc); // control_list analisys
            if(!io_err && error_msg ) {
              err("Illegal elements in control list", 185,stmt);
              return;
            } 
            if( ioc[ERR_] && error_msg)
               err("END= and ERR= specifiers are illegal in FDVM", 186,stmt);
                          
            Any_IO_Statement(stmt);
            if(!ioc[IOSTAT_])  // Keyed argument IOSTAT is absent
              ReplaceByIfStmt(stmt);  
            else {
              InsertSendIOSTAT(ioc[IOSTAT_]);
              ReplaceByIfStmt(stmt); 
              if(inparloop && error_msg)
                err("Illegal I/O statement in the range of parallel loop", 184,stmt);
            }
            return; 

}

void Inquiry_Statement(SgStatement *stmt, int error_msg)
{SgExpression *ioc[30];
 int io_err;
            send = 0;
            io_err=control_list_open(stmt->expr(1),ioc);
                                                   // control_list analisys
            if(!io_err && error_msg ) {
              err("Illegal elements in control list", 185,stmt);
              return;
            }
            if(ioc[ERR_] && error_msg){
               err("END= and ERR= specifiers are illegal in FDVM", 186,stmt);
               return;
            }
            Any_IO_Statement(stmt);
            InsertSendInquire(ioc);
            ReplaceByIfStmt(stmt); 
            if(inparloop && error_msg)
              err("Illegal I/O statement in the range of parallel loop", 184,stmt);
            
}
           
void FilePosition_Statement(SgStatement *stmt, int error_msg)
// BACKSPASE, REWIND, ENDFILE statements
{SgExpression *ioc[10];
 int io_err;
            send = 0;
            io_err = control_list1(stmt->expr(1),ioc); // control_list analisys
            if(!io_err && error_msg) {
              err("Illegal elements in control list", 185,stmt);
              return;
            }
            if((ioc[END_] || ioc[ERR_]) && error_msg)
              err("END= and ERR= specifiers are not allowed in FDVM", 186,stmt);   
            Any_IO_Statement(stmt);              
            if(!ioc[IOSTAT_])  // Keyed argument IOSTAT is absent
               ReplaceByIfStmt(stmt);  
            else {
            ReplaceContext(stmt);
            InsertSendIOSTAT(ioc[IOSTAT_]);
            ReplaceByIfStmt(stmt); 
            if(inparloop && error_msg)
             err("Illegal I/O statement in the range of parallel loop", 184,stmt);
            }
            return; 
} 

void ReadWritePrint_Statement(SgStatement *stmt, int error_msg)
// READ, WRITE, PRINT statements

{           SgSymbol *sio;
            SgExpression *e,*iol;
            SgExpression *ioc[10];
            int IOtype, io_err;
            
            send = 0;  
            // analizes IO control list and sets on ioc[]                       
            e = stmt->expr(1); // IO control
            io_err = IOcontrol(e,ioc,stmt->variant());           
            if(!io_err && error_msg){
               err("Illegal elements in control list", 185,stmt);
               return;
            }
            if((ioc[END_] || ioc[ERR_] || ioc[EOR_]) && error_msg) {
               err("END=, EOR= and ERR= specifiers are illegal in FDVM", 186,stmt);
               return;
            }
            
            if(ioc[UNIT_] && (ioc[UNIT_]->type()->variant() == T_STRING)) {
               SgKeywordValExp *kwe;
               if((kwe=isSgKeywordValExp(ioc[UNIT_])) && (!strcmp(kwe->value(),"*")))
                                                                       //"*" - system unit
                         ;
               else { // I/O to internal file           
                 if(ioc[UNIT_]->symbol() && HEADER(ioc[UNIT_]->symbol()) && error_msg)
                   Error("'%s' is distributed array", ioc[UNIT_]->symbol()->identifier(),   148,stmt);
                 if(error_msg)   
                   TestIOList(stmt->expr(0),stmt);
                     //err("I/O to internal file is not supported in FDVM", stmt);
                 return;
               }  
	    }   
            
            // analizes format specifier and determines type of I/O
            if(ioc[FMT_]) {
            
               SgKeywordValExp * kwe;
               kwe = isSgKeywordValExp(ioc[FMT_]);
               if(kwe) // Format
                  if(!strcmp(kwe->value(),"*"))
                     IOtype = 1; // formatted IO, controlled by IO-list  
                  else {
                     IOtype = 0; // illegal format specifier ??
                     if(error_msg) 
                       err("Invalid format specification", 189,stmt);
                     return;
                  }
               else
                     IOtype = 2; // formatted IO, controlled by format
                                 // specification or NAMELIST
            }
            else
                     IOtype = 3; // unformatted IO
            if(ioc[NML_])
              IOtype = 2; // formatted IO, controlled by  NAMELIST

            Any_IO_Statement(stmt);

           //looking through the IO-list
            iol = stmt->expr(0);
            if(!iol) {  // input list is absent 
              if(ioc[IOSTAT_]) 	
                 InsertSendIOSTAT(ioc[IOSTAT_]);              
              ReplaceByIfStmt(stmt);  
              return; 
            } 
             if((e = isSgArrayRefExp(iol->lhs())) &&  (HEADER(iol->lhs()->symbol()))) {
                                  // first item is distributed array refference
                if (iol->rhs() && error_msg)  {// there are other items in I/O-list
                  
                   err("Illegal I/O list ", 190,stmt);  
                   return;
                }
                if(ioc[IOSTAT_] && error_msg) {
                  err("IOSTAT= specifier is illegal in I/O of distributed array", 187,stmt);
                   return;
                 }
                if(!e->lhs())  //whole array
                   if(IOtype != 2) { 
                       sio = e->symbol();           
                           //buf_use[TypeIndex(sio->type()->baseType())] = 1; 
                       IO_ThroughBuffer(sio,stmt);
                       if(IN_COMPUTE_REGION && !inparloop && !in_checksection && error_msg)   
                         err("Illegal statement in the range of region (not implemented yet)", 576,stmt); 
                    }
                    else {
                       if( error_msg)
                         err("I/O of distributed array controlled by format specification or NAMELIST is not supported in FDVM", 191,stmt);
                         // illegal format specifier for I/O of distributed array
                       return; 
                    }
                else { 
                   if(error_msg)
                     err("Illegal I/O list item", 192,stmt);  
                   return;
                }   
             }
             else { // replicated variable list
	       if(!TestIOList(iol,stmt))
                  return;                                
                if (ioc[IOSTAT_] || (stmt->variant() == READ_STAT)) {
		   
                   if(stmt->variant() == READ_STAT)
                      InsertSendInputList(iol,ioc[IOSTAT_],stmt);
                   else
                      InsertSendIOSTAT(ioc[IOSTAT_]);
                }
                ReplaceByIfStmt(stmt);
                //if(IN_COMPUTE_REGION && !in_checksection)
                //  ChangeDistArrayRef(iol);
             }
             if(inparloop && (send || IN_COMPUTE_REGION) && error_msg)
               err("Illegal I/O statement in the range of parallel loop", 184,stmt);
             
}

void IO_ThroughBuffer(SgSymbol *ar, SgStatement *stmt)
{
 SgStatement *dost, *contst,*ifst;
 SgExpression *esize,*econd,*iodo, *iolist,*ubound,*are,*d, *eN[8];
 SgValueExp c1(1),c0(0);
 SgLabel *loop_lab; 
 //SgSymbol *sio;
 int i,l,rank,s,s0,N[8],itype,imem;
 int m = -1;
 int init,last,step;
 int M=0;
 
 contst = NULL;
 imem=ndvm;
 ReplaceContext(stmt);
 
 itype = TypeIndex(ar->type()->baseType());
 if(itype == -1)  //may be derived type 
 {
    Error("Illegal type's array in input-output statement: %s",ar->identifier(),999,stmt);
    return;
 } else
    buf_use[itype] = 1;
 l = rank = Rank(ar);  
 s = IOBufSize;  //SIZE_IO_BUF;
 for(i=1; i<=rank; i++) {
    //calculating size of i-th dimension
    esize = ReplaceParameter(ArrayDimSize(ar, i));
    eN[i] = NULL;
    if(esize && esize->variant()==STAR_RANGE)
    {
      Error("Assumed-size array: %s",ar->identifier(),162,stmt);
      return;
    }
    if(esize->isInteger())
      N[i] = esize->valueInteger();
    else 
      {N[i] = -1; eN[i] = esize;} //!! dummy argument 
    if((N[i] <= 0) && !eN[i])
    {
      Error("Array shape declaration error: '%s'", ar->identifier(),193, stmt);
      return;
    }
  }
    // calculating s
 for(i=1; i<=rank; i++) {
    if(eN[i]) {
      l=i-1;
      break;
    }
    s0 = s / N[i];
    if(!s0) { // s0 == 0
      l = i-1;  
      break;
    }
    else
      s = s0;
 }
 if(l==rank) { // generating assign statement: m = 1
   // m = ndvm;
   //doAssignStmtBefore(&c1.copy(),stmt); 
   M=1;
 }
 else
   m = ndvm++;

 if(l+1 <= rank) {
 // generating DO statement: DO label idvm01 = 0, N[l+1]-1, s 
          
   loop_lab = GetLabel();
   contst = new SgStatement(CONT_STAT);
   esize = eN[l+1] ? &(eN[l+1]->copy() - c1.copy()) : new SgValueExp(N[l+1]-1);
   dost= new SgForStmt(*loop_var[1], c0.copy(), *esize, *new SgValueExp(s), *contst);
   BIF_LABEL_USE(dost->thebif) = loop_lab->thelabel;
   (dost->lexNext())->setLabel(*loop_lab); 
 
   if(l+2 <= rank)   
     // generating DO nest:
     // DO label idvm02 = 0, N[rank]-1
     // DO label idvm03 = 0, N[rank-1]-1
     //   .  .  .
     // DO label idvm0j = 0, N[l+2]-1

                                       //for(i=rank; i>l+1; i--) {  //27.11.09
     for(i=l+2; i<=rank; i++) {
        esize = eN[i] ? &(eN[i]->copy() - c1.copy()) : new SgValueExp(N[i]-1);
        dost= new SgForStmt(*loop_var[rank-i+2], c0.copy(), *esize, *dost);  
                         
        BIF_LABEL_USE(dost->thebif) = loop_lab->thelabel;
     }

   cur_st->insertStmtAfter(*dost);

   for(i=l+1; i<=rank; i++)
      contst->lexNext()->extractStmt(); // extracting ENDDO

   if((N[l+1]<0) || (N[l+1]-(N[l+1]/s)*s)) {  
   // generating the construction
   // IF (Il+1 + s .LE. Nl+1) THEN 
   //    m = s
   // ELSE 
   //    m = Nl+1 - Il+1
   // ENDIF 
   // and then insert it before CONTINUE statement
     esize = eN[l+1] ? &(eN[l+1]->copy()) : new SgValueExp(N[l+1]);
     econd = & (( *new SgVarRefExp(*loop_var[1]) + *new SgValueExp(s)) <= *esize);
     ifst = new SgIfStmt(*econd,                                                                     *new SgAssignStmt(*DVM000(m),*new SgValueExp(s)),                   *new SgAssignStmt(*DVM000(m),*esize - *new SgVarRefExp(*loop_var[1])));
     contst -> insertStmtBefore(*ifst);
   }
   else 
     
     //dost->insertStmtBefore(*new SgAssignStmt(*DVM000(m),*new SgValueExp(s)));     
     M=s;
   //cur_st = ifst;   
   stmt->extractStmt();
   contst -> insertStmtBefore(*stmt);
   // transfering label over D0-statements
   BIF_LABEL(dost->thebif) = BIF_LABEL(stmt->thebif);
   BIF_LABEL(stmt->thebif) = NULL;
   //cur_st = stmt;
 }
 // creating implicit loop as element of I/O list:
 // (BUF(I0), I0= 1,N1*...*Nl*m)
 ubound = DVM000(m);
 N[0] = 1; 
 for(i=1; i<=l; i++)
      N[0] = N[0] * N[i];
 if(M)   // M= const
   ubound = new SgValueExp(N[0]*M);   
 else {
   ubound = DVM000(m);
   if(N[0]  != 1) 
   ubound = &( *ubound * (*new SgValueExp(N[0])) );   
 }   

 //   ubound = &( *ubound * (*new SgValueExp(N[0])));   
// iodo = new SgExpression(DDOT,&c1.copy(), ubound,NULL);
 iodo = & SgDDotOp(c1.copy(),*ubound);
 iodo = new SgExpression(SEQ,iodo,NULL,NULL);
 iodo = new  SgExpression(IOACCESS,NULL,iodo,loop_var[0]);
// iodo = new SgIOAccessExp(*loop_var[0], c1.copy(), *ubound);//Sage error
 iodo -> setLhs(new SgArrayRefExp(*bufIO[itype],                                                              *new SgVarRefExp(*loop_var[0])));
 iolist = new SgExprListExp(*iodo);
 // iolist -> setLhs(iodo);
// replacing I/O list in source I/O statement
 stmt -> setExpression(0,*iolist);
 //generating assign statement
 //dvm000(i) = ArrCpy(...)
 are = new SgArrayRefExp(*bufIO[Integer],c1.copy()); //!!! itype=>Integer (bufIO[itype])
 init = ndvm;
 //if(l+2 <= rank)
   for(i=2; i<(rank-l+1);i++ )
     doAssignStmtBefore(new SgVarRefExp(*loop_var[i]),stmt);
 if(l+1 <= rank)
   doAssignStmtBefore(new SgVarRefExp(*loop_var[1]),stmt);
 
 for(i=l; i; i-- )
  doAssignStmtBefore(new SgValueExp(-1),stmt); 
 last = ndvm;
 //if(l+2 <= rank) 
   for(i=2; i<(rank-l+1);i++ )
     doAssignStmtBefore(new SgVarRefExp(*loop_var[i]),stmt);
 if(l+1 <= rank) {
   d = new SgVarRefExp(*loop_var[1]);
   if(M != 1)  
     d = (M)? &(*d+(*new SgValueExp(M-1))) : &(*d+(*DVM000(m))-c1.copy()); 
   doAssignStmtBefore(d,stmt);
 }

 step = last+rank;
 if(l+1 <= rank) {
   ndvm = step + rank - l - 1;
   doAssignStmtBefore(&c1.copy(),stmt);
 }
 ndvm = step+rank;
 if(stmt->variant() == READ_STAT){ 
   doAssignStmtAfter (A_CopyTo_DA(are,HeaderRef(ar),init,last,step,2));
   if(dvm_debug) {
       if(contst)
         cur_st = contst;
       cur_st->insertStmtAfter(*D_Read(GetAddresDVM(HeaderRefInd(ar,1)))); 
   } 
 } else
   doAssignStmtBefore(DA_CopyTo_A(HeaderRef(ar),are,init,last,step,2),stmt);

// replace I/O statement by: IF(TstIO().NE.0) I/O-statement 
 ReplaceByIfStmt(stmt);

//calculating maximal number of used loop variables for I/O
 nio = (nio < (rank-l+1)) ? (rank-l+1) : nio;
 SET_DVM(imem);
} 

int IOcontrol(SgExpression *e, SgExpression *ioc[],int type)
// analizes IO_control list (e) and sets on ioc[]
{ SgKeywordValExp *kwe;
  SgExpression *ee,*el;
  int i;
  for(i=10; i; i--)
     ioc[i-1] = NULL;

  if(e->variant() == SPEC_PAIR) {
     if(type == PRINT_STAT)
        ioc[FMT_] = e->rhs();   
     else {
        // ioc[UNIT_] = e->rhs();
       kwe = isSgKeywordValExp(e->lhs());   
       if(!kwe)
          return(0);
       if     (!strcmp(kwe->value(),"unit"))
               ioc[UNIT_] = e->rhs();
       else if (!strcmp(kwe->value(),"fmt")) 
               ioc[FMT_]  = e->rhs();
       else
               return(0);
     }	       
     return(1);
  }
  
  if(e->variant() == EXPR_LIST){
    for(el=e; el; el = el->rhs()) {
       ee = el->lhs();
       if(ee->variant() != SPEC_PAIR)
          return(0); // IO_control list error
       kwe = isSgKeywordValExp(ee->lhs());   
       if(!kwe)
          return(0);
       if     (!strcmp(kwe->value(),"unit"))
               ioc[UNIT_] = ee->rhs();
       else if (!strcmp(kwe->value(),"fmt")) 
               ioc[FMT_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"nml")) 
               ioc[NML_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"rec")) 
               ioc[REC_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"iostat")) 
               ioc[IOSTAT_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"end")) 
               ioc[END_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"err")) 
               ioc[ERR_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"eor")) 
               ioc[EOR_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"size")) 
               ioc[SIZE_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"advance")) 
               ioc[ADVANCE_]  = ee->rhs();
       else
               return(0);
    }
    return(1);
  }    
  else
    return(0);
}    
    
int control_list1(SgExpression *e, SgExpression *ioc[])
// analizes control list (e) for statements BACKSPACE,REWIND and ENDFILE
// and sets on ioc[]
{ SgKeywordValExp *kwe;
  SgExpression *ee,*el;
  int i;
  for(i=10; i; i--)
     ioc[i-1] = NULL;

  if(e->variant() == SPEC_PAIR) {
     ioc[UNIT_] = e->rhs();   
     return(1);
  }
  
  if(e->variant() == EXPR_LIST){
    for(el=e; el; el = el->rhs()) {
       ee = el->lhs();
       if(ee->variant() != SPEC_PAIR)
          return(0); // IO_control list error
       kwe = isSgKeywordValExp(ee->lhs());   
       if(!kwe)
          return(0);
       if     (!strcmp(kwe->value(),"unit"))
               ioc[UNIT_] = ee->rhs();
       else if (!strcmp(kwe->value(),"iostat")) 
               ioc[IOSTAT_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"err")) 
               ioc[ERR_]  = ee->rhs();
       else
               return(0);
    }
    return(1);
  }    
  else
    return(0);
}    

int control_list_open(SgExpression *e, SgExpression *ioc[])
// analizes control list (e) for OPEN,CLOSE and INQUIRE statements
// and sets on ioc[]
{ SgKeywordValExp *kwe;
  SgExpression *ee,*el;
  int i;
  for(i=30; i; i--)
     ioc[i-1] = NULL;

  if(e->variant() == SPEC_PAIR) {
     ioc[UNIT_] = e->rhs();   
     return(1);
  }
  if(e->variant() == EXPR_LIST){
    for(el=e; el; el = el->rhs()) {
       ee = el->lhs();
       if(ee->variant() != SPEC_PAIR)
          return(0); // IO_control list error
       kwe = isSgKeywordValExp(ee->lhs());   
       if(!kwe)
          return(0);
       if     (!strcmp(kwe->value(),"unit"))
               ioc[UNIT_] = ee->rhs();
       else if (!strcmp(kwe->value(),"file")) 
               ioc[FILE_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"status")) 
               ioc[STATUS_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"iostat")) 
               ioc[IOSTAT_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"access")) 
               ioc[ACCESS_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"err")) 
               ioc[ERR_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"form")) 
               ioc[FORM_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"recl")) 
               ioc[RECL_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"blank")) 
               ioc[BLANK_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"exist"))
               ioc[EXIST_] = ee->rhs();
       else if (!strcmp(kwe->value(),"opened")) 
               ioc[OPENED_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"number")) 
               ioc[NUMBER_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"named")) 
               ioc[NAMED_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"name")) 
               ioc[NAME_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"sequential")) 
               ioc[SEQUENTIAL_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"direct")) 
               ioc[DIRECT_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"nextrec")) 
               ioc[NEXTREC_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"formatted")) 
               ioc[FORMATTED_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"unformatted")) 
               ioc[UNFORMATTED_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"position")) 
               ioc[POSITION_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"action")) 
               ioc[ACTION_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"readwrite")) 
               ioc[READWRITE_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"read")) 
               ioc[READ_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"write")) 
               ioc[WRITE_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"delim")) 
               ioc[DELIM_]  = ee->rhs();
       else if (!strcmp(kwe->value(),"pad")) 
               ioc[PAD_]  = ee->rhs();
       else
               return(0);
    }
    return(1);
  }    
  else
    return(0);
}    
    

void InsertSendIOSTAT(SgExpression * eios)
{int imem,icount;
 SgType *t;
 imem = ndvm;  
 doAssignStmtAfter(GetAddresMem(eios));
 t = eios->symbol() ? Base_Type(eios->symbol()->type()) : SgTypeInt();//type of IOSTAT var
 doAssignStmtAfter(TypeLengthExpr(t)); //type size
 //doAssignStmtAfter(new SgValueExp(TypeSize(t))); 14.03.03
 icount = ndvm;
 doAssignStmtAfter(new SgValueExp(1)); //count of memory areas = 1
 doCallAfter(SendMemory(icount,imem,imem+1));
 if(dvm_debug)
   InsertNewStatementAfter(D_Read(DVM000(imem)),cur_st,cur_st->controlParent());
 SET_DVM(imem);
}

void InsertSendInquire(SgExpression * eioc[])
{int imem,j,i,icount;
 imem = ndvm;
 j=0;
 for (i=4;i<20;i++)    
   if(eioc[i]) {
     doAssignStmtAfter(GetAddresMem(eioc[i]));
     j++;
  }
 for (i=4;i<20;i++)    
   if(eioc[i])
     doAssignStmtAfter(TypeLengthExpr(eioc[i]->type())); 
     //doAssignStmtAfter(new SgValueExp(TypeSize(eioc[i]->type()))); 14.03.03
 if(j) {
   icount = ndvm;
   doAssignStmtAfter(new SgValueExp(j)); //count of memory areas
   doCallAfter(SendMemory(icount,imem,imem+j));
   if(dvm_debug)
     for(i=0; i<j; i++)
       InsertNewStatementAfter(D_Read(DVM000(imem+i)),cur_st,cur_st->controlParent());
 }
 SET_DVM(imem);
}

void InsertSendInputList(SgExpression * input_list, SgExpression * io_stat,SgStatement *stmt)
{int imem,j,i,icount,iel;
 SgExpression *el,*ein,*iisize[100],*iinumb[100],*iielem[100];
 SgType *t;

 imp_loop = NULL;

 if(dvm_debug)
   for(i=0;i<100;i++)
     iinumb[i] = NULL;

 imem = ndvm;
 j=0; 
 if(io_stat) {
     doAssignStmtAfter(GetAddresMem(io_stat));
     t = io_stat->symbol() ? Base_Type(io_stat->symbol()->type()) : SgTypeInt();//type of IOSTAT var
     iisize[0] =  TypeLengthExpr(t); //new SgValueExp(TypeSize(t));
     j++;
   }
 for (el=input_list;el;el=el->rhs()) {    
   ein = el->lhs();  // input list item
   if(isSgIOAccessExp(ein)) //implicit loop
     { if(!SpecialKindImplicitLoop(el->rhs(),ein,&j, iisize, iielem, iinumb, stmt))
        ImplicitLoop(ein,&j, iisize, iielem, iinumb, stmt); 
     }
   else if(isSgArrayRefExp(ein) && !ein->lhs() && (ein->type()->variant()!=T_STRING)){//whole array 
      doAssignStmtAfter(GetAddresMem(FirstArrayElement(ein->symbol()))); 
      iisize[j] = InputItemLength(ein,stmt);
      if(dvm_debug){      
        iielem[j] = ElemLength(ein->symbol());
        iinumb[j] = NumbOfElem(iisize[j], iielem[j]);
      }
      j++;
   }
   else if(isSgArrayRefExp(ein)  && (ein->type()->variant()==T_ARRAY)){//section of array
      doAssignStmtAfter(GetAddresMem (ContinuousSection(ein) ? FirstElementOfSection(ein) : FirstArrayElement(ein->symbol()))); 
      iisize[j] = InputItemLength(ein,stmt);
      if(dvm_debug){      
        iielem[j] = ElemLength(ein->symbol());
        iinumb[j] = NumbOfElem(iisize[j], iielem[j]);
      }
      j++;  
   } else {       
      doAssignStmtAfter(GetAddresMem(ein->type()->variant()==T_ARRAY ? FirstElementOfSection(ein) : ein)); 
      iisize[j] = InputItemLength(ein,stmt);
      j++;
   }  
 }
 for(i=0;i<j;i++)
    doAssignStmtAfter(iisize[i]); 

icount = ndvm;
doAssignStmtAfter(new SgValueExp(j)); //count of memory areas
doCallAfter(SendMemory(icount,imem,imem+j));
if(dvm_debug){
  for(i=0;i<j;i++)
    if(iinumb[i] != NULL){ // input list item is array
      iel = ndvm;
      doAssignStmtAfter(iielem[i]); 
      doAssignStmtAfter(iinumb[i]); 
      InsertNewStatementAfter(D_ReadA(DVM000(imem+i),iel,iel+1),cur_st,cur_st->controlParent());
      SET_DVM(iel);
    } else
      InsertNewStatementAfter(D_Read(DVM000(imem+i)),cur_st,cur_st->controlParent());
}
SET_DVM(imem);
}

int SpecialKindImplicitLoop(SgExpression *el, SgExpression *ein, int *pj, SgExpression *iisize[], SgExpression *iielem[],SgExpression *iinumb[],SgStatement *stmt)
{
  SgExpression *ell, *e, *e1, *enumb, *elen, *bounds;
  SgSymbol *s;
  SgValueExp c1(1);

  if(el) return(0);  //number of input list items > 1
  ell = ein->lhs();
  if(ell->rhs()) return(0); //number of items of implicit loop list
  e = ell->lhs(); s = e->symbol();
  bounds = ein->rhs(); 
  if(bounds->rhs()) return(0);  //step of implicit loop is specified 
  if(isSgArrayRefExp(e)  && (e->type()->variant()!=T_STRING) && Rank(s)==1 && (isSgVarRefExp(e->lhs()->lhs())) && (e->lhs()->lhs()->symbol() == ein->symbol()) ) {
    e1 = &(e->copy()); 
    e1->lhs()->setLhs(bounds->lhs()->lhs()->copy());   
    doAssignStmtAfter(GetAddresMem(e1)); //initial address of array section
    enumb = &(bounds->lhs()->rhs()->copy() - bounds->lhs()->lhs()->copy() + c1);
    elen =  ElemLength(s);

    iisize[*pj] = &(*enumb * (*elen)); //array section length 
    if(dvm_debug) { 
       iielem[*pj] = elen; //ElemLength(s);
       iinumb[*pj] = enumb;
    }
    *pj = *pj+1;
    return (1);   
  }
  else
    return(0);   
         
}

void ImplicitLoop(SgExpression *ein, int *pj, SgExpression *iisize[], SgExpression *iielem[],SgExpression *iinumb[],SgStatement *stmt)
{
  SgExpression *ell, *e; 
  for (ell = ein->lhs();ell;ell=ell->rhs()){ //looking through item list of implicit loop
     e = ell->lhs();
     if(isSgIOAccessExp(e))
        ImplicitLoop(e,pj,iisize,iielem,iinumb,stmt); 
     else {
         if(isSgArrayRefExp(e)) {
             SgExpression *e1 ;
             SgSymbol *ar;
             int has_aster_or_1;

             if(!e->lhs() && e->type()->variant()==T_STRING) {//character object
               doAssignStmtAfter(GetAddresMem(e)); 
               iisize[*pj] = InputItemLength(e,stmt);
               *pj = *pj+1;   
               continue;
             }
             ar = e->symbol();
             has_aster_or_1 =  hasAsterOrOneInLastDim(ar); //testing last dimension : * or 1
             if(! has_aster_or_1) {
                if(isInSymbList(imp_loop,ar))
                  continue;
                else
                  imp_loop = AddToSymbList(imp_loop,ar);  
             }
             e1 = FirstArrayElement(ar);
             doAssignStmtAfter(GetAddresMem(e1)); //initial array address
             iisize[*pj] =ArrayLength(ar,stmt,0);// whole array length 
             if (has_aster_or_1)  //testing last dimension : * or 1
             {
                 if (ein->symbol() == lastDimInd(e->lhs()))
                     iisize[*pj] = CorrectLastOpnd(iisize[*pj], ar, ein->rhs(), stmt);
                 //correcting whole array length by implicit loop parameters
                 else
                     Error("Can not calculate array length: %s", ar->identifier(), 194, stmt);
             }

             if(dvm_debug) { 
               iielem[*pj] = ElemLength(ar);
               iinumb[*pj] = NumbOfElem(iisize[*pj], iielem[*pj]);
             }
             *pj = *pj+1;   
         }
         else if(e->variant() == ARRAY_OP) {//substring or substring of array element
             SgExpression *e1 ;
             if( !e->lhs()->lhs())  //substring 
             {
               doAssignStmtAfter(GetAddresMem(e->lhs())); 
               iisize[*pj] = InputItemLength(e->lhs(),stmt);
               *pj = *pj+1; 
               continue;   
             }
             //substring of array element  
             e1 = FirstArrayElement(e->lhs()->symbol());
             doAssignStmtAfter(GetAddresMem(e1)); //initial array address
             iisize[*pj] = ArrayLength(e->lhs()->symbol(),stmt,1); // whole array length 
             *pj = *pj+1;   
         }     
         else {
             doAssignStmtAfter(GetAddresMem(e)); 
             iisize[*pj] = InputItemLength(e,stmt);
             *pj = *pj+1;   
         }
     }
  }
}

/*
 * variant when substring is represented by ARRAY_REF node with 2 operands
 *
SgExpression * InputItemLength (SgExpression *e, SgStatement *stmt)
{
 if (isSgVarRefExp(e))
    return(new SgValueExp(TypeSize(e->type())));  
 if (isSgArrayRefExp(e)) 
    if(e->type()->variant()!=T_STRING) //whole array or array element of non-character type           
        if(e->lhs()) //array element
           return(new SgValueExp(TypeSize(e->symbol()->type()->baseType())));   
        else  //whole array
           return(ArrayLength(e->symbol(),stmt,1));   
    else { //variable, array element, substring or substring of array element of type CHARACTER 
      if(!(e->lhs())) //variable
        return(StringLengthExpr(e->symbol()->type(),e->symbol())); 
       //return(new SgValueExp(CharLength(e->symbol()->type()))); 14.03.03
     // e = e->lhs()->lhs(); //variant of e->lhs() is EXPR_LIST  
         
      if(!(e->rhs()) && (e->lhs()->lhs()->variant() != DDOT)) //array element of type CHARACTER 
           return(StringLengthExpr(e->symbol()->type()->baseType(),e->symbol()));   
              //return(new SgValueExp(CharLength(e->symbol()->type()->baseType())));   
     else
           return(SubstringLength(e)); 
   } 
  return(new SgValueExp(-1)); 
}

SgExpression *SubstringLength(SgExpression *sub)
{ //SgSubscriptExp *sbe;
  SgValueExp c1(1);
  SgExpression *e,*e1,*e2;
  SgType *t;
//err("Sorry, substring length calculating is not jet implemented",cur_st);
     if(sub->lhs()->lhs()->variant() == DDOT) {  //substring(sub has variant EXPR_LIST)
        e = sub->lhs()->lhs();     
        t=sub->symbol()->type();
     }
     else { //substring of array element
        e = sub->rhs();
        t=sub->symbol()->type()->baseType();
     } 
     if(e->lhs())
        e1 = &(e->lhs()->copy());
     else  
        e1 = &(c1.copy());

     if(e->rhs())
        e2 = &(e->rhs()->copy());
     else  
       e2 = StringLengthExpr(t,sub->symbol()); //new SgValueExp(CharLength(t)); 14.03.03
      return (&(*e2 - *e1 + c1));
}
*/


SgExpression * InputItemLength (SgExpression *e, SgStatement *stmt)
{
 if (isSgVarRefExp(e))
   return(TypeLengthExpr(e->type()));  
      //return(new SgValueExp(TypeSize(e->type()))); 14.03.03 
 if (isSgArrayRefExp(e))
 {
     if (e->symbol()->type()->variant() == T_STRING) // variable of type CHARACTER
         return(StringLengthExpr(e->symbol()->type(), e->symbol()));
     //return(new SgValueExp(CharLength(e->symbol()->type()))); 14.03.03
     else
     {
         if (e->lhs() && !isSgArrayType(e->type())) //array element
             return(TypeLengthExpr(e->symbol()->type()->baseType()));
         else if (e->lhs() && isSgArrayType(e->type())) //array section
             return(ContinuousSection(e) ? SectionLength(e, stmt, 1) : ArrayLength(e->symbol(), stmt, 1));
         else  //whole array
             return(ArrayLength(e->symbol(), stmt, 1));
     }
 }

 if (e->variant() == ARRAY_OP) //substring or substring of array element
           return(SubstringLength(e)); //substring

           return(new SgValueExp(-1)); 
}

SgExpression *SubstringLength(SgExpression *sub)
{ //SgSubscriptExp *sbe;
  SgValueExp c1(1);
  SgExpression *e,*e1,*e2;
  SgType *t;
  
//err("Sorry, substring length calculating is not jet implemented",cur_st);
     if(!sub->lhs()->lhs()){  //substring   
        t=sub->lhs()->symbol()->type();
        e = sub->rhs()->lhs(); // sub->rhs() has variant EXPR_LIST
     }
     else{                   //substring of array element
        t=sub->lhs()->symbol()->type()->baseType();
        e = sub->rhs(); 
     }  
     if(e->lhs())
        e1 = &(e->lhs()->copy());
     else  
        e1 = &(c1.copy());

     if(e->rhs())
        e2 = &(e->rhs()->copy());
     else  
       e2 = StringLengthExpr(t,sub->lhs()->symbol()); //new SgValueExp(CharLength(t));
      return (&(*e2 - *e1 + c1));
}

SgExpression *ArrayLength(SgSymbol *ar, SgStatement *stmt, int err)
{int i,rank;
 SgExpression *esize,*len; 
rank = Rank(ar);
len = TypeLengthExpr(ar->type()->baseType()); //length of one array element
  //len = new SgValueExp(TypeSize(ar->type()->baseType())); 14.03.03
for(i=1; i<=rank; i++) {
    //calculating size of i-th dimension
    esize = ReplaceParameter(ArrayDimSize(ar, i));
    if(err && esize && esize->variant()==STAR_RANGE)
      Error("Assumed-size array: %s",ar->identifier(),162,stmt);
    if(esize->isInteger())
      esize = new SgValueExp( esize->valueInteger());
    if(esize)
      len = &(*len * (*esize));
    
}
if (len->isInteger()) // calculating length if it is possible
    len = new SgValueExp( len->valueInteger()); 
return(len);
}

SgExpression *SectionLength(SgExpression *ea, SgStatement *stmt, int err)
{int i,rank;
 SgExpression *esize,*len, *el, *eup[7], *ein[7]; 
 //rank = ArraySectionRank(ea);
 rank = Rank(ea->symbol());  
 len = TypeLengthExpr(ea->symbol()->type()->baseType()); //length of one array element

  
 for(i=0,el=ea->lhs(); i<rank; i++,el=el->rhs()) {
    //calculating size of i-th dimension
    UpperBoundInTriplet(el->lhs(),ea->symbol(),i,eup);
    LowerBoundInTriplet(el->lhs(),ea->symbol(),i,ein);
    esize = &(*eup[i] - *ein[i] + *new SgValueExp(1));
       //if(err && esize && esize->variant()==STAR_RANGE)
       //  Error("Assumed-size array: %s",ar->identifier(),162,stmt);
       //if(esize->isInteger())
       //  esize = new SgValueExp( esize->valueInteger());
    if(esize)
      len = &(*len * (*esize));
    
}
     //if (len->isInteger()) // calculating length if it is possible
     //    len = new SgValueExp( len->valueInteger()); 
return(len);
}

SgExpression *ArrayLengthInElems(SgSymbol *ar, SgStatement *stmt, int err)
{int i,rank;
 SgExpression *esize,*len; 
rank = Rank(ar);
len =  new SgValueExp(1);
for(i=1; i<=rank; i++) {
    //calculating size of i-th dimension
    esize = ReplaceParameter(ArrayDimSize(ar, i));
    if(err && esize && esize->variant()==STAR_RANGE)
      Error("Assumed-size array: %s",ar->identifier(),162,stmt);
    if(esize->isInteger())
      esize = new SgValueExp( esize->valueInteger());
    if(esize)
      len = &(*len * (*esize));
    
}
if (len->isInteger()) // calculating length if it is possible
    len = new SgValueExp( len->valueInteger()); 
return(len);
}

SgExpression *NumbOfElem(SgExpression *es,SgExpression *el)
{SgExpression *e,*e1 = NULL,*ec;
 if(!es)
   return(NULL);
 if(es->isInteger())
   return(new SgValueExp( es->valueInteger() / el->valueInteger()));
                                                           //deleting on length of element
 ec = &es->copy();
 for(e=ec; e->variant() == MULT_OP; e=e->lhs())
    e1 = e;
 e1->setLhs(new SgValueExp(1)); //replace length of element by 1
 return(ec);
}

SgExpression *ElemLength(SgSymbol *ar)
{SgExpression *len;
len = TypeLengthExpr(ar->type()->baseType()); //length of one array element
//len = new SgValueExp(TypeSize(ar->type()->baseType()));  14.03.03
 return(len);
}

SgExpression *CorrectLastOpnd(SgExpression *len, SgSymbol *ar, SgExpression *bounds,SgStatement *stmt)
{SgExpression *elast;
 SgValueExp c1(1);
 if(!Rank(ar))
   return(len); //error situation 
 if(!bounds->rhs()){  //step of implicit loop is absent ,by default 1  
    elast=&(bounds->lhs()->rhs()->copy() - *Exprn(LowerBound(ar,Rank(ar)-1)) + c1);
                  //upper_bound_of_implicit_loop - lower_bound_of_last_dimension_of_array + 1
    if (elast->isInteger()) // calculating size if it is possible
      elast = new SgValueExp( elast->valueInteger()); 
    if(len->variant() == MULT_OP)       
      len->setRhs(elast); //replace last multiplicand of array length
    else
      len = &(*len * (*elast));//len is the length of array element,it is multiplied by elast
 }
 else // variant == SEQ,there is a step
    Error("Can not calculate array length: %s", ar->identifier(),194,stmt);
 if (len->isInteger()) // calculating length if it is possible
   len = new SgValueExp( len->valueInteger()); 
 return(len);
} 
            
SgSymbol *lastDimInd(SgExpression *el)
{//returns symbol of last subscript expression  if it is variable refference 
 //el - subscript list
 SgExpression *last = NULL;
 for(; el; el=el->rhs()) //search for last subscript
     last = el->lhs();
 if(isSgVarRefExp(last)) //is variable refference
    return(last->symbol());
 return(NULL);
}   

int hasAsterOrOneInLastDim(SgSymbol *ar)
{//is dummy argument or array in COMMON declared as a(n,n,*) or a(1)
 SgExpression *e;
 SgValueExp *ev;
 int rank;
 rank = Rank(ar);
 if(!rank)
   return(0);
 e=ArrayDimSize(ar,rank);
 if(e->variant()==STAR_RANGE)  
   return(1);
 if(rank==1 && (ev = isSgValueExp(e)) && ev->intValue() == 1)
    return(1);
 return(0);
}            

SgExpression *FirstArrayElement(SgSymbol *ar)
{//generating reference AR(L1,...,Ln), where Li - lower bound of i-th dimension
 int i;
 SgExpression *esl, *el, *e;
 el = NULL;
 for (i = Rank(ar); i; i--){
  esl = new SgExprListExp(*Exprn(LowerBound(ar,i-1)));
  esl->setRhs(el);
  el = esl;
 }
  e = new SgArrayRefExp(*ar);
  e->setLhs(el);
  return(e);
}
  
SgExpression *FirstElementOfSection(SgExpression *ea) //(SgSymbol *ar,SgExpression *elist) (SgExpression *ea)
{SgExpression *el, *ein[7];
 int i;
 SgExpression *esl, *e;
 SgSymbol * ar;
 ar = ea->symbol();
 if(!ea->lhs()) //whole array
   return(FirstArrayElement(ar));

 for(el=ea->lhs(),i=0; el; el=el->rhs(),i++)    
     LowerBoundInTriplet(el->lhs(),ar,i, ein);
 el = NULL;
 for (i = Rank(ar); i; i--){
   esl = new SgExprListExp(*Exprn(ein[i-1]));
   esl->setRhs(el);
   el = esl;
 }
  e = new SgArrayRefExp(*ar);
  e->setLhs(el);
  return(e);
}

int ArraySectionRank(SgExpression *ea)
{SgExpression *el;
 int rank;
 for(el=ea->lhs(),rank=0; el; el=el->rhs()) 
  if(el->lhs()->variant() == DDOT)
    rank++;
 return(rank);
}

int ContinuousSection(SgExpression *ea)
{ SgExpression *ei;

  ei = ea->lhs();
  if(ei->lhs()->variant() != DDOT)
     return(0);
  while(ei && isColon(ei->lhs()))
     ei = ei->rhs();
  if(!ei)      // (:,:,...:)
     return(1);
  //if(ei->lhs()->variant() == DDOT && ei->lhs()->lhs()->variant() == DDOT)   //there is step
  //   return (0);
  ei = ei->rhs();
  while(ei && ei->lhs()->variant() != DDOT)
     ei = ei->rhs();  
  if(!ei)
     return(1); 
  return(0);

}

int isColon(SgExpression *e)
{
  if(!e)
    return(0);
  if(e->variant() == DDOT && !e->lhs() && !e->rhs())
    return(1);
  return(0);
 
}