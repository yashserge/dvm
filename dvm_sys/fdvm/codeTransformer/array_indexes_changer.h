#pragma once

void arrayIndexesChanger(SgFile *file);

struct SearchInfo
{
    SgExpression *expr;
    SgStatement *lastSt;
    int none;
};