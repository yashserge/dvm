#pragma once

void transformer_log(bool print, const char *message);
void transformer_log(bool print, const char *message, int par);
void transformer_log(bool print, const char *message, const char *par);

void transformer_error(const char *message, const char *par);
void transformer_error(const char *message, int par);