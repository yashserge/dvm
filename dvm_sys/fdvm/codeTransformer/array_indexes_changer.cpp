#define _CRT_SECURE_NO_WARNINGS

#include <cstdio>
#include <cstring>
#include <fstream>
#include <iostream>
#include <cstdlib>

#include "dvm.h"
#include "array_indexes_changer.h"
#include "errors.h"

using namespace std;

static void copyLabelAndComentsToNext(SgStatement *curr)
{
    if (curr->hasLabel())
        curr->lexNext()->setLabel(*curr->label());
    if (curr->comments())
        curr->lexNext()->setComments(curr->comments());
}

//TODO: exetend and correct this checker
static void checkLoop(SgForStmt *&forSt)
{
    SgLabel *tmp = forSt->endOfLoop();
    SgStatement *curr = forSt->lexNext();

    // find the last node of this FOR with label
    while (1)
    {
        if (curr->hasLabel())
        {
            if (curr->label() == tmp)
                break;
        }

        curr = curr->lexNext();
    }

    // if this node - assign operator -> replace this to continue operator
    if (curr->variant() == ASSIGN_STAT)
    {
        curr->insertStmtAfter(*new SgContinueStmt(), *(curr->controlParent()));
        SgStatement *next = curr->lexNext();
        next->setLabel(*curr->label());

        BIF_LABEL(curr->thebif) = NULL;
    }

    curr = forSt->lexNext();
    // convert all arithmetic if to simple if with goto
    while (1)
    {
        if (curr->hasLabel())
        {
            if (curr->label() == tmp)
                break;
        }
        if (curr->variant() == ARITHIF_NODE)
        {
            SgExpression *cond = curr->expr(0);
            SgExpression *lb = curr->expr(1);
            SgLabel *arith_lab[3];

            int i = 0;
            while (lb)
            {
                SgLabel *lab = ((SgLabelRefExp *)(lb->lhs()))->label();
                arith_lab[i] = lab;
                i++;
                lb = lb->rhs();
            }

            SgStatement *replaceSt = new SgIfStmt(*cond < *new SgValueExp(0), *new SgGotoStmt(*arith_lab[0]), *new SgIfStmt(SgEqOp(*cond, *new SgValueExp(0)), *new SgGotoStmt(*arith_lab[1]), *new SgGotoStmt(*arith_lab[2])));
            curr->insertStmtAfter(*replaceSt, *curr->controlParent());
            copyLabelAndComentsToNext(curr);
            SgStatement *toDel = curr;
            curr = curr->lexNext();
            toDel->deleteStmt();
            continue;
        }
        curr = curr->lexNext();
    }

    curr = forSt->lexNext();
    // find all goto with FOR's label and replace to cycle statement
    while (1)
    {
        if (curr->hasLabel())
        {
            if (curr->label() == tmp)
                break;
        }

        if (curr->variant() == GOTO_NODE)
        {
            SgGotoStmt *gotoN = ((SgGotoStmt*)curr);
            if (gotoN->branchLabel() == tmp)
            {
                SgStatement *parent = gotoN->controlParent();
                if (parent->variant() == LOGIF_NODE)
                {
                    if (parent->controlParent()->variant() == FOR_NODE)
                    {
                        SgStatement *pparent = parent->controlParent();
                        if (((SgForStmt*)pparent)->endOfLoop() == tmp)
                        {
                            curr = curr->lexNext();
                            ((SgLogIfStmt*)parent)->extractStmtBody();
                            ((SgLogIfStmt*)parent)->setBody(*new SgCycleStmt());
                            continue;
                        }
                    }
                }
                else
                {
                    while (parent->variant() != FOR_NODE)
                        parent = parent->controlParent();
                    if (((SgForStmt*)parent)->endOfLoop() == tmp)
                    {
                        curr = curr->lexNext();
                        gotoN->insertStmtAfter(*new SgCycleStmt());
                        copyLabelAndComentsToNext(((SgStatement*)gotoN));
                        gotoN->deleteStmt();
                        continue;
                    }
                }
            }
        }
        curr = curr->lexNext();
    }
}

static void tryToConverLoop(SgForStmt *&forSt)
{
    if (forSt->isEnddoLoop() == false)
    {        
        int result = forSt->convertLoop();
        if (result == 0)
        {    
            checkLoop(forSt);
            result = forSt->convertLoop();
            if (result == 0)
                transformer_error("WARNING: can not convert to EndDo loop on line %d\n", forSt->lineNumber());
        }
    }
}

static bool findSymbolInExpression(SgExpression *currExp, const SgSymbol *symb)
{
    bool ret = false;
    if (currExp->symbol() == symb)
        ret = true;
    else
    {
        if (currExp->lhs())
        {
            ret = findSymbolInExpression(currExp->lhs(), symb);
            if (ret == false)
            {
                if (currExp->rhs())
                    ret = findSymbolInExpression(currExp->rhs(), symb);
            }
        }
    }
    return ret;
}

static void findExpression(SearchInfo *&sInfo, const SgSymbol *symb, SgStatement *inSt)
{
    SgStatement *curr = sInfo->lastSt;
    while (curr != inSt)
    {
        if (curr->variant() == ASSIGN_STAT)
        {
            if (curr->expr(0)->variant() == VAR_REF)
            {
                if (curr->expr(0)->symbol() == symb)
                {
                    if (sInfo->none == true && curr->controlParent()->variant() != IF_NODE && !findSymbolInExpression(curr->expr(1), symb))
                        sInfo->expr = curr->expr(1);
                    else
                    {
                        sInfo->expr = NULL;
                        sInfo->none = false;
                    }
                }
            }
        }
        curr = curr->lexNext();
    }
    sInfo->lastSt = inSt;
}

map<SgSymbol*, SearchInfo*> toReplace;
static void replaceInExpr(const vector<SgForStmt*> &parentLoops, SgExpression *parent, SgExpression *child, bool is_in_aray, const int line, SgStatement *inSt)
{
    if (is_in_aray)
    {
        if (child->variant() == VAR_REF)
        {
            bool match = false;
            for (int k = 0; k < parentLoops.size(); ++k)
            {
                if (parentLoops[k]->hasSymbol())
                {
                    if (parentLoops[k]->symbol() == child->symbol())
                    {
                        match = true;
                        break;
                    }                    
                }
            }

            if (match == false)
            {
                //printf("not eq on line %d\n", line);

                SgSymbol *repl = child->symbol();
                SearchInfo *sInfo = NULL;
                if (toReplace.find(repl) != toReplace.end())
                    sInfo = toReplace[repl];
                else
                {
                    sInfo = new SearchInfo();
                    sInfo->lastSt = parentLoops[0];
                    sInfo->expr = NULL;
                    sInfo->none = true;

                    toReplace[repl] = sInfo;
                }
                findExpression(sInfo, repl, inSt);
                
                if (sInfo->expr)
                {
                    printf("  replaced in line %d\n", line);
                    if (parent->lhs() == child)
                        parent->setLhs(sInfo->expr->copyPtr());
                    else if (parent->rhs() == child)
                        parent->setRhs(sInfo->expr->copyPtr());
                }
            }
        }
    }

    if (child->lhs())
        replaceInExpr(parentLoops, child, child->lhs(), is_in_aray || (child->variant() == ARRAY_REF), line, inSt);
    if (child->rhs())
        replaceInExpr(parentLoops, child, child->rhs(), is_in_aray || (child->variant() == ARRAY_REF), line, inSt);
}

static void replaceInLoop(vector<SgForStmt*> &parentLoops, SgStatement *&curr, SgStatement *lastLoopNode)
{    
    do
    {
        curr = curr->lexNext();

        if (curr->variant() == FOR_NODE)
        {
            //printf("in for node on line %d\n", curr->lineNumber());
            parentLoops.push_back((SgForStmt*)curr);      
            SgForStmt *forSt = (SgForStmt*)curr;

            tryToConverLoop(forSt);
            replaceInLoop(parentLoops, curr, forSt->lastNodeOfStmt());
            parentLoops.pop_back();
        }
        else if (curr->variant() == ASSIGN_STAT)
        {
            int line = curr->lineNumber();            
            SgExpression *left = curr->expr(0);
            SgExpression *right = curr->expr(1);

            replaceInExpr(parentLoops, NULL, left, false, line, curr);
            replaceInExpr(parentLoops, NULL, right, false, line, curr);
        }
        else if (curr->variant() == IF_NODE || curr->variant() == LOGIF_NODE)
        {
            int line = curr->lineNumber();
            SgExpression *cond = curr->expr(0);

            replaceInExpr(parentLoops, NULL, cond, false, line, curr);
        }
    } while (curr != lastLoopNode);
}

void arrayIndexesChanger(SgFile *file)
{
    int funcNum = file->numberOfFunctions();
    for (int i = 0; i < funcNum; ++i)
    {
        SgStatement *st = file->functions(i);
        SgStatement *lastNode = st->lastNodeOfStmt();
        vector<SgForStmt*> parentLoops;

        while (st != lastNode)
        {
            if (st->variant() == FOR_NODE)
            {
                //printf("FOR NODE in line %d\n", st->lineNumber());
                parentLoops.push_back((SgForStmt*)st);
                SgForStmt *forSt = (SgForStmt*)st;

                tryToConverLoop(forSt);
                replaceInLoop(parentLoops, st, forSt->lastNodeOfStmt());
                parentLoops.pop_back();
            }
            st = st->lexNext();
        }
    }
}
