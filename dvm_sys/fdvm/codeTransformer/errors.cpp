#include <cstdio>
#include <cstring>
#include <fstream>
#include <iostream>
#include <cstdlib>

#include "errors.h"

void transformer_log(bool print, const char *message)
{
    if (print)
        printf("%s\n", message);
}

void transformer_log(bool print, const char *message, int par)
{
    if (print)
    {
        printf(message, par);
        printf("\n");
    }
}

void transformer_log(bool print, const char *message, const char *par)
{
    if (print)
    {
        printf(message, par);
        printf("\n");
    }
}

void transformer_error(const char *message, const char *par)
{
    printf(message, par);
    printf("\n");
}

void transformer_error(const char *message, int par)
{
    printf(message, par);
    printf("\n");
}