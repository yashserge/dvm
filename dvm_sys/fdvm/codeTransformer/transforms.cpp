#define _CRT_SECURE_NO_WARNINGS

#include <cstdio>
#include <cstring>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include "dvm.h"

#include "errors.h"
#include "array_indexes_changer.h"

using namespace std;

#define DEBUG_LVL1 true
#define DEBUG_LVL2 false
#define DEBUG_LVL3 false

enum {
    UNROLL_LOOPS, CHANGE_ARRAY_INDEXES, CORRECT_CODE_STYLE, NONE
};

char* OnlyName(const char *filename)
{
    char *basename = new char[strlen(filename) + 1];
    int i;

    strcpy(basename, filename);
    for (i = strlen(filename) - 1; i >= 0; --i)
    {
        if (basename[i] == '.')
        {
            basename[i] = '\0';
            break;
        }
    }

    return basename;
}

char* OnlyExt(const char *filename)
{
    char *basename = new char[16];
    int i;

    int len = strlen(filename);
    for (i = len - 1; i >= 0; --i)
    {
        if (filename[i] == '.')
        {
            i++;
            int k;
            int bound = len - i;
            for (k = 0; k < bound; ++k, ++i)
                basename[k] = filename[i];
            basename[k] = '\0';
            break;
        }
    }

    return basename;
}

int main(int argc, char**argv)
{
    FILE *fout;
    char fout_name[128];
    char *fin_name;

    char *db_name = NULL;
    char *proj_name = "dvm.proj";
    int i, n;
    SgFile *file;
    int current_regim = NONE;

    bool need_to_unparce = true;
    bool need_to_save = false;

    for (int i = 0; i < argc; ++i)
    {
        const char *curr_arg = argv[i];
        switch (curr_arg[0])
        {
        case '-':
            if (curr_arg[1] == 'p')
            {
                i++;
                proj_name = argv[i];
                db_name = OnlyName(proj_name);
                i++;
            }
            else if (curr_arg[1] == 't')
            {
                i++;
                int par = atoi(argv[i]);
                if (par == 1)
                    current_regim = UNROLL_LOOPS;
                else if (par == 2)
                    current_regim = CHANGE_ARRAY_INDEXES;
                else if (par == 3)
                    current_regim = CORRECT_CODE_STYLE;
                i++;

            }
            else if (curr_arg[1] == 'h')
            {
                printf("Help info for transformer.\n\n");
                printf(" -p <project name>\n");
                printf(" -t <transform_number>\n");
                printf("    transform_number = 1:  loops unroller\n");
                printf("    transform_number = 2:  arrays indexes changer accoding to parent loops\n");
                printf("    transform_number = 3:  code style correcter\n");

                return 0;
            }
            break;
        default:
            break;
        }
    }

    SgProject project(proj_name);
        
    n = project.numberOfFiles();
    transformer_log(DEBUG_LVL1, "number of files in project %d", n);    

    // looking through the file list of project
    for (i = n - 1; i >= 0; --i)
    {
        file = &(project.file(i));
        fin_name = project.fileName(i);
        transformer_log(DEBUG_LVL1, "Analyzing: %s", fin_name);
               
        if (current_regim == CHANGE_ARRAY_INDEXES)
            arrayIndexesChanger(file);
        else if (current_regim == UNROLL_LOOPS)
        {

        }


        if (current_regim == CORRECT_CODE_STYLE || need_to_unparce)
        {
            sprintf(fout_name, "%s_.%s", OnlyName(fin_name), OnlyExt(fin_name));
            if ((fout = fopen(fout_name, "w")) == NULL)
                transformer_error("ERROR: Can't open file %s for write", fout_name);
            else
            {
                transformer_log(DEBUG_LVL1, "Unparsing to <%s> file", fout_name);
                file->unparse(fout);
                if (fclose(fout) < 0)
                    transformer_error("ERROR: Can't close file %s after write", fout_name);
            }
        }
                
        if (need_to_save)
        {
            sprintf(fout_name, "%s.dep", OnlyName(fin_name));
            file->saveDepFile(fout_name);
            transformer_log(DEBUG_LVL1, "Saving to <%s> file", fout_name);
        }
    }
}