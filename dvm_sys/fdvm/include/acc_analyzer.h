#pragma once
#define ACCAN_DEBUG 0

#define PRIVATE_ANALYSIS_NOT_CONDUCTED 650
#define PRIVATE_ANALYSIS_ADD_VAR 651
#define PRIVATE_ANALYSIS_REMOVE_VAR 652

struct AnalysedCallsList;

class ControlFlowItem
{
    unsigned int stmtNo;
    SgLabel* label;
    ControlFlowItem *jmp;
    SgLabel* label_jump;
    ControlFlowItem *next;
    bool leader;
    int bbno;
    bool is_parloop_start;
    SgExpression* private_list;
    bool is_parloop_end;
	SgStatement* prl_stmt;
	AnalysedCallsList* call;
	SgFunctionCallExp* func;
    union
    {
        SgStatement *stmt;
        SgExpression *expr;
    };
public:
    inline ControlFlowItem() : stmtNo(-1), label(NULL), jmp(NULL), label_jump(NULL), next(NULL), leader(false), bbno(0), stmt(NULL), is_parloop_start(false), is_parloop_end(false), private_list(NULL), call(NULL), func(NULL)
    { };

    inline ControlFlowItem(SgStatement *s, ControlFlowItem *n, AnalysedCallsList* c = NULL) : stmtNo(-1), label(s ? s->label() : NULL), jmp(NULL), label_jump(NULL), next(n), leader(false), bbno(0), stmt(s), is_parloop_start(false), is_parloop_end(false), private_list(NULL), call(c), func(NULL)
    { };

	inline ControlFlowItem(SgExpression *e, ControlFlowItem *j, ControlFlowItem *n, SgLabel* l, AnalysedCallsList* c = NULL) : stmtNo(-1), label(l), jmp(j), label_jump(NULL), next(n), leader(false), bbno(0), expr(e), is_parloop_start(false), is_parloop_end(false), private_list(NULL), call(c), func(NULL)
    { };

    inline ControlFlowItem(SgExpression *e, SgLabel* j, ControlFlowItem* n, SgLabel* l, AnalysedCallsList* c = NULL) : stmtNo(-1), label(l), jmp(NULL), label_jump(j), next(n), leader(false), bbno(0), expr(e), is_parloop_start(false), is_parloop_end(false), private_list(NULL), call(c), func(NULL)
    { };

    inline bool isEnumerated()
    { return stmtNo >= 0; }

    inline void setBBno(int bb)
    { bbno = bb; }

    inline int getBBno()
    { return bbno; }

    inline void AddNextItem(ControlFlowItem *n) 
    { next = n; }

    inline void MakeParloopStart()
    { is_parloop_start = true; }

    inline void MakeParloopEnd()
    { is_parloop_end = true; }

    inline bool IsParloopStart()
    { return is_parloop_start; }

    inline bool IsParloopEnd()
    { return is_parloop_end; }

    inline bool isUnconditionalJump()
    { return ((jmp != NULL || label_jump != NULL) && expr == NULL); }

    inline SgStatement* getStatement()
    {
        if (jmp == NULL)
            return stmt;
        else
            return NULL;
    }

    inline SgExpression* getExpression()
    {
        if (jmp != NULL)
            return expr;
        else
            return NULL;
    }

    inline ControlFlowItem* getJump()
    { return jmp; }

    inline ControlFlowItem* getNext() 
    { return next; }

    inline void setLeader()
    { leader = true; }

    inline unsigned int getStmtNo()
    { return stmtNo; }

    inline void setStmtNo(unsigned int no)
    { stmtNo = no; }

    inline int isLeader()
    { return leader; }

    inline void setLabel(SgLabel* l)
    { label = l; }

    inline SgLabel* getLabel()
    { return label; }

    inline void setLabelJump(SgLabel* l)
    { label_jump = l; }

    inline SgLabel* getLabelJump()
    { return label_jump; }

    inline void initJump(ControlFlowItem* item)
    { jmp = item; }

    inline void setPrivateList(SgExpression* p, SgStatement* s)
    { private_list = p; prl_stmt = s; }

    inline SgExpression* getPrivateList()
    { return private_list; }

	inline SgStatement* getPrivateListStatement()
	{ return prl_stmt; }

	inline AnalysedCallsList* getCall()
	{ return call; }

	inline void setFunctionCall(SgFunctionCallExp* f)
	{ func = f; }

	inline SgFunctionCallExp* getFunctionCall()
	{ return func; }

#if ACCAN_DEBUG
    void printDebugInfo();

#endif
    
    ~ControlFlowItem()
    {
        if (jmp != NULL)
            delete expr; // XXX: removing SgExpression may be dangerous
    }
};

class doLoopItem{
    int label;
    SgSymbol* name;
    ControlFlowItem* iter;
    ControlFlowItem* emptyAfter;
    bool current;
    doLoopItem* next;
public:
    inline doLoopItem(int l, SgSymbol* s, ControlFlowItem* i, ControlFlowItem* e) : label(l), name(s), iter(i), emptyAfter(e), current(true), next(NULL)
    { }
    inline void setNext(doLoopItem* n)
    { next = n; }
    inline void setNewLabel(int l)
    { label = l; }
    inline ControlFlowItem* getSourceForCycle()
    { return iter; }
    inline ControlFlowItem* getSourceForExit()
    { return emptyAfter; }
    inline SgSymbol* getName()
    { return name; }
    inline doLoopItem* getNext()
    { return next; }
    inline int getLabel()
    { return label; }
};

class doLoops{
    doLoopItem* first;
    doLoopItem* current;
    doLoopItem* findLoop(SgSymbol*);
    int parallel_depth;
    SgExpression* prl;
	SgStatement* prs;
public:
    inline doLoops() : first(NULL), current(NULL), parallel_depth(0)
    { }
    void addLoop(int l, SgSymbol* s, ControlFlowItem* i, ControlFlowItem* e);
    inline ControlFlowItem* getSourceForCycle()
    { return current ? current->getSourceForCycle() : NULL; }
    inline ControlFlowItem* getSourceForCycle(SgSymbol* loop)
    { return loop ? findLoop(loop)->getSourceForCycle() : getSourceForCycle(); }
    inline ControlFlowItem* getSourceForExit()
    { return current ? current->getSourceForExit() : NULL; }
    inline ControlFlowItem* getSourceForExit(SgSymbol* loop)
    { return loop ? findLoop(loop)->getSourceForExit() : getSourceForExit(); }
    ControlFlowItem* endLoop(ControlFlowItem* last);
    ControlFlowItem* checkStatementForLoopEnding(int label, ControlFlowItem* item);
    inline void setParallelDepth(int k, SgExpression* pl, SgStatement* ps)
    { parallel_depth = k; prl = pl; prs = ps; }
	inline SgStatement* GetParallelStatement()
	{ return prs; }
    inline bool isLastParallel()
    {
        if (parallel_depth > 0)
            return --parallel_depth == 0;
        return 0;
    }
    inline SgExpression* getPrivateList()
    { return prl; }
};

struct LabelCFI{
    int l;
    ControlFlowItem* item;
};

struct VarItem
{
    SgVarRefExp* var;
    VarItem* next;
};

class VarSet
{
    VarItem* list;
public:
    inline VarSet() : list(NULL)
    {}

    void addToSet(SgVarRefExp*);
    void intersect(VarSet*);
    void unite(VarSet*);
    void minus(VarSet*);
    bool belongs(SgVarRefExp*);
	bool belongs(SgSymbol*);
    bool equal(VarSet*);
    void print();
	void remove(SgVarRefExp*);
	SgVarRefExp* getFirst();

    inline bool isEmpty()
    { return list == NULL; }
};

class BaseBlock;
class ControlFlowGraph;
struct AnalysedCallsList;

struct BaseBlockItem
{
    BaseBlock* block;
    BaseBlockItem* next;
};

class BaseBlock
{
    int num;
	ControlFlowGraph* parent;
    ControlFlowItem* start;
    BaseBlockItem* prev;
    BaseBlockItem* succ;
    BaseBlock* lexNext;
    BaseBlock* lexPrev;
    VarSet* def;
    VarSet* use;
    VarSet* old_mrd_out;
    VarSet* old_mrd_in;
    VarSet* mrd_in;
    VarSet* mrd_out;
    VarSet* old_lv_out;
    VarSet* old_lv_in;
    VarSet* lv_in;
    VarSet* lv_out;
    bool undef;
    bool lv_undef;
    void setDefAndUse();
    char prev_status;
    void addExprToUse(SgExpression* e);
public:
    inline BaseBlock(ControlFlowItem* st, int n, ControlFlowGraph* par) : num(n), start(st), prev(NULL), lexNext(NULL), def(NULL), use(NULL), mrd_in(new VarSet()), mrd_out(new VarSet()), undef(true),
        lv_in(new VarSet()), lv_out(new VarSet()), lv_undef(false), succ(NULL), lexPrev(NULL), prev_status(-1), parent(par)
    {}

    inline void setNext(BaseBlock* next)
    { lexNext = next; }

    inline void setPrev(BaseBlock* prev)
    { lexPrev = prev; }

    void addToPrev(BaseBlock* pr);
    void addToSucc(BaseBlock* su);
    VarSet* getDef();
    VarSet* getUse();
    VarSet* getMrdIn();
    VarSet* getMrdOut();
    VarSet* getLVIn();
    VarSet* getLVOut();
    bool stepMrdIn();
    bool stepMrdOut();
    bool stepLVIn();
    bool stepLVOut();

    ControlFlowItem* containsParloopStart();
    ControlFlowItem* containsParloopEnd();

    ControlFlowItem* getStart();
    ControlFlowItem* getEnd();

    inline BaseBlock* getLexNext()
    { return lexNext; }

    inline BaseBlock* getLexPrev()
    { return lexPrev; }

    inline VarSet* getLiveIn()
    { return lv_in; }

    inline int getNum()
    { return num; }

    void print();
    void markAsReached();
    bool hasPrev();

	void ProcessProcedureHeader(bool, SgProcHedrStmt*, void*);
	void ProcessIntristicProcedure(bool, int narg, void* f);
	void ProcessUserProcedure(bool isFun, void* call, AnalysedCallsList* c);
	SgExpression* GetProcedureArgument(bool, void*, int);
	SgSymbol* GetProcedureName(bool, void*);
	void PrivateAnalysisForAllCalls();
};

class ControlFlowGraph
{
    BaseBlock* last;
    BaseBlock* first;
    VarSet* def;
    VarSet* use;
    VarSet* pri;
    void liveAnalysis();
public:
    ControlFlowGraph(ControlFlowItem* item, ControlFlowItem* end);
    VarSet* getPrivate();
    VarSet* getUse();
	VarSet* getDef();
    void privateAnalyzer();
	ControlFlowItem* getCFI() { return first->getStart(); }
};

struct AnalysedCallsList {
	SgStatement* header;
	ControlFlowGraph* graph;
	bool isIntristic;
	bool isPure;
	bool isFunction;
	AnalysedCallsList* next;
	bool hasBeenAnalysed;
	AnalysedCallsList(SgStatement* h, bool intr, bool pure, bool fun) { header = h; isIntristic = intr; isPure = pure; isFunction = fun; hasBeenAnalysed = false; }
	bool isArgIn(int num);
	bool isArgOut(int num);
};

class CallData
{
	AnalysedCallsList* calls_list;
	bool recursion_flag;
public:
	AnalysedCallsList* getLinkToCall(SgExpression*, SgStatement*);
	AnalysedCallsList* AddHeader(SgStatement*, bool isFun, SgSymbol* name);
	void AssociateGraphWithHeader(SgStatement*, ControlFlowGraph*);
	AnalysedCallsList* IsHeaderInList(SgStatement*);
	CallData() { recursion_flag = false; calls_list = NULL; }
	void printControlFlows();
};
