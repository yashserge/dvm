/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.3"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     PERCENT = 1,
     AMPERSAND = 2,
     ASTER = 3,
     CLUSTER = 4,
     COLON = 5,
     COMMA = 6,
     DASTER = 7,
     DEFINED_OPERATOR = 8,
     DOT = 9,
     DQUOTE = 10,
     GLOBAL_A = 11,
     LEFTAB = 12,
     LEFTPAR = 13,
     MINUS = 14,
     PLUS = 15,
     POINT_TO = 16,
     QUOTE = 17,
     RIGHTAB = 18,
     RIGHTPAR = 19,
     AND = 20,
     DSLASH = 21,
     EQV = 22,
     EQ = 23,
     EQUAL = 24,
     FFALSE = 25,
     GE = 26,
     GT = 27,
     LE = 28,
     LT = 29,
     NE = 30,
     NEQV = 31,
     NOT = 32,
     OR = 33,
     TTRUE = 34,
     SLASH = 35,
     XOR = 36,
     REFERENCE = 37,
     ACROSS = 38,
     ALIGN_WITH = 39,
     ALIGN = 40,
     ALLOCATABLE = 41,
     ALLOCATE = 42,
     ARITHIF = 43,
     ASSIGNMENT = 44,
     ASSIGN = 45,
     ASSIGNGOTO = 46,
     ASYNCHRONOUS = 47,
     ASYNCID = 48,
     ASYNCWAIT = 49,
     BACKSPACE = 50,
     BAD_CCONST = 51,
     BAD_SYMBOL = 52,
     BARRIER = 53,
     BLOCKDATA = 54,
     BLOCK = 55,
     BOZ_CONSTANT = 56,
     BYTE = 57,
     CALL = 58,
     CASE = 59,
     CHARACTER = 60,
     CHAR_CONSTANT = 61,
     CHECK = 62,
     CLOSE = 63,
     COMMON = 64,
     COMPLEX = 65,
     COMPGOTO = 66,
     CONSISTENT_GROUP = 67,
     CONSISTENT_SPEC = 68,
     CONSISTENT_START = 69,
     CONSISTENT_WAIT = 70,
     CONSISTENT = 71,
     CONSTRUCT_ID = 72,
     CONTAINS = 73,
     CONTINUE = 74,
     CORNER = 75,
     CYCLE = 76,
     DATA = 77,
     DEALLOCATE = 78,
     HPF_TEMPLATE = 79,
     DEBUG = 80,
     DEFAULT_CASE = 81,
     DEFINE = 82,
     DIMENSION = 83,
     DISTRIBUTE = 84,
     DOWHILE = 85,
     DOUBLEPRECISION = 86,
     DOUBLECOMPLEX = 87,
     DP_CONSTANT = 88,
     DVM_POINTER = 89,
     DYNAMIC = 90,
     ELEMENTAL = 91,
     ELSE = 92,
     ELSEIF = 93,
     ELSEWHERE = 94,
     ENDASYNCHRONOUS = 95,
     ENDDEBUG = 96,
     ENDINTERVAL = 97,
     ENDUNIT = 98,
     ENDDO = 99,
     ENDFILE = 100,
     ENDFORALL = 101,
     ENDIF = 102,
     ENDINTERFACE = 103,
     ENDMODULE = 104,
     ENDON = 105,
     ENDSELECT = 106,
     ENDTASK_REGION = 107,
     ENDTYPE = 108,
     ENDWHERE = 109,
     ENTRY = 110,
     EXIT = 111,
     EOLN = 112,
     EQUIVALENCE = 113,
     ERROR = 114,
     EXTERNAL = 115,
     F90 = 116,
     FIND = 117,
     FORALL = 118,
     FORMAT = 119,
     FUNCTION = 120,
     GATE = 121,
     GEN_BLOCK = 122,
     HEAP = 123,
     HIGH = 124,
     IDENTIFIER = 125,
     IMPLICIT = 126,
     IMPLICITNONE = 127,
     INCLUDE = 128,
     INDEPENDENT = 129,
     INDIRECT_ACCESS = 130,
     INDIRECT_GROUP = 131,
     INHERIT = 132,
     INQUIRE = 133,
     INTERFACEASSIGNMENT = 134,
     INTERFACEOPERATOR = 135,
     INTERFACE = 136,
     INTRINSIC = 137,
     INTEGER = 138,
     INTENT = 139,
     INTERVAL = 140,
     INOUT = 141,
     IN = 142,
     INT_CONSTANT = 143,
     LABEL = 144,
     LABEL_DECLARE = 145,
     LET = 146,
     LOGICAL = 147,
     LOGICALIF = 148,
     LOOP = 149,
     LOW = 150,
     MAXLOC = 151,
     MAX = 152,
     MAP = 153,
     MINLOC = 154,
     MIN = 155,
     MODULE_PROCEDURE = 156,
     MODULE = 157,
     MULT_BLOCK = 158,
     NAMEEQ = 159,
     NAMELIST = 160,
     NEW_VALUE = 161,
     NEW = 162,
     NULLIFY = 163,
     OCTAL_CONSTANT = 164,
     ONLY = 165,
     ON = 166,
     ON_DIR = 167,
     ONTO = 168,
     OPEN = 169,
     OPERATOR = 170,
     OPTIONAL = 171,
     OTHERWISE = 172,
     OUT = 173,
     OWN = 174,
     PARALLEL = 175,
     PARAMETER = 176,
     PAUSE = 177,
     PLAINDO = 178,
     PLAINGOTO = 179,
     POINTER = 180,
     POINTERLET = 181,
     PREFETCH = 182,
     PRINT = 183,
     PRIVATE = 184,
     PRODUCT = 185,
     PROGRAM = 186,
     PUBLIC = 187,
     PURE = 188,
     RANGE = 189,
     READ = 190,
     REALIGN_WITH = 191,
     REALIGN = 192,
     REAL = 193,
     REAL_CONSTANT = 194,
     RECURSIVE = 195,
     REDISTRIBUTE_NEW = 196,
     REDISTRIBUTE = 197,
     REDUCTION_GROUP = 198,
     REDUCTION_START = 199,
     REDUCTION_WAIT = 200,
     REDUCTION = 201,
     REMOTE_ACCESS_SPEC = 202,
     REMOTE_ACCESS = 203,
     REMOTE_GROUP = 204,
     RESET = 205,
     RESULT = 206,
     RETURN = 207,
     REWIND = 208,
     SAVE = 209,
     SECTION = 210,
     SELECT = 211,
     SEQUENCE = 212,
     SHADOW_COMPUTE = 213,
     SHADOW_GROUP = 214,
     SHADOW_RENEW = 215,
     SHADOW_START_SPEC = 216,
     SHADOW_START = 217,
     SHADOW_WAIT_SPEC = 218,
     SHADOW_WAIT = 219,
     SHADOW = 220,
     STAGE = 221,
     STATIC = 222,
     STAT = 223,
     STOP = 224,
     SUBROUTINE = 225,
     SUM = 226,
     SYNC = 227,
     TARGET = 228,
     TASK = 229,
     TASK_REGION = 230,
     THEN = 231,
     TO = 232,
     TRACEON = 233,
     TRACEOFF = 234,
     TRUNC = 235,
     TYPE = 236,
     TYPE_DECL = 237,
     UNDER = 238,
     UNKNOWN = 239,
     USE = 240,
     VIRTUAL = 241,
     VARIABLE = 242,
     WAIT = 243,
     WHERE = 244,
     WHERE_ASSIGN = 245,
     WHILE = 246,
     WITH = 247,
     WRITE = 248,
     COMMENT = 249,
     WGT_BLOCK = 250,
     HPF_PROCESSORS = 251,
     IOSTAT = 252,
     ERR = 253,
     END = 254,
     OMPDVM_ATOMIC = 255,
     OMPDVM_BARRIER = 256,
     OMPDVM_COPYIN = 257,
     OMPDVM_COPYPRIVATE = 258,
     OMPDVM_CRITICAL = 259,
     OMPDVM_ONETHREAD = 260,
     OMPDVM_DO = 261,
     OMPDVM_DYNAMIC = 262,
     OMPDVM_ENDCRITICAL = 263,
     OMPDVM_ENDDO = 264,
     OMPDVM_ENDMASTER = 265,
     OMPDVM_ENDORDERED = 266,
     OMPDVM_ENDPARALLEL = 267,
     OMPDVM_ENDPARALLELDO = 268,
     OMPDVM_ENDPARALLELSECTIONS = 269,
     OMPDVM_ENDPARALLELWORKSHARE = 270,
     OMPDVM_ENDSECTIONS = 271,
     OMPDVM_ENDSINGLE = 272,
     OMPDVM_ENDWORKSHARE = 273,
     OMPDVM_FIRSTPRIVATE = 274,
     OMPDVM_FLUSH = 275,
     OMPDVM_GUIDED = 276,
     OMPDVM_LASTPRIVATE = 277,
     OMPDVM_MASTER = 278,
     OMPDVM_NOWAIT = 279,
     OMPDVM_NONE = 280,
     OMPDVM_NUM_THREADS = 281,
     OMPDVM_ORDERED = 282,
     OMPDVM_PARALLEL = 283,
     OMPDVM_PARALLELDO = 284,
     OMPDVM_PARALLELSECTIONS = 285,
     OMPDVM_PARALLELWORKSHARE = 286,
     OMPDVM_RUNTIME = 287,
     OMPDVM_SECTION = 288,
     OMPDVM_SECTIONS = 289,
     OMPDVM_SCHEDULE = 290,
     OMPDVM_SHARED = 291,
     OMPDVM_SINGLE = 292,
     OMPDVM_THREADPRIVATE = 293,
     OMPDVM_WORKSHARE = 294,
     OMPDVM_NODES = 295,
     OMPDVM_IF = 296,
     IAND = 297,
     IEOR = 298,
     IOR = 299,
     ACC_REGION = 300,
     ACC_END_REGION = 301,
     ACC_CHECKSECTION = 302,
     ACC_END_CHECKSECTION = 303,
     ACC_GET_ACTUAL = 304,
     ACC_ACTUAL = 305,
     ACC_TARGETS = 306,
     ACC_ASYNC = 307,
     ACC_HOST = 308,
     ACC_CUDA = 309,
     ACC_LOCAL = 310,
     ACC_INLOCAL = 311,
     ACC_CUDA_BLOCK = 312,
     BY = 313,
     IO_MODE = 314,
     BINARY_OP = 317,
     UNARY_OP = 318
   };
#endif
/* Tokens.  */
#define PERCENT 1
#define AMPERSAND 2
#define ASTER 3
#define CLUSTER 4
#define COLON 5
#define COMMA 6
#define DASTER 7
#define DEFINED_OPERATOR 8
#define DOT 9
#define DQUOTE 10
#define GLOBAL_A 11
#define LEFTAB 12
#define LEFTPAR 13
#define MINUS 14
#define PLUS 15
#define POINT_TO 16
#define QUOTE 17
#define RIGHTAB 18
#define RIGHTPAR 19
#define AND 20
#define DSLASH 21
#define EQV 22
#define EQ 23
#define EQUAL 24
#define FFALSE 25
#define GE 26
#define GT 27
#define LE 28
#define LT 29
#define NE 30
#define NEQV 31
#define NOT 32
#define OR 33
#define TTRUE 34
#define SLASH 35
#define XOR 36
#define REFERENCE 37
#define ACROSS 38
#define ALIGN_WITH 39
#define ALIGN 40
#define ALLOCATABLE 41
#define ALLOCATE 42
#define ARITHIF 43
#define ASSIGNMENT 44
#define ASSIGN 45
#define ASSIGNGOTO 46
#define ASYNCHRONOUS 47
#define ASYNCID 48
#define ASYNCWAIT 49
#define BACKSPACE 50
#define BAD_CCONST 51
#define BAD_SYMBOL 52
#define BARRIER 53
#define BLOCKDATA 54
#define BLOCK 55
#define BOZ_CONSTANT 56
#define BYTE 57
#define CALL 58
#define CASE 59
#define CHARACTER 60
#define CHAR_CONSTANT 61
#define CHECK 62
#define CLOSE 63
#define COMMON 64
#define COMPLEX 65
#define COMPGOTO 66
#define CONSISTENT_GROUP 67
#define CONSISTENT_SPEC 68
#define CONSISTENT_START 69
#define CONSISTENT_WAIT 70
#define CONSISTENT 71
#define CONSTRUCT_ID 72
#define CONTAINS 73
#define CONTINUE 74
#define CORNER 75
#define CYCLE 76
#define DATA 77
#define DEALLOCATE 78
#define HPF_TEMPLATE 79
#define DEBUG 80
#define DEFAULT_CASE 81
#define DEFINE 82
#define DIMENSION 83
#define DISTRIBUTE 84
#define DOWHILE 85
#define DOUBLEPRECISION 86
#define DOUBLECOMPLEX 87
#define DP_CONSTANT 88
#define DVM_POINTER 89
#define DYNAMIC 90
#define ELEMENTAL 91
#define ELSE 92
#define ELSEIF 93
#define ELSEWHERE 94
#define ENDASYNCHRONOUS 95
#define ENDDEBUG 96
#define ENDINTERVAL 97
#define ENDUNIT 98
#define ENDDO 99
#define ENDFILE 100
#define ENDFORALL 101
#define ENDIF 102
#define ENDINTERFACE 103
#define ENDMODULE 104
#define ENDON 105
#define ENDSELECT 106
#define ENDTASK_REGION 107
#define ENDTYPE 108
#define ENDWHERE 109
#define ENTRY 110
#define EXIT 111
#define EOLN 112
#define EQUIVALENCE 113
#define ERROR 114
#define EXTERNAL 115
#define F90 116
#define FIND 117
#define FORALL 118
#define FORMAT 119
#define FUNCTION 120
#define GATE 121
#define GEN_BLOCK 122
#define HEAP 123
#define HIGH 124
#define IDENTIFIER 125
#define IMPLICIT 126
#define IMPLICITNONE 127
#define INCLUDE 128
#define INDEPENDENT 129
#define INDIRECT_ACCESS 130
#define INDIRECT_GROUP 131
#define INHERIT 132
#define INQUIRE 133
#define INTERFACEASSIGNMENT 134
#define INTERFACEOPERATOR 135
#define INTERFACE 136
#define INTRINSIC 137
#define INTEGER 138
#define INTENT 139
#define INTERVAL 140
#define INOUT 141
#define IN 142
#define INT_CONSTANT 143
#define LABEL 144
#define LABEL_DECLARE 145
#define LET 146
#define LOGICAL 147
#define LOGICALIF 148
#define LOOP 149
#define LOW 150
#define MAXLOC 151
#define MAX 152
#define MAP 153
#define MINLOC 154
#define MIN 155
#define MODULE_PROCEDURE 156
#define MODULE 157
#define MULT_BLOCK 158
#define NAMEEQ 159
#define NAMELIST 160
#define NEW_VALUE 161
#define NEW 162
#define NULLIFY 163
#define OCTAL_CONSTANT 164
#define ONLY 165
#define ON 166
#define ON_DIR 167
#define ONTO 168
#define OPEN 169
#define OPERATOR 170
#define OPTIONAL 171
#define OTHERWISE 172
#define OUT 173
#define OWN 174
#define PARALLEL 175
#define PARAMETER 176
#define PAUSE 177
#define PLAINDO 178
#define PLAINGOTO 179
#define POINTER 180
#define POINTERLET 181
#define PREFETCH 182
#define PRINT 183
#define PRIVATE 184
#define PRODUCT 185
#define PROGRAM 186
#define PUBLIC 187
#define PURE 188
#define RANGE 189
#define READ 190
#define REALIGN_WITH 191
#define REALIGN 192
#define REAL 193
#define REAL_CONSTANT 194
#define RECURSIVE 195
#define REDISTRIBUTE_NEW 196
#define REDISTRIBUTE 197
#define REDUCTION_GROUP 198
#define REDUCTION_START 199
#define REDUCTION_WAIT 200
#define REDUCTION 201
#define REMOTE_ACCESS_SPEC 202
#define REMOTE_ACCESS 203
#define REMOTE_GROUP 204
#define RESET 205
#define RESULT 206
#define RETURN 207
#define REWIND 208
#define SAVE 209
#define SECTION 210
#define SELECT 211
#define SEQUENCE 212
#define SHADOW_COMPUTE 213
#define SHADOW_GROUP 214
#define SHADOW_RENEW 215
#define SHADOW_START_SPEC 216
#define SHADOW_START 217
#define SHADOW_WAIT_SPEC 218
#define SHADOW_WAIT 219
#define SHADOW 220
#define STAGE 221
#define STATIC 222
#define STAT 223
#define STOP 224
#define SUBROUTINE 225
#define SUM 226
#define SYNC 227
#define TARGET 228
#define TASK 229
#define TASK_REGION 230
#define THEN 231
#define TO 232
#define TRACEON 233
#define TRACEOFF 234
#define TRUNC 235
#define TYPE 236
#define TYPE_DECL 237
#define UNDER 238
#define UNKNOWN 239
#define USE 240
#define VIRTUAL 241
#define VARIABLE 242
#define WAIT 243
#define WHERE 244
#define WHERE_ASSIGN 245
#define WHILE 246
#define WITH 247
#define WRITE 248
#define COMMENT 249
#define WGT_BLOCK 250
#define HPF_PROCESSORS 251
#define IOSTAT 252
#define ERR 253
#define END 254
#define OMPDVM_ATOMIC 255
#define OMPDVM_BARRIER 256
#define OMPDVM_COPYIN 257
#define OMPDVM_COPYPRIVATE 258
#define OMPDVM_CRITICAL 259
#define OMPDVM_ONETHREAD 260
#define OMPDVM_DO 261
#define OMPDVM_DYNAMIC 262
#define OMPDVM_ENDCRITICAL 263
#define OMPDVM_ENDDO 264
#define OMPDVM_ENDMASTER 265
#define OMPDVM_ENDORDERED 266
#define OMPDVM_ENDPARALLEL 267
#define OMPDVM_ENDPARALLELDO 268
#define OMPDVM_ENDPARALLELSECTIONS 269
#define OMPDVM_ENDPARALLELWORKSHARE 270
#define OMPDVM_ENDSECTIONS 271
#define OMPDVM_ENDSINGLE 272
#define OMPDVM_ENDWORKSHARE 273
#define OMPDVM_FIRSTPRIVATE 274
#define OMPDVM_FLUSH 275
#define OMPDVM_GUIDED 276
#define OMPDVM_LASTPRIVATE 277
#define OMPDVM_MASTER 278
#define OMPDVM_NOWAIT 279
#define OMPDVM_NONE 280
#define OMPDVM_NUM_THREADS 281
#define OMPDVM_ORDERED 282
#define OMPDVM_PARALLEL 283
#define OMPDVM_PARALLELDO 284
#define OMPDVM_PARALLELSECTIONS 285
#define OMPDVM_PARALLELWORKSHARE 286
#define OMPDVM_RUNTIME 287
#define OMPDVM_SECTION 288
#define OMPDVM_SECTIONS 289
#define OMPDVM_SCHEDULE 290
#define OMPDVM_SHARED 291
#define OMPDVM_SINGLE 292
#define OMPDVM_THREADPRIVATE 293
#define OMPDVM_WORKSHARE 294
#define OMPDVM_NODES 295
#define OMPDVM_IF 296
#define IAND 297
#define IEOR 298
#define IOR 299
#define ACC_REGION 300
#define ACC_END_REGION 301
#define ACC_CHECKSECTION 302
#define ACC_END_CHECKSECTION 303
#define ACC_GET_ACTUAL 304
#define ACC_ACTUAL 305
#define ACC_TARGETS 306
#define ACC_ASYNC 307
#define ACC_HOST 308
#define ACC_CUDA 309
#define ACC_LOCAL 310
#define ACC_INLOCAL 311
#define ACC_CUDA_BLOCK 312
#define BY 313
#define IO_MODE 314
#define BINARY_OP 317
#define UNARY_OP 318




/* Copy the first part of user declarations.  */
#line 316 "gram1.y"

#include <string.h>
#include "inc.h"
#include "extern.h"
#include "defines.h"
#include "fdvm.h"
#include "fm.h"

/* We may use builtin alloca */
#include "compatible.h"
#ifdef _NEEDALLOCAH_
#  include <alloca.h>
#endif

#define EXTEND_NODE 2  /* move the definition to h/ files. */

extern PTR_BFND global_bfnd, pred_bfnd;
extern PTR_SYMB star_symb;
extern PTR_SYMB global_list;
extern PTR_TYPE global_bool;
extern PTR_TYPE global_int;
extern PTR_TYPE global_float;
extern PTR_TYPE global_double;
extern PTR_TYPE global_char;
extern PTR_TYPE global_string;
extern PTR_TYPE global_string_2;
extern PTR_TYPE global_complex;
extern PTR_TYPE global_dcomplex;
extern PTR_TYPE global_gate;
extern PTR_TYPE global_event;
extern PTR_TYPE global_sequence;
extern PTR_TYPE global_default;
extern PTR_LABEL thislabel;
extern PTR_CMNT comments, cur_comment;
extern PTR_BFND last_bfnd;
extern PTR_TYPE impltype[];
extern int nioctl;
extern int maxdim;
extern long yystno;	/* statement label */
extern char stmtbuf[];	/* input buffer */
extern char *commentbuf;	/* comments buffer from scanner */
extern PTR_BLOB head_blob;
extern PTR_BLOB cur_blob;
extern PTR_TYPE vartype; /* variable type */
extern int end_group;
extern char saveall;
extern int privateall;
extern int needkwd;
extern int implkwd;
extern int opt_kwd_hedr;
/* added for FORTRAN 90 */
extern PTR_LLND first_unresolved_call;
extern PTR_LLND last_unresolved_call;
extern int data_stat;
extern char yyquote;

extern int warn_all;
extern int statement_kind; /* kind of statement: 1 - HPF-DVM-directive, 0 - Fortran statement*/ 
int extend_flag = 0;

static int do_name_err;
static int ndim;	/* number of dimension */
/*!!! hpf */
static int explicit_shape; /*  1 if shape specification is explicit */
/* static int varleng;*/	/* variable size */
static int lastwasbranch = NO;	/* set if last stmt was a branch stmt */
static int thiswasbranch = NO;	/* set if this stmt is a branch stmt */
static PTR_SYMB type_var = SMNULL;
static PTR_LLND stat_alloc = LLNULL; /* set if ALLOCATE/DEALLOCATE stmt has STAT-clause*/
/* static int subscripts_status = 0; */
static int type_options,type_opt;   /* The various options used to declare a name -
                                      RECURSIVE, POINTER, OPTIONAL etc.         */
static PTR_BFND module_scope;
static int position = IN_OUTSIDE;            
static int attr_ndim;           /* number of dimensions in DIMENSION (array_spec)
                                   attribute declaration */
static PTR_LLND attr_dims;     /* low level representation of array_spec in
                                   DIMENSION (array_spec) attribute declarartion. */
static int in_vec = NO;	      /* set if processing array constructor */


/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 397 "gram1.y"
{
    int token;
    char charv;
    char *charp;
    PTR_BFND bf_node;
    PTR_LLND ll_node;
    PTR_SYMB symbol;
    PTR_TYPE data_type;
    PTR_HASH hash_entry;
    PTR_LABEL label;
}
/* Line 187 of yacc.c.  */
#line 825 "gram1.tab.c"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */
#line 587 "gram1.y"

void add_scope_level();
void delete_beyond_scope_level();
PTR_HASH look_up_sym();
PTR_HASH just_look_up_sym();
PTR_HASH just_look_up_sym_in_scope();
PTR_HASH look_up_op();
PTR_SYMB make_constant();
PTR_SYMB make_scalar();
PTR_SYMB make_array();
PTR_SYMB make_pointer();
PTR_SYMB make_function();
PTR_SYMB make_external();
PTR_SYMB make_intrinsic();
PTR_SYMB make_procedure();
PTR_SYMB make_process();
PTR_SYMB make_program();
PTR_SYMB make_module();
PTR_SYMB make_common();
PTR_SYMB make_derived_type();
PTR_SYMB make_local_entity();
PTR_SYMB make_global_entity();
PTR_TYPE make_type_node();
PTR_TYPE lookup_type(), make_type();
void     process_type();
void     process_interface();
void     bind();
void     late_bind_if_needed();
PTR_SYMB component();
PTR_SYMB resolve_overloading();
PTR_BFND cur_scope();
PTR_BFND subroutine_call();
PTR_BFND process_call();
PTR_LLND deal_with_options();
PTR_LLND intrinsic_op_node();
PTR_LLND defined_op_node();
int is_substring_ref();
int is_array_section_ref();
PTR_LLND dim_expr(); 
PTR_BFND exit_stat();
PTR_BFND make_do();
PTR_BFND make_pardo();
PTR_BFND make_enddoall();
PTR_TYPE install_array(); 
PTR_SYMB install_entry(); 
void install_param_list();
PTR_LLND construct_entry_list();
void copy_sym_data();
PTR_LLND check_and_install();
PTR_HASH look_up();
PTR_BFND get_bfnd(); 
PTR_BLOB make_blob();
PTR_LABEL make_label();
PTR_LABEL make_label_node();
int is_interface_stat();
PTR_LLND make_llnd (); 
PTR_LLND make_llnd_label (); 
PTR_TYPE make_sa_type(); 
PTR_SYMB procedure_call();
PTR_BFND proc_list();
PTR_SYMB set_id_list();
PTR_LLND set_ll_list();
PTR_LLND add_to_lowLevelList(), add_to_lowList();
PTR_BFND set_stat_list() ;
PTR_BLOB follow_blob();
PTR_SYMB proc_decl_init();
PTR_CMNT make_comment();
PTR_HASH correct_symtab();
char *copyn();
char *convic();
int atoi();
PTR_BFND make_logif();
PTR_BFND make_if();
PTR_BFND make_forall();
void startproc();
void match_parameters();
void make_else();
void make_elseif();
void make_endif();
void make_elsewhere();
void make_elsewhere_mask();
void make_endwhere();
void make_endforall();
void make_endselect();
void make_extend();
void make_endextend();
void make_section();
void make_section_extend();
void doinclude();
void endproc();
void err();
void execerr();
void flline();
void warn();
void warn1();
void newprog();
void set_type();
void dclerr();
void enddcl();
void install_const();
void setimpl();
void copy_module_scope();

long convci();
void set_expr_type();
void errstr();
void yyerror();
void set_blobs();
void make_loop();
void startioctl();
void endioctl();
void redefine_func_arg_type();
int isResultVar();

/* used by FORTRAN M */
PTR_BFND make_processdo();
PTR_BFND make_processes();
PTR_BFND make_endprocesses();

PTR_BFND make_endparallel();/*OMP*/
PTR_BFND make_parallel();/*OMP*/
PTR_BFND make_endsingle();/*OMP*/
PTR_BFND make_single();/*OMP*/
PTR_BFND make_endmaster();/*OMP*/
PTR_BFND make_master();/*OMP*/
PTR_BFND make_endordered();/*OMP*/
PTR_BFND make_ordered();/*OMP*/
PTR_BFND make_endcritical();/*OMP*/
PTR_BFND make_critical();/*OMP*/
PTR_BFND make_endsections();/*OMP*/
PTR_BFND make_sections();/*OMP*/
PTR_BFND make_ompsection();/*OMP*/
PTR_BFND make_endparallelsections();/*OMP*/
PTR_BFND make_parallelsections();/*OMP*/
PTR_BFND make_endworkshare();/*OMP*/
PTR_BFND make_workshare();/*OMP*/
PTR_BFND make_endparallelworkshare();/*OMP*/
PTR_BFND make_parallelworkshare();/*OMP*/



/* Line 216 of yacc.c.  */
#line 978 "gram1.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int i)
#else
static int
YYID (i)
    int i;
#endif
{
  return i;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  2
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   5178

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  319
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  467
/* YYNRULES -- Number of rules.  */
#define YYNRULES  1153
/* YYNRULES -- Number of states.  */
#define YYNSTATES  2247

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   318

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint16 yytranslate[] =
{
       0,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,   163,   164,   165,   166,   167,   168,   169,   170,   171,
     172,   173,   174,   175,   176,   177,   178,   179,   180,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,   191,
     192,   193,   194,   195,   196,   197,   198,   199,   200,   201,
     202,   203,   204,   205,   206,   207,   208,   209,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,   220,   221,
     222,   223,   224,   225,   226,   227,   228,   229,   230,   231,
     232,   233,   234,   235,   236,   237,   238,   239,   240,   241,
     242,   243,   244,   245,   246,   247,   248,   249,   250,   251,
     252,   253,   254,   255,   256,   257,   258,   259,   260,   261,
     262,   263,   264,   265,   266,   267,   268,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   293,   294,   295,   296,   297,   298,   299,   300,   301,
     302,   303,   304,   305,   306,   307,   308,   309,   310,   311,
     312,   313,   314,   315,   316,     1,     2,   317,   318
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     4,     8,    12,    16,    20,    24,    27,
      29,    31,    33,    37,    41,    46,    52,    58,    62,    67,
      71,    72,    75,    78,    81,    83,    85,    90,    96,   101,
     107,   110,   116,   118,   119,   121,   122,   124,   125,   128,
     132,   134,   138,   140,   142,   144,   145,   146,   147,   149,
     151,   153,   155,   157,   159,   161,   163,   165,   167,   169,
     171,   173,   176,   181,   184,   190,   192,   194,   196,   198,
     200,   202,   204,   206,   208,   210,   212,   214,   217,   221,
     227,   233,   236,   238,   240,   242,   244,   246,   248,   250,
     252,   254,   256,   258,   260,   262,   264,   266,   268,   270,
     272,   274,   276,   278,   283,   291,   294,   298,   306,   313,
     314,   317,   323,   325,   330,   332,   334,   336,   339,   341,
     346,   348,   350,   352,   354,   356,   358,   361,   364,   367,
     369,   371,   379,   383,   388,   392,   397,   401,   404,   410,
     411,   414,   417,   423,   424,   431,   437,   438,   441,   445,
     447,   449,   451,   453,   455,   457,   459,   461,   463,   465,
     466,   468,   472,   477,   482,   483,   485,   491,   501,   503,
     505,   508,   511,   512,   513,   516,   519,   525,   530,   535,
     539,   544,   548,   553,   557,   561,   566,   572,   576,   581,
     587,   591,   595,   597,   601,   604,   609,   613,   618,   622,
     626,   630,   634,   638,   642,   644,   649,   651,   653,   658,
     662,   663,   664,   669,   671,   675,   677,   681,   684,   688,
     692,   697,   700,   701,   703,   705,   709,   715,   717,   721,
     722,   724,   732,   734,   738,   741,   744,   748,   750,   752,
     757,   761,   764,   766,   768,   770,   772,   776,   778,   782,
     784,   786,   793,   795,   797,   800,   803,   805,   809,   811,
     814,   817,   819,   823,   825,   829,   835,   837,   839,   841,
     844,   847,   851,   855,   857,   861,   865,   867,   871,   873,
     875,   879,   881,   885,   887,   889,   893,   899,   900,   901,
     903,   908,   913,   915,   919,   923,   926,   928,   932,   936,
     943,   950,   958,   960,   962,   966,   970,   972,   974,   978,
     982,   983,   987,   988,   991,   995,   997,   999,  1002,  1006,
    1008,  1010,  1012,  1016,  1018,  1022,  1024,  1026,  1030,  1035,
    1036,  1039,  1042,  1044,  1046,  1050,  1052,  1056,  1058,  1059,
    1060,  1061,  1064,  1065,  1067,  1069,  1071,  1074,  1077,  1082,
    1084,  1088,  1090,  1094,  1096,  1098,  1100,  1102,  1106,  1110,
    1114,  1118,  1122,  1125,  1128,  1131,  1135,  1139,  1143,  1147,
    1151,  1155,  1159,  1163,  1167,  1171,  1175,  1178,  1182,  1186,
    1188,  1190,  1192,  1194,  1196,  1198,  1204,  1211,  1216,  1222,
    1226,  1228,  1230,  1236,  1241,  1244,  1245,  1247,  1253,  1254,
    1256,  1258,  1262,  1264,  1268,  1271,  1273,  1275,  1277,  1279,
    1281,  1283,  1287,  1291,  1297,  1299,  1301,  1305,  1308,  1314,
    1319,  1324,  1328,  1331,  1333,  1334,  1335,  1342,  1344,  1346,
    1348,  1353,  1359,  1361,  1366,  1372,  1373,  1375,  1379,  1381,
    1383,  1385,  1388,  1392,  1396,  1399,  1401,  1404,  1407,  1410,
    1414,  1422,  1426,  1430,  1432,  1435,  1438,  1440,  1443,  1447,
    1449,  1451,  1457,  1465,  1466,  1473,  1478,  1490,  1504,  1509,
    1513,  1517,  1525,  1534,  1538,  1540,  1543,  1546,  1550,  1552,
    1556,  1557,  1559,  1560,  1562,  1564,  1567,  1573,  1580,  1582,
    1586,  1590,  1591,  1594,  1596,  1602,  1610,  1611,  1613,  1617,
    1621,  1628,  1634,  1641,  1646,  1652,  1658,  1661,  1663,  1665,
    1676,  1678,  1682,  1687,  1691,  1695,  1699,  1703,  1710,  1717,
    1723,  1732,  1735,  1739,  1743,  1751,  1759,  1760,  1762,  1767,
    1770,  1775,  1777,  1780,  1783,  1785,  1787,  1788,  1789,  1790,
    1793,  1796,  1799,  1802,  1805,  1808,  1811,  1815,  1820,  1823,
    1827,  1829,  1833,  1837,  1839,  1841,  1843,  1847,  1849,  1851,
    1853,  1855,  1857,  1861,  1865,  1867,  1872,  1874,  1876,  1878,
    1881,  1884,  1887,  1889,  1893,  1897,  1902,  1907,  1909,  1913,
    1915,  1921,  1923,  1925,  1927,  1931,  1935,  1939,  1943,  1947,
    1951,  1953,  1957,  1963,  1969,  1975,  1976,  1977,  1979,  1983,
    1985,  1987,  1991,  1995,  1999,  2003,  2006,  2010,  2014,  2015,
    2017,  2019,  2021,  2023,  2025,  2027,  2029,  2031,  2033,  2035,
    2037,  2039,  2041,  2043,  2045,  2047,  2049,  2051,  2053,  2055,
    2057,  2059,  2061,  2063,  2065,  2067,  2069,  2071,  2073,  2075,
    2077,  2079,  2081,  2083,  2085,  2087,  2089,  2091,  2093,  2095,
    2097,  2099,  2101,  2103,  2105,  2107,  2109,  2111,  2113,  2115,
    2117,  2119,  2121,  2123,  2125,  2130,  2135,  2139,  2141,  2145,
    2147,  2151,  2153,  2157,  2159,  2164,  2168,  2170,  2174,  2176,
    2180,  2182,  2187,  2192,  2197,  2201,  2205,  2207,  2211,  2215,
    2217,  2221,  2225,  2227,  2231,  2235,  2237,  2241,  2242,  2248,
    2255,  2264,  2266,  2270,  2272,  2274,  2276,  2281,  2283,  2284,
    2287,  2291,  2294,  2299,  2300,  2302,  2308,  2313,  2320,  2325,
    2327,  2329,  2332,  2338,  2343,  2348,  2355,  2357,  2361,  2363,
    2365,  2372,  2377,  2379,  2383,  2385,  2387,  2389,  2391,  2393,
    2397,  2399,  2401,  2403,  2410,  2415,  2417,  2422,  2424,  2426,
    2428,  2430,  2435,  2438,  2446,  2448,  2453,  2455,  2467,  2468,
    2471,  2475,  2477,  2481,  2483,  2487,  2489,  2493,  2495,  2499,
    2501,  2505,  2507,  2511,  2520,  2522,  2526,  2529,  2532,  2542,
    2544,  2548,  2550,  2555,  2557,  2561,  2563,  2565,  2566,  2568,
    2570,  2573,  2575,  2577,  2579,  2581,  2583,  2585,  2587,  2589,
    2591,  2593,  2602,  2609,  2618,  2625,  2627,  2634,  2641,  2648,
    2655,  2657,  2661,  2667,  2669,  2673,  2682,  2689,  2696,  2701,
    2707,  2713,  2716,  2719,  2720,  2722,  2726,  2728,  2733,  2741,
    2743,  2747,  2751,  2753,  2757,  2763,  2767,  2771,  2773,  2777,
    2779,  2781,  2785,  2789,  2793,  2797,  2808,  2817,  2828,  2829,
    2830,  2832,  2835,  2840,  2845,  2852,  2854,  2856,  2858,  2860,
    2862,  2864,  2866,  2868,  2870,  2872,  2874,  2881,  2886,  2891,
    2895,  2905,  2907,  2909,  2913,  2915,  2921,  2927,  2937,  2938,
    2940,  2944,  2948,  2952,  2956,  2963,  2967,  2971,  2975,  2979,
    2987,  2993,  2995,  2997,  3001,  3006,  3008,  3010,  3014,  3016,
    3018,  3022,  3026,  3029,  3033,  3038,  3043,  3049,  3055,  3057,
    3060,  3065,  3070,  3075,  3076,  3078,  3081,  3089,  3096,  3100,
    3104,  3112,  3118,  3120,  3124,  3126,  3131,  3134,  3138,  3142,
    3147,  3154,  3158,  3161,  3165,  3167,  3169,  3174,  3180,  3184,
    3191,  3194,  3199,  3202,  3204,  3208,  3212,  3213,  3215,  3218,
    3221,  3224,  3227,  3237,  3243,  3245,  3249,  3252,  3255,  3258,
    3260,  3262,  3264,  3266,  3268,  3270,  3272,  3274,  3276,  3278,
    3280,  3282,  3284,  3286,  3288,  3290,  3292,  3294,  3296,  3298,
    3300,  3302,  3304,  3306,  3308,  3310,  3312,  3315,  3318,  3323,
    3327,  3332,  3338,  3340,  3342,  3344,  3346,  3348,  3350,  3352,
    3354,  3356,  3362,  3365,  3368,  3371,  3374,  3377,  3383,  3385,
    3387,  3389,  3394,  3399,  3404,  3409,  3411,  3413,  3415,  3417,
    3419,  3421,  3423,  3425,  3427,  3429,  3431,  3433,  3435,  3437,
    3439,  3444,  3448,  3453,  3459,  3461,  3463,  3465,  3467,  3472,
    3476,  3479,  3484,  3488,  3493,  3497,  3502,  3508,  3510,  3512,
    3514,  3516,  3518,  3520,  3522,  3530,  3536,  3538,  3540,  3542,
    3544,  3549,  3553,  3558,  3564,  3566,  3568,  3573,  3577,  3582,
    3588,  3590,  3592,  3595,  3597,  3600,  3605,  3609,  3614,  3618,
    3623,  3629,  3631,  3633,  3635,  3637,  3639,  3641,  3643,  3645,
    3647,  3649,  3651,  3654,  3659,  3663,  3666,  3671,  3675,  3678,
    3682,  3685,  3688,  3691,  3694,  3697,  3700,  3704,  3707,  3713,
    3716,  3722,  3725,  3731,  3733,  3735,  3739,  3743,  3744,  3745,
    3747,  3749,  3751,  3753,  3755,  3757,  3761,  3764,  3770,  3775,
    3778,  3784,  3789,  3792,  3795,  3797,  3799,  3803,  3806,  3809,
    3812,  3817,  3822,  3827,  3832,  3837,  3842,  3844,  3846,  3848,
    3852,  3855,  3858,  3860
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int16 yyrhs[] =
{
     320,     0,    -1,    -1,   320,   321,   114,    -1,   322,   323,
     539,    -1,   322,   340,   539,    -1,   322,   485,   539,    -1,
     322,   130,   336,    -1,   322,   241,    -1,   251,    -1,     1,
      -1,   146,    -1,   188,   324,   331,    -1,    56,   324,   332,
      -1,   227,   324,   326,   333,    -1,   325,   227,   324,   326,
     333,    -1,   122,   324,   327,   333,   329,    -1,   328,   333,
     329,    -1,   112,   330,   333,   329,    -1,   159,   324,   330,
      -1,    -1,   197,   337,    -1,   190,   337,    -1,    93,   337,
      -1,   330,    -1,   330,    -1,   361,   122,   324,   330,    -1,
     361,   325,   122,   324,   330,    -1,   325,   122,   324,   330,
      -1,   325,   361,   122,   324,   330,    -1,   337,   338,    -1,
     337,   208,    15,   330,    21,    -1,   127,    -1,    -1,   330,
      -1,    -1,   330,    -1,    -1,    15,    21,    -1,    15,   334,
      21,    -1,   335,    -1,   334,     8,   335,    -1,   330,    -1,
       5,    -1,    63,    -1,    -1,    -1,    -1,   345,    -1,   346,
      -1,   347,    -1,   377,    -1,   373,    -1,   540,    -1,   382,
      -1,   383,    -1,   384,    -1,   442,    -1,   363,    -1,   378,
      -1,   388,    -1,   211,   452,    -1,   211,   452,   453,   419,
      -1,   121,   451,    -1,   178,   452,    15,   425,    21,    -1,
     353,    -1,   354,    -1,   359,    -1,   356,    -1,   358,    -1,
     374,    -1,   375,    -1,   376,    -1,   341,    -1,   429,    -1,
     427,    -1,   355,    -1,   138,   452,    -1,   138,   452,   330,
      -1,   137,   452,    15,   343,    21,    -1,   136,   452,    15,
      26,    21,    -1,   105,   494,    -1,    10,    -1,   342,    -1,
     344,    -1,    17,    -1,    16,    -1,     5,    -1,     9,    -1,
      37,    -1,    23,    -1,    22,    -1,    35,    -1,    38,    -1,
      34,    -1,    25,    -1,    32,    -1,    29,    -1,    28,    -1,
      31,    -1,    30,    -1,    33,    -1,    24,    -1,   239,   452,
     453,   330,    -1,   239,     8,   452,   337,   352,   453,   330,
      -1,   110,   452,    -1,   110,   452,   330,    -1,   361,   348,
     330,   452,   435,   367,   372,    -1,   347,     8,   330,   435,
     367,   372,    -1,    -1,     7,     7,    -1,     8,   337,   349,
       7,     7,    -1,   350,    -1,   349,     8,   337,   350,    -1,
     178,    -1,   352,    -1,    43,    -1,    85,   435,    -1,   117,
      -1,   141,    15,   351,    21,    -1,   139,    -1,   173,    -1,
     182,    -1,   211,    -1,   224,    -1,   230,    -1,   337,   144,
      -1,   337,   175,    -1,   337,   143,    -1,   189,    -1,   186,
      -1,   141,   452,    15,   351,    21,   453,   330,    -1,   353,
       8,   330,    -1,   173,   452,   453,   330,    -1,   354,     8,
     330,    -1,   224,   452,   453,   381,    -1,   355,     8,   381,
      -1,   186,   452,    -1,   186,   452,   453,   357,   421,    -1,
      -1,   214,   452,    -1,   189,   452,    -1,   189,   452,   453,
     360,   421,    -1,    -1,   365,   362,   515,   369,   516,   362,
      -1,   238,    15,   330,    21,   362,    -1,    -1,   364,   330,
      -1,   363,     8,   330,    -1,    13,    -1,     6,    -1,   366,
      -1,   140,    -1,   195,    -1,    67,    -1,    88,    -1,    89,
      -1,   149,    -1,    62,    -1,    -1,   368,    -1,     5,   472,
     362,    -1,     5,    15,     5,    21,    -1,     5,    15,   458,
      21,    -1,    -1,   368,    -1,    15,   535,   370,   371,    21,
      -1,    15,   535,   370,   371,     8,   535,   370,   371,    21,
      -1,   458,    -1,     5,    -1,   526,   458,    -1,   526,     5,
      -1,    -1,    -1,    26,   458,    -1,    18,   458,    -1,    85,
     453,   452,   330,   435,    -1,   373,     8,   330,   435,    -1,
      43,   452,   453,   381,    -1,   374,     8,   381,    -1,   182,
     452,   453,   381,    -1,   375,     8,   381,    -1,   230,   452,
     453,   381,    -1,   376,     8,   381,    -1,    66,   452,   381,
      -1,    66,   452,   380,   381,    -1,   377,   509,   380,   509,
     381,    -1,   377,     8,   381,    -1,   162,   452,   379,   461,
      -1,   378,   509,   379,   509,   461,    -1,   378,     8,   461,
      -1,    37,   330,    37,    -1,    23,    -1,    37,   330,    37,
      -1,   330,   435,    -1,   117,   452,   453,   330,    -1,   382,
       8,   330,    -1,   139,   452,   453,   330,    -1,   383,     8,
     330,    -1,   115,   452,   385,    -1,   384,     8,   385,    -1,
      15,   386,    21,    -1,   387,     8,   387,    -1,   386,     8,
     387,    -1,   330,    -1,   330,    15,   457,    21,    -1,   466,
      -1,   389,    -1,    79,   451,   390,   392,    -1,   389,   509,
     392,    -1,    -1,    -1,   393,    37,   394,    37,    -1,   395,
      -1,   393,     8,   395,    -1,   406,    -1,   394,     8,   406,
      -1,   396,   398,    -1,   396,   398,   399,    -1,   396,   398,
     400,    -1,   396,   398,   399,   400,    -1,   396,   403,    -1,
      -1,   330,    -1,   330,    -1,    15,   401,    21,    -1,    15,
     402,     7,   402,    21,    -1,   415,    -1,   401,     8,   415,
      -1,    -1,   415,    -1,    15,   404,     8,   397,    26,   401,
      21,    -1,   405,    -1,   404,     8,   405,    -1,   398,   399,
      -1,   398,   400,    -1,   398,   399,   400,    -1,   403,    -1,
     407,    -1,   396,   397,     5,   407,    -1,   410,     5,   407,
      -1,   396,   397,    -1,   409,    -1,   411,    -1,   413,    -1,
      36,    -1,    36,   240,   475,    -1,    27,    -1,    27,   240,
     475,    -1,    63,    -1,   408,    -1,   396,   461,    15,   535,
     454,    21,    -1,    58,    -1,   410,    -1,    17,   410,    -1,
      16,   410,    -1,   145,    -1,   145,   240,   475,    -1,   412,
      -1,    17,   412,    -1,    16,   412,    -1,   196,    -1,   196,
     240,   475,    -1,    90,    -1,    90,   240,   475,    -1,    15,
     414,     8,   414,    21,    -1,   411,    -1,   409,    -1,   416,
      -1,    17,   416,    -1,    16,   416,    -1,   415,    17,   416,
      -1,   415,    16,   416,    -1,   417,    -1,   416,     5,   417,
      -1,   416,    37,   417,    -1,   418,    -1,   418,     9,   417,
      -1,   145,    -1,   397,    -1,    15,   415,    21,    -1,   420,
      -1,   419,     8,   420,    -1,   381,    -1,   380,    -1,   422,
     424,   423,    -1,   421,     8,   422,   424,   423,    -1,    -1,
      -1,   330,    -1,   172,    15,   343,    21,    -1,    46,    15,
      26,    21,    -1,   426,    -1,   425,     8,   426,    -1,   330,
      26,   458,    -1,   158,   428,    -1,   330,    -1,   428,     8,
     330,    -1,   242,   452,   430,    -1,   242,   452,   430,     8,
     339,   433,    -1,   242,   452,   430,     8,   339,   167,    -1,
     242,   452,   430,     8,   339,   167,   431,    -1,   330,    -1,
     432,    -1,   431,     8,   432,    -1,   330,    18,   330,    -1,
     330,    -1,   434,    -1,   433,     8,   434,    -1,   330,    18,
     330,    -1,    -1,    15,   436,    21,    -1,    -1,   437,   438,
      -1,   436,     8,   438,    -1,   439,    -1,     7,    -1,   458,
       7,    -1,   458,     7,   439,    -1,     5,    -1,   458,    -1,
     441,    -1,   440,     8,   441,    -1,   145,    -1,   128,   452,
     443,    -1,   129,    -1,   444,    -1,   443,     8,   444,    -1,
     445,    15,   448,    21,    -1,    -1,   446,   447,    -1,   225,
     366,    -1,   361,    -1,   449,    -1,   448,     8,   449,    -1,
     450,    -1,   450,    16,   450,    -1,   127,    -1,    -1,    -1,
      -1,     7,     7,    -1,    -1,   456,    -1,   458,    -1,   476,
      -1,   526,   458,    -1,   535,   455,    -1,   456,     8,   535,
     455,    -1,   458,    -1,   457,     8,   458,    -1,   459,    -1,
      15,   458,    21,    -1,   474,    -1,   462,    -1,   470,    -1,
     477,    -1,   458,    17,   458,    -1,   458,    16,   458,    -1,
     458,     5,   458,    -1,   458,    37,   458,    -1,   458,     9,
     458,    -1,   342,   458,    -1,    17,   458,    -1,    16,   458,
      -1,   458,    25,   458,    -1,   458,    29,   458,    -1,   458,
      31,   458,    -1,   458,    28,   458,    -1,   458,    30,   458,
      -1,   458,    32,   458,    -1,   458,    24,   458,    -1,   458,
      33,   458,    -1,   458,    38,   458,    -1,   458,    35,   458,
      -1,   458,    22,   458,    -1,    34,   458,    -1,   458,    23,
     458,    -1,   458,   342,   458,    -1,    17,    -1,    16,    -1,
     330,    -1,   461,    -1,   464,    -1,   463,    -1,   461,    15,
     535,   454,    21,    -1,   461,    15,   535,   454,    21,   468,
      -1,   464,    15,   454,    21,    -1,   464,    15,   454,    21,
     468,    -1,   462,     3,   127,    -1,   461,    -1,   464,    -1,
     461,    15,   535,   454,    21,    -1,   464,    15,   454,    21,
      -1,   461,   468,    -1,    -1,   468,    -1,    15,   469,     7,
     469,    21,    -1,    -1,   458,    -1,   471,    -1,   471,   240,
     475,    -1,   472,    -1,   472,   240,   475,    -1,   473,   467,
      -1,    36,    -1,    27,    -1,   196,    -1,    90,    -1,   145,
      -1,    63,    -1,   461,   240,    63,    -1,   472,   240,    63,
      -1,    15,   459,     8,   459,    21,    -1,   461,    -1,   472,
      -1,   458,     7,   458,    -1,   458,     7,    -1,   458,     7,
     458,     7,   458,    -1,   458,     7,     7,   458,    -1,     7,
     458,     7,   458,    -1,     7,     7,   458,    -1,     7,   458,
      -1,     7,    -1,    -1,    -1,    14,   371,   478,   532,   479,
      20,    -1,   461,    -1,   464,    -1,   465,    -1,   481,     8,
     535,   465,    -1,   481,     8,   535,   526,   461,    -1,   480,
      -1,   482,     8,   535,   480,    -1,   482,     8,   535,   526,
     461,    -1,    -1,   461,    -1,   484,     8,   461,    -1,   506,
      -1,   505,    -1,   488,    -1,   496,   488,    -1,   100,   514,
     494,    -1,   101,   514,   493,    -1,   106,   494,    -1,   486,
      -1,   496,   486,    -1,   497,   506,    -1,   497,   233,    -1,
     496,   497,   233,    -1,    95,   514,    15,   458,    21,   233,
     493,    -1,    94,   514,   493,    -1,   104,   514,   493,    -1,
     489,    -1,    75,   514,    -1,   498,   506,    -1,   498,    -1,
     496,   498,    -1,   103,   514,   493,    -1,   541,    -1,   770,
      -1,    87,   514,    15,   458,    21,    -1,    87,   514,   515,
     504,   516,   574,   487,    -1,    -1,     8,   337,   248,    15,
     458,    21,    -1,   248,    15,   458,    21,    -1,   180,   514,
     515,   504,   516,   509,   502,    26,   458,     8,   458,    -1,
     180,   514,   515,   504,   516,   509,   502,    26,   458,     8,
     458,     8,   458,    -1,    61,   514,   490,   493,    -1,    83,
     514,   493,    -1,   108,   514,   493,    -1,   213,   514,   337,
      61,    15,   458,    21,    -1,   496,   213,   514,   337,    61,
      15,   458,    21,    -1,    15,   492,    21,    -1,   458,    -1,
     458,     7,    -1,     7,   458,    -1,   458,     7,   458,    -1,
     491,    -1,   492,     8,   491,    -1,    -1,   330,    -1,    -1,
     330,    -1,    74,    -1,   495,     7,    -1,   150,   514,    15,
     458,    21,    -1,   120,   514,    15,   499,   501,    21,    -1,
     500,    -1,   499,     8,   500,    -1,   502,    26,   476,    -1,
      -1,     8,   458,    -1,   330,    -1,   502,    26,   458,     8,
     458,    -1,   502,    26,   458,     8,   458,     8,   458,    -1,
      -1,   145,    -1,   111,   514,   493,    -1,    96,   514,   493,
      -1,    96,   514,    15,   458,    21,   493,    -1,   246,   514,
      15,   458,    21,    -1,   496,   246,   514,    15,   458,    21,
      -1,   507,   458,    26,   458,    -1,   183,   514,   462,    18,
     458,    -1,    47,   514,   441,   234,   330,    -1,    76,   514,
      -1,   508,    -1,   517,    -1,    45,   514,    15,   458,    21,
     441,     8,   441,     8,   441,    -1,   510,    -1,   510,    15,
      21,    -1,   510,    15,   511,    21,    -1,   209,   514,   469,
      -1,   513,   514,   469,    -1,    78,   514,   493,    -1,   113,
     514,   493,    -1,    44,   514,    15,   483,   481,    21,    -1,
      80,   514,    15,   483,   482,    21,    -1,   165,   514,    15,
     484,    21,    -1,   247,   514,    15,   458,    21,   462,    26,
     458,    -1,   148,   391,    -1,   181,   514,   441,    -1,    48,
     514,   330,    -1,    48,   514,   330,   509,    15,   440,    21,
      -1,    68,   514,    15,   440,    21,   509,   458,    -1,    -1,
       8,    -1,    60,   514,   330,   535,    -1,   535,   512,    -1,
     511,     8,   535,   512,    -1,   458,    -1,   526,   458,    -1,
       5,   441,    -1,   179,    -1,   226,    -1,    -1,    -1,    -1,
     518,   523,    -1,   518,   538,    -1,   518,     5,    -1,   518,
       9,    -1,   520,   523,    -1,   527,   523,    -1,   527,   522,
      -1,   527,   523,   530,    -1,   527,   522,     8,   530,    -1,
     528,   523,    -1,   528,   523,   532,    -1,   529,    -1,   529,
       8,   532,    -1,   519,   514,   536,    -1,    52,    -1,   210,
      -1,   102,    -1,   521,   514,   536,    -1,   135,    -1,   171,
      -1,    65,    -1,   538,    -1,     5,    -1,    15,   537,    21,
      -1,    15,   524,    21,    -1,   525,    -1,   524,     8,   535,
     525,    -1,   537,    -1,     5,    -1,     9,    -1,   526,   458,
      -1,   526,     5,    -1,   526,     9,    -1,   161,    -1,   192,
     514,   536,    -1,   250,   514,   536,    -1,   185,   514,   537,
     536,    -1,   185,   514,     5,   536,    -1,   531,    -1,   530,
       8,   531,    -1,   462,    -1,    15,   530,     8,   503,    21,
      -1,   459,    -1,   534,    -1,   533,    -1,   459,     8,   459,
      -1,   459,     8,   534,    -1,   534,     8,   459,    -1,   534,
       8,   534,    -1,   533,     8,   459,    -1,   533,     8,   534,
      -1,   474,    -1,    15,   458,    21,    -1,    15,   459,     8,
     503,    21,    -1,    15,   534,     8,   503,    21,    -1,    15,
     533,     8,   503,    21,    -1,    -1,    -1,   538,    -1,    15,
     537,    21,    -1,   462,    -1,   470,    -1,   537,   460,   537,
      -1,   537,     5,   537,    -1,   537,    37,   537,    -1,   537,
       9,   537,    -1,   460,   537,    -1,   537,    23,   537,    -1,
     127,    26,   458,    -1,    -1,   251,    -1,   542,    -1,   578,
      -1,   564,    -1,   543,    -1,   554,    -1,   549,    -1,   590,
      -1,   593,    -1,   667,    -1,   546,    -1,   555,    -1,   557,
      -1,   559,    -1,   561,    -1,   598,    -1,   604,    -1,   601,
      -1,   707,    -1,   705,    -1,   565,    -1,   579,    -1,   608,
      -1,   656,    -1,   654,    -1,   655,    -1,   657,    -1,   658,
      -1,   659,    -1,   660,    -1,   661,    -1,   669,    -1,   671,
      -1,   676,    -1,   673,    -1,   675,    -1,   679,    -1,   677,
      -1,   678,    -1,   690,    -1,   694,    -1,   695,    -1,   697,
      -1,   698,    -1,   699,    -1,   700,    -1,   701,    -1,   607,
      -1,   684,    -1,   685,    -1,   686,    -1,   689,    -1,   702,
      -1,   682,    -1,   706,    -1,    81,   452,   330,   435,    -1,
     542,     8,   330,   435,    -1,    92,   452,   544,    -1,   545,
      -1,   544,     8,   545,    -1,   330,    -1,   134,   452,   547,
      -1,   548,    -1,   547,     8,   548,    -1,   330,    -1,   222,
     452,   553,   550,    -1,    15,   551,    21,    -1,   552,    -1,
     551,     8,   552,    -1,   458,    -1,   458,     7,   458,    -1,
     330,    -1,   253,   452,   330,   435,    -1,   297,   452,   330,
     435,    -1,   554,     8,   330,   435,    -1,   133,   452,   556,
      -1,   555,     8,   556,    -1,   330,    -1,   206,   452,   558,
      -1,   557,     8,   558,    -1,   330,    -1,   200,   452,   560,
      -1,   559,     8,   560,    -1,   330,    -1,    69,   452,   562,
      -1,   561,     8,   562,    -1,   330,    -1,   170,   330,   435,
      -1,    -1,    86,   452,   568,   571,   563,    -1,   199,   514,
     568,   572,   574,   563,    -1,   199,   514,   572,   574,   563,
       7,     7,   566,    -1,   567,    -1,   566,     8,   567,    -1,
     568,    -1,   569,    -1,   330,    -1,   330,    15,   457,    21,
      -1,   330,    -1,    -1,   572,   574,    -1,    15,   573,    21,
      -1,   574,   575,    -1,   573,     8,   574,   575,    -1,    -1,
      57,    -1,    57,    15,   535,   577,    21,    -1,   124,    15,
     576,    21,    -1,   252,    15,   576,     8,   458,    21,    -1,
     160,    15,   458,    21,    -1,     5,    -1,   330,    -1,   526,
     458,    -1,   526,   458,     8,   526,   458,    -1,    42,   452,
     581,   583,    -1,   194,   514,   582,   583,    -1,   194,   514,
     583,     7,     7,   580,    -1,   582,    -1,   580,     8,   582,
      -1,   330,    -1,   461,    -1,    15,   588,    21,   337,   249,
     584,    -1,   587,    15,   585,    21,    -1,   586,    -1,   585,
       8,   586,    -1,   458,    -1,     5,    -1,   476,    -1,   330,
      -1,   589,    -1,   588,     8,   589,    -1,   330,    -1,     5,
      -1,     7,    -1,   591,     7,     7,   452,   330,   435,    -1,
     590,     8,   330,   435,    -1,   592,    -1,   591,     8,   337,
     592,    -1,    81,    -1,   253,    -1,   297,    -1,    92,    -1,
      85,    15,   436,    21,    -1,   222,   550,    -1,    42,    15,
     588,    21,   337,   249,   584,    -1,    42,    -1,    86,   572,
     574,   563,    -1,    86,    -1,   361,     8,   337,    91,   452,
      15,   594,    21,     7,     7,   596,    -1,    -1,   595,     7,
      -1,   594,     8,     7,    -1,   597,    -1,   596,     8,   597,
      -1,   330,    -1,   125,   452,   599,    -1,   600,    -1,   599,
       8,   600,    -1,   330,    -1,    73,   452,   602,    -1,   603,
      -1,   602,     8,   603,    -1,   330,    -1,    50,   452,   605,
      -1,    50,   452,     8,   337,    66,     7,     7,   605,    -1,
     606,    -1,   605,     8,   606,    -1,   330,   435,    -1,   163,
     514,    -1,   177,   514,    15,   609,    21,   337,   168,   611,
     614,    -1,   610,    -1,   609,     8,   610,    -1,   461,    -1,
     461,    15,   612,    21,    -1,   613,    -1,   612,     8,   613,
      -1,   458,    -1,     5,    -1,    -1,   615,    -1,   616,    -1,
     615,   616,    -1,   620,    -1,   640,    -1,   648,    -1,   617,
      -1,   625,    -1,   627,    -1,   626,    -1,   618,    -1,   621,
      -1,   622,    -1,     8,   337,   204,    15,   662,     7,   663,
      21,    -1,     8,   337,   204,    15,   663,    21,    -1,     8,
     337,    70,    15,   619,     7,   663,    21,    -1,     8,   337,
      70,    15,   663,    21,    -1,   330,    -1,     8,   337,   164,
      15,   624,    21,    -1,     8,   337,   276,    15,   624,    21,
      -1,     8,   337,   186,    15,   624,    21,    -1,     8,   337,
     314,    15,   623,    21,    -1,   458,    -1,   458,     8,   458,
      -1,   458,     8,   458,     8,   458,    -1,   462,    -1,   624,
       8,   462,    -1,     8,   337,   132,    15,   662,     7,   680,
      21,    -1,     8,   337,   132,    15,   680,    21,    -1,     8,
     337,   223,    15,   458,    21,    -1,     8,   337,    40,   628,
      -1,     8,   337,    40,   628,   628,    -1,    15,   574,   629,
     630,    21,    -1,   144,     7,    -1,   175,     7,    -1,    -1,
     631,    -1,   630,     8,   631,    -1,   653,    -1,   653,    15,
     632,    21,    -1,   653,    15,   632,    21,    15,   634,    21,
      -1,   633,    -1,   632,     8,   633,    -1,   458,     7,   458,
      -1,   635,    -1,   634,     8,   635,    -1,   636,     7,   637,
       7,   638,    -1,   636,     7,   637,    -1,   636,     7,   638,
      -1,   636,    -1,   637,     7,   638,    -1,   637,    -1,   638,
      -1,   337,   212,   639,    -1,   337,   152,   639,    -1,   337,
     126,   639,    -1,    15,   456,    21,    -1,     8,   337,   203,
      15,   641,   645,   642,     8,   644,    21,    -1,     8,   337,
     203,    15,   641,   645,   642,    21,    -1,     8,   337,   203,
      15,   641,   643,   642,     7,   644,    21,    -1,    -1,    -1,
     330,    -1,   337,   645,    -1,   644,     8,   337,   645,    -1,
     646,    15,   462,    21,    -1,   647,    15,   624,     8,   458,
      21,    -1,   228,    -1,   187,    -1,   157,    -1,   154,    -1,
      35,    -1,    22,    -1,    24,    -1,    33,    -1,   241,    -1,
     153,    -1,   156,    -1,     8,   337,   217,    15,   650,    21,
      -1,     8,   337,   218,   649,    -1,     8,   337,   220,   649,
      -1,     8,   337,   215,    -1,     8,   337,   215,    15,   653,
      15,   551,    21,    21,    -1,   330,    -1,   651,    -1,   650,
       8,   651,    -1,   653,    -1,   653,    15,   652,    77,    21,
      -1,   653,    15,   652,   551,    21,    -1,   653,    15,   652,
     551,    21,    15,   337,    77,    21,    -1,    -1,   461,    -1,
     219,   514,   649,    -1,   218,   514,   649,    -1,   221,   514,
     649,    -1,   220,   514,   649,    -1,   216,   514,   649,    15,
     650,    21,    -1,   201,   514,   643,    -1,   202,   514,   643,
      -1,    71,   514,   619,    -1,    72,   514,   619,    -1,   205,
     514,    15,   662,     7,   663,    21,    -1,   205,   514,    15,
     663,    21,    -1,   330,    -1,   664,    -1,   663,     8,   664,
      -1,   653,    15,   665,    21,    -1,   653,    -1,   666,    -1,
     665,     8,   666,    -1,   458,    -1,     7,    -1,   231,   452,
     668,    -1,   667,     8,   668,    -1,   330,   435,    -1,   232,
     514,   670,    -1,   232,   514,   670,   640,    -1,   232,   514,
     670,   618,    -1,   232,   514,   670,   640,   618,    -1,   232,
     514,   670,   618,   640,    -1,   330,    -1,   109,   514,    -1,
     670,    15,   458,    21,    -1,   670,    15,   476,    21,    -1,
     169,   514,   463,   674,    -1,    -1,   620,    -1,   107,   514,
      -1,   155,   514,   672,   337,   170,   570,   435,    -1,   155,
     514,   672,   337,   315,   462,    -1,   184,   514,   662,    -1,
     207,   514,   662,    -1,   132,   514,    15,   662,     7,   680,
      21,    -1,   132,   514,    15,   680,    21,    -1,   681,    -1,
     680,     8,   681,    -1,   653,    -1,   653,    15,   457,    21,
      -1,   131,   514,    -1,   131,   514,   620,    -1,   131,   514,
     683,    -1,   131,   514,   620,   683,    -1,     8,   337,   203,
      15,   624,    21,    -1,    49,   514,   688,    -1,    97,   514,
      -1,    51,   514,   688,    -1,   330,    -1,   687,    -1,   687,
      15,   457,    21,    -1,   118,   514,   462,    26,   462,    -1,
      82,   514,   693,    -1,    82,   514,   693,    15,   691,    21,
      -1,   535,   692,    -1,   691,     8,   535,   692,    -1,   526,
     458,    -1,   145,    -1,    98,   514,   693,    -1,   142,   514,
     696,    -1,    -1,   458,    -1,    99,   514,    -1,   235,   514,
      -1,   236,   514,    -1,    55,   514,    -1,    64,   514,   535,
      15,   511,    21,   371,   453,   624,    -1,   316,   514,    15,
     703,    21,    -1,   704,    -1,   703,     8,   704,    -1,   337,
     309,    -1,   337,   312,    -1,   337,   177,    -1,   756,    -1,
     709,    -1,   708,    -1,   726,    -1,   729,    -1,   730,    -1,
     731,    -1,   732,    -1,   738,    -1,   741,    -1,   746,    -1,
     747,    -1,   748,    -1,   751,    -1,   752,    -1,   753,    -1,
     754,    -1,   755,    -1,   757,    -1,   758,    -1,   759,    -1,
     760,    -1,   761,    -1,   762,    -1,   763,    -1,   764,    -1,
     765,    -1,   262,   514,    -1,   269,   514,    -1,   285,   514,
     574,   710,    -1,   285,   514,   574,    -1,   509,   574,   711,
     574,    -1,   710,   509,   574,   711,   574,    -1,   713,    -1,
     722,    -1,   717,    -1,   718,    -1,   714,    -1,   715,    -1,
     716,    -1,   720,    -1,   721,    -1,   768,    15,   769,   767,
      21,    -1,   186,   712,    -1,   276,   712,    -1,   279,   712,
      -1,   259,   712,    -1,   293,   712,    -1,    83,    15,   337,
     719,    21,    -1,   186,    -1,   293,    -1,   282,    -1,   298,
      15,   458,    21,    -1,   283,    15,   458,    21,    -1,   203,
      15,   723,    21,    -1,   574,   725,     7,   724,    -1,   624,
      -1,    17,    -1,    16,    -1,     5,    -1,    37,    -1,   157,
      -1,   154,    -1,    35,    -1,    22,    -1,    24,    -1,    33,
      -1,   299,    -1,   300,    -1,   301,    -1,   241,    -1,   291,
     514,   574,   727,    -1,   291,   514,   574,    -1,   509,   574,
     728,   574,    -1,   727,   509,   574,   728,   574,    -1,   713,
      -1,   722,    -1,   714,    -1,   715,    -1,   273,   514,   574,
     745,    -1,   273,   514,   574,    -1,   290,   514,    -1,   263,
     514,   574,   733,    -1,   263,   514,   574,    -1,   266,   514,
     574,   745,    -1,   266,   514,   574,    -1,   509,   574,   734,
     574,    -1,   733,   509,   574,   734,   574,    -1,   713,    -1,
     722,    -1,   714,    -1,   715,    -1,   736,    -1,   735,    -1,
     284,    -1,   292,    15,   337,   737,     8,   458,    21,    -1,
     292,    15,   337,   737,    21,    -1,   224,    -1,    92,    -1,
     278,    -1,   289,    -1,   294,   514,   574,   739,    -1,   294,
     514,   574,    -1,   509,   574,   740,   574,    -1,   739,   509,
     574,   740,   574,    -1,   713,    -1,   714,    -1,   274,   514,
     574,   742,    -1,   274,   514,   574,    -1,   509,   574,   743,
     574,    -1,   742,   509,   574,   743,   574,    -1,   745,    -1,
     744,    -1,   260,   712,    -1,   281,    -1,   296,   514,    -1,
     275,   514,   574,   745,    -1,   275,   514,   574,    -1,   286,
     514,   574,   749,    -1,   286,   514,   574,    -1,   509,   574,
     750,   574,    -1,   749,   509,   574,   750,   574,    -1,   713,
      -1,   722,    -1,   717,    -1,   718,    -1,   714,    -1,   715,
      -1,   716,    -1,   720,    -1,   721,    -1,   736,    -1,   735,
      -1,   270,   514,    -1,   287,   514,   574,   710,    -1,   287,
     514,   574,    -1,   271,   514,    -1,   288,   514,   574,   710,
      -1,   288,   514,   574,    -1,   272,   514,    -1,   295,   452,
     712,    -1,   280,   514,    -1,   267,   514,    -1,   284,   514,
      -1,   268,   514,    -1,   258,   514,    -1,   257,   514,    -1,
     277,   514,   712,    -1,   277,   514,    -1,   261,   514,    15,
     461,    21,    -1,   261,   514,    -1,   265,   514,    15,   461,
      21,    -1,   265,   514,    -1,    37,   330,   768,    37,   769,
      -1,   766,    -1,   461,    -1,   767,     8,   766,    -1,   767,
       8,   461,    -1,    -1,    -1,   771,    -1,   784,    -1,   772,
      -1,   785,    -1,   773,    -1,   774,    -1,   302,   514,   775,
      -1,   304,   514,    -1,   306,   514,    15,   781,    21,    -1,
     306,   514,    15,    21,    -1,   306,   514,    -1,   307,   514,
      15,   781,    21,    -1,   307,   514,    15,    21,    -1,   307,
     514,    -1,   337,   338,    -1,   776,    -1,   777,    -1,   776,
       8,   777,    -1,   337,   778,    -1,   337,   780,    -1,   337,
     779,    -1,   143,    15,   781,    21,    -1,   144,    15,   781,
      21,    -1,   175,    15,   781,    21,    -1,   312,    15,   781,
      21,    -1,   313,    15,   781,    21,    -1,   308,    15,   782,
      21,    -1,   309,    -1,   624,    -1,   783,    -1,   782,     8,
     783,    -1,   337,   310,    -1,   337,   311,    -1,   303,    -1,
     305,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   729,   729,   730,   734,   736,   750,   781,   790,   796,
     805,   814,   830,   842,   852,   859,   865,   870,   875,   899,
     926,   940,   942,   944,   948,   965,   979,  1003,  1019,  1033,
    1051,  1053,  1060,  1064,  1065,  1072,  1073,  1081,  1082,  1084,
    1088,  1089,  1093,  1097,  1103,  1113,  1117,  1122,  1129,  1130,
    1131,  1132,  1133,  1134,  1135,  1136,  1137,  1138,  1139,  1140,
    1141,  1142,  1147,  1152,  1159,  1161,  1162,  1163,  1164,  1165,
    1166,  1167,  1168,  1169,  1170,  1171,  1172,  1175,  1179,  1187,
    1195,  1204,  1212,  1216,  1218,  1222,  1224,  1226,  1228,  1230,
    1232,  1234,  1236,  1238,  1240,  1242,  1244,  1246,  1248,  1250,
    1252,  1254,  1256,  1261,  1270,  1280,  1288,  1298,  1319,  1339,
    1340,  1342,  1346,  1348,  1352,  1356,  1358,  1362,  1368,  1372,
    1374,  1378,  1382,  1386,  1390,  1394,  1400,  1404,  1408,  1414,
    1419,  1426,  1437,  1450,  1461,  1474,  1484,  1497,  1502,  1509,
    1512,  1517,  1522,  1529,  1532,  1542,  1556,  1559,  1578,  1605,
    1607,  1619,  1627,  1628,  1629,  1630,  1631,  1632,  1633,  1638,
    1639,  1643,  1645,  1652,  1657,  1658,  1660,  1662,  1675,  1681,
    1687,  1696,  1705,  1718,  1719,  1721,  1725,  1740,  1755,  1773,
    1794,  1814,  1836,  1853,  1871,  1878,  1885,  1892,  1905,  1912,
    1919,  1930,  1934,  1936,  1941,  1959,  1970,  1982,  1994,  2008,
    2014,  2021,  2027,  2033,  2041,  2048,  2064,  2067,  2076,  2078,
    2082,  2086,  2106,  2110,  2112,  2116,  2117,  2120,  2122,  2124,
    2126,  2128,  2131,  2134,  2138,  2144,  2148,  2152,  2154,  2159,
    2160,  2164,  2168,  2170,  2174,  2176,  2178,  2183,  2187,  2189,
    2191,  2194,  2196,  2197,  2198,  2199,  2200,  2201,  2202,  2203,
    2206,  2207,  2213,  2216,  2217,  2219,  2223,  2224,  2227,  2228,
    2230,  2234,  2235,  2236,  2237,  2239,  2242,  2243,  2252,  2254,
    2261,  2268,  2275,  2284,  2286,  2288,  2292,  2294,  2298,  2307,
    2314,  2321,  2323,  2327,  2331,  2337,  2339,  2344,  2348,  2352,
    2359,  2366,  2376,  2378,  2382,  2394,  2397,  2406,  2419,  2425,
    2431,  2437,  2445,  2455,  2457,  2461,  2482,  2507,  2509,  2513,
    2545,  2546,  2550,  2550,  2555,  2559,  2567,  2576,  2585,  2595,
    2601,  2604,  2606,  2610,  2618,  2633,  2640,  2642,  2646,  2662,
    2662,  2666,  2668,  2680,  2682,  2686,  2692,  2704,  2716,  2733,
    2762,  2763,  2771,  2772,  2776,  2778,  2780,  2791,  2795,  2801,
    2803,  2807,  2809,  2811,  2815,  2817,  2821,  2823,  2825,  2827,
    2829,  2831,  2833,  2835,  2837,  2839,  2841,  2843,  2845,  2847,
    2849,  2851,  2853,  2855,  2857,  2859,  2861,  2863,  2865,  2869,
    2870,  2881,  2955,  2967,  2969,  2973,  3103,  3153,  3197,  3239,
    3296,  3298,  3300,  3338,  3381,  3392,  3393,  3397,  3402,  3403,
    3407,  3409,  3415,  3417,  3423,  3433,  3439,  3446,  3452,  3460,
    3468,  3484,  3494,  3507,  3514,  3516,  3539,  3541,  3543,  3545,
    3547,  3549,  3551,  3553,  3557,  3557,  3557,  3571,  3573,  3596,
    3598,  3600,  3616,  3618,  3620,  3634,  3637,  3639,  3647,  3649,
    3651,  3653,  3707,  3727,  3742,  3751,  3754,  3804,  3810,  3815,
    3833,  3835,  3837,  3839,  3841,  3844,  3850,  3852,  3854,  3857,
    3859,  3886,  3895,  3904,  3905,  3907,  3912,  3919,  3927,  3929,
    3933,  3936,  3938,  3942,  3948,  3950,  3952,  3954,  3958,  3960,
    3969,  3970,  3977,  3978,  3982,  3986,  4007,  4010,  4014,  4016,
    4023,  4028,  4029,  4040,  4057,  4080,  4105,  4106,  4113,  4115,
    4117,  4119,  4121,  4125,  4202,  4214,  4221,  4223,  4224,  4226,
    4235,  4242,  4249,  4257,  4262,  4267,  4270,  4273,  4276,  4279,
    4282,  4286,  4304,  4309,  4328,  4347,  4351,  4352,  4355,  4359,
    4364,  4371,  4373,  4375,  4379,  4380,  4391,  4406,  4410,  4417,
    4420,  4430,  4443,  4456,  4460,  4463,  4466,  4470,  4479,  4482,
    4486,  4488,  4494,  4498,  4500,  4502,  4509,  4513,  4515,  4517,
    4521,  4540,  4556,  4565,  4574,  4576,  4580,  4606,  4621,  4636,
    4653,  4661,  4670,  4678,  4683,  4688,  4710,  4726,  4728,  4732,
    4734,  4741,  4743,  4745,  4749,  4751,  4753,  4755,  4757,  4759,
    4763,  4766,  4769,  4775,  4781,  4790,  4794,  4801,  4803,  4807,
    4809,  4811,  4816,  4821,  4826,  4831,  4840,  4845,  4851,  4852,
    4867,  4868,  4869,  4870,  4871,  4872,  4873,  4874,  4875,  4876,
    4877,  4878,  4879,  4880,  4881,  4882,  4883,  4884,  4885,  4888,
    4889,  4890,  4891,  4892,  4893,  4894,  4895,  4896,  4897,  4898,
    4899,  4900,  4901,  4902,  4903,  4904,  4905,  4906,  4907,  4908,
    4909,  4910,  4911,  4912,  4913,  4914,  4915,  4916,  4917,  4918,
    4919,  4920,  4921,  4922,  4926,  4946,  4968,  4972,  4974,  4978,
    4991,  4995,  4997,  5001,  5012,  5023,  5027,  5029,  5033,  5035,
    5045,  5057,  5077,  5097,  5119,  5125,  5134,  5142,  5148,  5156,
    5163,  5169,  5178,  5182,  5188,  5196,  5210,  5224,  5229,  5245,
    5260,  5288,  5290,  5294,  5296,  5300,  5329,  5352,  5373,  5374,
    5378,  5399,  5401,  5405,  5413,  5417,  5422,  5424,  5426,  5428,
    5436,  5446,  5456,  5485,  5500,  5507,  5525,  5527,  5531,  5545,
    5571,  5584,  5600,  5602,  5605,  5607,  5613,  5617,  5645,  5647,
    5651,  5659,  5665,  5668,  5719,  5778,  5780,  5783,  5787,  5791,
    5795,  5812,  5824,  5828,  5832,  5842,  5847,  5854,  5863,  5863,
    5874,  5885,  5887,  5891,  5902,  5906,  5908,  5912,  5923,  5927,
    5929,  5933,  5945,  5947,  5954,  5956,  5960,  5976,  5984,  6006,
    6008,  6012,  6015,  6025,  6027,  6031,  6033,  6042,  6043,  6047,
    6049,  6054,  6055,  6056,  6057,  6058,  6059,  6060,  6061,  6062,
    6063,  6066,  6071,  6075,  6079,  6083,  6096,  6100,  6104,  6108,
    6111,  6113,  6115,  6119,  6121,  6125,  6130,  6134,  6138,  6140,
    6144,  6152,  6158,  6165,  6168,  6170,  6174,  6176,  6180,  6192,
    6194,  6198,  6202,  6204,  6208,  6210,  6212,  6214,  6216,  6218,
    6220,  6224,  6228,  6232,  6236,  6240,  6247,  6253,  6258,  6261,
    6264,  6277,  6279,  6283,  6285,  6290,  6296,  6302,  6308,  6314,
    6320,  6326,  6332,  6338,  6347,  6353,  6370,  6372,  6380,  6388,
    6390,  6394,  6398,  6400,  6404,  6406,  6414,  6418,  6430,  6433,
    6459,  6461,  6465,  6467,  6471,  6475,  6479,  6488,  6492,  6496,
    6501,  6505,  6517,  6519,  6523,  6528,  6532,  6534,  6538,  6540,
    6544,  6549,  6556,  6579,  6581,  6583,  6585,  6587,  6591,  6602,
    6606,  6621,  6628,  6635,  6636,  6640,  6644,  6652,  6656,  6660,
    6668,  6673,  6687,  6689,  6693,  6695,  6704,  6706,  6708,  6710,
    6746,  6750,  6754,  6758,  6762,  6774,  6776,  6780,  6783,  6785,
    6789,  6794,  6801,  6804,  6812,  6816,  6821,  6823,  6830,  6834,
    6838,  6842,  6846,  6850,  6853,  6855,  6859,  6861,  6863,  6866,
    6868,  6869,  6870,  6871,  6872,  6873,  6874,  6875,  6876,  6877,
    6878,  6879,  6880,  6881,  6882,  6883,  6884,  6885,  6886,  6887,
    6888,  6889,  6890,  6891,  6892,  6893,  6896,  6902,  6908,  6914,
    6920,  6924,  6930,  6931,  6932,  6933,  6934,  6935,  6936,  6937,
    6938,  6941,  6946,  6951,  6957,  6963,  6969,  6974,  6980,  6986,
    6992,  6999,  7005,  7011,  7018,  7022,  7024,  7030,  7037,  7043,
    7049,  7055,  7061,  7067,  7073,  7079,  7085,  7091,  7097,  7103,
    7113,  7118,  7124,  7128,  7134,  7135,  7136,  7137,  7140,  7148,
    7154,  7160,  7165,  7171,  7178,  7184,  7188,  7194,  7195,  7196,
    7197,  7198,  7199,  7202,  7211,  7215,  7221,  7228,  7235,  7242,
    7251,  7257,  7263,  7267,  7273,  7274,  7277,  7283,  7289,  7293,
    7300,  7301,  7304,  7310,  7316,  7321,  7329,  7335,  7340,  7347,
    7351,  7357,  7358,  7359,  7360,  7361,  7362,  7363,  7364,  7365,
    7366,  7367,  7371,  7376,  7381,  7388,  7393,  7399,  7405,  7410,
    7415,  7420,  7424,  7429,  7434,  7438,  7443,  7447,  7453,  7458,
    7464,  7469,  7475,  7485,  7489,  7493,  7497,  7503,  7506,  7510,
    7511,  7512,  7513,  7514,  7515,  7518,  7522,  7526,  7528,  7530,
    7534,  7536,  7538,  7542,  7544,  7548,  7550,  7554,  7557,  7560,
    7565,  7567,  7569,  7571,  7573,  7577,  7581,  7586,  7590,  7592,
    7596,  7598,  7602,  7606
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "PERCENT", "AMPERSAND", "ASTER",
  "CLUSTER", "COLON", "COMMA", "DASTER", "DEFINED_OPERATOR", "DOT",
  "DQUOTE", "GLOBAL_A", "LEFTAB", "LEFTPAR", "MINUS", "PLUS", "POINT_TO",
  "QUOTE", "RIGHTAB", "RIGHTPAR", "AND", "DSLASH", "EQV", "EQ", "EQUAL",
  "FFALSE", "GE", "GT", "LE", "LT", "NE", "NEQV", "NOT", "OR", "TTRUE",
  "SLASH", "XOR", "REFERENCE", "ACROSS", "ALIGN_WITH", "ALIGN",
  "ALLOCATABLE", "ALLOCATE", "ARITHIF", "ASSIGNMENT", "ASSIGN",
  "ASSIGNGOTO", "ASYNCHRONOUS", "ASYNCID", "ASYNCWAIT", "BACKSPACE",
  "BAD_CCONST", "BAD_SYMBOL", "BARRIER", "BLOCKDATA", "BLOCK",
  "BOZ_CONSTANT", "BYTE", "CALL", "CASE", "CHARACTER", "CHAR_CONSTANT",
  "CHECK", "CLOSE", "COMMON", "COMPLEX", "COMPGOTO", "CONSISTENT_GROUP",
  "CONSISTENT_SPEC", "CONSISTENT_START", "CONSISTENT_WAIT", "CONSISTENT",
  "CONSTRUCT_ID", "CONTAINS", "CONTINUE", "CORNER", "CYCLE", "DATA",
  "DEALLOCATE", "HPF_TEMPLATE", "DEBUG", "DEFAULT_CASE", "DEFINE",
  "DIMENSION", "DISTRIBUTE", "DOWHILE", "DOUBLEPRECISION", "DOUBLECOMPLEX",
  "DP_CONSTANT", "DVM_POINTER", "DYNAMIC", "ELEMENTAL", "ELSE", "ELSEIF",
  "ELSEWHERE", "ENDASYNCHRONOUS", "ENDDEBUG", "ENDINTERVAL", "ENDUNIT",
  "ENDDO", "ENDFILE", "ENDFORALL", "ENDIF", "ENDINTERFACE", "ENDMODULE",
  "ENDON", "ENDSELECT", "ENDTASK_REGION", "ENDTYPE", "ENDWHERE", "ENTRY",
  "EXIT", "EOLN", "EQUIVALENCE", "ERROR", "EXTERNAL", "F90", "FIND",
  "FORALL", "FORMAT", "FUNCTION", "GATE", "GEN_BLOCK", "HEAP", "HIGH",
  "IDENTIFIER", "IMPLICIT", "IMPLICITNONE", "INCLUDE", "INDEPENDENT",
  "INDIRECT_ACCESS", "INDIRECT_GROUP", "INHERIT", "INQUIRE",
  "INTERFACEASSIGNMENT", "INTERFACEOPERATOR", "INTERFACE", "INTRINSIC",
  "INTEGER", "INTENT", "INTERVAL", "INOUT", "IN", "INT_CONSTANT", "LABEL",
  "LABEL_DECLARE", "LET", "LOGICAL", "LOGICALIF", "LOOP", "LOW", "MAXLOC",
  "MAX", "MAP", "MINLOC", "MIN", "MODULE_PROCEDURE", "MODULE",
  "MULT_BLOCK", "NAMEEQ", "NAMELIST", "NEW_VALUE", "NEW", "NULLIFY",
  "OCTAL_CONSTANT", "ONLY", "ON", "ON_DIR", "ONTO", "OPEN", "OPERATOR",
  "OPTIONAL", "OTHERWISE", "OUT", "OWN", "PARALLEL", "PARAMETER", "PAUSE",
  "PLAINDO", "PLAINGOTO", "POINTER", "POINTERLET", "PREFETCH", "PRINT",
  "PRIVATE", "PRODUCT", "PROGRAM", "PUBLIC", "PURE", "RANGE", "READ",
  "REALIGN_WITH", "REALIGN", "REAL", "REAL_CONSTANT", "RECURSIVE",
  "REDISTRIBUTE_NEW", "REDISTRIBUTE", "REDUCTION_GROUP", "REDUCTION_START",
  "REDUCTION_WAIT", "REDUCTION", "REMOTE_ACCESS_SPEC", "REMOTE_ACCESS",
  "REMOTE_GROUP", "RESET", "RESULT", "RETURN", "REWIND", "SAVE", "SECTION",
  "SELECT", "SEQUENCE", "SHADOW_COMPUTE", "SHADOW_GROUP", "SHADOW_RENEW",
  "SHADOW_START_SPEC", "SHADOW_START", "SHADOW_WAIT_SPEC", "SHADOW_WAIT",
  "SHADOW", "STAGE", "STATIC", "STAT", "STOP", "SUBROUTINE", "SUM", "SYNC",
  "TARGET", "TASK", "TASK_REGION", "THEN", "TO", "TRACEON", "TRACEOFF",
  "TRUNC", "TYPE", "TYPE_DECL", "UNDER", "UNKNOWN", "USE", "VIRTUAL",
  "VARIABLE", "WAIT", "WHERE", "WHERE_ASSIGN", "WHILE", "WITH", "WRITE",
  "COMMENT", "WGT_BLOCK", "HPF_PROCESSORS", "IOSTAT", "ERR", "END",
  "OMPDVM_ATOMIC", "OMPDVM_BARRIER", "OMPDVM_COPYIN", "OMPDVM_COPYPRIVATE",
  "OMPDVM_CRITICAL", "OMPDVM_ONETHREAD", "OMPDVM_DO", "OMPDVM_DYNAMIC",
  "OMPDVM_ENDCRITICAL", "OMPDVM_ENDDO", "OMPDVM_ENDMASTER",
  "OMPDVM_ENDORDERED", "OMPDVM_ENDPARALLEL", "OMPDVM_ENDPARALLELDO",
  "OMPDVM_ENDPARALLELSECTIONS", "OMPDVM_ENDPARALLELWORKSHARE",
  "OMPDVM_ENDSECTIONS", "OMPDVM_ENDSINGLE", "OMPDVM_ENDWORKSHARE",
  "OMPDVM_FIRSTPRIVATE", "OMPDVM_FLUSH", "OMPDVM_GUIDED",
  "OMPDVM_LASTPRIVATE", "OMPDVM_MASTER", "OMPDVM_NOWAIT", "OMPDVM_NONE",
  "OMPDVM_NUM_THREADS", "OMPDVM_ORDERED", "OMPDVM_PARALLEL",
  "OMPDVM_PARALLELDO", "OMPDVM_PARALLELSECTIONS",
  "OMPDVM_PARALLELWORKSHARE", "OMPDVM_RUNTIME", "OMPDVM_SECTION",
  "OMPDVM_SECTIONS", "OMPDVM_SCHEDULE", "OMPDVM_SHARED", "OMPDVM_SINGLE",
  "OMPDVM_THREADPRIVATE", "OMPDVM_WORKSHARE", "OMPDVM_NODES", "OMPDVM_IF",
  "IAND", "IEOR", "IOR", "ACC_REGION", "ACC_END_REGION",
  "ACC_CHECKSECTION", "ACC_END_CHECKSECTION", "ACC_GET_ACTUAL",
  "ACC_ACTUAL", "ACC_TARGETS", "ACC_ASYNC", "ACC_HOST", "ACC_CUDA",
  "ACC_LOCAL", "ACC_INLOCAL", "ACC_CUDA_BLOCK", "BY", "IO_MODE",
  "BINARY_OP", "UNARY_OP", "$accept", "program", "stat", "thislabel",
  "entry", "new_prog", "proc_attr", "procname", "funcname", "typedfunc",
  "opt_result_clause", "name", "progname", "blokname", "arglist", "args",
  "arg", "filename", "needkeyword", "keywordoff",
  "keyword_if_colon_follow", "spec", "interface", "defined_op", "operator",
  "intrinsic_op", "type_dcl", "end_type", "dcl", "options",
  "attr_spec_list", "attr_spec", "intent_spec", "access_spec", "intent",
  "optional", "static", "private", "private_attr", "sequence", "public",
  "public_attr", "type", "opt_key_hedr", "attrib", "att_type", "typespec",
  "typename", "lengspec", "proper_lengspec", "selector", "clause",
  "end_ioctl", "initial_value", "dimension", "allocatable", "pointer",
  "target", "common", "namelist", "namelist_group", "comblock", "var",
  "external", "intrinsic", "equivalence", "equivset", "equivlist",
  "equi_object", "data", "data1", "data_in", "in_data", "datapair",
  "datalvals", "datarvals", "datalval", "data_null", "d_name", "dataname",
  "datasubs", "datarange", "iconexprlist", "opticonexpr", "dataimplieddo",
  "dlist", "dataelt", "datarval", "datavalue", "BOZ_const", "int_const",
  "unsignedint", "real_const", "unsignedreal", "complex_const_data",
  "complex_part", "iconexpr", "iconterm", "iconfactor", "iconprimary",
  "savelist", "saveitem", "use_name_list", "use_key_word",
  "no_use_key_word", "use_name", "paramlist", "paramitem",
  "module_proc_stmt", "proc_name_list", "use_stat", "module_name",
  "only_list", "only_name", "rename_list", "rename_name", "dims",
  "dimlist", "@1", "dim", "ubound", "labellist", "label", "implicit",
  "implist", "impitem", "imptype", "@2", "type_implicit", "letgroups",
  "letgroup", "letter", "inside", "in_dcl", "opt_double_colon",
  "funarglist", "funarg", "funargs", "subscript_list", "expr", "uexpr",
  "addop", "ident", "lhs", "array_ele_substring_func_ref",
  "structure_component", "array_element", "asubstring", "opt_substring",
  "substring", "opt_expr", "simple_const", "numeric_bool_const",
  "integer_constant", "string_constant", "complex_const", "kind",
  "triplet", "vec", "@3", "@4", "allocate_object", "allocation_list",
  "allocate_object_list", "stat_spec", "pointer_name_list", "exec",
  "do_while", "opt_while", "plain_do", "case", "case_selector",
  "case_value_range", "case_value_range_list", "opt_construct_name",
  "opt_unit_name", "construct_name", "construct_name_colon", "logif",
  "forall", "forall_list", "forall_expr", "opt_forall_cond", "do_var",
  "dospec", "dotarget", "whereable", "iffable", "let", "goto", "opt_comma",
  "call", "callarglist", "callarg", "stop", "end_spec", "intonlyon",
  "intonlyoff", "io", "iofmove", "fmkwd", "iofctl", "ctlkwd", "infmt",
  "ioctl", "ctllist", "ioclause", "nameeq", "read", "write", "print",
  "inlist", "inelt", "outlist", "out2", "other", "in_ioctl", "start_ioctl",
  "fexpr", "unpar_fexpr", "cmnt", "dvm_specification", "dvm_exec",
  "dvm_template", "dvm_dynamic", "dyn_array_name_list", "dyn_array_name",
  "dvm_inherit", "dummy_array_name_list", "dummy_array_name", "dvm_shadow",
  "shadow_attr_stuff", "sh_width_list", "sh_width", "sh_array_name",
  "dvm_processors", "dvm_indirect_group", "indirect_group_name",
  "dvm_remote_group", "remote_group_name", "dvm_reduction_group",
  "reduction_group_name", "dvm_consistent_group", "consistent_group_name",
  "opt_onto", "dvm_distribute", "dvm_redistribute", "dist_name_list",
  "distributee", "dist_name", "pointer_ar_elem", "processors_name",
  "opt_dist_format_clause", "dist_format_clause", "dist_format_list",
  "opt_key_word", "dist_format", "array_name", "shadow_width", "dvm_align",
  "dvm_realign", "realignee_list", "alignee", "realignee",
  "align_directive_stuff", "align_base", "align_subscript_list",
  "align_subscript", "align_base_name", "dim_ident_list", "dim_ident",
  "dvm_combined_dir", "dvm_attribute_list", "dvm_attribute", "dvm_pointer",
  "dimension_list", "@5", "pointer_var_list", "pointer_var", "dvm_heap",
  "heap_array_name_list", "heap_array_name", "dvm_consistent",
  "consistent_array_name_list", "consistent_array_name", "dvm_asyncid",
  "async_id_list", "async_id", "dvm_new_value", "dvm_parallel_on",
  "loop_var_list", "loop_var", "distribute_cycles", "par_subscript_list",
  "par_subscript", "opt_spec", "spec_list", "par_spec",
  "remote_access_spec", "consistent_spec", "consistent_group", "new_spec",
  "private_spec", "cuda_block_spec", "sizelist", "variable_list",
  "indirect_access_spec", "stage_spec", "across_spec", "in_out_across",
  "opt_in_out", "dependent_array_list", "dependent_array",
  "dependence_list", "dependence", "section_spec_list", "section_spec",
  "ar_section", "low_section", "high_section", "section", "reduction_spec",
  "opt_key_word_r", "no_opt_key_word_r", "reduction_group",
  "reduction_list", "reduction", "reduction_op", "loc_op", "shadow_spec",
  "shadow_group_name", "shadow_list", "shadow", "opt_corner",
  "array_ident", "dvm_shadow_start", "dvm_shadow_wait", "dvm_shadow_group",
  "dvm_reduction_start", "dvm_reduction_wait", "dvm_consistent_start",
  "dvm_consistent_wait", "dvm_remote_access", "group_name",
  "remote_data_list", "remote_data", "remote_index_list", "remote_index",
  "dvm_task", "task_array", "dvm_task_region", "task_name",
  "dvm_end_task_region", "task", "dvm_on", "opt_new_spec", "dvm_end_on",
  "dvm_map", "dvm_prefetch", "dvm_reset", "dvm_indirect_access",
  "indirect_list", "indirect_reference", "hpf_independent",
  "hpf_reduction_spec", "dvm_asynchronous", "dvm_endasynchronous",
  "dvm_asyncwait", "async_ident", "async", "dvm_f90", "dvm_debug_dir",
  "debparamlist", "debparam", "fragment_number", "dvm_enddebug_dir",
  "dvm_interval_dir", "interval_number", "dvm_endinterval_dir",
  "dvm_traceon_dir", "dvm_traceoff_dir", "dvm_barrier_dir", "dvm_check",
  "dvm_io_mode_dir", "mode_list", "mode_spec",
  "omp_specification_directive", "omp_execution_directive",
  "ompdvm_onethread", "omp_parallel_end_directive",
  "omp_parallel_begin_directive", "parallel_clause_list",
  "parallel_clause", "omp_variable_list_in_par", "ompprivate_clause",
  "ompfirstprivate_clause", "omplastprivate_clause", "ompcopyin_clause",
  "ompshared_clause", "ompdefault_clause", "def_expr", "ompif_clause",
  "ompnumthreads_clause", "ompreduction_clause", "ompreduction",
  "ompreduction_vars", "ompreduction_op", "omp_sections_begin_directive",
  "sections_clause_list", "sections_clause", "omp_sections_end_directive",
  "omp_section_directive", "omp_do_begin_directive",
  "omp_do_end_directive", "do_clause_list", "do_clause",
  "ompordered_clause", "ompschedule_clause", "ompschedule_op",
  "omp_single_begin_directive", "single_clause_list", "single_clause",
  "omp_single_end_directive", "end_single_clause_list",
  "end_single_clause", "ompcopyprivate_clause", "ompnowait_clause",
  "omp_workshare_begin_directive", "omp_workshare_end_directive",
  "omp_parallel_do_begin_directive", "paralleldo_clause_list",
  "paralleldo_clause", "omp_parallel_do_end_directive",
  "omp_parallel_sections_begin_directive",
  "omp_parallel_sections_end_directive",
  "omp_parallel_workshare_begin_directive",
  "omp_parallel_workshare_end_directive", "omp_threadprivate_directive",
  "omp_master_begin_directive", "omp_master_end_directive",
  "omp_ordered_begin_directive", "omp_ordered_end_directive",
  "omp_barrier_directive", "omp_atomic_directive", "omp_flush_directive",
  "omp_critical_begin_directive", "omp_critical_end_directive",
  "omp_common_var", "omp_variable_list", "op_slash_1", "op_slash_0",
  "acc_directive", "acc_region", "acc_checksection", "acc_get_actual",
  "acc_actual", "opt_clause", "acc_clause_list", "acc_clause",
  "data_clause", "targets_clause", "async_clause", "acc_var_list",
  "computer_list", "computer", "acc_end_region", "acc_end_checksection", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   315,   316,     1,     2,     3,     4,     5,     6,     7,
       8,     9,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167,
     168,   169,   170,   171,   172,   173,   174,   175,   176,   177,
     178,   179,   180,   181,   182,   183,   184,   185,   186,   187,
     188,   189,   190,   191,   192,   193,   194,   195,   196,   197,
     198,   199,   200,   201,   202,   203,   204,   205,   206,   207,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   221,   222,   223,   224,   225,   226,   227,
     228,   229,   230,   231,   232,   233,   234,   235,   236,   237,
     238,   239,   240,   241,   242,   243,   244,   245,   246,   247,
     248,   249,   250,   251,   252,   253,   254,   255,   256,   257,
     258,   259,   260,   261,   262,   263,   264,   265,   266,   267,
     268,   269,   270,   271,   272,   273,   274,   275,   276,   277,
     278,   279,   280,   281,   282,   283,   284,   285,   286,   287,
     288,   289,   290,   291,   292,   293,   294,   295,   296,   297,
     298,   299,   300,   301,   302,   303,   304,   305,   306,   307,
     308,   309,   310,   311,   312,   313,   314,   317,   318
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint16 yyr1[] =
{
       0,   319,   320,   320,   321,   321,   321,   321,   321,   321,
     321,   322,   323,   323,   323,   323,   323,   323,   323,   323,
     324,   325,   325,   325,   326,   327,   328,   328,   328,   328,
     329,   329,   330,   331,   331,   332,   332,   333,   333,   333,
     334,   334,   335,   335,   336,   337,   338,   339,   340,   340,
     340,   340,   340,   340,   340,   340,   340,   340,   340,   340,
     340,   340,   340,   340,   340,   340,   340,   340,   340,   340,
     340,   340,   340,   340,   340,   340,   340,   341,   341,   341,
     341,   341,   342,   343,   343,   344,   344,   344,   344,   344,
     344,   344,   344,   344,   344,   344,   344,   344,   344,   344,
     344,   344,   344,   345,   345,   346,   346,   347,   347,   348,
     348,   348,   349,   349,   350,   350,   350,   350,   350,   350,
     350,   350,   350,   350,   350,   350,   351,   351,   351,   352,
     352,   353,   353,   354,   354,   355,   355,   356,   356,   357,
     358,   359,   359,   360,   361,   361,   362,   363,   363,   364,
     364,   365,   366,   366,   366,   366,   366,   366,   366,   367,
     367,   368,   368,   368,   369,   369,   369,   369,   370,   370,
     370,   370,   371,   372,   372,   372,   373,   373,   374,   374,
     375,   375,   376,   376,   377,   377,   377,   377,   378,   378,
     378,   379,   380,   380,   381,   382,   382,   383,   383,   384,
     384,   385,   386,   386,   387,   387,   387,   388,   389,   389,
     390,   391,   392,   393,   393,   394,   394,   395,   395,   395,
     395,   395,   396,   397,   398,   399,   400,   401,   401,   402,
     402,   403,   404,   404,   405,   405,   405,   405,   406,   406,
     406,   407,   407,   407,   407,   407,   407,   407,   407,   407,
     407,   407,   408,   409,   409,   409,   410,   410,   411,   411,
     411,   412,   412,   412,   412,   413,   414,   414,   415,   415,
     415,   415,   415,   416,   416,   416,   417,   417,   418,   418,
     418,   419,   419,   420,   420,   421,   421,   422,   423,   424,
     424,   424,   425,   425,   426,   427,   428,   428,   429,   429,
     429,   429,   430,   431,   431,   432,   432,   433,   433,   434,
     435,   435,   437,   436,   436,   438,   438,   438,   438,   439,
     439,   440,   440,   441,   442,   442,   443,   443,   444,   446,
     445,   447,   447,   448,   448,   449,   449,   450,   451,   452,
     453,   453,   454,   454,   455,   455,   455,   456,   456,   457,
     457,   458,   458,   458,   459,   459,   459,   459,   459,   459,
     459,   459,   459,   459,   459,   459,   459,   459,   459,   459,
     459,   459,   459,   459,   459,   459,   459,   459,   459,   460,
     460,   461,   462,   462,   462,   463,   463,   463,   463,   464,
     465,   465,   465,   465,   466,   467,   467,   468,   469,   469,
     470,   470,   470,   470,   470,   471,   471,   471,   471,   472,
     473,   473,   473,   474,   475,   475,   476,   476,   476,   476,
     476,   476,   476,   476,   478,   479,   477,   480,   480,   481,
     481,   481,   482,   482,   482,   483,   484,   484,   485,   485,
     485,   485,   485,   485,   485,   485,   485,   485,   485,   485,
     485,   485,   485,   485,   485,   485,   485,   485,   485,   485,
     485,   486,   486,   487,   487,   487,   488,   488,   489,   489,
     489,   489,   489,   490,   491,   491,   491,   491,   492,   492,
     493,   493,   494,   494,   495,   496,   497,   498,   499,   499,
     500,   501,   501,   502,   503,   503,   504,   504,   505,   505,
     505,   505,   505,   506,   506,   506,   506,   506,   506,   506,
     506,   506,   506,   506,   506,   506,   506,   506,   506,   506,
     506,   507,   508,   508,   508,   508,   509,   509,   510,   511,
     511,   512,   512,   512,   513,   513,   514,   515,   516,   517,
     517,   517,   517,   517,   517,   517,   517,   517,   517,   517,
     517,   517,   518,   519,   519,   519,   520,   521,   521,   521,
     522,   522,   523,   523,   524,   524,   525,   525,   525,   525,
     525,   525,   526,   527,   528,   529,   529,   530,   530,   531,
     531,   532,   532,   532,   533,   533,   533,   533,   533,   533,
     534,   534,   534,   534,   534,   535,   536,   537,   537,   538,
     538,   538,   538,   538,   538,   538,   538,   538,   539,   539,
     540,   540,   540,   540,   540,   540,   540,   540,   540,   540,
     540,   540,   540,   540,   540,   540,   540,   540,   540,   541,
     541,   541,   541,   541,   541,   541,   541,   541,   541,   541,
     541,   541,   541,   541,   541,   541,   541,   541,   541,   541,
     541,   541,   541,   541,   541,   541,   541,   541,   541,   541,
     541,   541,   541,   541,   542,   542,   543,   544,   544,   545,
     546,   547,   547,   548,   549,   550,   551,   551,   552,   552,
     553,   554,   554,   554,   555,   555,   556,   557,   557,   558,
     559,   559,   560,   561,   561,   562,   563,   563,   564,   565,
     565,   566,   566,   567,   567,   568,   569,   570,   571,   571,
     572,   573,   573,   574,   575,   575,   575,   575,   575,   575,
     576,   577,   577,   578,   579,   579,   580,   580,   581,   582,
     583,   584,   585,   585,   586,   586,   586,   587,   588,   588,
     589,   589,   589,   590,   590,   591,   591,   592,   592,   592,
     592,   592,   592,   592,   592,   592,   592,   593,   595,   594,
     594,   596,   596,   597,   598,   599,   599,   600,   601,   602,
     602,   603,   604,   604,   605,   605,   606,   607,   608,   609,
     609,   610,   611,   612,   612,   613,   613,   614,   614,   615,
     615,   616,   616,   616,   616,   616,   616,   616,   616,   616,
     616,   617,   617,   618,   618,   619,   620,   620,   621,   622,
     623,   623,   623,   624,   624,   625,   625,   626,   627,   627,
     628,   629,   629,   629,   630,   630,   631,   631,   631,   632,
     632,   633,   634,   634,   635,   635,   635,   635,   635,   635,
     635,   636,   637,   638,   639,   640,   640,   640,   641,   642,
     643,   644,   644,   645,   645,   646,   646,   646,   646,   646,
     646,   646,   646,   646,   647,   647,   648,   648,   648,   648,
     648,   649,   650,   650,   651,   651,   651,   651,   652,   653,
     654,   654,   655,   655,   656,   657,   658,   659,   660,   661,
     661,   662,   663,   663,   664,   664,   665,   665,   666,   666,
     667,   667,   668,   669,   669,   669,   669,   669,   670,   671,
     672,   672,   673,   674,   674,   675,   676,   676,   677,   678,
     679,   679,   680,   680,   681,   681,   682,   682,   682,   682,
     683,   684,   685,   686,   687,   688,   688,   689,   690,   690,
     691,   691,   692,   693,   694,   695,   696,   696,   697,   698,
     699,   700,   701,   702,   703,   703,   704,   704,   704,   705,
     706,   706,   706,   706,   706,   706,   706,   706,   706,   706,
     706,   706,   706,   706,   706,   706,   706,   706,   706,   706,
     706,   706,   706,   706,   706,   706,   707,   708,   709,   709,
     710,   710,   711,   711,   711,   711,   711,   711,   711,   711,
     711,   712,   713,   714,   715,   716,   717,   718,   719,   719,
     719,   720,   721,   722,   723,   724,   725,   725,   725,   725,
     725,   725,   725,   725,   725,   725,   725,   725,   725,   725,
     726,   726,   727,   727,   728,   728,   728,   728,   729,   729,
     730,   731,   731,   732,   732,   733,   733,   734,   734,   734,
     734,   734,   734,   735,   736,   736,   737,   737,   737,   737,
     738,   738,   739,   739,   740,   740,   741,   741,   742,   742,
     743,   743,   744,   745,   746,   747,   747,   748,   748,   749,
     749,   750,   750,   750,   750,   750,   750,   750,   750,   750,
     750,   750,   751,   752,   752,   753,   754,   754,   755,   756,
     757,   758,   759,   760,   761,   762,   763,   763,   764,   764,
     765,   765,   766,   767,   767,   767,   767,   768,   769,   770,
     770,   770,   770,   770,   770,   771,   772,   773,   773,   773,
     774,   774,   774,   775,   775,   776,   776,   777,   777,   777,
     778,   778,   778,   778,   778,   779,   780,   781,   782,   782,
     783,   783,   784,   785
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     0,     3,     3,     3,     3,     3,     2,     1,
       1,     1,     3,     3,     4,     5,     5,     3,     4,     3,
       0,     2,     2,     2,     1,     1,     4,     5,     4,     5,
       2,     5,     1,     0,     1,     0,     1,     0,     2,     3,
       1,     3,     1,     1,     1,     0,     0,     0,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     2,     4,     2,     5,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     2,     3,     5,
       5,     2,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     4,     7,     2,     3,     7,     6,     0,
       2,     5,     1,     4,     1,     1,     1,     2,     1,     4,
       1,     1,     1,     1,     1,     1,     2,     2,     2,     1,
       1,     7,     3,     4,     3,     4,     3,     2,     5,     0,
       2,     2,     5,     0,     6,     5,     0,     2,     3,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     0,
       1,     3,     4,     4,     0,     1,     5,     9,     1,     1,
       2,     2,     0,     0,     2,     2,     5,     4,     4,     3,
       4,     3,     4,     3,     3,     4,     5,     3,     4,     5,
       3,     3,     1,     3,     2,     4,     3,     4,     3,     3,
       3,     3,     3,     3,     1,     4,     1,     1,     4,     3,
       0,     0,     4,     1,     3,     1,     3,     2,     3,     3,
       4,     2,     0,     1,     1,     3,     5,     1,     3,     0,
       1,     7,     1,     3,     2,     2,     3,     1,     1,     4,
       3,     2,     1,     1,     1,     1,     3,     1,     3,     1,
       1,     6,     1,     1,     2,     2,     1,     3,     1,     2,
       2,     1,     3,     1,     3,     5,     1,     1,     1,     2,
       2,     3,     3,     1,     3,     3,     1,     3,     1,     1,
       3,     1,     3,     1,     1,     3,     5,     0,     0,     1,
       4,     4,     1,     3,     3,     2,     1,     3,     3,     6,
       6,     7,     1,     1,     3,     3,     1,     1,     3,     3,
       0,     3,     0,     2,     3,     1,     1,     2,     3,     1,
       1,     1,     3,     1,     3,     1,     1,     3,     4,     0,
       2,     2,     1,     1,     3,     1,     3,     1,     0,     0,
       0,     2,     0,     1,     1,     1,     2,     2,     4,     1,
       3,     1,     3,     1,     1,     1,     1,     3,     3,     3,
       3,     3,     2,     2,     2,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     2,     3,     3,     1,
       1,     1,     1,     1,     1,     5,     6,     4,     5,     3,
       1,     1,     5,     4,     2,     0,     1,     5,     0,     1,
       1,     3,     1,     3,     2,     1,     1,     1,     1,     1,
       1,     3,     3,     5,     1,     1,     3,     2,     5,     4,
       4,     3,     2,     1,     0,     0,     6,     1,     1,     1,
       4,     5,     1,     4,     5,     0,     1,     3,     1,     1,
       1,     2,     3,     3,     2,     1,     2,     2,     2,     3,
       7,     3,     3,     1,     2,     2,     1,     2,     3,     1,
       1,     5,     7,     0,     6,     4,    11,    13,     4,     3,
       3,     7,     8,     3,     1,     2,     2,     3,     1,     3,
       0,     1,     0,     1,     1,     2,     5,     6,     1,     3,
       3,     0,     2,     1,     5,     7,     0,     1,     3,     3,
       6,     5,     6,     4,     5,     5,     2,     1,     1,    10,
       1,     3,     4,     3,     3,     3,     3,     6,     6,     5,
       8,     2,     3,     3,     7,     7,     0,     1,     4,     2,
       4,     1,     2,     2,     1,     1,     0,     0,     0,     2,
       2,     2,     2,     2,     2,     2,     3,     4,     2,     3,
       1,     3,     3,     1,     1,     1,     3,     1,     1,     1,
       1,     1,     3,     3,     1,     4,     1,     1,     1,     2,
       2,     2,     1,     3,     3,     4,     4,     1,     3,     1,
       5,     1,     1,     1,     3,     3,     3,     3,     3,     3,
       1,     3,     5,     5,     5,     0,     0,     1,     3,     1,
       1,     3,     3,     3,     3,     2,     3,     3,     0,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     4,     4,     3,     1,     3,     1,
       3,     1,     3,     1,     4,     3,     1,     3,     1,     3,
       1,     4,     4,     4,     3,     3,     1,     3,     3,     1,
       3,     3,     1,     3,     3,     1,     3,     0,     5,     6,
       8,     1,     3,     1,     1,     1,     4,     1,     0,     2,
       3,     2,     4,     0,     1,     5,     4,     6,     4,     1,
       1,     2,     5,     4,     4,     6,     1,     3,     1,     1,
       6,     4,     1,     3,     1,     1,     1,     1,     1,     3,
       1,     1,     1,     6,     4,     1,     4,     1,     1,     1,
       1,     4,     2,     7,     1,     4,     1,    11,     0,     2,
       3,     1,     3,     1,     3,     1,     3,     1,     3,     1,
       3,     1,     3,     8,     1,     3,     2,     2,     9,     1,
       3,     1,     4,     1,     3,     1,     1,     0,     1,     1,
       2,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     8,     6,     8,     6,     1,     6,     6,     6,     6,
       1,     3,     5,     1,     3,     8,     6,     6,     4,     5,
       5,     2,     2,     0,     1,     3,     1,     4,     7,     1,
       3,     3,     1,     3,     5,     3,     3,     1,     3,     1,
       1,     3,     3,     3,     3,    10,     8,    10,     0,     0,
       1,     2,     4,     4,     6,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     6,     4,     4,     3,
       9,     1,     1,     3,     1,     5,     5,     9,     0,     1,
       3,     3,     3,     3,     6,     3,     3,     3,     3,     7,
       5,     1,     1,     3,     4,     1,     1,     3,     1,     1,
       3,     3,     2,     3,     4,     4,     5,     5,     1,     2,
       4,     4,     4,     0,     1,     2,     7,     6,     3,     3,
       7,     5,     1,     3,     1,     4,     2,     3,     3,     4,
       6,     3,     2,     3,     1,     1,     4,     5,     3,     6,
       2,     4,     2,     1,     3,     3,     0,     1,     2,     2,
       2,     2,     9,     5,     1,     3,     2,     2,     2,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     2,     2,     4,     3,
       4,     5,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     5,     2,     2,     2,     2,     2,     5,     1,     1,
       1,     4,     4,     4,     4,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       4,     3,     4,     5,     1,     1,     1,     1,     4,     3,
       2,     4,     3,     4,     3,     4,     5,     1,     1,     1,
       1,     1,     1,     1,     7,     5,     1,     1,     1,     1,
       4,     3,     4,     5,     1,     1,     4,     3,     4,     5,
       1,     1,     2,     1,     2,     4,     3,     4,     3,     4,
       5,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     2,     4,     3,     2,     4,     3,     2,     3,
       2,     2,     2,     2,     2,     2,     3,     2,     5,     2,
       5,     2,     5,     1,     1,     3,     3,     0,     0,     1,
       1,     1,     1,     1,     1,     3,     2,     5,     4,     2,
       5,     4,     2,     2,     1,     1,     3,     2,     2,     2,
       4,     4,     4,     4,     4,     4,     1,     1,     1,     3,
       2,     2,     1,     1
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
       2,     0,     1,    10,    11,     9,     0,     0,     3,   150,
     149,   754,   339,   536,   536,   536,   536,   536,   339,   536,
     553,   536,    20,   536,   536,   158,   536,   559,   339,   154,
     536,   339,   536,   536,   339,   484,   536,   536,   536,   338,
     536,   747,   536,   536,   340,   756,   536,   155,   156,   750,
      45,   536,   536,   536,   536,   536,   536,   536,   536,   555,
     536,   536,   482,   482,   536,   536,   536,   339,   536,     0,
     536,   339,   339,   536,   536,   338,    20,   339,   339,   325,
       0,   536,   536,   339,   339,   557,   339,   339,   339,   339,
     152,   339,   536,   211,   157,   536,   536,     0,    20,   339,
     536,   536,   536,   558,   339,   536,   339,   534,   536,   536,
     339,   536,   536,   536,   339,    20,   339,    45,   536,   536,
     153,    45,   536,   339,   536,   536,   536,   339,   536,   536,
     554,   339,   536,   339,   536,   536,   536,   536,   536,   339,
     339,   535,    20,   339,   339,   536,   536,   536,     0,   339,
       8,   339,   536,   536,   536,   748,   536,   536,   536,   536,
     536,   536,   536,   536,   536,   536,   536,   536,   536,   536,
     536,   536,   536,   536,   536,   536,   536,   536,   536,   536,
     536,   536,   339,   536,   749,   536,  1152,   536,  1153,   536,
     536,   536,   608,     0,    37,   608,    73,    48,    49,    50,
      65,    66,    76,    68,    69,    67,   109,    58,     0,   146,
     151,    52,    70,    71,    72,    51,    59,    54,    55,    56,
      60,   207,    75,    74,    57,   608,   445,   440,   453,     0,
       0,     0,   456,   439,   438,     0,   507,   510,   536,   508,
       0,   536,     0,   536,     0,     0,   550,    53,   459,   610,
     613,   619,   615,   614,   620,   621,   622,   623,   612,   629,
     611,   630,   616,     0,   745,   617,   624,   626,   625,   656,
     631,   633,   634,   632,   635,   636,   637,   638,   639,   618,
     640,   641,   643,   644,   642,   646,   647,   645,   662,   657,
     658,   659,   660,   648,   649,   650,   651,   652,   653,   654,
     655,   661,   628,   663,   627,   961,   960,   962,   963,   964,
     965,   966,   967,   968,   969,   970,   971,   972,   973,   974,
     975,   976,   959,   977,   978,   979,   980,   981,   982,   983,
     984,   985,   460,  1119,  1121,  1123,  1124,  1120,  1122,     0,
       0,   340,     0,     0,     0,     0,     0,     0,     0,   951,
      35,     0,     0,   595,     0,     0,     0,     0,     0,     0,
     454,   506,   480,   210,     0,     0,     0,   480,     0,   312,
     339,   713,     0,   713,   537,     0,    23,   480,     0,   480,
     932,     0,   948,   482,   480,   480,   480,    32,   483,    81,
     444,   915,   480,   909,   105,   480,    37,   480,     0,   340,
       0,     0,    63,     0,     0,   329,    44,     7,   926,     0,
       0,     0,     0,     0,    77,   340,     0,   946,   521,     0,
       0,   296,   295,     0,     0,   777,     0,     0,   340,     0,
       0,   537,     0,   340,     0,     0,     0,   340,    33,   340,
      22,   596,     0,    21,     0,     0,     0,     0,     0,     0,
       0,   398,   340,    45,   140,     0,     0,     0,     0,     0,
       0,     0,   752,   340,     0,   340,     0,     0,   949,   950,
       0,   339,   340,     0,     0,     0,   596,     0,  1105,  1104,
    1109,   986,   713,  1111,   713,  1101,  1103,   987,  1092,  1095,
    1098,   713,   713,   713,  1107,  1100,  1102,   713,   713,   713,
     713,  1040,   713,   713,  1117,  1074,     0,    45,  1126,  1129,
    1132,     0,   609,     4,    20,    20,     0,     0,    45,     5,
       0,     0,     0,     0,     0,    45,    20,     0,     0,     0,
     147,   537,     0,     0,     0,     0,   527,     0,   527,     0,
       0,     0,     0,   527,   222,     6,   485,   536,   536,   446,
     441,     0,   457,   448,   447,   455,    82,   172,     0,     0,
       0,   406,     0,   405,   410,   408,   409,   407,   381,     0,
       0,   351,   382,   354,   384,   383,   355,   400,   402,   395,
     353,   356,   595,   398,   541,   542,     0,   380,   379,    32,
       0,   599,   600,   539,     0,   597,   596,     0,   543,   596,
     561,   545,   544,   597,   548,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    45,     0,   741,   742,   740,     0,
     738,   728,     0,     0,   435,     0,   323,     0,   523,   934,
     935,   931,    45,   310,   772,   774,   933,    36,    13,   595,
       0,   480,     0,   192,     0,   310,     0,   184,     0,   695,
     693,   805,   887,   888,   771,   768,   769,   481,   515,   222,
     435,   310,   943,   938,   469,   341,     0,     0,     0,     0,
       0,   705,   708,   697,     0,   496,   669,   666,   667,   451,
       0,     0,   499,   944,   442,   443,   458,   452,   470,   106,
     498,    45,   516,     0,   199,     0,   382,     0,     0,    37,
      25,   767,   764,   765,   324,   326,     0,     0,    45,   927,
     928,     0,   686,   684,   673,   670,   671,     0,     0,    78,
       0,    45,   947,   945,     0,   908,     0,    45,     0,    19,
       0,     0,     0,     0,   913,     0,     0,     0,   496,   522,
       0,     0,   891,   918,   596,     0,   596,   597,   139,    34,
      12,   143,   573,     0,   729,     0,     0,     0,   713,   692,
     690,   850,   885,   886,     0,   689,   687,   919,   399,   513,
       0,     0,   871,     0,   881,   880,   883,   882,   678,     0,
     676,   680,     0,     0,    37,    24,     0,   310,   900,   903,
       0,    45,     0,   302,   298,     0,     0,   574,   310,     0,
     526,     0,  1044,  1039,   526,  1076,  1106,     0,   526,   526,
     526,   526,   526,   526,  1099,   310,    46,  1125,  1134,  1135,
       0,     0,    45,     0,     0,    20,    43,    38,    42,     0,
      40,    17,    46,   310,   132,   134,   136,   110,     0,     0,
      20,   339,   148,   164,   310,   179,   181,   183,   187,   526,
     190,   526,   196,   198,   200,   209,     0,   213,     0,    45,
       0,   449,   424,     0,   351,   364,   363,   376,   362,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   595,     0,
       0,   595,     0,     0,   398,   404,   396,   511,     0,     0,
     514,   567,   568,   572,     0,   564,     0,   566,     0,   605,
       0,     0,     0,     0,     0,   552,   566,   556,     0,     0,
     579,   546,   577,     0,     0,   351,   353,   549,   583,   582,
     551,   310,   310,   685,   688,   691,   694,   310,   339,     0,
     901,     0,    45,   723,   178,     0,     0,     0,     0,     0,
       0,   312,   776,     0,   528,     0,   474,   478,     0,   468,
     595,     0,   194,   185,     0,   321,     0,   208,     0,   664,
     595,     0,   751,   319,   316,   313,   315,   320,   310,   713,
     710,   719,   714,     0,     0,     0,   711,   697,   713,     0,
     755,     0,   497,   538,     0,     0,     0,    18,   204,     0,
       0,     0,   206,   195,     0,   493,   491,   488,     0,    45,
       0,   329,     0,     0,   332,   330,     0,    45,   929,   381,
     879,   924,     0,     0,   922,     0,     0,    87,    88,    86,
      85,    91,    90,   102,    95,    98,    97,   100,    99,    96,
     101,    94,    92,    89,    93,    83,     0,    84,   197,     0,
       0,     0,     0,     0,   297,     0,   188,   436,     0,    45,
     914,   912,   133,   781,     0,   779,     0,     0,   292,   538,
     180,     0,   576,     0,   575,   287,   287,     0,   724,     0,
     713,   697,   895,     0,     0,   892,   284,   283,    62,   281,
       0,     0,     0,     0,   675,   674,   135,    14,   182,   902,
      45,   905,   904,   146,     0,   103,    47,     0,     0,   681,
       0,   713,   526,     0,  1073,  1043,  1038,   713,   526,  1075,
    1118,   713,   526,   713,   526,   526,   526,   713,   526,   713,
     526,   682,     0,     0,     0,     0,  1146,     0,     0,  1133,
    1137,  1139,  1138,    45,  1128,   813,  1147,     0,  1131,     0,
       0,     0,   954,    28,    37,     0,     0,    39,     0,    30,
     159,   116,   310,   339,   118,   120,     0,   121,   114,   122,
     130,   129,   123,   124,   125,     0,   112,   115,    26,     0,
     310,     0,   595,   165,   538,   177,     0,     0,   222,   222,
       0,   224,   217,   221,     0,     0,     0,   352,     0,   359,
     361,   358,   357,   375,   377,   371,   365,   503,   368,   366,
     369,   367,   370,   372,   374,   360,   373,   378,   595,   411,
     389,     0,   343,     0,   414,   415,   401,   412,   403,     0,
     595,   512,     0,   531,   529,     0,   595,   563,   570,   571,
     569,   598,   607,   602,   604,   606,   603,   601,   562,   547,
       0,     0,     0,   351,     0,     0,     0,     0,     0,   665,
     683,   744,     0,   754,   747,     0,   756,   750,     0,   748,
     749,   746,   739,     0,   390,   391,   429,     0,     0,   505,
       0,     0,   349,     0,     0,   775,   476,   475,     0,   473,
       0,   193,     0,   526,   770,   427,   428,   432,     0,     0,
       0,   314,   317,   176,     0,   595,     0,     0,     0,   698,
     709,   310,   461,   713,   668,     0,   480,     0,     0,   201,
       0,   394,   937,     0,     0,     0,    16,   766,   327,   337,
       0,   333,   335,   331,     0,     0,     0,     0,     0,     0,
       0,   921,   672,    80,    79,   128,   126,   127,   340,   486,
     423,     0,     0,     0,     0,   191,     0,   519,     0,     0,
      45,     0,     0,    64,   526,   504,   598,   138,     0,   142,
      45,     0,   697,     0,     0,     0,     0,   890,     0,     0,
       0,   872,   874,   679,   677,     0,    45,   907,    45,   906,
     145,   340,     0,   501,     0,  1108,     0,   713,  1110,     0,
     713,     0,     0,   713,     0,   713,     0,   713,     0,   713,
       0,     0,     0,    45,     0,     0,     0,  1136,     0,  1127,
    1130,   958,   956,   957,    45,   953,    15,    29,    41,     0,
     173,   160,   117,     0,    45,     0,    45,    27,   159,     0,
     146,     0,   146,   186,   189,   214,     0,     0,     0,   247,
     245,   252,   249,   263,   256,   261,     0,     0,   215,   238,
     250,   242,   253,   243,   258,   244,     0,   237,     0,   232,
     229,   218,   219,     0,     0,   425,   351,     0,   387,   595,
     347,   344,   345,     0,   398,     0,   533,   532,     0,     0,
     578,   352,     0,     0,     0,   351,   585,   351,   589,   351,
     587,   310,     0,   595,   595,   595,   517,     0,     0,     0,
     936,     0,   311,   477,   479,   172,   322,     0,   595,   518,
       0,   940,   595,   939,   318,   320,   712,     0,   720,     0,
       0,     0,   696,   463,   480,   500,     0,   203,   202,   381,
     492,   489,   487,     0,   490,     0,   328,     0,     0,     0,
       0,     0,     0,   923,     0,     0,   422,   417,   910,   911,
     707,   310,   917,   437,   780,     0,   294,   293,     0,   287,
       0,     0,   289,   288,     0,   725,   726,   699,     0,   899,
     898,     0,   896,     0,   893,   282,     0,     0,   884,   878,
       0,     0,     0,     0,     0,   300,     0,   299,   307,     0,
    1117,     0,  1117,  1117,  1053,     0,  1047,  1049,  1050,  1048,
     713,  1052,  1051,     0,  1117,   713,  1071,  1070,     0,     0,
    1114,  1113,     0,     0,  1117,     0,  1117,     0,   713,   992,
     996,   997,   998,   994,   995,   999,  1000,   993,     0,  1081,
    1085,  1086,  1087,  1083,  1084,  1088,  1089,  1082,  1091,  1090,
     713,     0,  1034,  1036,  1037,  1035,   713,     0,  1064,  1065,
     713,     0,     0,     0,     0,     0,     0,  1148,     0,     0,
     814,   955,     0,     0,     0,   108,   758,     0,   111,     0,
     173,     0,     0,   161,   169,   172,   168,     0,   144,   267,
     253,   266,     0,   255,   260,   254,   259,     0,     0,     0,
       0,     0,   222,   212,   223,   241,     0,   222,   234,   235,
       0,     0,     0,     0,   278,   223,   279,     0,     0,   227,
     268,   273,   276,   229,   220,     0,   502,     0,   413,   385,
     388,     0,   346,     0,   530,   565,   566,     0,     0,   351,
       0,     0,     0,   743,   737,   753,     0,   595,     0,     0,
       0,   524,   350,     0,   340,   525,     0,   942,     0,     0,
       0,   716,   718,     0,    45,     0,   462,   450,   205,   334,
     336,     0,     0,     0,   925,   920,   131,   421,     0,     0,
     416,   916,     0,     0,     0,     0,     0,   285,     0,     0,
       0,     0,   894,   889,   471,   873,     0,     0,   848,   104,
     306,   301,   303,     0,     0,     0,  1002,   713,  1003,  1004,
      45,  1045,   713,  1072,  1068,   713,  1117,     0,  1001,    45,
    1005,     0,  1006,     0,   990,   713,  1079,   713,  1032,   713,
    1062,   713,  1140,  1141,  1142,  1150,  1151,    45,  1145,  1143,
    1144,    31,   175,   174,     0,     0,   119,   113,   107,   162,
     163,     0,   171,   170,     0,   248,   246,   264,   257,   262,
     216,   222,   595,     0,   240,   236,   223,     0,   233,     0,
     270,   269,     0,   225,   229,     0,     0,     0,     0,     0,
     230,     0,   426,   386,   348,   397,     0,   580,   592,   594,
     593,     0,     0,   393,   430,     0,     0,   773,     0,   433,
       0,   941,   721,   715,     0,     0,     0,   806,   930,   807,
     420,   419,     0,     0,   787,     0,   288,     0,     0,   730,
     727,   705,   700,   701,   703,   704,   897,     0,     0,   381,
       0,     0,     0,     0,     0,   309,   308,   520,     0,     0,
       0,  1046,  1069,     0,  1116,  1115,     0,     0,     0,   991,
    1080,  1033,  1063,  1149,     0,     0,   759,   595,   166,     0,
     239,   595,   241,     0,   280,   228,     0,   272,   271,   274,
     275,   277,   472,     0,   735,   734,   736,     0,   732,   392,
     431,     0,   952,   434,     0,   717,     0,     0,   418,     0,
      45,   778,   788,   789,   794,   798,   791,   799,   800,   795,
     797,   796,   792,   793,     0,   286,   291,   290,     0,     0,
     875,   876,     0,   804,   860,   861,   862,   859,   864,   858,
     865,   857,   856,   855,   863,   849,   849,     0,     0,   305,
     304,  1018,  1017,  1016,  1023,  1024,  1025,  1022,  1019,  1021,
    1020,  1029,  1026,  1027,  1028,     0,  1013,  1057,  1056,  1058,
    1059,     0,  1118,  1008,  1010,  1009,     0,  1012,  1011,   760,
       0,     0,   265,     0,     0,   227,   226,     0,     0,   731,
     509,     0,     0,   465,   786,   785,     0,   783,     0,   790,
       0,     0,   702,    45,     0,     0,     0,     0,     0,     0,
       0,  1055,  1112,  1007,     0,   172,   251,   231,   494,   733,
     722,     0,     0,   782,     0,     0,     0,     0,   869,     0,
       0,     0,     0,     0,   466,   706,     0,   803,    45,    45,
     846,     0,     0,  1015,  1014,     0,   763,   757,   761,     0,
       0,   464,   784,   713,   818,     0,     0,     0,     0,     0,
     867,   868,     0,     0,     0,     0,     0,     0,     0,   853,
       0,  1054,     0,   167,   495,   823,   819,     0,     0,     0,
       0,     0,     0,     0,     0,   810,     0,   467,   877,   851,
      45,   847,   845,     0,   354,   762,     0,     0,     0,     0,
     816,   808,     0,   802,     0,   866,   817,     0,   809,     0,
     854,   821,   822,     0,   824,   826,     0,     0,     0,   811,
     852,     0,   820,     0,   815,   801,     0,     0,   825,     0,
       0,   829,   870,   812,     0,     0,   827,   831,   830,    45,
       0,     0,   832,   837,   839,   840,     0,     0,     0,    45,
     828,    45,    45,   595,   843,   842,   841,   833,     0,   835,
     836,     0,   838,     0,    45,   844,   834
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,     6,     7,   192,   350,   193,   784,   699,   194,
     831,   568,   750,   638,   518,   829,   830,   407,   832,  1139,
    1392,   195,   196,   569,  1046,  1047,   197,   198,   199,   528,
    1175,  1176,  1050,  1177,   200,   201,   202,   203,  1075,   204,
     205,  1076,   206,   531,   207,   208,   209,   210,  1430,  1431,
    1184,  1685,   862,  1675,   211,   212,   213,   214,   215,   216,
     731,  1086,  1087,   217,   218,   219,   694,   999,  1000,   220,
     221,   659,   418,   855,   856,  1456,   857,   858,  1716,  1466,
    1471,  1472,  1717,  1718,  1467,  1468,  1469,  1458,  1459,  1460,
    1461,  1690,  1463,  1464,  1465,  1692,  1880,  1720,  1721,  1722,
    1088,  1089,  1367,  1368,  1787,  1573,  1067,  1068,   222,   422,
     223,   794,  1801,  1802,  1597,  1598,   952,   666,   667,   975,
     976,   964,   965,   224,   704,   705,   706,   707,  1015,  1330,
    1331,  1332,   363,   340,   370,  1221,  1480,  1222,  1281,   924,
     571,   590,   572,   573,   574,   575,  1276,  1002,   895,  1730,
     769,   576,   577,   578,   579,   580,  1226,  1482,   581,  1196,
    1727,  1297,  1277,  1298,   945,  1058,   225,   226,  1766,   227,
     228,   641,   957,   958,   658,   389,   229,   230,   231,   232,
    1006,  1007,  1324,  1737,  1738,   993,   233,   234,   235,   236,
    1121,   237,   898,  1234,   238,   342,   675,  1313,   239,   240,
     241,   242,   243,   601,   593,   904,   905,   906,   244,   245,
     246,   921,   922,   927,   928,   929,  1223,   752,   594,   747,
     513,   247,   248,   249,   250,   677,   678,   251,   715,   716,
     252,   462,   779,   780,   782,   253,   254,   713,   255,   766,
     256,   760,   257,   650,   990,   258,   259,  1922,  1923,  1924,
    1925,  1561,   987,   373,   669,   670,   986,  1529,  1760,   260,
     261,  1575,   622,   755,   756,  1745,  1977,  1978,  1746,   619,
     620,   262,   263,   264,   265,  1844,  1845,  2127,  2128,   266,
     702,   703,   267,   655,   656,   268,   634,   635,   269,   270,
    1064,  1065,  1914,  2076,  2077,  1991,  1992,  1993,  1994,  1995,
     652,  1996,  1997,  1998,  2166,  1146,  1999,  2000,  2001,  2134,
    2178,  2193,  2194,  2210,  2211,  2221,  2222,  2223,  2224,  2225,
    2234,  2002,  1932,  2085,   762,  2147,  2026,  2027,  2028,  2003,
     773,  1380,  1381,  1796,  1082,   271,   272,   273,   274,   275,
     276,   277,   278,   743,  1084,  1085,  1581,  1582,   279,   788,
     280,   726,   281,   727,   282,  1061,   283,   284,   285,   286,
     287,  1023,  1024,   288,   710,   289,   290,   291,   630,   631,
     292,   293,  1300,  1521,   663,   294,   295,   723,   296,   297,
     298,   299,   300,   301,  1151,  1152,   302,   303,   304,   305,
     306,  1122,  1628,   806,  1606,  1607,  1608,  1632,  1633,  1634,
    2056,  1635,  1636,  1609,  1939,  2124,  2045,   307,  1128,  1656,
     308,   309,   310,   311,  1112,  1610,  1611,  1612,  2051,   312,
    1130,  1660,   313,  1118,  1615,  1616,  1617,   314,   315,   316,
    1124,  1650,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,  1621,  1622,   807,
    1401,   332,   333,   334,   335,   336,   817,   818,   819,  1140,
    1141,  1142,  1147,  1666,  1667,   337,   338
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -1981
static const yytype_int16 yypact[] =
{
   -1981,    77, -1981, -1981, -1981, -1981,    61,  4115, -1981, -1981,
   -1981,    93, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981,   137, -1981, -1981,   458,   113, -1981, -1981, -1981,   137,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981,   168,   168, -1981, -1981, -1981, -1981, -1981,   168,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
     259, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981,   168, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,   364,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,   447,   496,
   -1981, -1981, -1981, -1981, -1981,   137, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981,   137, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981,   281,  1210,   536,   281, -1981, -1981, -1981,   583,
     603,   614,   625, -1981, -1981, -1981,   594,   645,   168, -1981,
   -1981,   656,   675,   686,   709,   558,   237,   715,   735,   796,
   -1981,   448, -1981, -1981, -1981,   281, -1981, -1981, -1981,   824,
     647,  1811,  2307, -1981, -1981,  3018, -1981,   820, -1981, -1981,
    2062, -1981,   867, -1981,   742,   867,   849, -1981, -1981,   876,
   -1981, -1981, -1981,   894,   926,   928,   935,   940, -1981, -1981,
   -1981, -1981,   951,   842, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,   957,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,   162,
     168,   997,   954,  1012,   884,   168,   168,   149,   168, -1981,
     168,   168,  1026, -1981,   477,  1028,   168,   168,   168,   168,
   -1981, -1981,   168, -1981,  1032,   168,   912,   168,  1057, -1981,
   -1981, -1981,   168, -1981,  1095,   168, -1981,   168,  1106,   159,
   -1981,   912, -1981,   168,   168,   168,   168, -1981, -1981, -1981,
   -1981, -1981,   168, -1981,   168,   168,   536,   168,  1115,   997,
     168,  1117, -1981,   168,   168, -1981, -1981, -1981,  1128,  1125,
     168,   168,  1129,  1132,   168,   997,  1135,  3018, -1981,  1147,
     168, -1981,  1158,   168,  1149, -1981,  1177,   168,   997,  1182,
    1198, -1981,   884,   997,   168,   168,  1603,   119,   168,   120,
   -1981, -1981,   233, -1981,   283,   168,   168,   168,  1200,   168,
     168,  3018,   141, -1981, -1981,   168,   168,   168,   168,   168,
    3018,   168, -1981,   997,   168,   997,   168,   168, -1981, -1981,
     168, -1981,   997,   168,  1203,  1205, -1981,   168, -1981, -1981,
    1218, -1981, -1981,  1223, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981,  1226, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981,   168, -1981, -1981,  1230,
    1232,  1237, -1981, -1981, -1981, -1981,  1134,   155, -1981, -1981,
     168,   168,   168,   168,  1256, -1981, -1981,  1142,   168,   168,
   -1981, -1981,   168,   168,   168,   168,   168,   589,   168,  1149,
     168,   168,  1115, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981,  1033, -1981, -1981, -1981, -1981, -1981, -1981,  3018,  3018,
    3018, -1981,  3018, -1981, -1981, -1981, -1981, -1981, -1981,  3018,
    3224, -1981,   150,  1262, -1981,  1252, -1981,  1029,  1031,  1269,
   -1981, -1981,  1265,  3018, -1981, -1981,  2408, -1981, -1981,  1261,
     889,  1262, -1981, -1981,   791,    -9, -1981,  2408, -1981, -1981,
   -1981,  1280,   433,   173,  3046,  3046,   168,   168,   168,   168,
     168,   168,   168,  1285, -1981,   168, -1981, -1981, -1981,   592,
   -1981, -1981,  1278,   168, -1981,  3018, -1981,  1067,   191, -1981,
    1288, -1981, -1981,  1289,  1292, -1981, -1981, -1981, -1981, -1981,
    2641,   168,  1290, -1981,   168,  1289,   168, -1981,   884, -1981,
   -1981, -1981, -1981, -1981, -1981,  1298, -1981, -1981, -1981, -1981,
   -1981,  1289, -1981,  1293, -1981, -1981,   608,  1212,   168,   644,
      83, -1981,  1294,  1137,  3018,  1167, -1981,  1305, -1981, -1981,
    3018,  3018, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981,   168, -1981,   168,  1299,   169,   168,   536,
   -1981, -1981,  1309, -1981,  1310, -1981,  1304,   801, -1981,  1313,
   -1981,   168, -1981, -1981, -1981,  1314, -1981,  1297,  3349, -1981,
     168, -1981,  5140, -1981,  3018, -1981,  1311, -1981,   168, -1981,
     168,   168,   168,  1262,   620,   168,   168,   168,  1167, -1981,
     168,   463, -1981, -1981, -1981,   889,   791, -1981, -1981, -1981,
   -1981, -1981, -1981,   162, -1981,  1278,  1318,  1294, -1981, -1981,
   -1981, -1981, -1981, -1981,   168, -1981, -1981, -1981,  5140, -1981,
     477,  1266, -1981,  1320, -1981, -1981, -1981, -1981,  3267,   651,
   -1981, -1981,   364,   168,   536, -1981,   168,  1289, -1981,  1325,
    1307, -1981,   168, -1981,  1329,  3018,  3018, -1981,  1289,   168,
     183,   168,  1060,  1060,   186,  1060, -1981,  1327,   218,   275,
     394,   396,   442,   478, -1981,  1289,   649, -1981,  1335, -1981,
     122,   166, -1981,   168,   168, -1981, -1981, -1981, -1981,   654,
   -1981, -1981,  1136,  1289, -1981, -1981, -1981, -1981,  2229,   168,
   -1981, -1981, -1981,   187,  1289, -1981, -1981, -1981, -1981,  1337,
   -1981,  1337, -1981, -1981, -1981, -1981,   452, -1981,   457, -1981,
    1331, -1981, -1981,  3412,  1340,  1342,  1342,  1837, -1981,  3018,
    3018,  3018,  3018,  3018,  3018,  3018,  3018,  3018,  3018,  3018,
    3018,  3018,  3018,  3018,  3018,  3018,  3018,  3018, -1981,  1286,
    1228,  1332,    34,   384,  3018, -1981, -1981, -1981,   668,  1455,
   -1981, -1981, -1981, -1981,   680, -1981,  1579,   994,  3018,  1343,
     889,   889,   889,   889,   889, -1981,  1122, -1981,   433,   433,
    1262,  1348, -1981,  3046,  5140,   116,   133, -1981,  1352,  1353,
   -1981,  1289,  1289, -1981, -1981, -1981, -1981,  1289, -1981,    53,
   -1981,   162, -1981, -1981, -1981,   168,  3443,   168,  1347,  3018,
    1300, -1981, -1981,   168, -1981,  3018,  3482, -1981,   719, -1981,
   -1981,  1330, -1981, -1981,   725, -1981,   168, -1981,   168, -1981,
   -1981,  1212, -1981, -1981, -1981, -1981, -1981,  3519,  1289, -1981,
   -1981, -1981,  1349,  1355,  1358,  1361, -1981,  1137, -1981,   168,
   -1981,  3580, -1981, -1981,   168,  3629,  3674, -1981,  1362,   734,
    1360,  1269, -1981, -1981,   168, -1981,  1370, -1981,  1354, -1981,
     168, -1981,  1255,   101, -1981, -1981,   -71, -1981, -1981,  1376,
   -1981,  1372,  1378,   752, -1981,   168,  1367, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981,  1368, -1981, -1981,   379,
    1369,  3729,  2671,   -91, -1981,  1356, -1981, -1981,   754, -1981,
   -1981, -1981, -1981, -1981,   760, -1981,  1366,   762, -1981, -1981,
   -1981,  3018, -1981,  1274, -1981, -1981, -1981,   781, -1981,  1387,
   -1981,  1137,  1380,  1389,   798, -1981, -1981, -1981,  1390, -1981,
    1382,   168,  3018,  3018, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981,  1391,  1392, -1981,   678, -1981, -1981,  3760,  3794, -1981,
    1381, -1981,   479,  1383, -1981, -1981, -1981, -1981,   480, -1981,
   -1981, -1981,   484, -1981,   520,   523,   537, -1981,   547, -1981,
     570, -1981,  1386,  1388,  1394,  1395, -1981,  1396,  1398, -1981,
   -1981, -1981, -1981, -1981, -1981,  1262,  1399,  1385, -1981,  1397,
     -40,   802, -1981, -1981,   536,   168,    81, -1981,  1401, -1981,
    1412, -1981,  1289, -1981, -1981, -1981,  1405, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981,   982, -1981, -1981, -1981,   168,
    1289,   115, -1981, -1981, -1981, -1981,   168,   168, -1981,   961,
     457, -1981,  1406, -1981,  1364,  3018,  3046, -1981,  3018,  1342,
    1342,   462,   462,  1837,   902,  3121,  3061,  5140,  3061,  3061,
    3061,  3061,  3061,  3121,  2131,  1342,  2131,  1476,  1332, -1981,
   -1981,  1402,  1414,  2493, -1981, -1981, -1981, -1981, -1981,  1419,
   -1981, -1981,   884,  5140, -1981,  3018, -1981, -1981, -1981, -1981,
    5140,   434,  5140,  1343,  1343,   905,  1343,   525, -1981,  1348,
    1420,   433,  3844,  1421,  1422,  1425,  3046,  3046,  3046, -1981,
   -1981, -1981,   168,  1423, -1981,  1424,  1294, -1981,   364, -1981,
   -1981, -1981, -1981,  1178,   671,   697, -1981,   805,   884, -1981,
     884,   817,  5140,  1429,   826, -1981,  5140,  3018,  2641, -1981,
     831, -1981,   884,  1337, -1981,   779,   800, -1981,   833,  1279,
     835, -1981,  1895, -1981,    83, -1981,   168,  3018,   168, -1981,
   -1981,  1289, -1981, -1981, -1981,  1209,   168,  3018,   168, -1981,
     168, -1981,  1262,  3018,  1426,  2671, -1981, -1981, -1981, -1981,
     891, -1981,  1427, -1981,  1430,  1431,  1435,  1241,  3018,   168,
     168, -1981, -1981, -1981, -1981, -1981, -1981, -1981,   997, -1981,
    2701,  3155,  1432,   168,   168, -1981,   168, -1981,   314,   168,
   -1981,  3018,   168, -1981,  1337,  5140, -1981,  1444,   112,  1444,
   -1981,   168,  1137,  1450,  2773,   168,   168, -1981,   477,  3018,
     916, -1981,  1443,  5140, -1981,    29, -1981, -1981, -1981, -1981,
   -1981,   997,   -16, -1981,   168, -1981,   746, -1981, -1981,   326,
   -1981,   161,   870, -1981,   761, -1981,   460, -1981,    49, -1981,
     168,   168,   168, -1981,   168,   168,   649, -1981,   168, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,   168,
     882, -1981, -1981,  1446, -1981,  1452, -1981, -1981,  1412,  1996,
   -1981,  1684, -1981, -1981, -1981, -1981,   580,   362,   362,  1222,
    1227, -1981, -1981,  1233,  1236,  1238,   507,   168, -1981, -1981,
   -1981, -1981,  1458, -1981, -1981, -1981,  1406, -1981,  1460, -1981,
     438,  1451, -1981,  1462,  4101, -1981,  1459,  1463,  1269, -1981,
   -1981,  4418, -1981,  3018,  3018,  1455, -1981,  5140,  2408,   433,
   -1981,   142,  3046,  3046,  3046,   144, -1981,   165, -1981,   176,
   -1981,  1289,   168, -1981,  1332, -1981, -1981,  1471,   947,  3018,
   -1981,  1481, -1981,  5140, -1981, -1981, -1981,  3018, -1981, -1981,
    3018, -1981, -1981, -1981, -1981,  5140, -1981,  1279, -1981,  1469,
    4449,  1475, -1981,   121,   168, -1981,   962, -1981, -1981,  1477,
    5140, -1981, -1981,  4418, -1981,  1255, -1981,  1255,   168,   168,
     168,   963,   965, -1981,   168,  3018,  4483,  2875, -1981, -1981,
   -1981,  1289,  1262, -1981, -1981,  1334,  5140, -1981,   168, -1981,
    1480,  1495, -1981, -1981,  1263,  1507, -1981, -1981,  1512, -1981,
    5140,   973, -1981,   977, -1981, -1981,  4514,   168, -1981, -1981,
    1506,  1508,  1319,  1454,   168,   168,  1509,  1517, -1981,   388,
   -1981,  1513, -1981, -1981, -1981,  1514, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981,   746, -1981, -1981, -1981, -1981,   326,   168,
   -1981, -1981,   984,  1518, -1981,  1520, -1981,  1521, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,   870, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981,   761, -1981, -1981, -1981, -1981, -1981,   460, -1981, -1981,
   -1981,    49,  1511,  1516,  1519,   696,  1000, -1981,  1522,  1526,
    1262, -1981,  1528,  3018,  3018, -1981, -1981,  1530, -1981,  2684,
     882,  1531,  4545, -1981, -1981, -1981,  5140,  2095, -1981, -1981,
   -1981, -1981,  1533, -1981, -1981, -1981, -1981,    34,    34,    34,
      34,    34,   961, -1981,  1523,  1534,  1527,   961,  1451, -1981,
     457,   438,    86,    86, -1981, -1981, -1981,  1015,  1546,   721,
     266, -1981,  1545,   438, -1981,  3018, -1981,  1535, -1981,  1269,
   -1981,  2493,  5140,  1537, -1981, -1981,   791,  1553,  1538,  1025,
    1559,  1562,  1565, -1981, -1981, -1981,  1572,  1332,  1570,    17,
     884, -1981,  5140,   168,   997,  5140,    17,  5140,  1279,  3018,
    1571, -1981, -1981,  3018, -1981,  1583, -1981, -1981, -1981, -1981,
   -1981,  1027,  1041,  1044, -1981, -1981, -1981,  5140,  3018,  3018,
    4579, -1981,   168,  1575,   112,  1576,  3349, -1981,   168,   168,
     168,  2773, -1981, -1981, -1981, -1981,  2974,   168, -1981, -1981,
    1585,  1596, -1981,   168,   168,  3018, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981,   161, -1981, -1981,
   -1981,  3018, -1981,  3018, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981,  5140,  5140,  1047,  1598, -1981, -1981, -1981, -1981,
   -1981,  1050, -1981,  5140,   580, -1981, -1981, -1981, -1981, -1981,
   -1981,   961, -1981,   168, -1981, -1981,  1592,  1584, -1981,  1083,
     266,   266,   438, -1981,   438,    86,    86,    86,    86,    86,
     996,  4610, -1981, -1981, -1981, -1981,  3018, -1981, -1981, -1981,
   -1981,  1788,  1588,   830, -1981,   168,  1609,  1292,   168, -1981,
     168, -1981,  4641, -1981,  4672,  1363,  3018, -1981, -1981, -1981,
    5140,  5140,  3018,  1606,  1614,  3018, -1981,  1605,  1607, -1981,
   -1981,  1612,  1616, -1981, -1981, -1981, -1981,  1608,  1055,  1624,
    1625,  1058,  1048,   168,   168, -1981, -1981,  5140,   958,  1613,
     246, -1981, -1981,  1599, -1981, -1981,   254,  4703,  4734, -1981,
   -1981, -1981, -1981, -1981,  1626,  1628, -1981, -1981, -1981,  1617,
   -1981,  1332, -1981,   438, -1981,   996,  1620,   266,   266, -1981,
   -1981, -1981, -1981,  4765, -1981,  4418, -1981,  1059, -1981,   850,
   -1981,   884,  1399, -1981,  1279, -1981,  1622,  4796,  5140,  2374,
   -1981, -1981,  1614, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981,  4827, -1981, -1981, -1981,  3018,   168,
   -1981,  1629,   168, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981,  1630,  1632, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981,  1641, -1981, -1981, -1981, -1981,
   -1981,  1061, -1981, -1981, -1981, -1981,  1633, -1981, -1981, -1981,
    1642,  1684, -1981,  1634,  1066,   996, -1981,  3018,  1788, -1981,
   -1981,  3018,  3018, -1981, -1981,  5140,  1070, -1981,    50, -1981,
    3018,  1076, -1981, -1981,  1084,  1643,  1086,   168,   168,   168,
    3018, -1981, -1981, -1981,   168, -1981, -1981, -1981,  4858, -1981,
    5140,  4889,  2374, -1981,  1638,  1645,  1646,  1649,  1650,  1653,
     168,   168,  1655,  1656,  4920, -1981,  1581, -1981, -1981, -1981,
   -1981,   546,  1668,  1399, -1981,  4951, -1981,  1669, -1981,  1657,
    3018, -1981, -1981, -1981,  1638,   168,   168,   168,   168,   168,
   -1981, -1981,  3018,  3018,  3018,  1661,  1053,  1088,  1112, -1981,
    3018, -1981,   168, -1981,  5140,     2, -1981,  1683,  1146,  1153,
    1685,  1157,  1676,  1164,  4982,  5013,  1674,  5140, -1981, -1981,
   -1981, -1981, -1981,  5044,   893, -1981,  1689,  1690,   168,   168,
   -1981, -1981,   168, -1981,  3018, -1981, -1981,  3018, -1981,  1053,
   -1981, -1981, -1981,  1168, -1981,  1687,  1187,  1190,  1195,  5075,
   -1981,   168, -1981,  3018, -1981, -1981,  1682,  3018, -1981,  5109,
    1204, -1981, -1981,  5140,  3018,  3018,  1692,  5140, -1981, -1981,
     437,  1213, -1981,  1697,  1698, -1981,  1693,  1693,  1693, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,   424,  1702,
   -1981,  1586, -1981,  1215, -1981, -1981, -1981
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
   -1981, -1981, -1981, -1981, -1981,    73,  1504,   892, -1981, -1981,
    -626,   -38, -1981, -1981,  -359, -1981,   557, -1981,   -50,   885,
   -1981, -1981, -1981,  1745,   -67, -1981, -1981, -1981, -1981, -1981,
   -1981,    42,   288,   619, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981,  -161, -1041, -1981, -1981, -1981,   712,   289,   883,
   -1981,  -333, -1485,    51, -1981, -1981, -1981, -1981, -1981, -1981,
    1193,  -271,     4, -1981, -1981, -1981,  1191, -1981,  -557, -1981,
   -1981, -1981, -1981,  1075, -1981, -1981,   541, -1150, -1399,   877,
     270, -1312,  -226,  -136,   881, -1981,    30,    39, -1623, -1981,
   -1396, -1143, -1395,  -292, -1981,  -112, -1430, -1598,  -776, -1981,
   -1981,   366,   669,   180,  -165,   -31, -1981,   392, -1981, -1981,
   -1981, -1981, -1981,  -179, -1981,   -48,  -120,   810, -1981,   793,
     465,   488,  -338, -1981, -1981,   759, -1981, -1981, -1981, -1981,
     226,   225,  1701,  2178,  -330, -1205,    46,  -460, -1294,   691,
    -492,  -494,  1225,   269,  1351,  -927,    31, -1981, -1981,  -565,
    -564,  -192, -1981,  -820, -1981,  -548,  -825, -1040, -1981, -1981,
   -1981,    23, -1981, -1981,  1121, -1981, -1981,  1552, -1981,  1555,
   -1981, -1981,   498, -1981,  -350,    -3, -1981, -1981,  1558,  1560,
   -1981,   466, -1981,  -682,  -262,  1054, -1981,   939, -1981, -1981,
    -195, -1981,   834,   306, -1981,  3913,  -251,  -938, -1981, -1981,
   -1981, -1981, -1981, -1981,   535, -1981,   308,  -861, -1981, -1981,
   -1981,   325, -1175,  -577,   874,  -813,  -345,  -413,  -393,   261,
     -78, -1981, -1981, -1981, -1981, -1981,   806, -1981, -1981,   776,
   -1981,  1024, -1743,   714, -1981, -1981, -1981,  1201, -1981,  1199,
   -1981,  1202, -1981,  1217,  -926, -1981, -1981, -1981,  -199,  -216,
   -1981, -1981, -1981,  -380, -1981,  -281,   509,   506, -1981, -1981,
   -1981, -1981, -1981, -1338,  -504,    28, -1981,  -248, -1981,  1068,
     890, -1981, -1981,   887, -1981, -1981, -1981, -1981,  -329, -1981,
   -1981,   822, -1981, -1981,   864, -1981,    80,   895, -1981, -1981,
   -1981,   476, -1981, -1981,  -259, -1981, -1981,  -148, -1981,  -714,
    -348,  -353, -1981, -1981, -1981, -1468, -1981, -1981, -1981,  -287,
   -1981, -1981,  -352, -1981,  -365, -1981,  -372, -1981,  -370, -1600,
    -973,  -715, -1981,  -162,  -438,  -249, -1980, -1981, -1981, -1981,
    -454,  -267,   286, -1981,  -704, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981,  -449, -1321,   499, -1981,    90, -1981,  1267,
   -1981,  1410, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1317,   543, -1981,  1175, -1981, -1981, -1981, -1981,  1540,
   -1981, -1981, -1981,   127,  1525, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981,   468, -1981, -1981, -1981, -1981,
   -1981,   451,   252,  -489, -1283, -1145, -1315, -1334, -1308, -1307,
   -1981, -1302, -1300, -1177, -1981, -1981, -1981, -1981, -1981,   236,
   -1981, -1981, -1981, -1981, -1981,   284, -1298, -1288, -1981, -1981,
   -1981,   238, -1981, -1981,   280, -1981,   391, -1981, -1981, -1981,
   -1981,   250, -1981, -1981, -1981, -1981, -1981, -1981, -1981, -1981,
   -1981, -1981, -1981, -1981, -1981, -1981, -1981,    85, -1981,    87,
    -145, -1981, -1981, -1981, -1981, -1981, -1981, -1981,   765, -1981,
   -1981, -1981,  -772, -1981,    67, -1981, -1981
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1118
static const yytype_int16 yytable[] =
{
     376,   767,   774,   775,   776,   777,   627,  1021,   642,   763,
     653,   623,  1352,  1477,   896,   814,  1008,   664,  1275,   900,
     537,   539,  1552,  1536,   388,   388,   544,   679,   930,   682,
    1754,   396,   516,  1576,   685,   686,   687,   691,  1235,  1457,
    1719,  1296,   688,   746,  1551,   690,  1462,   692,   592,  1149,
    1689,  1691,   592,  1928,  1583,   709,   926,   926,  1705,   421,
     390,  1309,  1390,   797,   758,   997,   864,   440,  1228,   695,
    1642,   443,  1225,  1225,  1102,  1101,  1490,     2,     3,  1353,
    1771,  1772,  1773,   646,  1864,   720,   826,  1631,   981,  1641,
    2104,  1654,   673,  1334,   739,  1263,  1643,  1644,   735,  1590,
     914,  1711,  1645,   740,  1646,  -540,  1648,   748,   339,   751,
    1255,   387,   925,   925,  1870,  1871,  1649,   519,   943,  1629,
    1590,  1639,   770,  1652,  1256,  1658,   368,   368,   371,  1764,
    1439,  1364,  1335,   783,  1264,   786,  -581,  1421,  1265,  1266,
     982,  -590,   792,  1144,   387,  1267,  2176,   545,   368,   403,
    -591,  1595,  -584,  -590,  1709,  1373,   672,   632,  1570,  1724,
     826,   387,  -591,    25,  -584,   888,  2169,   616,    29,   617,
     530,   423,   890,  -588,   681,     8,   827,  2177,   903,   566,
     738,  -560,  2105,   915,  -586,  -588,   917,  1148,   438,    47,
      48,   543,  1181,   907,   543,  1004,  -586,   909,  1619,   543,
    1851,   800,  1182,   802,   916,  1336,  -526,   983,   387,  2200,
     803,   804,   805,   387,  1334,   464,   808,   809,   810,   811,
    -339,   812,   813,     4,  1354,  1637,   543,  1647,   757,  1655,
    -581,  1714,  1591,  -137,  -141,  1600,  2106,   899,  1960,   387,
    -339,    90,  -540,   984,   592,   538,  1442,  -590,   753,   387,
      94,  1078,   914,  1591,  2107,   -61,  -591,  1630,  -584,  1640,
     566,  1653,  1022,  1659,  -339,  2108,   849,  2109,  2110,  1422,
    2111,  1877,  1423,  2112,  -526,  1268,   387,  1967,  1968,  -588,
     843,  1869,   387,   543,  1571,  1544,   387,  -560,   387,   387,
    -586,   959,   988,   387,   954,   387,   120, -1042,   371,  1748,
   -1067,   618,   621,  1878,  1693,  1695,  1269,   628,   629,   633,
     629,  1867,   637,   639,  1490,  1083,   645,  1642,   649,   651,
     651,   654,   406,  1631,   657,  1602,  1336,   661,     5,   657,
    1229,  1072,  -989,  1074,   671,   985,  1641,   676,  2047,   657,
    1009,   657,  1654,  1643,  1644,   388,   657,   657,   657,  1645,
    1270,  1646,  1073,  1648,   657,  1629,   689,   657,   647,   657,
     387,  1440,  1483,  1649,  2113,   700,   701,  -581,  1639,  1765,
    -137,  -141,   712,   714,  1652,   926,   719,  1080,  1658,   460,
     684,  1060,   725,  1326,  -590,   729,  1387,  1382,  1389, -1078,
     889,   890,   -61,  -591,   592,  -584,  1865,   742,   592,  1683,
     749,  1688,   543,   771,   543,   592,   671,   759,   761,   761,
     387,   765,   742,   914,  1805,   914,  -588,   772,   772,   772,
     772,   772,   914,   781,  -560,  1097,   785,  -586,   787,   725,
    1982,  1253,   790,   948, -1042,   793,  1321, -1067,  1520,   798,
    2053,  2198,  1965,  1496,  1498,  1500,  1577,  1227,   919,  -562,
     543,  1920,  1453,  1711,  1712,  1713,   543,   816,  1689,  1691,
    1188,  1637,   470,  -526,  1962,   368,   890,   869,   815,  -989,
    2048,   870,  1190,   369,  1647,   838,  1931,  1081,  1334,   828,
    1655,  1071,   833,   834,   835,   645,   543,   543,   543,  1189,
     841,   842,   543,  1630,   844,   645,   645,   645,   645,   885,
     643,   595,   852,   853,   471,   603,  1640,  1454, -1094,   591,
   -1097,   387,  1653,   591,   644,  1702,  1659,  1243,  1244,  1245,
    1246,  1247,  1345,  1346,  2049,   962, -1078,   836,   543,   566,
     910,   543,   512,  2065,   911,  2050,  2054,   845,   846,   847,
     848,   969,  1892,  1218,  1703,   543,  1014,  2055,  -562,   890,
    2226,   517,  1457,   592,  1347,   543, -1031,  1863,  1455,  1462,
     387,  -562,   913,  2226,   939,   387,   536,  2149,   931,   932,
     712,   765,   759,   649,   937,  -526,  2227,   787,   543,   914,
    1687,  -526,   950,  1714,   387,   645,  1614,   823,   824,  2227,
    1336,   520, -1061, -1041, -1066,  -526,  1447,  1448,  -988,   839,
     941,   524,   525,   657,   387,  1111,   961,  1114,   645,  1117,
    2129,   521,   643,   942,  1123,   899,   971,  1127,  1129,  1475,
    2122,  2123,   522,  -384,  1235,  1299,   644,   944,  1059,   972,
     978,  2240,  2242,   523, -1077,  1021,  1021, -1093,  1662,  1663,
    1664,  1008,  1668,  1669,  2246, -1094,  1600, -1097,   926,  2228,
     963, -1096,   979,   529,  1186,   998,  1187,  1003,  1016,  1093,
    1005, -1030,  1156,  1601,   532,   980,  1759,  1099,  2159,   697,
    1453,  1049,  1094,  1019,  -382,  1157,  1230,  1053,  1109,  1496,
    1498,  1500,  1048,   533, -1060,  -562,  1503,    50,  1236,  1231,
    1054,  2084,  1055, -1031,   534,  1131,   733,  1062,  1304,  1066,
    -383,  1237,   645,   741,   925,   591,  1476,  1310,   926,   926,
     926,  1863,  1504,  1160,  2081,   618,   526,   535,   592,   592,
     592,   592,   592,   540,  1185,  1454,  1019,  1288,  -230, -1061,
   -1041, -1066,   645,  1292,    46,  -988,  1602,  1875,  1876,  1603,
    1289,  1104,  1318,   541,  1070,   645,  1293,   600,   645,   914,
     914,   914,   914,   914,  1105,  1319,  2063,   586,   587,   588,
    1340,  1537,  1356,  1538,  1495,  1497,  1499,    74,  1359,   561,
    1362, -1077,  1150,  1341, -1093,  1357,  1455,   598,   563,   602,
     604,  1360,  -382,  1363,   117,  1153,   785,  1096, -1096,   941,
    1098,   121,  1132,  1133,   888,  1426,   910,    95, -1030,  1372,
     911,  1178,  1370,  -383,   542,   564,  1376,   587,   588,  1194,
    1424,  1259,  1260,  1505,   912,   891,  2161,  1261,  2158,  1377,
    1191, -1060,  1275,  1425,  1134,  1509,  1506,   108,   913,  1296,
    1396,   546,   565,  -387,   971,   582,  1399,  1441,  1510,  1230,
    1402,  1518,  1404,  1522,  1623,   894,  1406,  1512,  1408,   613,
     614,  1976,  1515,  -385,  1519,   591,  1523,   605,  1303,   591,
     547,  2197,  2196,    25,  1170,   894,   591,  1171,    29,   589,
    1483,   920,  1855,  1856,  1857,  1858,  1859,  1225,  1225,  1225,
    1225,  1225,   597,  1382,   606,  1485,  1783,   566,  1895,    47,
      48,  1488,  1273,   548,  1486,  1900,   890,  1520,  1155,  1545,
    1673,  -814,   607,   618,   745,   587,   588,   869,  1674,  1279,
     910,   870,  1546,  1179,   911,   633,   561,  1397,   871,   872,
    1733,   587,   588,  1400,  1587,   563,   570,  1403,   654,  1405,
    1403,  1403,  1600,  1407,   608,  1409,   609,  1588,   567,   885,
    1507,    90,   913,   610,   926,   926,   926,  1600,   611,  1601,
      94,  1311,   564,  1623,  1516,  1292,   676,  1135,  1136,   612,
    1527,  1137,  1138,  2031,  1601,   615,  1535,  1337,  1751,   624,
    1509,  1509,   701,  1340,  2032,  2033,  1446,  1447,  1448,   565,
    2034,  1791,  2035,  1768,  1774,  1376,  1775,   714,  1449,  1435,
    1436,  2036,  1817,  2037,  1792,  2038,   120,  1450,  1793,   910,
    1739,  1497,  1499,   911,   368,  1818,  1835,  1836,  1837,  1358,
     587,   588,  1875,  1876,   591,  1241,   589,   912,  1554,  1451,
    1624,  1838,  1602,  1872,  1452,  1603,  1013,   625,  1976,   626,
    1604,   913,  1533,  -584,   566,  1418,  1873,  1602,  1605,   148,
    1603,   640,  1432,   648,  1625,  1604,  1728,   660,  1907,  1418,
    1385,  1453,  1418,  1605,  1626,  1954,  1600,   662,  1957,  1627,
    1438,  1594,  1908,  1093,   665,  1909,  1376,  2068,  1955,  2090,
    2014,  1958,  2015,  1601,  1872,  2014,  2011,  2015,  2102,  2013,
    2069,  2016,  2091,  2017,  1509,   567,  2016,  2097,  2017,  1145,
    1145,  2103,  1376,  1416,  2119,  1736,  2170,  2115,  1517,  1875,
    1876,  1969,  1970,  1971,  1964,  2117,  1454,  2120,   722,  2171,
     674,  1806,  2039,  1808,  1809,  2040,  1613,  1427,   828,  1618,
    2170,   680,  1638,  2071,  1651,  1813,  1657,   910,  1661,  1624,
     693,   911,   698,  2172,  1731,  1820,   708,  1822,   587,   588,
     711,  1437,   768,  1248,   717,   912,  1602,   718,   645,  1603,
     721,   778,  1191,  1625,  1340,  1694,  1696,  1455,  1747,   913,
    1749,  1418,   724,  1626,  1883,  1376,   728,  2180,  1627,  1568,
     554,   555,  1587,  1756,  2181,   387,  2201,  1758,  2183,   591,
     591,   591,   591,   591,  1767,  2185,   730,   920,   920,  2202,
    1443,  1532,   732,  1115,  1116,  1340,  1119,   736,  1376,  2041,
    1687,  2018,  2019,  1093,  2020,  2021,  2018,  2019,  2204,  2020,
    2021,  2205,  2215,   737,   733,   764,  2206,   973,   795,   974,
     796,  2229,   556,  1479,  1501,  2216,   557,   558,   559,   560,
    1740,  1741,  1742,   799,  2230,  2022,  2245,   733,   801,   561,
    2022, -1117,   914,  1249,  1250,   820,   562,   821,   563,   863,
     865,   866,   822,   867,  2235,  2236,   825,  2042,  2043,  2044,
     868,  1125,  1126,   837,   840,   890,   861,   891,  1528,   892,
    1528,   893,    25,  1322,   768,   564,  2023,    29,   657,   910,
     998,  2023,   998,   911,   894,  1539,   897,   908,   918,  2024,
     587,   588,   938,   753,  2024,  1366,   592,   912,    47,    48,
     953,   947,   565,   949,   951,   960,   966,   989,   970,   371,
    1565,   913,   992,   994,   888,  1560,   946,  1010,  1011,  1012,
    1574,  1017,  1025,  1026,  1066,  1079,  1052,  1090,  1103,  1811,
    1572,   956,   514,  1100,  1814,  1091,  1592,  1106,  1593,   387,
     645,  1114,  1120,  1143,  1158,   543,  1195,  1824,  1198,  1219,
      90,   870,   911,  -342,  1596,  1220,  1251,   566,   977,    94,
    1257,  1258,  1280,  1665,  1305,   991,  1283,  1291,  1320,  1826,
    1306,   995,   996,  1307,  1150,  1828,  1308,  1317,  1323,  1830,
    1325,  1743,  1329,  -891,  1049,  1339,  1679,  1338,  1343,  1344,
    1348,  1672,  1361,  1355,  1371,  1374,  1375,  1379,  1378,  1386,
    1388,  1410,  1395,  1411,  1398,   120,  1419,  1418,   567,  1412,
    1413,  1414,  1896,  1415,  1883,  1051,  1429,  1181,  1420,  1704,
    1434,  1470,  1479,  1478,  1898,  1473,  1484,  1502,  1489,  1492,
    1493,  1021,  1715,  1494,  2162,  1382,  1511,   515,   339,   369,
     903,  1781,  1534,  1547,  1335,  1548,  1549,  1542,   148,  1930,
    1550,  1539,  1569,  1559,  1539,  1539,  1539,  1578,  1589,  1678,
    1232,  1676,  1697,  1707,  1744,   556,  1723,  1698,  1710,   557,
     558,   559,   560,  1699,  2195,  1021,  1700,  1725,  1701,  1750,
    1728,   869,   561,  1763,  1729,   870,  1107,  1108,  1753,   562,
    1761,   563,   871,   872,  2025,  1785,   657,  2195,   873,   874,
     875,   876,  1782,  -493,   878,   879,   880,   881,   882,   883,
    1786,   884,  1788,   885,   886,  1789,  1776,  1961,   564,  1790,
     920,  1797,  1591,  1798,  1590,  1804,  1938,  1803,  1807,  1810,
    1005,  1941,  1832,  1819,  1942,  1821,  1823,  1833,  -381,  1861,
    1834,  1854,  1862,  1839,  1949,   565,  1950,  1840,  1951,  1841,
    1952,  1846,  1849,  1874,  1879,  1882,  1799,  1800,  1885,  1887,
    1199,  1200,  1201,  1202,  1203,  1204,  1205,  1206,  1207,  1208,
    1209,  1210,  1211,  1212,  1213,  1214,  1215,  1216,  1217,  1886,
    1888,  1816,   387,  1889,  1238,   768,  1890,  1891,  1239,   556,
    1233,  1893,  1903,   557,   558,   559,   560,  1240,  1906,  1242,
     566,  1915,  1917,  1933,  1934,  1956,   561,  -224,   744,  1979,
    1963,  1986,  2061,   562,  1252,   563,   903,  1981,   745,   587,
     588,  1989,  1990,  1562,  2009,   696,  2006,  2008,  2007,  2010,
     561,  -805,  2012,  2059,  2046,  2060,  2052,  2072,  2062,   563,
    1282,  2066,   564,  2070,  2083,  2087,  1286,  2088,  2089,  2094,
    2118,   567,   696,  2133,  2093,  2096,  2140,  2141,  2145,   696,
    2135,  2136,   977,  1599,  2137,  2138,   564,   754,  2139,   565,
    2142,  2143,  1866,  1715,  1715,  1715,  2150,  2152,  2153,  1145,
    1145,  1145,  2168,  1145,  1145,  1715,  2157,  1670,  2160,  1684,
    2179,  2184,  2182,   565,   556,  2188,  2191,  2192,   557,   558,
     559,   560,  2203,  2212,  2231,  2232,   387,  2219,  2233,  2244,
     527,   561,  2226,  1428,  1905,   633,  1154,  1159,   562,  1918,
     563,  1847,  1677,  1391,   566,  1333,  1183,  1680,  2095,  1445,
     589,  1848,   851,   854,   967,  1192,  1708,  2064,  1966,  1193,
    1868,  1860,  1959,  1351,  1585,  1369,  1572,   564,   566,  1784,
    1744,  2005,  1921,  1916,  1567,  2030,  1936,   591,   920,  1929,
    1940,  1284,  1365,   850,  1301,  1935,  1596,  1524,  1508,  1946,
    1328,  1769,  1770,  2243,   565,   567,   402,  1884,   734,  1899,
    1894,   968,   549,  1383,   778,   550,  1514,  1665,   551,  1541,
     552,  1734,  1069,  1974,  1290,  1350,  1735,  1254,   556,   567,
    1314,  1342,   557,   558,   559,   560,  1095,  1384,   934,   933,
    2082,   387,   935,  1526,  1531,   561,  1919,  1145,  1145,  1145,
    2099,  1077,   562,  2175,   563,  1704,  1271,   696,   936,   566,
    1294,  1272,  1327,  1897,  1715,  1564,  1715,  1715,  1715,  1715,
    1715,  1715,   869,  2132,  2079,   903,   870,  2156,  1285,  2208,
    2218,   564,  2155,   871,   872,    13,    14,  2237,    15,    16,
     874,  2239,   876,    20,  2086,   878,   879,   880,   881,   882,
    2148,    23,  2163,  1795,   885,  1584,    27,   789,   565,    30,
     567,  1926,   940,  1553,  1018,  1901,  1474,    37,   636,    38,
    1825,    40,  1671,  1829,   761,  2029,  1800,  1812,  1815,  1831,
     973,  1827,  1945,  1943,  1953,   556,   683,  2092,  1417,   557,
     558,   559,   560,    59,  1481,   387,     0,     0,  1001,     0,
       0,     0,   561,     0,    70,  1715,  1487,     0,     0,   562,
       0,   563,     0,   566,     0,     0,  1020,     0,     0,     0,
    2078,     0,     0,     0,     0,     0,    85,     0,     0,     0,
       0,     0,     0,     0,     0,     0,  1056,  1057,   564,    93,
       0,  1063,     0,     0,     0,     0,     0,     0,     0,     0,
       0,  1921,     0,     0,     0,     0,   101,     0,  1513,   956,
       0,     0,   103,     0,   567,   565,     0,     0,     0,  1020,
     107,     0,   109,  1525,   111,     0,   113,     0,  1530,     0,
       0,  1681,     0,   118,     0,     0,   556,     0,  1282,     0,
     557,   558,   559,   560,  1540,     0,  1543,     0,   733,     0,
     129,   130,   387,   561,  1110,   733,  1113,     0,     0,  1282,
     562,     0,   563,  2116,     0,     0,     0,   141,     0,     0,
     566,  1556,     0,     0,   553,   696,   696,     0,     0,     0,
       0,     0,  1566,     0,     0,     0,  2126,     0,   153,   564,
       0,   154,     0,     0,     0,  1580,     0,   584,  2146,  2146,
    1586,   585,   772,   772,     0,     0,     0,   586,   587,   588,
       0,     0,     0,     0,     0,     0,   565,     0,     0,   561,
       0,   567,     0,     0,     0,     0,     0,  1019,   563,  1019,
    1852,     0,     0,     0,     0,   556,     0,     0,     0,   557,
     558,   559,   560,     0,  2126,     0,     0,  1224,  1224,     0,
    2189,     0,   561,   387,     0,   564,     0,     0,     0,   562,
    1682,   563,  1686,     0,     0,     0,   869,     0,     0,     0,
     870,   566,     0,   696,   696,     0,     0,   871,   872,     0,
       0,     0,   565,   873,   874,     0,   876,     0,   564,   878,
     879,   880,   881,   882,     0,     0,     0,  1145,   885,  2220,
    1274,     0,     0,     0,  1732,   768,  1233,     0,     0,  2220,
       0,  2238,  2241,     0,     0,   565,     0,     0,     0,   589,
     341,     0,   567,  1295,  2241,     0,   347,     0,     0,     0,
    1752,     0,     0,     0,     0,     0,   354,   566,  1755,   356,
       0,  1757,   359,     0,     0,     0,     0,     0,     0,   365,
       0,     0,   387,   372,     0,     0,     0,   375,     0,   696,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     566,     0,     0,     0,     0,   394,  1777,     0,  1780,   398,
     399,     0,     0,     0,     0,   404,   405,     0,   567,     0,
       0,   410,   411,     0,   412,   413,   414,   415,     0,   416,
       0,     0,  1161,     0,     0,     0,     0,   424,     0,     0,
       0,     0,   428,     0,   430,     0,     0,     0,   433,     0,
       0,   567,   437,     0,   439,     0,     0,     0,     0,     0,
       0,   445,     0,     0,     0,   449,     0,     0,     0,   452,
       0,   454,     0,     0,  1162,   887,  1020,   461,   463,     0,
    1163,   465,   466,     0,     0,     0,     0,   472,     0,   473,
       0,     0,     0,   477,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,  1164,     0,     0,     0,
       0,    13,    14,     0,    15,    16,  2121,  1145,  1145,    20,
     504,     0,   506,     0,  1842,  1843,     0,    23,  1165,     0,
    1166,     0,    27,     0,     0,    30,     0,     0,  1853,  2074,
       0,     0,     0,    37,   556,    38,     0,    40,   557,   558,
     559,   560,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   561,  1167,     0,     0,  1145,     0,  1168,   562,    59,
     563,  1169,  1444,   901,     0,  1170,  1881,   902,  1171,  2174,
      70,     0,  1481,   745,   587,   588,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   561,     0,   564,     0,     0,
    1172,     0,    85,     0,   563,     0,     0,     0,     0,     0,
    1902,     0,     0,  1173,  1904,    93,     0,     0,     0,  1174,
       0,     0,     0,  1045,   565,     0,     0,   887,     0,  1910,
    1911,   564,   101,     0,     0,     0,   696,     0,   103,     0,
       0,     0,  1580,     0,     0,     0,   107,   778,   109,     0,
     111,     0,   113,     0,     0,     0,  1937,     0,   565,   118,
    1350,   387,     0,   556,     0,     0,     0,   557,   558,   559,
     560,     0,  1947,   887,  1948,     0,   129,   130,     0,   566,
     561,     0,     0,   887,     0,     0,     0,   562,     0,   563,
       0,     0,     0,   141,     0,   589,     0,     0,     0,     0,
       0,     0,     0,  1001,     0,  1001,     0,     0,   668,     0,
       0,     0,     0,   566,   153,     0,   564,   154,     0,     0,
       0,     0,     0,     0,  1020,  1020,     0,     0,     0,   903,
     567,     0,     0,     0,     0,     0,     0,  1973,     0,   696,
       0,  1563,  1975,   565,  1063,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   754,  1987,     0,     0,
    1020,  1020,     0,  1988,   567,     0,  2004,     0,   887,     0,
     887,   887,   887,   887,     0,     0,     0,     0,     0,   696,
     387,     0,     0,     0,     0,     0,  1620,     0,     0,     0,
       0,     0,     0,     0,     0,   696,   696,   696,   566,   696,
     696,     0,     0,   696,     0,     0,     0,     0,   955,   791,
       0,   556,     0,     0,   903,   557,   558,   559,   560,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   561,   887,
       0,     0,     0,     0,     0,   562,     0,   563,  1350,     0,
    2075,   556,  1706,     0,     0,   557,   558,   559,   560,   567,
       0,   887,     0,     0,     0,     0,     0,     0,   561,  1282,
       0,   887,     0,     0,   564,   562,     0,   563,  1555,     0,
       0,   556,     0,     0,   696,   557,   558,   559,   560,     0,
       0,     0,   887,     0,     0,     0,     0,  1161,   561,     0,
       0,   565,     0,     0,   564,   562,   887,   563,     0,     0,
     887,   887,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,  1686,     0,     0,     0,     0,     0,  2098,  1975,
       0,   565,  2100,  2101,   564,     0,     0,     0,   387,  1162,
       0,  2114,     0,   696,   696,   696,     0,     0,     0,     0,
    1579,  2125,     0,   556,     0,     0,   566,   557,   558,   559,
     560,   565,     0,  2075,     0,     0,   887,     0,   387,     0,
     561,  1164,     0,     0,     0,     0,     0,   562,     0,   563,
       0,     0,  1020,     0,     0,     0,   566,     0,     0,     0,
       0,  2154,     0,  1165,     0,  1166,     0,     0,   387,     0,
       0,     0,     0,  2164,  2165,  2167,   564,   567,     0,     0,
       0,  2173,     0,     0,     0,     0,   566,     0,     0,     0,
       0,     0,   887,   887,     0,     0,     0,  1167,     0,     0,
       0,     0,  1168,   565,     0,     0,  1169,   567,     0,     0,
    1170,     0,     0,  1171,     0,   778,     0,     0,  2199,     0,
       0,     0,  1779,     0,     0,   556,     0,     0,     0,   557,
     558,   559,   560,     0,  2209,  1172,     0,   567,  2213,     0,
     387,     0,   561,     0,     0,  2217,  2209,     0,  1173,   562,
       0,   563,     0,     0,  1174,     0,     0,     0,   566,     0,
       0,     0,  1224,  1224,  1224,  1224,  1224,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   564,     0,
       0,     0,     0,     0,   887,   887,   887,   887,   887,   887,
     887,   887,   887,   887,   887,   887,   887,   887,   887,   887,
     887,   887,   887,     0,     0,   565,     0,     0,     0,   567,
       0,     0,     0,     0,  1274,     0,     0,     0,   887,     0,
       0,  1295,     0,     0,   556,   887,     0,   887,   557,   558,
     559,   560,     0,     0,     0,     0,     0,   887,     0,     0,
       0,   561,   387,     0,     0,     0,     0,  1913,   562,     0,
     563,     0,     0,     0,   754,     0,     0,     0,     0,  1180,
     566,     0,  1020,     0,     0,     0,     0,   887,   556,     0,
       0,   887,   557,   558,   559,   560,     0,   564,     0,     0,
       0,     0,  1944,     0,     0,   561,     0,     0,     0,     0,
       0,  1927,   562,     0,   563,     0,   556,     0,     0,     0,
     557,   923,   559,   560,   565,     0,   869,     0,     0,     0,
     870,   567,     0,   561,     0,     0,     0,   871,   872,     0,
     562,   564,   563,     0,   874,     0, -1118,     0,  1706, -1118,
   -1118, -1118, -1118, -1118,     0,     0,   887,     0,   885,     0,
       0,   387,     0,     0,     0,     0,     0,     0,   565,   564,
     887,     0,     0,     0,     0,     0,  1262,     0,     0,   566,
    1980,     0,     0,   696,     0,  1983,   869,     0,   887,     0,
     870,     0,     0,     0,     0,     0,   565,   871,   872,     0,
       0,     0,     0,   873,   874,   387,   876,     0,     0,   878,
     879,   880,   881,   882,     0,     0,   884,     0,   885,   886,
     869,     0,  1557,   566,   870,   556,     0,     0,     0,     0,
     567,   871,   872,   387,     0,     0,  1558,   873,   874,   875,
     876,     0,     0,   878,   879,   880,   881,   882,   883,     0,
     884,   566,   885,   886,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   567,     0,     0,     0,     0,   887,
       0,     0,     0,     0,     0,     0,   887,     0,     0,   869,
       0,     0,   887,   870,   556,     0,     0,  1020,     0,     0,
     871,   872,   567,     0,     0,     0,   873,   874,   875,   876,
     877,     0,   878,   879,   880,   881,   882,   883,   887,   884,
       0,   885,   886,     0,     0,     0,     0,     0,     0,     0,
     887,     0,   869,     0,  1092,   887,   870,   556,     0,     0,
       0,     0,     0,   871,   872,   887,     0,     0,   887,   873,
     874,   875,   876,     0,     0,   878,   879,   880,   881,   882,
     883,   887,   884,     0,   885,   886,     0,     0,     0,     0,
       0,   887,   696,   696,   696,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   887,     0,     0,     0,     0,
       0,   887,     0,     0,     0,     0,     0,     0,     0,     0,
       0,  1433,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,  1027,     0,     0,     0,  1028,   556,
    1020,   696,  1020,  1020,  1020,  1029,  1030,     0,     0,     0,
       0,  1031,  1032,  1033,  1034,     0,     0,  1035,  1036,  1037,
    1038,  1039,  1040,  1041,  1042,     0,  1043,  1044,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,  1020,  1020,     0,     0,  1020,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   869,     0,     0,
       0,   870,   556,     0,     0,     0,  1020,   887,   871,   872,
       0,   887,     0,  1197,   873,   874,   875,   876,     0,     0,
     878,   879,   880,   881,   882,   883,     0,   884,   869,   885,
     886,     0,   870,   556,     0,     0,     0,     0,     0,   871,
     872,     0,     0,     0,  1278,   873,   874,   875,   876,     0,
       0,   878,   879,   880,   881,   882,   883,   887,   884,     0,
     885,   886,     0,     0,     0,     0,     0,   869,     0,  1287,
       0,   870,   556,     0,     0,     0,     0,   887,   871,   872,
     887,     0,   887,     0,   873,   874,   875,   876,     0,     0,
     878,   879,   880,   881,   882,   883,     0,   884,     0,   885,
     886,     0,   887,     0,   869,   887,  1302,     0,   870,   556,
       0,  1045,     0,     0,     0,   871,   872,     0,     0,     0,
       0,   873,   874,   875,   876,     0,     0,   878,   879,   880,
     881,   882,   883,     0,   884,     0,   885,   886,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   869,     0,   887,   887,   870,
     556,     0,     0,     0,     0,     0,   871,   872,   887,     0,
       0,  1312,   873,   874,   875,   876,     0,     0,   878,   879,
     880,   881,   882,   883,     0,   884,     0,   885,   886,     0,
       0,     0,     0,     0,     0,     0,   887,     0,     0,     0,
       0,     0,     0,     0,   869,     0,     0,     0,   870,   556,
       0,     0,     0,     0,     0,   871,   872,   887,     0,   887,
    1315,   873,   874,   875,   876,   887,   887,   878,   879,   880,
     881,   882,   883,     0,   884,     0,   885,   886,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   869,
       0,     0,   887,   870,   556,     0,     0,     0,     0,     0,
     871,   872,   887,   887,     0,  1316,   873,   874,   875,   876,
       0,     0,   878,   879,   880,   881,   882,   883,     0,   884,
       0,   885,   886,     0,     0,     0,     0,     0,   887,     0,
     887,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   887,   887,   869,     0,     0,     0,   870,   556,
       0,     0,     0,     0,     0,   871,   872,     0,     0,   887,
    1349,   873,   874,   875,   876,     0,     0,   878,   879,   880,
     881,   882,   883,     0,   884,   869,   885,   886,     0,   870,
     556,     0,     0,     0,     0,     0,   871,   872,     0,     0,
       0,  1393,   873,   874,   875,   876,     0,     0,   878,   879,
     880,   881,   882,   883,     0,   884,     0,   885,   886,   869,
       0,     0,     0,   870,   556,     0,     0,     0,     0,     0,
     871,   872,     0,     0,     0,  1394,   873,   874,   875,   876,
     887,     0,   878,   879,   880,   881,   882,   883,     0,   884,
       0,   885,   886,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   887,     0,   887,   887,     0,     0,   869,
       0,     0,     0,   870,   556,     0,     0,     0,     0,   887,
     871,   872,     0,     0,     0,  1491,   873,   874,   875,   876,
     887,     0,   878,   879,   880,   881,   882,   883,     0,   884,
       0,   885,   886,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   887,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   887,
     887,     0,   887,     0,     0,     0,     0,     0,   887,     0,
       0,     0,     0,     0,     0,     0,     0,   343,   344,   345,
     346,     0,   348,     0,   349,     0,   351,   352,     0,   353,
       0,     0,     0,   355,   887,   357,   358,     0,     0,   360,
     361,   362,     0,   364,   887,   366,   367,     0,   887,   374,
       0,     0,   887,     0,   377,   378,   379,   380,   381,   382,
     383,   384,     0,   385,   386,     0,     0,   391,   392,   393,
       0,   395,     0,   397,     0,     0,   400,   401,     0,     0,
       0,     0,     0,     0,   408,   409,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   417,     0,     0,   419,   420,
       0,     0,     0,   425,   426,   427,     0,     0,   429,     0,
       0,   431,   432,     0,   434,   435,   436,     0,     0,     0,
       0,   441,   442,     0,     0,   444,     0,   446,   447,   448,
       0,   450,   451,     0,     0,   453,     0,   455,   456,   457,
     458,   459,     0,     0,     0,     0,     0,     0,   467,   468,
     469,     0,     0,     0,     0,   474,   475,   476,     0,   478,
     479,   480,   481,   482,   483,   484,   485,   486,   487,   488,
     489,   490,   491,   492,   493,   494,   495,   496,   497,   498,
     499,   500,   501,   502,   503,     0,   505,     0,   507,     0,
     508,     0,   509,   510,   511,     0,   869,     0,     0,     0,
     870,   556,     0,     0,     0,     0,     0,   871,   872,     0,
       0,     9,  1726,   873,   874,   875,   876,     0,    10,   878,
     879,   880,   881,   882,   883,     0,   884,     0,   885,   886,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   583,     0,     0,   596,     0,   599,    11,    12,    13,
      14,     0,    15,    16,    17,    18,    19,    20,     0,     0,
      21,    22,     0,     0,     0,    23,    24,    25,     0,    26,
      27,    28,    29,    30,    31,     0,    32,    33,    34,    35,
      36,    37,     0,    38,    39,    40,    41,    42,    43,     0,
      44,    45,    46,    47,    48,     0,     0,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,     0,
      71,     0,    72,    73,     0,    74,    75,    76,     0,     0,
      77,     0,     0,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,     0,     0,
       0,     0,     0,    93,    94,    95,     0,     0,     0,     0,
      96,     0,     0,    97,    98,     0,     0,    99,   100,     0,
     101,     0,     0,     0,   102,     0,   103,     0,   104,     0,
       0,     0,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,     0,   115,   116,   117,     0,   118,     0,   119,
     120,     0,   121,     0,   122,   123,   124,   125,     0,     0,
     126,   127,   128,     0,   129,   130,   131,     0,   132,   133,
       0,   134,     0,   135,   136,   137,   138,   139,     0,   140,
       0,   141,   142,     0,     0,   143,   144,   145,     0,     0,
     146,   147,     0,   148,   149,     0,   150,   151,     0,     0,
       0,   152,   153,     0,     0,   154,     0,     0,   155,     0,
       0,     0,   156,   157,     0,     0,   158,   159,   160,     0,
     161,   162,   163,   164,   165,   166,   167,   168,   169,   170,
     171,     0,   172,     0,     0,   173,     0,     0,     0,   174,
     175,   176,   177,   178,     0,   179,   180,     0,     0,   181,
     182,   183,   184,     0,     0,     0,     0,   185,   186,   187,
     188,   189,   190,   869,     0,  1557,     0,   870,   556,     0,
       0,   191,     0,     0,   871,   872,     0,     0,     0,     0,
     873,   874,   875,   876,     0,     0,   878,   879,   880,   881,
     882,   883,     0,   884,   869,   885,   886,     0,   870,   556,
     859,   860,     0,     0,     0,   871,   872,     0,     0,     0,
    1762,   873,   874,   875,   876,     0,     0,   878,   879,   880,
     881,   882,   883,     0,   884,     0,   885,   886,   869,     0,
    1778,     0,   870,   556,     0,     0,     0,     0,     0,   871,
     872,     0,     0,     0,     0,   873,   874,   875,   876,     0,
       0,   878,   879,   880,   881,   882,   883,     0,   884,   869,
     885,   886,     0,   870,   556,     0,     0,     0,     0,     0,
     871,   872,     0,     0,     0,  1794,   873,   874,   875,   876,
       0,     0,   878,   879,   880,   881,   882,   883,     0,   884,
     869,   885,   886,     0,   870,   556,     0,     0,     0,     0,
       0,   871,   872,     0,     0,     0,  1850,   873,   874,   875,
     876,     0,     0,   878,   879,   880,   881,   882,   883,     0,
     884,     0,   885,   886,   869,     0,  1912,     0,   870,   556,
       0,     0,     0,     0,     0,   871,   872,     0,     0,     0,
       0,   873,   874,   875,   876,     0,     0,   878,   879,   880,
     881,   882,   883,     0,   884,   869,   885,   886,     0,   870,
     556,     0,     0,     0,     0,     0,   871,   872,     0,     0,
       0,  1972,   873,   874,   875,   876,     0,     0,   878,   879,
     880,   881,   882,   883,     0,   884,   869,   885,   886,  1984,
     870,   556,     0,     0,     0,     0,     0,   871,   872,     0,
       0,     0,     0,   873,   874,   875,   876,     0,     0,   878,
     879,   880,   881,   882,   883,     0,   884,   869,   885,   886,
       0,   870,   556,     0,     0,     0,     0,     0,   871,   872,
       0,     0,     0,  1985,   873,   874,   875,   876,     0,     0,
     878,   879,   880,   881,   882,   883,     0,   884,   869,   885,
     886,     0,   870,   556,     0,     0,     0,     0,     0,   871,
     872,     0,     0,     0,  2057,   873,   874,   875,   876,     0,
       0,   878,   879,   880,   881,   882,   883,     0,   884,   869,
     885,   886,     0,   870,   556,     0,     0,     0,     0,     0,
     871,   872,     0,     0,     0,  2058,   873,   874,   875,   876,
       0,     0,   878,   879,   880,   881,   882,   883,     0,   884,
     869,   885,   886,  2067,   870,   556,     0,     0,     0,     0,
       0,   871,   872,     0,     0,     0,     0,   873,   874,   875,
     876,     0,     0,   878,   879,   880,   881,   882,   883,     0,
     884,   869,   885,   886,     0,   870,   556,     0,     0,     0,
       0,     0,   871,   872,     0,     0,     0,  2073,   873,   874,
     875,   876,     0,     0,   878,   879,   880,   881,   882,   883,
       0,   884,   869,   885,   886,  2080,   870,   556,     0,     0,
       0,     0,     0,   871,   872,     0,     0,     0,     0,   873,
     874,   875,   876,     0,     0,   878,   879,   880,   881,   882,
     883,     0,   884,   869,   885,   886,  2130,   870,   556,     0,
       0,     0,     0,     0,   871,   872,     0,     0,     0,     0,
     873,   874,   875,   876,     0,     0,   878,   879,   880,   881,
     882,   883,     0,   884,   869,   885,   886,     0,   870,   556,
       0,     0,     0,     0,     0,   871,   872,     0,     0,     0,
    2131,   873,   874,   875,   876,     0,     0,   878,   879,   880,
     881,   882,   883,     0,   884,   869,   885,   886,  2144,   870,
     556,     0,     0,     0,     0,     0,   871,   872,     0,     0,
       0,     0,   873,   874,   875,   876,     0,     0,   878,   879,
     880,   881,   882,   883,     0,   884,   869,   885,   886,     0,
     870,   556,     0,     0,     0,     0,     0,   871,   872,     0,
       0,     0,  2151,   873,   874,   875,   876,     0,     0,   878,
     879,   880,   881,   882,   883,     0,   884,   869,   885,   886,
       0,   870,   556,     0,     0,     0,     0,     0,   871,   872,
       0,     0,     0,  2186,   873,   874,   875,   876,     0,     0,
     878,   879,   880,   881,   882,   883,     0,   884,   869,   885,
     886,  2187,   870,   556,     0,     0,     0,     0,     0,   871,
     872,     0,     0,     0,     0,   873,   874,   875,   876,     0,
       0,   878,   879,   880,   881,   882,   883,     0,   884,   869,
     885,   886,     0,   870,   556,     0,     0,     0,     0,     0,
     871,   872,     0,     0,     0,  2190,   873,   874,   875,   876,
       0,     0,   878,   879,   880,   881,   882,   883,     0,   884,
     869,   885,   886,  2207,   870,   556,     0,     0,     0,     0,
       0,   871,   872,     0,     0,     0,     0,   873,   874,   875,
     876,     0,     0,   878,   879,   880,   881,   882,   883,     0,
     884,     0,   885,   886,   869,     0,  2214,     0,   870,   556,
       0,     0,     0,     0,     0,   871,   872,     0,     0,     0,
       0,   873,   874,   875,   876,     0,     0,   878,   879,   880,
     881,   882,   883,     0,   884,   869,   885,   886,     0,   870,
     556,     0,     0,     0,     0,     0,   871,   872,     0,     0,
       0,     0,   873,   874,   875,   876,     0,     0,   878,   879,
     880,   881,   882,   883,     0,   884,     0,   885,   886
};

static const yytype_int16 yycheck[] =
{
      50,   450,   456,   457,   458,   459,   344,   711,   353,   447,
     358,   341,  1052,  1218,   579,   504,   698,   367,   945,   583,
     215,   216,  1339,  1317,    62,    63,   221,   377,   605,   379,
    1515,    69,   193,  1371,   384,   385,   386,   396,   899,  1189,
    1470,   968,   392,   436,  1338,   395,  1189,   397,   240,   821,
    1446,  1446,   244,  1796,  1375,   408,   604,   605,  1457,    97,
      63,   987,  1103,   476,   444,   691,   558,   117,   893,   399,
    1404,   121,   892,   893,   789,   789,  1251,     0,     1,   170,
    1548,  1549,  1550,   354,  1707,   415,     5,  1402,     5,  1404,
      40,  1406,   373,   164,   432,    42,  1404,  1404,   428,    70,
     594,    15,  1404,   433,  1404,   114,  1404,   437,    15,   439,
     923,   127,   604,   605,  1712,  1713,  1404,   195,   622,  1402,
      70,  1404,   452,  1406,     8,  1408,     7,     7,    15,     8,
      15,  1069,   203,   463,    81,   465,    20,   177,    85,    86,
      57,     8,   472,    21,   127,    92,   144,   225,     7,    76,
       8,   167,     8,    20,  1466,  1081,   372,     8,    46,  1471,
       5,   127,    20,    62,    20,    15,  2146,     5,    67,     7,
     208,    98,     3,     8,    15,   114,    21,   175,   161,   145,
     431,     8,   132,   596,     8,    20,   599,    21,   115,    88,
      89,     8,     5,   586,     8,    26,    20,   590,    37,     8,
    1685,   482,    15,   484,   597,   276,    15,   124,   127,  2189,
     491,   492,   493,   127,   164,   142,   497,   498,   499,   500,
     127,   502,   503,   146,   315,  1402,     8,  1404,   444,  1406,
     114,   145,   203,   114,   114,   186,   186,   582,  1861,   127,
     127,   140,   251,   160,   436,     8,  1184,   114,    15,   127,
     149,   755,   746,   203,   204,   114,   114,  1402,   114,  1404,
     145,  1406,   711,  1408,   127,   215,   537,   217,   218,   309,
     220,     5,   312,   223,    37,   222,   127,  1875,  1876,   114,
     531,  1711,   127,     8,   172,  1325,   127,   114,   127,   127,
     114,   641,   672,   127,   639,   127,   195,   114,    15,  1504,
     114,   339,   340,    37,  1447,  1448,   253,   345,   346,   347,
     348,  1710,   350,   351,  1489,   764,   354,  1651,   356,   357,
     358,   359,    63,  1638,   362,   276,   276,   365,   251,   367,
     894,   744,   114,   746,   372,   252,  1651,   375,    92,   377,
     699,   379,  1657,  1651,  1651,   383,   384,   385,   386,  1651,
     297,  1651,   745,  1651,   392,  1638,   394,   395,   354,   397,
     127,  1181,  1223,  1651,   314,   403,   404,   251,  1651,   248,
     251,   251,   410,   411,  1657,   923,   414,   757,  1661,    15,
     383,   734,   420,  1009,   251,   423,  1101,  1091,  1102,   114,
     240,     3,   251,   251,   586,   251,  1708,   435,   590,  1440,
     438,  1442,     8,   453,     8,   597,   444,   445,   446,   447,
     127,   449,   450,   907,    26,   909,   251,   455,   456,   457,
     458,   459,   916,   461,   251,   784,   464,   251,   466,   467,
    1898,   923,   470,   628,   251,   473,  1001,   251,  1299,   477,
     186,  2184,  1872,  1256,  1257,  1258,  1372,    63,    15,    15,
       8,  1789,    90,    15,    16,    17,     8,   507,  1854,  1854,
       8,  1638,    15,    15,  1863,     7,     3,     5,   506,   251,
     224,     9,    15,    15,  1651,   525,  1797,   758,   164,   517,
    1657,    18,   520,   521,   522,   523,     8,     8,     8,    37,
     528,   529,     8,  1638,   532,   533,   534,   535,   536,    37,
      23,   240,   540,   541,     8,   244,  1651,   145,   114,   240,
     114,   127,  1657,   244,    37,     8,  1661,   910,   911,   912,
     913,   914,   143,   144,   278,   645,   251,   523,     8,   145,
       5,     8,   251,  1963,     9,   289,   282,   533,   534,   535,
     536,   661,  1747,   888,    37,     8,   707,   293,   114,     3,
     126,    15,  1702,   745,   175,     8,   114,  1707,   196,  1702,
     127,   127,    37,   126,   614,   127,     8,    21,   606,   607,
     608,   609,   610,   611,   612,   127,   152,   615,     8,  1073,
    1441,    23,   632,   145,   127,   623,   260,   514,   515,   152,
     276,     8,   114,   114,   114,    37,    16,    17,   114,   526,
       8,     7,     8,   641,   127,   800,   644,   281,   646,   804,
    2095,     8,    23,    21,   809,   960,     8,   812,   813,  1196,
    2088,  2089,     8,     3,  1485,   970,    37,   623,     8,    21,
     668,  2231,  2232,     8,   114,  1339,  1340,   114,  1410,  1411,
    1412,  1323,  1414,  1415,  2244,   251,   186,   251,  1196,   212,
     646,   114,     8,     8,   849,   693,   851,   695,   708,     8,
     698,   114,     8,   203,     8,    21,  1527,   787,  2136,   400,
      90,   721,    21,   711,     3,    21,     8,   727,   798,  1492,
    1493,  1494,   720,     8,   114,   251,    15,    93,     8,    21,
     728,  2012,   730,   251,     8,   815,   427,   735,   979,   737,
       3,    21,   740,   434,  1196,   436,  1198,   988,  1256,  1257,
    1258,  1861,    15,   833,  2008,   753,   122,     8,   910,   911,
     912,   913,   914,     8,   844,   145,   764,     8,     7,   251,
     251,   251,   770,     8,    87,   251,   276,    16,    17,   279,
      21,   791,     8,     8,   740,   783,    21,     5,   786,  1243,
    1244,  1245,  1246,  1247,   792,    21,  1961,    15,    16,    17,
       8,  1318,     8,  1320,  1256,  1257,  1258,   120,     8,    27,
       8,   251,   822,    21,   251,    21,   196,   242,    36,   244,
     245,    21,     3,    21,   190,   823,   824,   783,   251,     8,
     786,   197,   143,   144,    15,  1154,     5,   150,   251,  1080,
       9,   839,    21,     3,     8,    63,     8,    16,    17,   859,
       8,   931,   932,     8,    23,    15,  2137,   937,  2135,    21,
     858,   251,  1749,    21,   175,     8,    21,   180,    37,  1756,
    1111,     7,    90,     3,     8,    15,  1117,  1182,    21,     8,
    1121,     8,  1123,     8,    83,    15,  1127,    21,  1129,     7,
       8,  1891,    21,     3,    21,   586,    21,     8,   978,   590,
     213,  2182,  2179,    62,   186,    15,   597,   189,    67,   127,
    1731,   602,  1697,  1698,  1699,  1700,  1701,  1697,  1698,  1699,
    1700,  1701,    15,  1587,     8,  1230,  1568,   145,  1749,    88,
      89,  1236,   942,   246,  1232,  1756,     3,  1758,   825,     8,
      18,     8,     8,   941,    15,    16,    17,     5,    26,   947,
       5,     9,    21,   840,     9,   953,    27,  1112,    16,    17,
    1484,    16,    17,  1118,     8,    36,   235,  1122,   966,  1124,
    1125,  1126,   186,  1128,     8,  1130,     8,    21,   196,    37,
    1278,   140,    37,     8,  1492,  1493,  1494,   186,     8,   203,
     149,   989,    63,    83,  1292,     8,   994,   308,   309,     8,
    1305,   312,   313,     5,   203,     8,  1316,  1017,    21,    15,
       8,     8,  1010,     8,    16,    17,    15,    16,    17,    90,
      22,     8,    24,    21,    21,     8,    21,  1025,    27,     7,
       8,    33,     8,    35,    21,    37,   195,    36,    21,     5,
    1492,  1493,  1494,     9,     7,    21,   310,   311,     8,  1059,
      16,    17,    16,    17,   745,    21,   127,    23,  1348,    58,
     259,    21,   276,     8,    63,   279,   225,    15,  2068,   145,
     284,    37,  1313,     8,   145,     8,    21,   276,   292,   238,
     279,    15,  1162,    15,   283,   284,    21,    15,    21,     8,
    1100,    90,     8,   292,   293,     8,   186,   145,     8,   298,
    1180,  1391,    21,     8,     7,    21,     8,     8,    21,     8,
      22,    21,    24,   203,     8,    22,    21,    24,     8,    21,
      21,    33,    21,    35,     8,   196,    33,    21,    35,   820,
     821,    21,     8,  1143,     8,  1488,     8,    21,  1293,    16,
      17,  1877,  1878,  1879,    21,    21,   145,    21,   417,    21,
      15,  1600,   154,  1602,  1603,   157,  1397,  1155,  1156,  1400,
       8,    15,  1403,  1984,  1405,  1614,  1407,     5,  1409,   259,
      15,     9,    15,    21,  1479,  1624,     8,  1626,    16,    17,
      15,  1179,   451,    21,    15,    23,   276,    15,  1186,   279,
      15,   460,  1190,   283,     8,  1447,  1448,   196,  1503,    37,
    1505,     8,    15,   293,  1729,     8,     8,    21,   298,  1364,
     231,   232,     8,  1518,    21,   127,     8,  1522,    21,   910,
     911,   912,   913,   914,  1534,    21,    37,   918,   919,    21,
    1186,  1311,    15,   802,   803,     8,   805,    15,     8,   241,
    2061,   153,   154,     8,   156,   157,   153,   154,    21,   156,
     157,    21,     8,    15,   945,    15,    21,     5,    15,     7,
      15,     8,    10,     8,  1262,    21,    14,    15,    16,    17,
    1492,  1493,  1494,    15,    21,   187,    21,   968,    15,    27,
     187,    15,  1736,   918,   919,    15,    34,    15,    36,   558,
     559,   560,    15,   562,  2227,  2228,   122,   299,   300,   301,
     569,   810,   811,     7,   122,     3,   233,    15,  1306,   240,
    1308,   240,    62,  1004,   583,    63,   228,    67,  1316,     5,
    1318,   228,  1320,     9,    15,  1323,    21,    26,     8,   241,
      16,    17,     7,    15,   241,    21,  1488,    23,    88,    89,
       8,   234,    90,    15,    15,    15,     8,   170,    15,    15,
    1360,    37,   145,     8,    15,  1353,   625,     8,     8,    15,
    1370,     8,     8,    26,  1362,     7,    15,    61,    21,  1610,
    1368,   640,   122,     8,  1615,    15,  1386,     8,  1388,   127,
    1378,   281,    15,     8,   208,     8,    15,  1628,     8,    63,
     140,     9,     9,    21,  1392,   127,     8,   145,   667,   149,
       8,     8,    15,  1413,    15,   674,    66,    37,     8,  1650,
      15,   680,   681,    15,  1424,  1656,    15,    15,     8,  1660,
      26,  1501,   127,     7,  1434,     7,  1436,    15,    21,    21,
      21,  1429,    26,    37,     7,    15,     7,    15,     8,     8,
       8,    15,    21,    15,    21,   195,    21,     8,   196,    15,
      15,    15,  1750,    15,  1979,   724,    15,     5,    21,  1457,
      15,    15,     8,    21,  1754,    61,     7,   249,     8,     8,
       8,  2135,  1470,     8,  2138,  2139,     7,   227,    15,    15,
     161,  1561,   233,    16,   203,    15,    15,    21,   238,  1797,
      15,  1489,     8,    21,  1492,  1493,  1494,     7,    15,     7,
       5,    15,   240,     5,  1502,    10,    15,   240,     8,    14,
      15,    16,    17,   240,  2178,  2179,   240,    15,   240,     8,
      21,     5,    27,     8,    21,     9,   795,   796,     7,    34,
      21,    36,    16,    17,  1932,    15,  1534,  2201,    22,    23,
      24,    25,   168,    26,    28,    29,    30,    31,    32,    33,
      15,    35,   249,    37,    38,     8,  1554,  1862,    63,     7,
    1251,    15,   203,    15,    70,     8,  1807,    18,    15,    15,
    1568,  1812,    21,    15,  1815,    15,    15,    21,    15,     5,
      21,     8,    15,    21,  1825,    90,  1827,    21,  1829,    21,
    1831,    21,    21,     7,     9,    20,  1594,  1595,    21,    21,
     869,   870,   871,   872,   873,   874,   875,   876,   877,   878,
     879,   880,   881,   882,   883,   884,   885,   886,   887,    26,
      21,  1619,   127,    21,     5,   894,    21,    15,     9,    10,
     899,    21,    21,    14,    15,    16,    17,   906,    15,   908,
     145,    26,    26,    18,     8,     7,    27,    15,     5,    21,
      26,   248,  1957,    34,   923,    36,   161,     8,    15,    16,
      17,    15,     8,  1354,     8,   400,    21,    15,    21,    21,
      27,     7,     7,     7,    21,     7,    37,    15,    21,    36,
     949,    21,    63,  1981,    15,    15,   955,    15,     7,     7,
       7,   196,   427,    15,    21,    21,  2110,  2111,    77,   434,
      15,    15,   971,  1394,    15,    15,    63,   442,    15,    90,
      15,    15,  1710,  1711,  1712,  1713,     8,     8,    21,  1410,
    1411,  1412,    21,  1414,  1415,  1723,  2135,  1418,  2137,     5,
       7,    15,     7,    90,    10,    21,     7,     7,    14,    15,
      16,    17,    15,    21,     7,     7,   127,    15,    15,     7,
     206,    27,   126,  1156,  1764,  1753,   824,   832,    34,  1786,
      36,  1679,  1434,  1104,   145,  1013,   843,  1438,  2061,  1188,
     127,  1680,   539,   542,   659,   858,  1466,  1963,  1874,   858,
    1710,  1702,  1854,  1052,  1378,  1076,  1784,    63,   145,  1569,
    1788,  1916,  1790,  1784,  1362,  1934,  1804,  1488,  1489,  1797,
    1810,   951,  1071,   538,   971,  1803,  1804,  1302,  1280,  1819,
    1011,  1545,  1547,  2233,    90,   196,    75,  1731,   427,  1756,
    1749,   660,   230,  1092,  1093,   230,  1288,  1837,   230,  1323,
     230,  1485,   738,     5,   960,     7,  1488,   923,    10,   196,
     994,  1025,    14,    15,    16,    17,   782,  1093,   609,   608,
    2009,   127,   610,  1304,  1308,    27,  1788,  1548,  1549,  1550,
    2068,   753,    34,  2152,    36,  1863,   939,   602,   611,   145,
     966,   941,  1010,  1753,  1872,  1359,  1874,  1875,  1876,  1877,
    1878,  1879,     5,  2102,  1992,   161,     9,  2134,   953,  2201,
    2215,    63,  2133,    16,    17,    44,    45,  2229,    47,    48,
      23,  2231,    25,    52,  2026,    28,    29,    30,    31,    32,
    2119,    60,  2139,  1587,    37,  1376,    65,   467,    90,    68,
     196,  1791,   615,  1340,   709,  1758,  1195,    76,   348,    78,
    1638,    80,  1424,  1657,  1932,  1933,  1934,  1613,  1618,  1661,
       5,  1651,  1817,  1816,  1837,    10,   381,  2052,  1143,    14,
      15,    16,    17,   102,  1223,   127,    -1,    -1,   693,    -1,
      -1,    -1,    27,    -1,   113,  1963,  1235,    -1,    -1,    34,
      -1,    36,    -1,   145,    -1,    -1,   711,    -1,    -1,    -1,
    1990,    -1,    -1,    -1,    -1,    -1,   135,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   731,   732,    63,   148,
      -1,   736,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,  2009,    -1,    -1,    -1,    -1,   165,    -1,  1287,  1288,
      -1,    -1,   171,    -1,   196,    90,    -1,    -1,    -1,   764,
     179,    -1,   181,  1302,   183,    -1,   185,    -1,  1307,    -1,
      -1,     5,    -1,   192,    -1,    -1,    10,    -1,  1317,    -1,
      14,    15,    16,    17,  1323,    -1,  1325,    -1,  1749,    -1,
     209,   210,   127,    27,   799,  1756,   801,    -1,    -1,  1338,
      34,    -1,    36,  2083,    -1,    -1,    -1,   226,    -1,    -1,
     145,  1350,    -1,    -1,   233,   820,   821,    -1,    -1,    -1,
      -1,    -1,  1361,    -1,    -1,    -1,  2094,    -1,   247,    63,
      -1,   250,    -1,    -1,    -1,  1374,    -1,     5,  2118,  2119,
    1379,     9,  2110,  2111,    -1,    -1,    -1,    15,    16,    17,
      -1,    -1,    -1,    -1,    -1,    -1,    90,    -1,    -1,    27,
      -1,   196,    -1,    -1,    -1,    -1,    -1,  2135,    36,  2137,
       5,    -1,    -1,    -1,    -1,    10,    -1,    -1,    -1,    14,
      15,    16,    17,    -1,  2152,    -1,    -1,   892,   893,    -1,
    2170,    -1,    27,   127,    -1,    63,    -1,    -1,    -1,    34,
    1439,    36,  1441,    -1,    -1,    -1,     5,    -1,    -1,    -1,
       9,   145,    -1,   918,   919,    -1,    -1,    16,    17,    -1,
      -1,    -1,    90,    22,    23,    -1,    25,    -1,    63,    28,
      29,    30,    31,    32,    -1,    -1,    -1,  1898,    37,  2219,
     945,    -1,    -1,    -1,  1483,  1484,  1485,    -1,    -1,  2229,
      -1,  2231,  2232,    -1,    -1,    90,    -1,    -1,    -1,   127,
      12,    -1,   196,   968,  2244,    -1,    18,    -1,    -1,    -1,
    1509,    -1,    -1,    -1,    -1,    -1,    28,   145,  1517,    31,
      -1,  1520,    34,    -1,    -1,    -1,    -1,    -1,    -1,    41,
      -1,    -1,   127,    45,    -1,    -1,    -1,    49,    -1,  1004,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     145,    -1,    -1,    -1,    -1,    67,  1555,    -1,  1557,    71,
      72,    -1,    -1,    -1,    -1,    77,    78,    -1,   196,    -1,
      -1,    83,    84,    -1,    86,    87,    88,    89,    -1,    91,
      -1,    -1,    43,    -1,    -1,    -1,    -1,    99,    -1,    -1,
      -1,    -1,   104,    -1,   106,    -1,    -1,    -1,   110,    -1,
      -1,   196,   114,    -1,   116,    -1,    -1,    -1,    -1,    -1,
      -1,   123,    -1,    -1,    -1,   127,    -1,    -1,    -1,   131,
      -1,   133,    -1,    -1,    85,   570,  1091,   139,   140,    -1,
      91,   143,   144,    -1,    -1,    -1,    -1,   149,    -1,   151,
      -1,    -1,    -1,   155,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   117,    -1,    -1,    -1,
      -1,    44,    45,    -1,    47,    48,  2087,  2088,  2089,    52,
     182,    -1,   184,    -1,  1673,  1674,    -1,    60,   139,    -1,
     141,    -1,    65,    -1,    -1,    68,    -1,    -1,  1687,     5,
      -1,    -1,    -1,    76,    10,    78,    -1,    80,    14,    15,
      16,    17,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    27,   173,    -1,    -1,  2136,    -1,   178,    34,   102,
      36,   182,  1187,     5,    -1,   186,  1725,     9,   189,  2150,
     113,    -1,  1731,    15,    16,    17,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    27,    -1,    63,    -1,    -1,
     211,    -1,   135,    -1,    36,    -1,    -1,    -1,    -1,    -1,
    1759,    -1,    -1,   224,  1763,   148,    -1,    -1,    -1,   230,
      -1,    -1,    -1,   718,    90,    -1,    -1,   722,    -1,  1778,
    1779,    63,   165,    -1,    -1,    -1,  1251,    -1,   171,    -1,
      -1,    -1,  1791,    -1,    -1,    -1,   179,  1796,   181,    -1,
     183,    -1,   185,    -1,    -1,    -1,  1805,    -1,    90,   192,
       7,   127,    -1,    10,    -1,    -1,    -1,    14,    15,    16,
      17,    -1,  1821,   768,  1823,    -1,   209,   210,    -1,   145,
      27,    -1,    -1,   778,    -1,    -1,    -1,    34,    -1,    36,
      -1,    -1,    -1,   226,    -1,   127,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,  1318,    -1,  1320,    -1,    -1,   370,    -1,
      -1,    -1,    -1,   145,   247,    -1,    63,   250,    -1,    -1,
      -1,    -1,    -1,    -1,  1339,  1340,    -1,    -1,    -1,   161,
     196,    -1,    -1,    -1,    -1,    -1,    -1,  1886,    -1,  1354,
      -1,  1356,  1891,    90,  1359,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,  1371,  1906,    -1,    -1,
    1375,  1376,    -1,  1912,   196,    -1,  1915,    -1,   863,    -1,
     865,   866,   867,   868,    -1,    -1,    -1,    -1,    -1,  1394,
     127,    -1,    -1,    -1,    -1,    -1,  1401,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,  1410,  1411,  1412,   145,  1414,
    1415,    -1,    -1,  1418,    -1,    -1,    -1,    -1,     7,   471,
      -1,    10,    -1,    -1,   161,    14,    15,    16,    17,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    27,   924,
      -1,    -1,    -1,    -1,    -1,    34,    -1,    36,     7,    -1,
    1989,    10,  1457,    -1,    -1,    14,    15,    16,    17,   196,
      -1,   946,    -1,    -1,    -1,    -1,    -1,    -1,    27,  2008,
      -1,   956,    -1,    -1,    63,    34,    -1,    36,     7,    -1,
      -1,    10,    -1,    -1,  1489,    14,    15,    16,    17,    -1,
      -1,    -1,   977,    -1,    -1,    -1,    -1,    43,    27,    -1,
      -1,    90,    -1,    -1,    63,    34,   991,    36,    -1,    -1,
     995,   996,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,  2061,    -1,    -1,    -1,    -1,    -1,  2067,  2068,
      -1,    90,  2071,  2072,    63,    -1,    -1,    -1,   127,    85,
      -1,  2080,    -1,  1548,  1549,  1550,    -1,    -1,    -1,    -1,
       7,  2090,    -1,    10,    -1,    -1,   145,    14,    15,    16,
      17,    90,    -1,  2102,    -1,    -1,  1051,    -1,   127,    -1,
      27,   117,    -1,    -1,    -1,    -1,    -1,    34,    -1,    36,
      -1,    -1,  1587,    -1,    -1,    -1,   145,    -1,    -1,    -1,
      -1,  2130,    -1,   139,    -1,   141,    -1,    -1,   127,    -1,
      -1,    -1,    -1,  2142,  2143,  2144,    63,   196,    -1,    -1,
      -1,  2150,    -1,    -1,    -1,    -1,   145,    -1,    -1,    -1,
      -1,    -1,  1107,  1108,    -1,    -1,    -1,   173,    -1,    -1,
      -1,    -1,   178,    90,    -1,    -1,   182,   196,    -1,    -1,
     186,    -1,    -1,   189,    -1,  2184,    -1,    -1,  2187,    -1,
      -1,    -1,     7,    -1,    -1,    10,    -1,    -1,    -1,    14,
      15,    16,    17,    -1,  2203,   211,    -1,   196,  2207,    -1,
     127,    -1,    27,    -1,    -1,  2214,  2215,    -1,   224,    34,
      -1,    36,    -1,    -1,   230,    -1,    -1,    -1,   145,    -1,
      -1,    -1,  1697,  1698,  1699,  1700,  1701,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    63,    -1,
      -1,    -1,    -1,    -1,  1199,  1200,  1201,  1202,  1203,  1204,
    1205,  1206,  1207,  1208,  1209,  1210,  1211,  1212,  1213,  1214,
    1215,  1216,  1217,    -1,    -1,    90,    -1,    -1,    -1,   196,
      -1,    -1,    -1,    -1,  1749,    -1,    -1,    -1,  1233,    -1,
      -1,  1756,    -1,    -1,    10,  1240,    -1,  1242,    14,    15,
      16,    17,    -1,    -1,    -1,    -1,    -1,  1252,    -1,    -1,
      -1,    27,   127,    -1,    -1,    -1,    -1,  1782,    34,    -1,
      36,    -1,    -1,    -1,  1789,    -1,    -1,    -1,    -1,   841,
     145,    -1,  1797,    -1,    -1,    -1,    -1,  1282,    10,    -1,
      -1,  1286,    14,    15,    16,    17,    -1,    63,    -1,    -1,
      -1,    -1,  1817,    -1,    -1,    27,    -1,    -1,    -1,    -1,
      -1,    77,    34,    -1,    36,    -1,    10,    -1,    -1,    -1,
      14,    15,    16,    17,    90,    -1,     5,    -1,    -1,    -1,
       9,   196,    -1,    27,    -1,    -1,    -1,    16,    17,    -1,
      34,    63,    36,    -1,    23,    -1,    25,    -1,  1863,    28,
      29,    30,    31,    32,    -1,    -1,  1351,    -1,    37,    -1,
      -1,   127,    -1,    -1,    -1,    -1,    -1,    -1,    90,    63,
    1365,    -1,    -1,    -1,    -1,    -1,   938,    -1,    -1,   145,
    1895,    -1,    -1,  1898,    -1,  1900,     5,    -1,  1383,    -1,
       9,    -1,    -1,    -1,    -1,    -1,    90,    16,    17,    -1,
      -1,    -1,    -1,    22,    23,   127,    25,    -1,    -1,    28,
      29,    30,    31,    32,    -1,    -1,    35,    -1,    37,    38,
       5,    -1,     7,   145,     9,    10,    -1,    -1,    -1,    -1,
     196,    16,    17,   127,    -1,    -1,    21,    22,    23,    24,
      25,    -1,    -1,    28,    29,    30,    31,    32,    33,    -1,
      35,   145,    37,    38,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   196,    -1,    -1,    -1,    -1,  1474,
      -1,    -1,    -1,    -1,    -1,    -1,  1481,    -1,    -1,     5,
      -1,    -1,  1487,     9,    10,    -1,    -1,  2012,    -1,    -1,
      16,    17,   196,    -1,    -1,    -1,    22,    23,    24,    25,
      26,    -1,    28,    29,    30,    31,    32,    33,  1513,    35,
      -1,    37,    38,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    1525,    -1,     5,    -1,     7,  1530,     9,    10,    -1,    -1,
      -1,    -1,    -1,    16,    17,  1540,    -1,    -1,  1543,    22,
      23,    24,    25,    -1,    -1,    28,    29,    30,    31,    32,
      33,  1556,    35,    -1,    37,    38,    -1,    -1,    -1,    -1,
      -1,  1566,  2087,  2088,  2089,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,  1580,    -1,    -1,    -1,    -1,
      -1,  1586,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,  1163,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,     5,    -1,    -1,    -1,     9,    10,
    2135,  2136,  2137,  2138,  2139,    16,    17,    -1,    -1,    -1,
      -1,    22,    23,    24,    25,    -1,    -1,    28,    29,    30,
      31,    32,    33,    34,    35,    -1,    37,    38,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,  2178,  2179,    -1,    -1,  2182,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,     5,    -1,    -1,
      -1,     9,    10,    -1,    -1,    -1,  2201,  1682,    16,    17,
      -1,  1686,    -1,    21,    22,    23,    24,    25,    -1,    -1,
      28,    29,    30,    31,    32,    33,    -1,    35,     5,    37,
      38,    -1,     9,    10,    -1,    -1,    -1,    -1,    -1,    16,
      17,    -1,    -1,    -1,    21,    22,    23,    24,    25,    -1,
      -1,    28,    29,    30,    31,    32,    33,  1732,    35,    -1,
      37,    38,    -1,    -1,    -1,    -1,    -1,     5,    -1,     7,
      -1,     9,    10,    -1,    -1,    -1,    -1,  1752,    16,    17,
    1755,    -1,  1757,    -1,    22,    23,    24,    25,    -1,    -1,
      28,    29,    30,    31,    32,    33,    -1,    35,    -1,    37,
      38,    -1,  1777,    -1,     5,  1780,     7,    -1,     9,    10,
      -1,  1786,    -1,    -1,    -1,    16,    17,    -1,    -1,    -1,
      -1,    22,    23,    24,    25,    -1,    -1,    28,    29,    30,
      31,    32,    33,    -1,    35,    -1,    37,    38,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,     5,    -1,  1842,  1843,     9,
      10,    -1,    -1,    -1,    -1,    -1,    16,    17,  1853,    -1,
      -1,    21,    22,    23,    24,    25,    -1,    -1,    28,    29,
      30,    31,    32,    33,    -1,    35,    -1,    37,    38,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,  1881,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,     5,    -1,    -1,    -1,     9,    10,
      -1,    -1,    -1,    -1,    -1,    16,    17,  1902,    -1,  1904,
      21,    22,    23,    24,    25,  1910,  1911,    28,    29,    30,
      31,    32,    33,    -1,    35,    -1,    37,    38,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,     5,
      -1,    -1,  1937,     9,    10,    -1,    -1,    -1,    -1,    -1,
      16,    17,  1947,  1948,    -1,    21,    22,    23,    24,    25,
      -1,    -1,    28,    29,    30,    31,    32,    33,    -1,    35,
      -1,    37,    38,    -1,    -1,    -1,    -1,    -1,  1973,    -1,
    1975,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,  1987,  1988,     5,    -1,    -1,    -1,     9,    10,
      -1,    -1,    -1,    -1,    -1,    16,    17,    -1,    -1,  2004,
      21,    22,    23,    24,    25,    -1,    -1,    28,    29,    30,
      31,    32,    33,    -1,    35,     5,    37,    38,    -1,     9,
      10,    -1,    -1,    -1,    -1,    -1,    16,    17,    -1,    -1,
      -1,    21,    22,    23,    24,    25,    -1,    -1,    28,    29,
      30,    31,    32,    33,    -1,    35,    -1,    37,    38,     5,
      -1,    -1,    -1,     9,    10,    -1,    -1,    -1,    -1,    -1,
      16,    17,    -1,    -1,    -1,    21,    22,    23,    24,    25,
    2075,    -1,    28,    29,    30,    31,    32,    33,    -1,    35,
      -1,    37,    38,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,  2098,    -1,  2100,  2101,    -1,    -1,     5,
      -1,    -1,    -1,     9,    10,    -1,    -1,    -1,    -1,  2114,
      16,    17,    -1,    -1,    -1,    21,    22,    23,    24,    25,
    2125,    -1,    28,    29,    30,    31,    32,    33,    -1,    35,
      -1,    37,    38,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  2154,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  2164,
    2165,    -1,  2167,    -1,    -1,    -1,    -1,    -1,  2173,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    14,    15,    16,
      17,    -1,    19,    -1,    21,    -1,    23,    24,    -1,    26,
      -1,    -1,    -1,    30,  2199,    32,    33,    -1,    -1,    36,
      37,    38,    -1,    40,  2209,    42,    43,    -1,  2213,    46,
      -1,    -1,  2217,    -1,    51,    52,    53,    54,    55,    56,
      57,    58,    -1,    60,    61,    -1,    -1,    64,    65,    66,
      -1,    68,    -1,    70,    -1,    -1,    73,    74,    -1,    -1,
      -1,    -1,    -1,    -1,    81,    82,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    92,    -1,    -1,    95,    96,
      -1,    -1,    -1,   100,   101,   102,    -1,    -1,   105,    -1,
      -1,   108,   109,    -1,   111,   112,   113,    -1,    -1,    -1,
      -1,   118,   119,    -1,    -1,   122,    -1,   124,   125,   126,
      -1,   128,   129,    -1,    -1,   132,    -1,   134,   135,   136,
     137,   138,    -1,    -1,    -1,    -1,    -1,    -1,   145,   146,
     147,    -1,    -1,    -1,    -1,   152,   153,   154,    -1,   156,
     157,   158,   159,   160,   161,   162,   163,   164,   165,   166,
     167,   168,   169,   170,   171,   172,   173,   174,   175,   176,
     177,   178,   179,   180,   181,    -1,   183,    -1,   185,    -1,
     187,    -1,   189,   190,   191,    -1,     5,    -1,    -1,    -1,
       9,    10,    -1,    -1,    -1,    -1,    -1,    16,    17,    -1,
      -1,     6,    21,    22,    23,    24,    25,    -1,    13,    28,
      29,    30,    31,    32,    33,    -1,    35,    -1,    37,    38,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   238,    -1,    -1,   241,    -1,   243,    42,    43,    44,
      45,    -1,    47,    48,    49,    50,    51,    52,    -1,    -1,
      55,    56,    -1,    -1,    -1,    60,    61,    62,    -1,    64,
      65,    66,    67,    68,    69,    -1,    71,    72,    73,    74,
      75,    76,    -1,    78,    79,    80,    81,    82,    83,    -1,
      85,    86,    87,    88,    89,    -1,    -1,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,    -1,
     115,    -1,   117,   118,    -1,   120,   121,   122,    -1,    -1,
     125,    -1,    -1,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,   141,   142,    -1,    -1,
      -1,    -1,    -1,   148,   149,   150,    -1,    -1,    -1,    -1,
     155,    -1,    -1,   158,   159,    -1,    -1,   162,   163,    -1,
     165,    -1,    -1,    -1,   169,    -1,   171,    -1,   173,    -1,
      -1,    -1,   177,   178,   179,   180,   181,   182,   183,   184,
     185,   186,    -1,   188,   189,   190,    -1,   192,    -1,   194,
     195,    -1,   197,    -1,   199,   200,   201,   202,    -1,    -1,
     205,   206,   207,    -1,   209,   210,   211,    -1,   213,   214,
      -1,   216,    -1,   218,   219,   220,   221,   222,    -1,   224,
      -1,   226,   227,    -1,    -1,   230,   231,   232,    -1,    -1,
     235,   236,    -1,   238,   239,    -1,   241,   242,    -1,    -1,
      -1,   246,   247,    -1,    -1,   250,    -1,    -1,   253,    -1,
      -1,    -1,   257,   258,    -1,    -1,   261,   262,   263,    -1,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,    -1,   277,    -1,    -1,   280,    -1,    -1,    -1,   284,
     285,   286,   287,   288,    -1,   290,   291,    -1,    -1,   294,
     295,   296,   297,    -1,    -1,    -1,    -1,   302,   303,   304,
     305,   306,   307,     5,    -1,     7,    -1,     9,    10,    -1,
      -1,   316,    -1,    -1,    16,    17,    -1,    -1,    -1,    -1,
      22,    23,    24,    25,    -1,    -1,    28,    29,    30,    31,
      32,    33,    -1,    35,     5,    37,    38,    -1,     9,    10,
     547,   548,    -1,    -1,    -1,    16,    17,    -1,    -1,    -1,
      21,    22,    23,    24,    25,    -1,    -1,    28,    29,    30,
      31,    32,    33,    -1,    35,    -1,    37,    38,     5,    -1,
       7,    -1,     9,    10,    -1,    -1,    -1,    -1,    -1,    16,
      17,    -1,    -1,    -1,    -1,    22,    23,    24,    25,    -1,
      -1,    28,    29,    30,    31,    32,    33,    -1,    35,     5,
      37,    38,    -1,     9,    10,    -1,    -1,    -1,    -1,    -1,
      16,    17,    -1,    -1,    -1,    21,    22,    23,    24,    25,
      -1,    -1,    28,    29,    30,    31,    32,    33,    -1,    35,
       5,    37,    38,    -1,     9,    10,    -1,    -1,    -1,    -1,
      -1,    16,    17,    -1,    -1,    -1,    21,    22,    23,    24,
      25,    -1,    -1,    28,    29,    30,    31,    32,    33,    -1,
      35,    -1,    37,    38,     5,    -1,     7,    -1,     9,    10,
      -1,    -1,    -1,    -1,    -1,    16,    17,    -1,    -1,    -1,
      -1,    22,    23,    24,    25,    -1,    -1,    28,    29,    30,
      31,    32,    33,    -1,    35,     5,    37,    38,    -1,     9,
      10,    -1,    -1,    -1,    -1,    -1,    16,    17,    -1,    -1,
      -1,    21,    22,    23,    24,    25,    -1,    -1,    28,    29,
      30,    31,    32,    33,    -1,    35,     5,    37,    38,     8,
       9,    10,    -1,    -1,    -1,    -1,    -1,    16,    17,    -1,
      -1,    -1,    -1,    22,    23,    24,    25,    -1,    -1,    28,
      29,    30,    31,    32,    33,    -1,    35,     5,    37,    38,
      -1,     9,    10,    -1,    -1,    -1,    -1,    -1,    16,    17,
      -1,    -1,    -1,    21,    22,    23,    24,    25,    -1,    -1,
      28,    29,    30,    31,    32,    33,    -1,    35,     5,    37,
      38,    -1,     9,    10,    -1,    -1,    -1,    -1,    -1,    16,
      17,    -1,    -1,    -1,    21,    22,    23,    24,    25,    -1,
      -1,    28,    29,    30,    31,    32,    33,    -1,    35,     5,
      37,    38,    -1,     9,    10,    -1,    -1,    -1,    -1,    -1,
      16,    17,    -1,    -1,    -1,    21,    22,    23,    24,    25,
      -1,    -1,    28,    29,    30,    31,    32,    33,    -1,    35,
       5,    37,    38,     8,     9,    10,    -1,    -1,    -1,    -1,
      -1,    16,    17,    -1,    -1,    -1,    -1,    22,    23,    24,
      25,    -1,    -1,    28,    29,    30,    31,    32,    33,    -1,
      35,     5,    37,    38,    -1,     9,    10,    -1,    -1,    -1,
      -1,    -1,    16,    17,    -1,    -1,    -1,    21,    22,    23,
      24,    25,    -1,    -1,    28,    29,    30,    31,    32,    33,
      -1,    35,     5,    37,    38,     8,     9,    10,    -1,    -1,
      -1,    -1,    -1,    16,    17,    -1,    -1,    -1,    -1,    22,
      23,    24,    25,    -1,    -1,    28,    29,    30,    31,    32,
      33,    -1,    35,     5,    37,    38,     8,     9,    10,    -1,
      -1,    -1,    -1,    -1,    16,    17,    -1,    -1,    -1,    -1,
      22,    23,    24,    25,    -1,    -1,    28,    29,    30,    31,
      32,    33,    -1,    35,     5,    37,    38,    -1,     9,    10,
      -1,    -1,    -1,    -1,    -1,    16,    17,    -1,    -1,    -1,
      21,    22,    23,    24,    25,    -1,    -1,    28,    29,    30,
      31,    32,    33,    -1,    35,     5,    37,    38,     8,     9,
      10,    -1,    -1,    -1,    -1,    -1,    16,    17,    -1,    -1,
      -1,    -1,    22,    23,    24,    25,    -1,    -1,    28,    29,
      30,    31,    32,    33,    -1,    35,     5,    37,    38,    -1,
       9,    10,    -1,    -1,    -1,    -1,    -1,    16,    17,    -1,
      -1,    -1,    21,    22,    23,    24,    25,    -1,    -1,    28,
      29,    30,    31,    32,    33,    -1,    35,     5,    37,    38,
      -1,     9,    10,    -1,    -1,    -1,    -1,    -1,    16,    17,
      -1,    -1,    -1,    21,    22,    23,    24,    25,    -1,    -1,
      28,    29,    30,    31,    32,    33,    -1,    35,     5,    37,
      38,     8,     9,    10,    -1,    -1,    -1,    -1,    -1,    16,
      17,    -1,    -1,    -1,    -1,    22,    23,    24,    25,    -1,
      -1,    28,    29,    30,    31,    32,    33,    -1,    35,     5,
      37,    38,    -1,     9,    10,    -1,    -1,    -1,    -1,    -1,
      16,    17,    -1,    -1,    -1,    21,    22,    23,    24,    25,
      -1,    -1,    28,    29,    30,    31,    32,    33,    -1,    35,
       5,    37,    38,     8,     9,    10,    -1,    -1,    -1,    -1,
      -1,    16,    17,    -1,    -1,    -1,    -1,    22,    23,    24,
      25,    -1,    -1,    28,    29,    30,    31,    32,    33,    -1,
      35,    -1,    37,    38,     5,    -1,     7,    -1,     9,    10,
      -1,    -1,    -1,    -1,    -1,    16,    17,    -1,    -1,    -1,
      -1,    22,    23,    24,    25,    -1,    -1,    28,    29,    30,
      31,    32,    33,    -1,    35,     5,    37,    38,    -1,     9,
      10,    -1,    -1,    -1,    -1,    -1,    16,    17,    -1,    -1,
      -1,    -1,    22,    23,    24,    25,    -1,    -1,    28,    29,
      30,    31,    32,    33,    -1,    35,    -1,    37,    38
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint16 yystos[] =
{
       0,   320,     0,     1,   146,   251,   321,   322,   114,     6,
      13,    42,    43,    44,    45,    47,    48,    49,    50,    51,
      52,    55,    56,    60,    61,    62,    64,    65,    66,    67,
      68,    69,    71,    72,    73,    74,    75,    76,    78,    79,
      80,    81,    82,    83,    85,    86,    87,    88,    89,    92,
      93,    94,    95,    96,    97,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   115,   117,   118,   120,   121,   122,   125,   128,   129,
     130,   131,   132,   133,   134,   135,   136,   137,   138,   139,
     140,   141,   142,   148,   149,   150,   155,   158,   159,   162,
     163,   165,   169,   171,   173,   177,   178,   179,   180,   181,
     182,   183,   184,   185,   186,   188,   189,   190,   192,   194,
     195,   197,   199,   200,   201,   202,   205,   206,   207,   209,
     210,   211,   213,   214,   216,   218,   219,   220,   221,   222,
     224,   226,   227,   230,   231,   232,   235,   236,   238,   239,
     241,   242,   246,   247,   250,   253,   257,   258,   261,   262,
     263,   265,   266,   267,   268,   269,   270,   271,   272,   273,
     274,   275,   277,   280,   284,   285,   286,   287,   288,   290,
     291,   294,   295,   296,   297,   302,   303,   304,   305,   306,
     307,   316,   323,   325,   328,   340,   341,   345,   346,   347,
     353,   354,   355,   356,   358,   359,   361,   363,   364,   365,
     366,   373,   374,   375,   376,   377,   378,   382,   383,   384,
     388,   389,   427,   429,   442,   485,   486,   488,   489,   495,
     496,   497,   498,   505,   506,   507,   508,   510,   513,   517,
     518,   519,   520,   521,   527,   528,   529,   540,   541,   542,
     543,   546,   549,   554,   555,   557,   559,   561,   564,   565,
     578,   579,   590,   591,   592,   593,   598,   601,   604,   607,
     608,   654,   655,   656,   657,   658,   659,   660,   661,   667,
     669,   671,   673,   675,   676,   677,   678,   679,   682,   684,
     685,   686,   689,   690,   694,   695,   697,   698,   699,   700,
     701,   702,   705,   706,   707,   708,   709,   726,   729,   730,
     731,   732,   738,   741,   746,   747,   748,   751,   752,   753,
     754,   755,   756,   757,   758,   759,   760,   761,   762,   763,
     764,   765,   770,   771,   772,   773,   774,   784,   785,    15,
     452,   452,   514,   514,   514,   514,   514,   452,   514,   514,
     324,   514,   514,   514,   452,   514,   452,   514,   514,   452,
     514,   514,   514,   451,   514,   452,   514,   514,     7,    15,
     453,    15,   452,   572,   514,   452,   337,   514,   514,   514,
     514,   514,   514,   514,   514,   514,   514,   127,   330,   494,
     494,   514,   514,   514,   452,   514,   330,   514,   452,   452,
     514,   514,   451,   324,   452,   452,    63,   336,   514,   514,
     452,   452,   452,   452,   452,   452,   452,   514,   391,   514,
     514,   330,   428,   324,   452,   514,   514,   514,   452,   514,
     452,   514,   514,   452,   514,   514,   514,   452,   324,   452,
     337,   514,   514,   337,   514,   452,   514,   514,   514,   452,
     514,   514,   452,   514,   452,   514,   514,   514,   514,   514,
      15,   452,   550,   452,   324,   452,   452,   514,   514,   514,
      15,     8,   452,   452,   514,   514,   514,   452,   514,   514,
     514,   514,   514,   514,   514,   514,   514,   514,   514,   514,
     514,   514,   514,   514,   514,   514,   514,   514,   514,   514,
     514,   514,   514,   514,   452,   514,   452,   514,   514,   514,
     514,   514,   251,   539,   122,   227,   361,    15,   333,   539,
       8,     8,     8,     8,     7,     8,   122,   325,   348,     8,
     330,   362,     8,     8,     8,     8,     8,   509,     8,   509,
       8,     8,     8,     8,   509,   539,     7,   213,   246,   486,
     488,   497,   498,   233,   506,   506,    10,    14,    15,    16,
      17,    27,    34,    36,    63,    90,   145,   196,   330,   342,
     458,   459,   461,   462,   463,   464,   470,   471,   472,   473,
     474,   477,    15,   514,     5,     9,    15,    16,    17,   127,
     460,   462,   470,   523,   537,   538,   514,    15,   523,   514,
       5,   522,   523,   538,   523,     8,     8,     8,     8,     8,
       8,     8,     8,     7,     8,     8,     5,     7,   330,   588,
     589,   330,   581,   453,    15,    15,   145,   441,   330,   330,
     687,   688,     8,   330,   605,   606,   688,   330,   332,   330,
      15,   490,   535,    23,    37,   330,   380,   381,    15,   330,
     562,   330,   619,   619,   330,   602,   603,   330,   493,   390,
      15,   330,   145,   693,   493,     7,   436,   437,   452,   573,
     574,   330,   568,   574,    15,   515,   330,   544,   545,   493,
      15,    15,   493,   693,   494,   493,   493,   493,   493,   330,
     493,   333,   493,    15,   385,   453,   461,   462,    15,   327,
     330,   330,   599,   600,   443,   444,   445,   446,     8,   620,
     683,    15,   330,   556,   330,   547,   548,    15,    15,   330,
     453,    15,   458,   696,    15,   330,   670,   672,     8,   330,
      37,   379,    15,   462,   463,   453,    15,    15,   515,   441,
     453,   462,   330,   662,     5,    15,   537,   538,   453,   330,
     331,   453,   536,    15,   461,   582,   583,   568,   572,   330,
     560,   330,   643,   643,    15,   330,   558,   662,   458,   469,
     453,   337,   330,   649,   649,   649,   649,   649,   458,   551,
     552,   330,   553,   453,   326,   330,   453,   330,   668,   670,
     330,   452,   453,   330,   430,    15,    15,   536,   330,    15,
     574,    15,   574,   574,   574,   574,   712,   768,   574,   574,
     574,   574,   574,   574,   712,   330,   337,   775,   776,   777,
      15,    15,    15,   324,   324,   122,     5,    21,   330,   334,
     335,   329,   337,   330,   330,   330,   381,     7,   337,   324,
     122,   330,   330,   515,   330,   381,   381,   381,   381,   380,
     461,   379,   330,   330,   385,   392,   393,   395,   396,   514,
     514,   233,   371,   458,   459,   458,   458,   458,   458,     5,
       9,    16,    17,    22,    23,    24,    25,    26,    28,    29,
      30,    31,    32,    33,    35,    37,    38,   342,    15,   240,
       3,    15,   240,   240,    15,   467,   468,    21,   511,   535,
     469,     5,     9,   161,   524,   525,   526,   537,    26,   537,
       5,     9,    23,    37,   460,   536,   537,   536,     8,    15,
     462,   530,   531,    15,   458,   459,   474,   532,   533,   534,
     532,   330,   330,   556,   558,   560,   562,   330,     7,   337,
     668,     8,    21,   583,   381,   483,   458,   234,   509,    15,
     337,    15,   435,     8,   535,     7,   458,   491,   492,   493,
      15,   330,   435,   381,   440,   441,     8,   392,   483,   435,
      15,     8,    21,     5,     7,   438,   439,   458,   330,     8,
      21,     5,    57,   124,   160,   252,   575,   571,   572,   170,
     563,   458,   145,   504,     8,   458,   458,   329,   330,   386,
     387,   461,   466,   330,    26,   330,   499,   500,   502,   333,
       8,     8,    15,   225,   361,   447,   337,     8,   683,   330,
     461,   653,   662,   680,   681,     8,    26,     5,     9,    16,
      17,    22,    23,    24,    25,    28,    29,    30,    31,    32,
      33,    34,    35,    37,    38,   342,   343,   344,   330,   337,
     351,   458,    15,   337,   330,   330,   461,   461,   484,     8,
     620,   674,   330,   461,   609,   610,   330,   425,   426,   504,
     381,    18,   536,   537,   536,   357,   360,   588,   583,     7,
     572,   574,   653,   662,   663,   664,   380,   381,   419,   420,
      61,    15,     7,     8,    21,   550,   381,   333,   381,   435,
       8,   618,   640,    21,   337,   330,     8,   458,   458,   435,
     461,   509,   733,   461,   281,   745,   745,   509,   742,   745,
      15,   509,   710,   509,   749,   710,   710,   509,   727,   509,
     739,   435,   143,   144,   175,   308,   309,   312,   313,   338,
     778,   779,   780,     8,    21,   462,   624,   781,    21,   781,
     337,   703,   704,   330,   326,   324,     8,    21,   208,   338,
     435,    43,    85,    91,   117,   139,   141,   173,   178,   182,
     186,   189,   211,   224,   230,   349,   350,   352,   330,   324,
     452,     5,    15,   368,   369,   435,   509,   509,     8,    37,
      15,   330,   398,   403,   337,    15,   478,    21,     8,   458,
     458,   458,   458,   458,   458,   458,   458,   458,   458,   458,
     458,   458,   458,   458,   458,   458,   458,   458,   535,    63,
     127,   454,   456,   535,   461,   472,   475,    63,   475,   469,
       8,    21,     5,   458,   512,   526,     8,    21,     5,     9,
     458,    21,   458,   537,   537,   537,   537,   537,    21,   530,
     530,     8,   458,   459,   533,   534,     8,     8,     8,   435,
     435,   435,   452,    42,    81,    85,    86,    92,   222,   253,
     297,   592,   589,   337,   461,   464,   465,   481,    21,   330,
      15,   457,   458,    66,   436,   606,   458,     7,     8,    21,
     511,    37,     8,    21,   603,   461,   464,   480,   482,   535,
     691,   438,     7,   435,   574,    15,    15,    15,    15,   563,
     574,   330,    21,   516,   545,    21,    21,    15,     8,    21,
       8,   468,   462,     8,   501,    26,   329,   600,   444,   127,
     448,   449,   450,   366,   164,   203,   276,   337,    15,     7,
       8,    21,   548,    21,    21,   143,   144,   175,    21,    21,
       7,   458,   476,   170,   315,    37,     8,    21,   337,     8,
      21,    26,     8,    21,   516,   458,    21,   421,   422,   421,
      21,     7,   574,   563,    15,     7,     8,    21,     8,    15,
     650,   651,   653,   458,   552,   337,     8,   640,     8,   618,
     362,   352,   339,    21,    21,    21,   574,   509,    21,   574,
     509,   769,   574,   509,   574,   509,   574,   509,   574,   509,
      15,    15,    15,    15,    15,    15,   337,   777,     8,    21,
      21,   177,   309,   312,     8,    21,   333,   330,   335,    15,
     367,   368,   435,   452,    15,     7,     8,   330,   435,    15,
     472,   535,   516,   381,   461,   395,    15,    16,    17,    27,
      36,    58,    63,    90,   145,   196,   394,   396,   406,   407,
     408,   409,   410,   411,   412,   413,   398,   403,   404,   405,
      15,   399,   400,    61,   458,   532,   459,   454,    21,     8,
     455,   458,   476,   526,     7,   535,   441,   458,   535,     8,
     531,    21,     8,     8,     8,   459,   534,   459,   534,   459,
     534,   330,   249,    15,    15,     8,    21,   441,   440,     8,
      21,     7,    21,   458,   491,    21,   441,   509,     8,    21,
     526,   692,     8,    21,   439,   458,   575,   535,   330,   576,
     458,   576,   435,   574,   233,   493,   457,   387,   387,   330,
     458,   500,    21,   458,   476,     8,    21,    16,    15,    15,
      15,   457,   680,   681,   453,     7,   458,     7,    21,    21,
     330,   570,   462,   461,   610,   337,   458,   426,   509,     8,
      46,   172,   330,   424,   337,   580,   582,   563,     7,     7,
     458,   665,   666,   663,   664,   420,   458,     8,    21,    15,
      70,   203,   337,   337,   453,   167,   330,   433,   434,   462,
     186,   203,   276,   279,   284,   292,   713,   714,   715,   722,
     734,   735,   736,   574,   260,   743,   744,   745,   574,    37,
     461,   766,   767,    83,   259,   283,   293,   298,   711,   713,
     714,   715,   716,   717,   718,   720,   721,   722,   574,   713,
     714,   715,   716,   717,   718,   720,   721,   722,   735,   736,
     750,   574,   713,   714,   715,   722,   728,   574,   713,   714,
     740,   574,   781,   781,   781,   337,   782,   783,   781,   781,
     462,   704,   330,    18,    26,   372,    15,   351,     7,   337,
     367,     5,   458,   362,     5,   370,   458,   526,   362,   409,
     410,   411,   414,   410,   412,   410,   412,   240,   240,   240,
     240,   240,     8,    37,   330,   397,   461,     5,   399,   400,
       8,    15,    16,    17,   145,   330,   397,   401,   402,   415,
     416,   417,   418,    15,   400,    15,    21,   479,    21,    21,
     468,   535,   458,   469,   512,   525,   537,   502,   503,   459,
     503,   503,   503,   435,   330,   584,   587,   535,   454,   535,
       8,    21,   458,     7,   371,   458,   535,   458,   535,   526,
     577,    21,    21,     8,     8,   248,   487,   493,    21,   449,
     450,   624,   624,   624,    21,    21,   330,   458,     7,     7,
     458,   435,   168,   502,   422,    15,    15,   423,   249,     8,
       7,     8,    21,    21,    21,   651,   652,    15,    15,   330,
     330,   431,   432,    18,     8,    26,   712,    15,   712,   712,
      15,   574,   734,   712,   574,   743,   330,     8,    21,    15,
     712,    15,   712,    15,   574,   711,   574,   750,   574,   728,
     574,   740,    21,    21,    21,   310,   311,     8,    21,    21,
      21,    21,   458,   458,   594,   595,    21,   350,   372,    21,
      21,   371,     5,   458,     8,   475,   475,   475,   475,   475,
     406,     5,    15,   396,   407,   400,   330,   397,   405,   415,
     416,   416,     8,    21,     7,    16,    17,     5,    37,     9,
     415,   458,    20,   468,   455,    21,    26,    21,    21,    21,
      21,    15,   454,    21,   465,   526,   441,   605,   453,   480,
     526,   692,   458,    21,   458,   337,    15,    21,    21,    21,
     458,   458,     7,   461,   611,    26,   424,    26,   343,   584,
     582,   330,   566,   567,   568,   569,   666,    77,   551,   330,
     619,   663,   641,    18,     8,   330,   434,   458,   574,   723,
     337,   574,   574,   768,   461,   766,   337,   458,   458,   574,
     574,   574,   574,   783,     8,    21,     7,     8,    21,   414,
     407,   535,   397,    26,    21,   415,   402,   416,   416,   417,
     417,   417,    21,   458,     5,   458,   476,   585,   586,    21,
     461,     8,   624,   461,     8,    21,   248,   458,   458,    15,
       8,   614,   615,   616,   617,   618,   620,   621,   622,   625,
     626,   627,   640,   648,   458,   423,    21,    21,    15,     8,
      21,    21,     7,    21,    22,    24,    33,    35,   153,   154,
     156,   157,   187,   228,   241,   643,   645,   646,   647,   330,
     432,     5,    16,    17,    22,    24,    33,    35,    37,   154,
     157,   241,   299,   300,   301,   725,    21,    92,   224,   278,
     289,   737,    37,   186,   282,   293,   719,    21,    21,     7,
       7,   535,    21,   454,   401,   415,    21,     8,     8,    21,
     441,   526,    15,    21,     5,   458,   612,   613,   337,   616,
       8,   457,   567,    15,   663,   642,   642,    15,    15,     7,
       8,    21,   769,    21,     7,   370,    21,    21,   458,   586,
     458,   458,     8,    21,    40,   132,   186,   204,   215,   217,
     218,   220,   223,   314,   458,    21,   337,    21,     7,     8,
      21,   462,   624,   624,   724,   458,   330,   596,   597,   371,
       8,    21,   613,    15,   628,    15,    15,    15,    15,    15,
     649,   649,    15,    15,     8,    77,   337,   644,   644,    21,
       8,    21,     8,    21,   458,   574,   628,   662,   680,   624,
     662,   663,   653,   650,   458,   458,   623,   458,    21,   645,
       8,    21,    21,   458,   462,   597,   144,   175,   629,     7,
      21,    21,     7,    21,    15,    21,    21,     8,    21,   337,
      21,     7,     7,   630,   631,   653,   680,   663,   551,   458,
     645,     8,    21,    15,    21,    21,    21,     8,   631,   458,
     632,   633,    21,   458,     7,     8,    21,   458,   633,    15,
     337,   634,   635,   636,   637,   638,   126,   152,   212,     8,
      21,     7,     7,    15,   639,   639,   639,   635,   337,   637,
     638,   337,   638,   456,     7,    21,   638
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *bottom, yytype_int16 *top)
#else
static void
yy_stack_print (bottom, top)
    yytype_int16 *bottom;
    yytype_int16 *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      fprintf (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      fprintf (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */

int yylex();

/* The look-ahead symbol.  */
int yychar;

/* The semantic value of the look-ahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
  
  int yystate;
  int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int yytoken = 0;
#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  yytype_int16 yyssa[YYINITDEPTH];
  yytype_int16 *yyss = yyssa;
  yytype_int16 *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  YYSTYPE *yyvsp;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     look-ahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to look-ahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 729 "gram1.y"
    { (yyval.bf_node) = BFNULL; ;}
    break;

  case 3:
#line 731 "gram1.y"
    { (yyval.bf_node) = set_stat_list((yyvsp[(1) - (3)].bf_node),(yyvsp[(2) - (3)].bf_node)); ;}
    break;

  case 4:
#line 735 "gram1.y"
    { lastwasbranch = NO;  (yyval.bf_node) = BFNULL; ;}
    break;

  case 5:
#line 737 "gram1.y"
    {
	       if ((yyvsp[(2) - (3)].bf_node) != BFNULL) 
               {	    
	          (yyvsp[(2) - (3)].bf_node)->label = (yyvsp[(1) - (3)].label);
	          (yyval.bf_node) = (yyvsp[(2) - (3)].bf_node);
	 	  if (is_openmp_stmt) {            /*OMP*/
			is_openmp_stmt = 0;
			if((yyvsp[(2) - (3)].bf_node)) {                        /*OMP*/
				if ((yyvsp[(2) - (3)].bf_node)->decl_specs != -BIT_OPENMP) (yyvsp[(2) - (3)].bf_node)->decl_specs = BIT_OPENMP; /*OMP*/
			}                               /*OMP*/
		  }                                       /*OMP*/
               }
	    ;}
    break;

  case 6:
#line 751 "gram1.y"
    { PTR_BFND p;

	     if(lastwasbranch && ! thislabel)
               /*if (warn_all)
		 warn("statement cannot be reached", 36);*/
	     lastwasbranch = thiswasbranch;
	     thiswasbranch = NO;
	     if((yyvsp[(2) - (3)].bf_node)) (yyvsp[(2) - (3)].bf_node)->label = (yyvsp[(1) - (3)].label);
	     if((yyvsp[(1) - (3)].label) && (yyvsp[(2) - (3)].bf_node)) (yyvsp[(1) - (3)].label)->statbody = (yyvsp[(2) - (3)].bf_node); /*8.11.06 podd*/
	     if((yyvsp[(1) - (3)].label)) {
		/*$1->statbody = $2;*/ /*8.11.06 podd*/
		if((yyvsp[(1) - (3)].label)->labtype == LABFORMAT)
		  err("label already that of a format",39);
		else
		  (yyvsp[(1) - (3)].label)->labtype = LABEXEC;
	     }
	     if (is_openmp_stmt) {            /*OMP*/
			is_openmp_stmt = 0;
			if((yyvsp[(2) - (3)].bf_node)) {                        /*OMP*/
				if ((yyvsp[(2) - (3)].bf_node)->decl_specs != -BIT_OPENMP) (yyvsp[(2) - (3)].bf_node)->decl_specs = BIT_OPENMP; /*OMP*/
			}                               /*OMP*/
	     }                                       /*OMP*/
             for (p = pred_bfnd; (yyvsp[(1) - (3)].label) && 
		  ((p->variant == FOR_NODE)||(p->variant == WHILE_NODE)) &&
                  (p->entry.for_node.doend) &&
		  (p->entry.for_node.doend->stateno == (yyvsp[(1) - (3)].label)->stateno);
		  p = p->control_parent)
                ++end_group;
	     (yyval.bf_node) = (yyvsp[(2) - (3)].bf_node);
     ;}
    break;

  case 7:
#line 782 "gram1.y"
    { /* PTR_LLND p; */
			doinclude( (yyvsp[(3) - (3)].charp) );
/*			p = make_llnd(fi, STRING_VAL, LLNULL, LLNULL, SMNULL);
			p->entry.string_val = $3;
			p->type = global_string;
			$$ = get_bfnd(fi, INCLUDE_STAT, SMNULL, p, LLNULL); */
			(yyval.bf_node) = BFNULL;
		;}
    break;

  case 8:
#line 791 "gram1.y"
    {
	      err("Unclassifiable statement", 10);
	      flline();
	      (yyval.bf_node) = BFNULL;
	    ;}
    break;

  case 9:
#line 797 "gram1.y"
    { PTR_CMNT p;
	    p = make_comment(fi,commentbuf, FULL);
            if (last_bfnd && last_bfnd->control_parent &&((last_bfnd->control_parent->variant == LOGIF_NODE)
	       ||(last_bfnd->control_parent->variant == FORALL_STAT))) 	      
               last_bfnd->control_parent->entry.Template.cmnt_ptr = p; 	   
            else last_bfnd->entry.Template.cmnt_ptr = p;
 	    (yyval.bf_node) = BFNULL;           ;}
    break;

  case 10:
#line 806 "gram1.y"
    { 
	      flline();	 needkwd = NO;	inioctl = NO;
/*!!!*/
              opt_kwd_ = NO; optcorner = 0; opt_kwd_hedr = NO; opt_kwd_r = NO; as_op_kwd_= NO; optcorner = NO;
	      yyerrok; yyclearin;  (yyval.bf_node) = BFNULL;
	    ;}
    break;

  case 11:
#line 815 "gram1.y"
    {
	    if(yystno)
	      {
	      (yyval.label) = thislabel =	make_label_node(fi,yystno);
	      thislabel->scope = cur_scope();
	      if (thislabel->labdefined && (thislabel->scope == cur_scope()))
		 errstr("Label %s already defined",convic(thislabel->stateno),40);
	      else
		 thislabel->labdefined = YES;
	      }
	    else
	      (yyval.label) = thislabel = LBNULL;
	    ;}
    break;

  case 12:
#line 831 "gram1.y"
    { PTR_BFND p;

	        if (pred_bfnd != global_bfnd)
		    err("Misplaced PROGRAM statement", 33);
		p = get_bfnd(fi,PROG_HEDR, (yyvsp[(3) - (3)].symbol), LLNULL, LLNULL, LLNULL);
		(yyvsp[(3) - (3)].symbol)->entry.prog_decl.prog_hedr=p;
 		set_blobs(p, global_bfnd, NEW_GROUP1);
	        add_scope_level(p, NO);
	        position = IN_PROC;
	    ;}
    break;

  case 13:
#line 843 "gram1.y"
    {  PTR_BFND q = BFNULL;

	      (yyvsp[(3) - (3)].symbol)->variant = PROCEDURE_NAME;
	      (yyvsp[(3) - (3)].symbol)->decl = YES;   /* variable declaration has been seen. */
	      q = get_bfnd(fi,BLOCK_DATA, (yyvsp[(3) - (3)].symbol), LLNULL, LLNULL, LLNULL);
	      set_blobs(q, global_bfnd, NEW_GROUP1);
              add_scope_level(q, NO);
	    ;}
    break;

  case 14:
#line 853 "gram1.y"
    { 
              install_param_list((yyvsp[(3) - (4)].symbol), (yyvsp[(4) - (4)].symbol), LLNULL, PROCEDURE_NAME); 
	      /* if there is only a control end the control parent is not set */
              
	     ;}
    break;

  case 15:
#line 860 "gram1.y"
    { install_param_list((yyvsp[(4) - (5)].symbol), (yyvsp[(5) - (5)].symbol), LLNULL, PROCEDURE_NAME); 
              if((yyvsp[(1) - (5)].ll_node)->variant == RECURSIVE_OP) 
                   (yyvsp[(4) - (5)].symbol)->attr = (yyvsp[(4) - (5)].symbol)->attr | RECURSIVE_BIT;
              pred_bfnd->entry.Template.ll_ptr3 = (yyvsp[(1) - (5)].ll_node);
            ;}
    break;

  case 16:
#line 866 "gram1.y"
    {
              install_param_list((yyvsp[(3) - (5)].symbol), (yyvsp[(4) - (5)].symbol), (yyvsp[(5) - (5)].ll_node), FUNCTION_NAME);  
  	      pred_bfnd->entry.Template.ll_ptr1 = (yyvsp[(5) - (5)].ll_node);
            ;}
    break;

  case 17:
#line 871 "gram1.y"
    {
              install_param_list((yyvsp[(1) - (3)].symbol), (yyvsp[(2) - (3)].symbol), (yyvsp[(3) - (3)].ll_node), FUNCTION_NAME); 
	      pred_bfnd->entry.Template.ll_ptr1 = (yyvsp[(3) - (3)].ll_node);
	    ;}
    break;

  case 18:
#line 876 "gram1.y"
    {PTR_BFND p, bif;
	     PTR_SYMB q = SMNULL;
             PTR_LLND l = LLNULL;

	     if(parstate==OUTSIDE || procclass==CLMAIN || procclass==CLBLOCK)
	        err("Misplaced ENTRY statement", 35);

	     bif = cur_scope();
	     if (bif->variant == FUNC_HEDR) {
	        q = make_function((yyvsp[(2) - (4)].hash_entry), bif->entry.Template.symbol->type, YES);
	        l = construct_entry_list(q, (yyvsp[(3) - (4)].symbol), FUNCTION_NAME); 
             }
             else if ((bif->variant == PROC_HEDR) || 
                      (bif->variant == PROS_HEDR) || /* added for FORTRAN M */
                      (bif->variant == PROG_HEDR)) {
	             q = make_procedure((yyvsp[(2) - (4)].hash_entry), YES);
  	             l = construct_entry_list(q, (yyvsp[(3) - (4)].symbol), PROCEDURE_NAME); 
             }
	     p = get_bfnd(fi,ENTRY_STAT, q, l, (yyvsp[(4) - (4)].ll_node), LLNULL);
	     set_blobs(p, pred_bfnd, SAME_GROUP);
             q->decl = YES;   /*4.02.03*/
             q->entry.proc_decl.proc_hedr = p; /*5.02.03*/
	    ;}
    break;

  case 19:
#line 900 "gram1.y"
    { PTR_SYMB s;
	      PTR_BFND p;
/*
	      s = make_global_entity($3, MODULE_NAME, global_default, NO);
	      s->decl = YES;  
	      p = get_bfnd(fi, MODULE_STMT, s, LLNULL, LLNULL, LLNULL);
	      s->entry.Template.func_hedr = p;
	      set_blobs(p, pred_bfnd, NEW_GROUP1);
              add_scope_level(p, NO);
*/
	      /*position = IN_MODULE;*/


               s = make_module((yyvsp[(3) - (3)].hash_entry));
	       s->decl = YES;   /* variable declaration has been seen. */
	        if (pred_bfnd != global_bfnd)
		    err("Misplaced MODULE statement", 33);
              p = get_bfnd(fi, MODULE_STMT, s, LLNULL, LLNULL, LLNULL);
	      s->entry.Template.func_hedr = p; /* !!!????*/
	      set_blobs(p, global_bfnd, NEW_GROUP1);
	      add_scope_level(p, NO);	
	      position =  IN_MODULE;    /*IN_PROC*/
              privateall = 0;
            ;}
    break;

  case 20:
#line 926 "gram1.y"
    { newprog(); 
	      if (position == IN_OUTSIDE)
	           position = IN_PROC;
              else if (position != IN_INTERNAL_PROC){ 
                if(!is_interface_stat(pred_bfnd))
	           position--;
              }
              else {
                if(!is_interface_stat(pred_bfnd))
                  err("Internal procedures can not contain procedures",304);
              }
	    ;}
    break;

  case 21:
#line 941 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, RECURSIVE_OP, LLNULL, LLNULL, SMNULL); ;}
    break;

  case 22:
#line 943 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, PURE_OP, LLNULL, LLNULL, SMNULL); ;}
    break;

  case 23:
#line 945 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, ELEMENTAL_OP, LLNULL, LLNULL, SMNULL); ;}
    break;

  case 24:
#line 949 "gram1.y"
    { PTR_BFND p;

	      (yyval.symbol) = make_procedure((yyvsp[(1) - (1)].hash_entry), LOCAL);
	      (yyval.symbol)->decl = YES;   /* variable declaration has been seen. */
             /* if (pred_bfnd != global_bfnd)
		 {
	         err("Misplaced SUBROUTINE statement", 34);
		 }  
              */
	      p = get_bfnd(fi,PROC_HEDR, (yyval.symbol), LLNULL, LLNULL, LLNULL);
              (yyval.symbol)->entry.proc_decl.proc_hedr = p;
	      set_blobs(p, pred_bfnd, NEW_GROUP1);
              add_scope_level(p, NO);
            ;}
    break;

  case 25:
#line 966 "gram1.y"
    { PTR_BFND p;

	      (yyval.symbol) = make_function((yyvsp[(1) - (1)].hash_entry), TYNULL, LOCAL);
	      (yyval.symbol)->decl = YES;   /* variable declaration has been seen. */
             /* if (pred_bfnd != global_bfnd)
	         err("Misplaced FUNCTION statement", 34); */
	      p = get_bfnd(fi,FUNC_HEDR, (yyval.symbol), LLNULL, LLNULL, LLNULL);
              (yyval.symbol)->entry.func_decl.func_hedr = p;
	      set_blobs(p, pred_bfnd, NEW_GROUP1);
              add_scope_level(p, NO);
            ;}
    break;

  case 26:
#line 980 "gram1.y"
    { PTR_BFND p;
             PTR_LLND l;

	      (yyval.symbol) = make_function((yyvsp[(4) - (4)].hash_entry), (yyvsp[(1) - (4)].data_type), LOCAL);
	      (yyval.symbol)->decl = YES;   /* variable declaration has been seen. */
              l = make_llnd(fi, TYPE_OP, LLNULL, LLNULL, SMNULL);
              l->type = (yyvsp[(1) - (4)].data_type);
	      p = get_bfnd(fi,FUNC_HEDR, (yyval.symbol), LLNULL, l, LLNULL);
              (yyval.symbol)->entry.func_decl.func_hedr = p;
            /*  if (pred_bfnd != global_bfnd)
	         err("Misplaced FUNCTION statement", 34);*/
	      set_blobs(p, pred_bfnd, NEW_GROUP1);
              add_scope_level(p, NO);
/*
	      $$ = make_function($4, $1, LOCAL);
	      $$->decl = YES;
	      p = get_bfnd(fi,FUNC_HEDR, $$, LLNULL, LLNULL, LLNULL);
              if (pred_bfnd != global_bfnd)
	         errstr("cftn.gram: misplaced SUBROUTINE statement.");
	      set_blobs(p, pred_bfnd, NEW_GROUP1);
              add_scope_level(p, NO);
*/
           ;}
    break;

  case 27:
#line 1004 "gram1.y"
    { PTR_BFND p;
             PTR_LLND l;
	      (yyval.symbol) = make_function((yyvsp[(5) - (5)].hash_entry), (yyvsp[(1) - (5)].data_type), LOCAL);
	      (yyval.symbol)->decl = YES;   /* variable declaration has been seen. */
              if((yyvsp[(2) - (5)].ll_node)->variant == RECURSIVE_OP)
	         (yyval.symbol)->attr = (yyval.symbol)->attr | RECURSIVE_BIT;
              l = make_llnd(fi, TYPE_OP, LLNULL, LLNULL, SMNULL);
              l->type = (yyvsp[(1) - (5)].data_type);
             /* if (pred_bfnd != global_bfnd)
	         err("Misplaced FUNCTION statement", 34);*/
	      p = get_bfnd(fi,FUNC_HEDR, (yyval.symbol), LLNULL, l, (yyvsp[(2) - (5)].ll_node));
              (yyval.symbol)->entry.func_decl.func_hedr = p;
	      set_blobs(p, pred_bfnd, NEW_GROUP1);
              add_scope_level(p, NO);
            ;}
    break;

  case 28:
#line 1020 "gram1.y"
    { PTR_BFND p;

	      (yyval.symbol) = make_function((yyvsp[(4) - (4)].hash_entry), TYNULL, LOCAL);
	      (yyval.symbol)->decl = YES;   /* variable declaration has been seen. */
              if((yyvsp[(1) - (4)].ll_node)->variant == RECURSIVE_OP)
	        (yyval.symbol)->attr = (yyval.symbol)->attr | RECURSIVE_BIT;
              /*if (pred_bfnd != global_bfnd)
	         err("Misplaced FUNCTION statement",34);*/
	      p = get_bfnd(fi,FUNC_HEDR, (yyval.symbol), LLNULL, LLNULL, (yyvsp[(1) - (4)].ll_node));
              (yyval.symbol)->entry.func_decl.func_hedr = p;
	      set_blobs(p, pred_bfnd, NEW_GROUP1);
              add_scope_level(p, NO);
            ;}
    break;

  case 29:
#line 1034 "gram1.y"
    { PTR_BFND p;
              PTR_LLND l;
	      (yyval.symbol) = make_function((yyvsp[(5) - (5)].hash_entry), (yyvsp[(2) - (5)].data_type), LOCAL);
	      (yyval.symbol)->decl = YES;   /* variable declaration has been seen. */
              if((yyvsp[(1) - (5)].ll_node)->variant == RECURSIVE_OP)
	        (yyval.symbol)->attr = (yyval.symbol)->attr | RECURSIVE_BIT;
              l = make_llnd(fi, TYPE_OP, LLNULL, LLNULL, SMNULL);
              l->type = (yyvsp[(2) - (5)].data_type);
             /* if (pred_bfnd != global_bfnd)
	          err("Misplaced FUNCTION statement",34);*/
	      p = get_bfnd(fi,FUNC_HEDR, (yyval.symbol), LLNULL, l, (yyvsp[(1) - (5)].ll_node));
              (yyval.symbol)->entry.func_decl.func_hedr = p;
	      set_blobs(p, pred_bfnd, NEW_GROUP1);
              add_scope_level(p, NO);
            ;}
    break;

  case 30:
#line 1052 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 31:
#line 1054 "gram1.y"
    { PTR_SYMB s;
              s = make_scalar((yyvsp[(4) - (5)].hash_entry), TYNULL, LOCAL);
              (yyval.ll_node) = make_llnd(fi, VAR_REF, LLNULL, LLNULL, s);
            ;}
    break;

  case 32:
#line 1061 "gram1.y"
    { (yyval.hash_entry) = look_up_sym(yytext); ;}
    break;

  case 33:
#line 1064 "gram1.y"
    { (yyval.symbol) = make_program(look_up_sym("_MAIN")); ;}
    break;

  case 34:
#line 1066 "gram1.y"
    {
              (yyval.symbol) = make_program((yyvsp[(1) - (1)].hash_entry));
	      (yyval.symbol)->decl = YES;   /* variable declaration has been seen. */
            ;}
    break;

  case 35:
#line 1072 "gram1.y"
    { (yyval.symbol) = make_program(look_up_sym("_BLOCK")); ;}
    break;

  case 36:
#line 1074 "gram1.y"
    {
              (yyval.symbol) = make_program((yyvsp[(1) - (1)].hash_entry)); 
	      (yyval.symbol)->decl = YES;   /* variable declaration has been seen. */
	    ;}
    break;

  case 37:
#line 1081 "gram1.y"
    { (yyval.symbol) = SMNULL; ;}
    break;

  case 38:
#line 1083 "gram1.y"
    { (yyval.symbol) = SMNULL; ;}
    break;

  case 39:
#line 1085 "gram1.y"
    { (yyval.symbol) = (yyvsp[(2) - (3)].symbol); ;}
    break;

  case 41:
#line 1090 "gram1.y"
    { (yyval.symbol) = set_id_list((yyvsp[(1) - (3)].symbol), (yyvsp[(3) - (3)].symbol)); ;}
    break;

  case 42:
#line 1094 "gram1.y"
    {
	      (yyval.symbol) = make_scalar((yyvsp[(1) - (1)].hash_entry), TYNULL, IO);
            ;}
    break;

  case 43:
#line 1098 "gram1.y"
    { (yyval.symbol) = star_symb; ;}
    break;

  case 44:
#line 1104 "gram1.y"
    { char *s;

	      s = copyn(yyleng+1, yytext);
	      s[yyleng] = '\0';
	      (yyval.charp) = s;
	    ;}
    break;

  case 45:
#line 1113 "gram1.y"
    { needkwd = 1; ;}
    break;

  case 46:
#line 1117 "gram1.y"
    { needkwd = NO; ;}
    break;

  case 47:
#line 1122 "gram1.y"
    { colon_flag = YES; ;}
    break;

  case 61:
#line 1143 "gram1.y"
    {
	      saveall = YES;
	      (yyval.bf_node) = get_bfnd(fi,SAVE_DECL, SMNULL, LLNULL, LLNULL, LLNULL);
	    ;}
    break;

  case 62:
#line 1148 "gram1.y"
    {
	      (yyval.bf_node) = get_bfnd(fi,SAVE_DECL, SMNULL, (yyvsp[(4) - (4)].ll_node), LLNULL, LLNULL);
            ;}
    break;

  case 63:
#line 1153 "gram1.y"
    { PTR_LLND p;

	      p = make_llnd(fi,STMT_STR, LLNULL, LLNULL, SMNULL);
	      p->entry.string_val = copys(stmtbuf);
	      (yyval.bf_node) = get_bfnd(fi,FORMAT_STAT, SMNULL, p, LLNULL, LLNULL);
             ;}
    break;

  case 64:
#line 1160 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,PARAM_DECL, SMNULL, (yyvsp[(4) - (5)].ll_node), LLNULL, LLNULL); ;}
    break;

  case 77:
#line 1176 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, INTERFACE_STMT, SMNULL, LLNULL, LLNULL, LLNULL); 
             /* add_scope_level($$, NO); */    
            ;}
    break;

  case 78:
#line 1180 "gram1.y"
    { PTR_SYMB s;

	      s = make_procedure((yyvsp[(3) - (3)].hash_entry), LOCAL);
	      s->variant = INTERFACE_NAME;
	      (yyval.bf_node) = get_bfnd(fi, INTERFACE_STMT, s, LLNULL, LLNULL, LLNULL);
              /*add_scope_level($$, NO);*/
	    ;}
    break;

  case 79:
#line 1188 "gram1.y"
    { PTR_SYMB s;

	      s = make_function((yyvsp[(4) - (5)].hash_entry), global_default, LOCAL);
	      s->variant = INTERFACE_NAME;
	      (yyval.bf_node) = get_bfnd(fi, INTERFACE_OPERATOR, s, LLNULL, LLNULL, LLNULL);
              /*add_scope_level($$, NO);*/
	    ;}
    break;

  case 80:
#line 1196 "gram1.y"
    { PTR_SYMB s;


	      s = make_procedure(look_up_sym("="), LOCAL);
	      s->variant = INTERFACE_NAME;
	      (yyval.bf_node) = get_bfnd(fi, INTERFACE_ASSIGNMENT, s, LLNULL, LLNULL, LLNULL);
              /*add_scope_level($$, NO);*/
	    ;}
    break;

  case 81:
#line 1205 "gram1.y"
    { parstate = INDCL;
              (yyval.bf_node) = get_bfnd(fi, CONTROL_END, SMNULL, LLNULL, LLNULL, LLNULL); 
	      /*process_interface($$);*/ /*podd 01.02.03*/
              /*delete_beyond_scope_level(pred_bfnd);*/
	    ;}
    break;

  case 82:
#line 1213 "gram1.y"
    { (yyval.hash_entry) = look_up_sym(yytext); ;}
    break;

  case 83:
#line 1217 "gram1.y"
    { (yyval.hash_entry) = (yyvsp[(1) - (1)].hash_entry); ;}
    break;

  case 84:
#line 1219 "gram1.y"
    { (yyval.hash_entry) = (yyvsp[(1) - (1)].hash_entry); ;}
    break;

  case 85:
#line 1223 "gram1.y"
    { (yyval.hash_entry) = look_up_op(PLUS); ;}
    break;

  case 86:
#line 1225 "gram1.y"
    { (yyval.hash_entry) = look_up_op(MINUS); ;}
    break;

  case 87:
#line 1227 "gram1.y"
    { (yyval.hash_entry) = look_up_op(ASTER); ;}
    break;

  case 88:
#line 1229 "gram1.y"
    { (yyval.hash_entry) = look_up_op(DASTER); ;}
    break;

  case 89:
#line 1231 "gram1.y"
    { (yyval.hash_entry) = look_up_op(SLASH); ;}
    break;

  case 90:
#line 1233 "gram1.y"
    { (yyval.hash_entry) = look_up_op(DSLASH); ;}
    break;

  case 91:
#line 1235 "gram1.y"
    { (yyval.hash_entry) = look_up_op(AND); ;}
    break;

  case 92:
#line 1237 "gram1.y"
    { (yyval.hash_entry) = look_up_op(OR); ;}
    break;

  case 93:
#line 1239 "gram1.y"
    { (yyval.hash_entry) = look_up_op(XOR); ;}
    break;

  case 94:
#line 1241 "gram1.y"
    { (yyval.hash_entry) = look_up_op(NOT); ;}
    break;

  case 95:
#line 1243 "gram1.y"
    { (yyval.hash_entry) = look_up_op(EQ); ;}
    break;

  case 96:
#line 1245 "gram1.y"
    { (yyval.hash_entry) = look_up_op(NE); ;}
    break;

  case 97:
#line 1247 "gram1.y"
    { (yyval.hash_entry) = look_up_op(GT); ;}
    break;

  case 98:
#line 1249 "gram1.y"
    { (yyval.hash_entry) = look_up_op(GE); ;}
    break;

  case 99:
#line 1251 "gram1.y"
    { (yyval.hash_entry) = look_up_op(LT); ;}
    break;

  case 100:
#line 1253 "gram1.y"
    { (yyval.hash_entry) = look_up_op(LE); ;}
    break;

  case 101:
#line 1255 "gram1.y"
    { (yyval.hash_entry) = look_up_op(NEQV); ;}
    break;

  case 102:
#line 1257 "gram1.y"
    { (yyval.hash_entry) = look_up_op(EQV); ;}
    break;

  case 103:
#line 1262 "gram1.y"
    {
             PTR_SYMB s;
         
             type_var = s = make_derived_type((yyvsp[(4) - (4)].hash_entry), TYNULL, LOCAL);	
             (yyval.bf_node) = get_bfnd(fi, STRUCT_DECL, s, LLNULL, LLNULL, LLNULL);
             add_scope_level((yyval.bf_node), NO);
	   ;}
    break;

  case 104:
#line 1271 "gram1.y"
    { PTR_SYMB s;
         
             type_var = s = make_derived_type((yyvsp[(7) - (7)].hash_entry), TYNULL, LOCAL);	
	     s->attr = s->attr | type_opt;
             (yyval.bf_node) = get_bfnd(fi, STRUCT_DECL, s, (yyvsp[(5) - (7)].ll_node), LLNULL, LLNULL);
             add_scope_level((yyval.bf_node), NO);
	   ;}
    break;

  case 105:
#line 1281 "gram1.y"
    {
	     (yyval.bf_node) = get_bfnd(fi, CONTROL_END, SMNULL, LLNULL, LLNULL, LLNULL);
	     if (type_var != SMNULL)
               process_type(type_var, (yyval.bf_node));
             type_var = SMNULL;
	     delete_beyond_scope_level(pred_bfnd);
           ;}
    break;

  case 106:
#line 1289 "gram1.y"
    {
             (yyval.bf_node) = get_bfnd(fi, CONTROL_END, SMNULL, LLNULL, LLNULL, LLNULL);
	     if (type_var != SMNULL)
               process_type(type_var, (yyval.bf_node));
             type_var = SMNULL;
	     delete_beyond_scope_level(pred_bfnd);	
           ;}
    break;

  case 107:
#line 1299 "gram1.y"
    { 
	      PTR_LLND q, r, l;
	     /* PTR_SYMB s;*/
	      PTR_TYPE t;
	      int type_opts;

	      vartype = (yyvsp[(1) - (7)].data_type);
              if((yyvsp[(6) - (7)].ll_node) && vartype->variant != T_STRING)
                errstr("Non character entity  %s  has length specification",(yyvsp[(3) - (7)].hash_entry)->ident,41);
              t = make_type_node(vartype, (yyvsp[(6) - (7)].ll_node));
	      type_opts = type_options;
	      if ((yyvsp[(5) - (7)].ll_node)) type_opts = type_opts | DIMENSION_BIT;
	      if ((yyvsp[(5) - (7)].ll_node))
		 q = deal_with_options((yyvsp[(3) - (7)].hash_entry), t, type_opts, (yyvsp[(5) - (7)].ll_node), ndim, (yyvsp[(7) - (7)].ll_node), (yyvsp[(5) - (7)].ll_node));
	      else q = deal_with_options((yyvsp[(3) - (7)].hash_entry), t, type_opts, attr_dims, attr_ndim, (yyvsp[(7) - (7)].ll_node), (yyvsp[(5) - (7)].ll_node));
	      r = make_llnd(fi, EXPR_LIST, q, LLNULL, SMNULL);
	      l = make_llnd(fi, TYPE_OP, LLNULL, LLNULL, SMNULL);
	      l->type = vartype;
	      (yyval.bf_node) = get_bfnd(fi,VAR_DECL, SMNULL, r, l, (yyvsp[(2) - (7)].ll_node));
	    ;}
    break;

  case 108:
#line 1320 "gram1.y"
    { 
	      PTR_LLND q, r;
	    /*  PTR_SYMB s;*/
              PTR_TYPE t;
	      int type_opts;
              if((yyvsp[(5) - (6)].ll_node) && vartype->variant != T_STRING)
                errstr("Non character entity  %s  has length specification",(yyvsp[(3) - (6)].hash_entry)->ident,41);
              t = make_type_node(vartype, (yyvsp[(5) - (6)].ll_node));
	      type_opts = type_options;
	      if ((yyvsp[(4) - (6)].ll_node)) type_opts = type_opts | DIMENSION_BIT;
	      if ((yyvsp[(4) - (6)].ll_node))
		 q = deal_with_options((yyvsp[(3) - (6)].hash_entry), t, type_opts, (yyvsp[(4) - (6)].ll_node), ndim, (yyvsp[(6) - (6)].ll_node), (yyvsp[(4) - (6)].ll_node));
	      else q = deal_with_options((yyvsp[(3) - (6)].hash_entry), t, type_opts, attr_dims, attr_ndim, (yyvsp[(6) - (6)].ll_node), (yyvsp[(4) - (6)].ll_node));
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (6)].bf_node)->entry.Template.ll_ptr1);
       	    ;}
    break;

  case 109:
#line 1339 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 110:
#line 1341 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 111:
#line 1343 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(3) - (5)].ll_node); ;}
    break;

  case 112:
#line 1347 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 113:
#line 1349 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node), EXPR_LIST); ;}
    break;

  case 114:
#line 1353 "gram1.y"
    { type_options = type_options | PARAMETER_BIT; 
              (yyval.ll_node) = make_llnd(fi, PARAMETER_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 115:
#line 1357 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 116:
#line 1359 "gram1.y"
    { type_options = type_options | ALLOCATABLE_BIT;
              (yyval.ll_node) = make_llnd(fi, ALLOCATABLE_OP, LLNULL, LLNULL, SMNULL);
	    ;}
    break;

  case 117:
#line 1363 "gram1.y"
    { type_options = type_options | DIMENSION_BIT;
	      attr_ndim = ndim;
	      attr_dims = (yyvsp[(2) - (2)].ll_node);
              (yyval.ll_node) = make_llnd(fi, DIMENSION_OP, (yyvsp[(2) - (2)].ll_node), LLNULL, SMNULL);
            ;}
    break;

  case 118:
#line 1369 "gram1.y"
    { type_options = type_options | EXTERNAL_BIT;
              (yyval.ll_node) = make_llnd(fi, EXTERNAL_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 119:
#line 1373 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(3) - (4)].ll_node); ;}
    break;

  case 120:
#line 1375 "gram1.y"
    { type_options = type_options | INTRINSIC_BIT;
              (yyval.ll_node) = make_llnd(fi, INTRINSIC_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 121:
#line 1379 "gram1.y"
    { type_options = type_options | OPTIONAL_BIT;
              (yyval.ll_node) = make_llnd(fi, OPTIONAL_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 122:
#line 1383 "gram1.y"
    { type_options = type_options | POINTER_BIT;
              (yyval.ll_node) = make_llnd(fi, POINTER_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 123:
#line 1387 "gram1.y"
    { type_options = type_options | SAVE_BIT; 
              (yyval.ll_node) = make_llnd(fi, SAVE_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 124:
#line 1391 "gram1.y"
    { type_options = type_options | SAVE_BIT; 
              (yyval.ll_node) = make_llnd(fi, STATIC_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 125:
#line 1395 "gram1.y"
    { type_options = type_options | TARGET_BIT; 
              (yyval.ll_node) = make_llnd(fi, TARGET_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 126:
#line 1401 "gram1.y"
    { type_options = type_options | IN_BIT;  type_opt = IN_BIT; 
              (yyval.ll_node) = make_llnd(fi, IN_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 127:
#line 1405 "gram1.y"
    { type_options = type_options | OUT_BIT;  type_opt = OUT_BIT; 
              (yyval.ll_node) = make_llnd(fi, OUT_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 128:
#line 1409 "gram1.y"
    { type_options = type_options | INOUT_BIT;  type_opt = INOUT_BIT;
              (yyval.ll_node) = make_llnd(fi, INOUT_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 129:
#line 1415 "gram1.y"
    { type_options = type_options | PUBLIC_BIT; 
              type_opt = PUBLIC_BIT;
              (yyval.ll_node) = make_llnd(fi, PUBLIC_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 130:
#line 1420 "gram1.y"
    { type_options =  type_options | PRIVATE_BIT;
               type_opt = PRIVATE_BIT;
              (yyval.ll_node) = make_llnd(fi, PRIVATE_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 131:
#line 1427 "gram1.y"
    { 
	      PTR_LLND q, r;
	      PTR_SYMB s;

              s = make_scalar((yyvsp[(7) - (7)].hash_entry), TYNULL, LOCAL);
	      s->attr = s->attr | type_opt;	
	      q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi, INTENT_STMT, SMNULL, r, (yyvsp[(4) - (7)].ll_node), LLNULL);
	    ;}
    break;

  case 132:
#line 1438 "gram1.y"
    { 
	      PTR_LLND q, r;
	      PTR_SYMB s;

              s = make_scalar((yyvsp[(3) - (3)].hash_entry), TYNULL, LOCAL);	
	      s->attr = s->attr | type_opt;
	      q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
  	    ;}
    break;

  case 133:
#line 1451 "gram1.y"
    { 
	      PTR_LLND q, r;
	      PTR_SYMB s;

              s = make_scalar((yyvsp[(4) - (4)].hash_entry), TYNULL, LOCAL);	
	      s->attr = s->attr | OPTIONAL_BIT;
	      q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi, OPTIONAL_STMT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 134:
#line 1462 "gram1.y"
    { 
	      PTR_LLND q, r;
	      PTR_SYMB s;

              s = make_scalar((yyvsp[(3) - (3)].hash_entry), TYNULL, LOCAL);	
	      s->attr = s->attr | OPTIONAL_BIT;
	      q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
  	    ;}
    break;

  case 135:
#line 1475 "gram1.y"
    { 
	      PTR_LLND r;
	      PTR_SYMB s;

              s = (yyvsp[(4) - (4)].ll_node)->entry.Template.symbol; 
              s->attr = s->attr | SAVE_BIT;
	      r = make_llnd(fi,EXPR_LIST, (yyvsp[(4) - (4)].ll_node), LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi, STATIC_STMT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 136:
#line 1485 "gram1.y"
    { 
	      PTR_LLND r;
	      PTR_SYMB s;

              s = (yyvsp[(3) - (3)].ll_node)->entry.Template.symbol;
              s->attr = s->attr | SAVE_BIT;
	      r = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
  	    ;}
    break;

  case 137:
#line 1498 "gram1.y"
    {
	      privateall = 1;
	      (yyval.bf_node) = get_bfnd(fi, PRIVATE_STMT, SMNULL, LLNULL, LLNULL, LLNULL);
	    ;}
    break;

  case 138:
#line 1503 "gram1.y"
    {
	      /*type_options = type_options | PRIVATE_BIT;*/
	      (yyval.bf_node) = get_bfnd(fi, PRIVATE_STMT, SMNULL, (yyvsp[(5) - (5)].ll_node), LLNULL, LLNULL);
            ;}
    break;

  case 139:
#line 1509 "gram1.y"
    {type_opt = PRIVATE_BIT;;}
    break;

  case 140:
#line 1513 "gram1.y"
    { 
	      (yyval.bf_node) = get_bfnd(fi, SEQUENCE_STMT, SMNULL, LLNULL, LLNULL, LLNULL);
            ;}
    break;

  case 141:
#line 1518 "gram1.y"
    {
	      /*saveall = YES;*/ /*14.03.03*/
	      (yyval.bf_node) = get_bfnd(fi, PUBLIC_STMT, SMNULL, LLNULL, LLNULL, LLNULL);
	    ;}
    break;

  case 142:
#line 1523 "gram1.y"
    {
	      /*type_options = type_options | PUBLIC_BIT;*/
	      (yyval.bf_node) = get_bfnd(fi, PUBLIC_STMT, SMNULL, (yyvsp[(5) - (5)].ll_node), LLNULL, LLNULL);
            ;}
    break;

  case 143:
#line 1529 "gram1.y"
    {type_opt = PUBLIC_BIT;;}
    break;

  case 144:
#line 1533 "gram1.y"
    {
	      type_options = 0;
              /* following block added by dbg */
	      ndim = 0;
	      attr_ndim = 0;
	      attr_dims = LLNULL;
	      /* end section added by dbg */
              (yyval.data_type) = make_type_node((yyvsp[(1) - (6)].data_type), (yyvsp[(4) - (6)].ll_node));
            ;}
    break;

  case 145:
#line 1543 "gram1.y"
    { PTR_TYPE t;

	      type_options = 0;
	      ndim = 0;
	      attr_ndim = 0;
	      attr_dims = LLNULL;
              t = lookup_type((yyvsp[(3) - (5)].hash_entry));
	      vartype = t;
	      (yyval.data_type) = make_type_node(t, LLNULL);
            ;}
    break;

  case 146:
#line 1556 "gram1.y"
    {opt_kwd_hedr = YES;;}
    break;

  case 147:
#line 1561 "gram1.y"
    { PTR_TYPE p;
	      PTR_LLND q;
	      PTR_SYMB s;
              s = (yyvsp[(2) - (2)].hash_entry)->id_attr;
	      if (s)
		   s->attr = (yyvsp[(1) - (2)].token);
	      else {
		p = undeftype ? global_unknown : impltype[*(yyvsp[(2) - (2)].hash_entry)->ident - 'a'];
                s = install_entry((yyvsp[(2) - (2)].hash_entry), SOFT);
		s->attr = (yyvsp[(1) - (2)].token);
                set_type(s, p, LOCAL);
	      }
	      q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(2) - (2)].hash_entry)->id_attr);
	      q = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi,ATTR_DECL, SMNULL, q, LLNULL, LLNULL);
	    ;}
    break;

  case 148:
#line 1580 "gram1.y"
    { PTR_TYPE p;
	      PTR_LLND q, r;
	      PTR_SYMB s;
	      int att;

	      att = (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1->entry.Template.ll_ptr1->
		    entry.Template.symbol->attr;
              s = (yyvsp[(3) - (3)].hash_entry)->id_attr;
	      if (s)
		   s->attr = att;
	      else {
		p = undeftype ? global_unknown : impltype[*(yyvsp[(3) - (3)].hash_entry)->ident - 'a'];
                s = install_entry((yyvsp[(3) - (3)].hash_entry), SOFT);
		s->attr = att;
                set_type(s, p, LOCAL);
	      }
	      q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].hash_entry)->id_attr);
	      for (r = (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1;
		   r->entry.list.next;
		   r = r->entry.list.next) ;
	      r->entry.list.next = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);

	    ;}
    break;

  case 149:
#line 1606 "gram1.y"
    { (yyval.token) = ATT_GLOBAL; ;}
    break;

  case 150:
#line 1608 "gram1.y"
    { (yyval.token) = ATT_CLUSTER; ;}
    break;

  case 151:
#line 1620 "gram1.y"
    {
/*		  varleng = ($1<0 || $1==TYLONG ? 0 : typesize[$1]); */
		  vartype = (yyvsp[(1) - (1)].data_type);
		;}
    break;

  case 152:
#line 1627 "gram1.y"
    { (yyval.data_type) = global_int; ;}
    break;

  case 153:
#line 1628 "gram1.y"
    { (yyval.data_type) = global_float; ;}
    break;

  case 154:
#line 1629 "gram1.y"
    { (yyval.data_type) = global_complex; ;}
    break;

  case 155:
#line 1630 "gram1.y"
    { (yyval.data_type) = global_double; ;}
    break;

  case 156:
#line 1631 "gram1.y"
    { (yyval.data_type) = global_dcomplex; ;}
    break;

  case 157:
#line 1632 "gram1.y"
    { (yyval.data_type) = global_bool; ;}
    break;

  case 158:
#line 1633 "gram1.y"
    { (yyval.data_type) = global_string; ;}
    break;

  case 159:
#line 1638 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 160:
#line 1640 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 161:
#line 1644 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, LEN_OP, (yyvsp[(2) - (3)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 162:
#line 1646 "gram1.y"
    { PTR_LLND l;

                 l = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL); 
                 l->entry.string_val = (char *)"*";
                 (yyval.ll_node) = make_llnd(fi, LEN_OP, l,l, SMNULL);
                ;}
    break;

  case 163:
#line 1653 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi, LEN_OP, (yyvsp[(3) - (4)].ll_node), (yyvsp[(3) - (4)].ll_node), SMNULL);;}
    break;

  case 164:
#line 1657 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 165:
#line 1659 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 166:
#line 1661 "gram1.y"
    { /*$$ = make_llnd(fi, PAREN_OP, $2, LLNULL, SMNULL);*/  (yyval.ll_node) = (yyvsp[(3) - (5)].ll_node);  ;}
    break;

  case 167:
#line 1669 "gram1.y"
    { if((yyvsp[(7) - (9)].ll_node)->variant==LENGTH_OP && (yyvsp[(3) - (9)].ll_node)->variant==(yyvsp[(7) - (9)].ll_node)->variant)
                 (yyvsp[(7) - (9)].ll_node)->variant=KIND_OP;
               (yyval.ll_node) = make_llnd(fi, CONS, (yyvsp[(3) - (9)].ll_node), (yyvsp[(7) - (9)].ll_node), SMNULL); 
             ;}
    break;

  case 168:
#line 1676 "gram1.y"
    { if(vartype->variant == T_STRING)
                (yyval.ll_node) = make_llnd(fi,LENGTH_OP,(yyvsp[(1) - (1)].ll_node),LLNULL,SMNULL);
              else
                (yyval.ll_node) = make_llnd(fi,KIND_OP,(yyvsp[(1) - (1)].ll_node),LLNULL,SMNULL);
            ;}
    break;

  case 169:
#line 1682 "gram1.y"
    { PTR_LLND l;
	      l = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
	      l->entry.string_val = (char *)"*";
              (yyval.ll_node) = make_llnd(fi,LENGTH_OP,l,LLNULL,SMNULL);
            ;}
    break;

  case 170:
#line 1688 "gram1.y"
    { /* $$ = make_llnd(fi, SPEC_PAIR, $2, LLNULL, SMNULL); */
	     char *q;
             q = (yyvsp[(1) - (2)].ll_node)->entry.string_val;
  	     if (strcmp(q, "len") == 0)
               (yyval.ll_node) = make_llnd(fi,LENGTH_OP,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);
             else
                (yyval.ll_node) = make_llnd(fi,KIND_OP,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);              
            ;}
    break;

  case 171:
#line 1697 "gram1.y"
    { PTR_LLND l;
	      l = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
	      l->entry.string_val = (char *)"*";
              (yyval.ll_node) = make_llnd(fi,LENGTH_OP,l,LLNULL,SMNULL);
            ;}
    break;

  case 172:
#line 1705 "gram1.y"
    {endioctl();;}
    break;

  case 173:
#line 1718 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 174:
#line 1720 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (2)].ll_node); ;}
    break;

  case 175:
#line 1722 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (2)].ll_node); ;}
    break;

  case 176:
#line 1726 "gram1.y"
    { PTR_SYMB s;
	      PTR_LLND q, r;
	      if(! (yyvsp[(5) - (5)].ll_node)) {
		err("No dimensions in DIMENSION statement", 42);
	      }
              if(statement_kind == 1) /*DVM-directive*/
                err("No shape specification", 65);                
	      s = make_array((yyvsp[(4) - (5)].hash_entry), TYNULL, (yyvsp[(5) - (5)].ll_node), ndim, LOCAL);
	      s->attr = s->attr | DIMENSION_BIT;
	      q = make_llnd(fi,ARRAY_REF, (yyvsp[(5) - (5)].ll_node), LLNULL, s);
	      s->type->entry.ar_decl.ranges = (yyvsp[(5) - (5)].ll_node);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi,DIM_STAT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 177:
#line 1741 "gram1.y"
    {  PTR_SYMB s;
	      PTR_LLND q, r;
	      if(! (yyvsp[(4) - (4)].ll_node)) {
		err("No dimensions in DIMENSION statement", 42);
	      }
	      s = make_array((yyvsp[(3) - (4)].hash_entry), TYNULL, (yyvsp[(4) - (4)].ll_node), ndim, LOCAL);
	      s->attr = s->attr | DIMENSION_BIT;
	      q = make_llnd(fi,ARRAY_REF, (yyvsp[(4) - (4)].ll_node), LLNULL, s);
	      s->type->entry.ar_decl.ranges = (yyvsp[(4) - (4)].ll_node);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (4)].bf_node)->entry.Template.ll_ptr1);
	;}
    break;

  case 178:
#line 1757 "gram1.y"
    {/* PTR_SYMB s;*/
	      PTR_LLND r;

	         /*if(!$5) {
		   err("No dimensions in ALLOCATABLE statement",305);		
	           }
	          s = make_array($4, TYNULL, $5, ndim, LOCAL);
	          s->attr = s->attr | ALLOCATABLE_BIT;
	          q = make_llnd(fi,ARRAY_REF, $5, LLNULL, s);
	          s->type->entry.ar_decl.ranges = $5;
                  r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
                */
              (yyvsp[(4) - (4)].ll_node)->entry.Template.symbol->attr = (yyvsp[(4) - (4)].ll_node)->entry.Template.symbol->attr | ALLOCATABLE_BIT;
	      r = make_llnd(fi,EXPR_LIST, (yyvsp[(4) - (4)].ll_node), LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi, ALLOCATABLE_STMT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 179:
#line 1775 "gram1.y"
    {  /*PTR_SYMB s;*/
	      PTR_LLND r;

	        /*  if(! $4) {
		      err("No dimensions in ALLOCATABLE statement",305);
		
	            }
	           s = make_array($3, TYNULL, $4, ndim, LOCAL);
	           s->attr = s->attr | ALLOCATABLE_BIT;
	           q = make_llnd(fi,ARRAY_REF, $4, LLNULL, s);
	           s->type->entry.ar_decl.ranges = $4;
	           r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
                */
              (yyvsp[(3) - (3)].ll_node)->entry.Template.symbol->attr = (yyvsp[(3) - (3)].ll_node)->entry.Template.symbol->attr | ALLOCATABLE_BIT;
              r = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	;}
    break;

  case 180:
#line 1795 "gram1.y"
    { PTR_SYMB s;
	      PTR_LLND  r;
           
	          /*  if(! $5) {
		      err("No dimensions in POINTER statement",306);	    
	              } 
	             s = make_array($4, TYNULL, $5, ndim, LOCAL);
	             s->attr = s->attr | POINTER_BIT;
	             q = make_llnd(fi,ARRAY_REF, $5, LLNULL, s);
	             s->type->entry.ar_decl.ranges = $5;
                   */

                  /*s = make_pointer( $4->entry.Template.symbol->parent, TYNULL, LOCAL);*/ /*17.02.03*/
                 /*$4->entry.Template.symbol->attr = $4->entry.Template.symbol->attr | POINTER_BIT;*/
              s = (yyvsp[(4) - (4)].ll_node)->entry.Template.symbol; /*17.02.03*/
              s->attr = s->attr | POINTER_BIT;
	      r = make_llnd(fi,EXPR_LIST, (yyvsp[(4) - (4)].ll_node), LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi, POINTER_STMT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 181:
#line 1815 "gram1.y"
    {  PTR_SYMB s;
	      PTR_LLND r;

     	        /*  if(! $4) {
	        	err("No dimensions in POINTER statement",306);
	            }
	           s = make_array($3, TYNULL, $4, ndim, LOCAL);
	           s->attr = s->attr | POINTER_BIT;
	           q = make_llnd(fi,ARRAY_REF, $4, LLNULL, s);
	           s->type->entry.ar_decl.ranges = $4;
                */

                /*s = make_pointer( $3->entry.Template.symbol->parent, TYNULL, LOCAL);*/ /*17.02.03*/
                /*$3->entry.Template.symbol->attr = $3->entry.Template.symbol->attr | POINTER_BIT;*/
              s = (yyvsp[(3) - (3)].ll_node)->entry.Template.symbol; /*17.02.03*/
              s->attr = s->attr | POINTER_BIT;
	      r = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	;}
    break;

  case 182:
#line 1837 "gram1.y"
    {/* PTR_SYMB s;*/
	      PTR_LLND r;


	     /* if(! $5) {
		err("No dimensions in TARGET statement",307);
	      }
	      s = make_array($4, TYNULL, $5, ndim, LOCAL);
	      s->attr = s->attr | TARGET_BIT;
	      q = make_llnd(fi,ARRAY_REF, $5, LLNULL, s);
	      s->type->entry.ar_decl.ranges = $5;
             */
              (yyvsp[(4) - (4)].ll_node)->entry.Template.symbol->attr = (yyvsp[(4) - (4)].ll_node)->entry.Template.symbol->attr | TARGET_BIT;
	      r = make_llnd(fi,EXPR_LIST, (yyvsp[(4) - (4)].ll_node), LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi, TARGET_STMT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 183:
#line 1854 "gram1.y"
    {  /*PTR_SYMB s;*/
	      PTR_LLND r;

	     /* if(! $4) {
		err("No dimensions in TARGET statement",307);
	      }
	      s = make_array($3, TYNULL, $4, ndim, LOCAL);
	      s->attr = s->attr | TARGET_BIT;
	      q = make_llnd(fi,ARRAY_REF, $4, LLNULL, s);
	      s->type->entry.ar_decl.ranges = $4;
              */
              (yyvsp[(3) - (3)].ll_node)->entry.Template.symbol->attr = (yyvsp[(3) - (3)].ll_node)->entry.Template.symbol->attr | TARGET_BIT;
	      r = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	;}
    break;

  case 184:
#line 1872 "gram1.y"
    { PTR_LLND p, q;

              p = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      q = make_llnd(fi,COMM_LIST, p, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi,COMM_STAT, SMNULL, q, LLNULL, LLNULL);
	    ;}
    break;

  case 185:
#line 1879 "gram1.y"
    { PTR_LLND p, q;

              p = make_llnd(fi,EXPR_LIST, (yyvsp[(4) - (4)].ll_node), LLNULL, SMNULL);
	      q = make_llnd(fi,COMM_LIST, p, LLNULL, (yyvsp[(3) - (4)].symbol));
	      (yyval.bf_node) = get_bfnd(fi,COMM_STAT, SMNULL, q, LLNULL, LLNULL);
	    ;}
    break;

  case 186:
#line 1886 "gram1.y"
    { PTR_LLND p, q;

              p = make_llnd(fi,EXPR_LIST, (yyvsp[(5) - (5)].ll_node), LLNULL, SMNULL);
	      q = make_llnd(fi,COMM_LIST, p, LLNULL, (yyvsp[(3) - (5)].symbol));
	      add_to_lowList(q, (yyvsp[(1) - (5)].bf_node)->entry.Template.ll_ptr1);
	    ;}
    break;

  case 187:
#line 1893 "gram1.y"
    { PTR_LLND p, q, r;

              p = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      q = make_llnd(fi,COMM_LIST, p, LLNULL, SMNULL);
	      for (r = (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1;
		   r->entry.list.next;
		   r = r->entry.list.next);
	      add_to_lowLevelList(p, r->entry.Template.ll_ptr1);
	    ;}
    break;

  case 188:
#line 1906 "gram1.y"
    { PTR_LLND q, r;

              q = make_llnd(fi,EXPR_LIST, (yyvsp[(4) - (4)].ll_node), LLNULL, SMNULL);
	      r = make_llnd(fi,NAMELIST_LIST, q, LLNULL, (yyvsp[(3) - (4)].symbol));
	      (yyval.bf_node) = get_bfnd(fi,NAMELIST_STAT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 189:
#line 1913 "gram1.y"
    { PTR_LLND q, r;

              q = make_llnd(fi,EXPR_LIST, (yyvsp[(5) - (5)].ll_node), LLNULL, SMNULL);
	      r = make_llnd(fi,NAMELIST_LIST, q, LLNULL, (yyvsp[(3) - (5)].symbol));
	      add_to_lowList(r, (yyvsp[(1) - (5)].bf_node)->entry.Template.ll_ptr1);
	    ;}
    break;

  case 190:
#line 1920 "gram1.y"
    { PTR_LLND q, r;

              q = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      for (r = (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1;
		   r->entry.list.next;
		   r = r->entry.list.next);
	      add_to_lowLevelList(q, r->entry.Template.ll_ptr1);
	    ;}
    break;

  case 191:
#line 1931 "gram1.y"
    { (yyval.symbol) =  make_local_entity((yyvsp[(2) - (3)].hash_entry), NAMELIST_NAME,global_default,LOCAL); ;}
    break;

  case 192:
#line 1935 "gram1.y"
    { (yyval.symbol) = NULL; /*make_common(look_up_sym("*"));*/ ;}
    break;

  case 193:
#line 1937 "gram1.y"
    { (yyval.symbol) = make_common((yyvsp[(2) - (3)].hash_entry)); ;}
    break;

  case 194:
#line 1942 "gram1.y"
    {  PTR_SYMB s;
	
	      if((yyvsp[(2) - (2)].ll_node)) {
		s = make_array((yyvsp[(1) - (2)].hash_entry), TYNULL, (yyvsp[(2) - (2)].ll_node), ndim, LOCAL);
                s->attr = s->attr | DIMENSION_BIT;
		s->type->entry.ar_decl.ranges = (yyvsp[(2) - (2)].ll_node);
		(yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(2) - (2)].ll_node), LLNULL, s);
	      }
	      else {
		s = make_scalar((yyvsp[(1) - (2)].hash_entry), TYNULL, LOCAL);	
		(yyval.ll_node) = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
	      }

          ;}
    break;

  case 195:
#line 1960 "gram1.y"
    { PTR_LLND p, q;
              PTR_SYMB s;

	      s = make_external((yyvsp[(4) - (4)].hash_entry), TYNULL);
	      s->attr = s->attr | EXTERNAL_BIT;
              q = make_llnd(fi, VAR_REF, LLNULL, LLNULL, s);
	      p = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi,EXTERN_STAT, SMNULL, p, LLNULL, LLNULL);
	    ;}
    break;

  case 196:
#line 1971 "gram1.y"
    { PTR_LLND p, q;
              PTR_SYMB s;

	      s = make_external((yyvsp[(3) - (3)].hash_entry), TYNULL);
	      s->attr = s->attr | EXTERNAL_BIT;
              p = make_llnd(fi, VAR_REF, LLNULL, LLNULL, s);
	      q = make_llnd(fi,EXPR_LIST, p, LLNULL, SMNULL);
	      add_to_lowLevelList(q, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	    ;}
    break;

  case 197:
#line 1983 "gram1.y"
    { PTR_LLND p, q;
              PTR_SYMB s;

	      s = make_intrinsic((yyvsp[(4) - (4)].hash_entry), TYNULL); /*make_function($3, TYNULL, NO);*/
	      s->attr = s->attr | INTRINSIC_BIT;
              q = make_llnd(fi, VAR_REF, LLNULL, LLNULL, s);
	      p = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi,INTRIN_STAT, SMNULL, p,
			     LLNULL, LLNULL);
	    ;}
    break;

  case 198:
#line 1995 "gram1.y"
    { PTR_LLND p, q;
              PTR_SYMB s;

	      s = make_intrinsic((yyvsp[(3) - (3)].hash_entry), TYNULL); /* make_function($3, TYNULL, NO);*/
	      s->attr = s->attr | INTRINSIC_BIT;
              p = make_llnd(fi, VAR_REF, LLNULL, LLNULL, s);
	      q = make_llnd(fi,EXPR_LIST, p, LLNULL, SMNULL);
	      add_to_lowLevelList(q, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	    ;}
    break;

  case 199:
#line 2009 "gram1.y"
    {
	      (yyval.bf_node) = get_bfnd(fi,EQUI_STAT, SMNULL, (yyvsp[(3) - (3)].ll_node),
			     LLNULL, LLNULL);
	    ;}
    break;

  case 200:
#line 2015 "gram1.y"
    { 
	      add_to_lowLevelList((yyvsp[(3) - (3)].ll_node), (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	    ;}
    break;

  case 201:
#line 2022 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,EQUI_LIST, (yyvsp[(2) - (3)].ll_node), LLNULL, SMNULL);
           ;}
    break;

  case 202:
#line 2028 "gram1.y"
    { PTR_LLND p;
	      p = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      (yyval.ll_node) = make_llnd(fi,EXPR_LIST, (yyvsp[(1) - (3)].ll_node), p, SMNULL);
	    ;}
    break;

  case 203:
#line 2034 "gram1.y"
    { PTR_LLND p;

	      p = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      add_to_lowLevelList(p, (yyvsp[(1) - (3)].ll_node));
	    ;}
    break;

  case 204:
#line 2042 "gram1.y"
    {  PTR_SYMB s;
           s=make_scalar((yyvsp[(1) - (1)].hash_entry),TYNULL,LOCAL);
           (yyval.ll_node) = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
           s->attr = s->attr | EQUIVALENCE_BIT;
            /*$$=$1; $$->entry.Template.symbol->attr = $$->entry.Template.symbol->attr | EQUIVALENCE_BIT; */
        ;}
    break;

  case 205:
#line 2049 "gram1.y"
    {  PTR_SYMB s;
           s=make_array((yyvsp[(1) - (4)].hash_entry),TYNULL,LLNULL,0,LOCAL);
           (yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(3) - (4)].ll_node), LLNULL, s);
           s->attr = s->attr | EQUIVALENCE_BIT;
            /*$$->entry.Template.symbol->attr = $$->entry.Template.symbol->attr | EQUIVALENCE_BIT; */
        ;}
    break;

  case 207:
#line 2068 "gram1.y"
    { PTR_LLND p;
              data_stat = NO;
	      p = make_llnd(fi,STMT_STR, LLNULL, LLNULL,
			    SMNULL);
              p->entry.string_val = copys(stmtbuf);
	      (yyval.bf_node) = get_bfnd(fi,DATA_DECL, SMNULL, p, LLNULL, LLNULL);
            ;}
    break;

  case 210:
#line 2082 "gram1.y"
    {data_stat = YES;;}
    break;

  case 211:
#line 2086 "gram1.y"
    {
	      if (parstate == OUTSIDE)
	         { PTR_BFND p;

		   p = get_bfnd(fi,PROG_HEDR,
                                make_program(look_up_sym("_MAIN")),
                                LLNULL, LLNULL, LLNULL);
		   set_blobs(p, global_bfnd, NEW_GROUP1);
	           add_scope_level(p, NO);
		   position = IN_PROC; 
	  	   /*parstate = INDCL;*/
                 }
	      if(parstate < INDCL)
		{
		  /* enddcl();*/
		  parstate = INDCL;
		}
	    ;}
    break;

  case 222:
#line 2131 "gram1.y"
    {;;}
    break;

  case 223:
#line 2135 "gram1.y"
    { (yyval.symbol)= make_scalar((yyvsp[(1) - (1)].hash_entry), TYNULL, LOCAL);;}
    break;

  case 224:
#line 2139 "gram1.y"
    { (yyval.symbol)= make_scalar((yyvsp[(1) - (1)].hash_entry), TYNULL, LOCAL); 
              (yyval.symbol)->attr = (yyval.symbol)->attr | DATA_BIT; 
            ;}
    break;

  case 225:
#line 2145 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DATA_SUBS, (yyvsp[(2) - (3)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 226:
#line 2149 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DATA_RANGE, (yyvsp[(2) - (5)].ll_node), (yyvsp[(4) - (5)].ll_node), SMNULL); ;}
    break;

  case 227:
#line 2153 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 228:
#line 2155 "gram1.y"
    { (yyval.ll_node) = add_to_lowLevelList((yyvsp[(3) - (3)].ll_node), (yyvsp[(1) - (3)].ll_node)); ;}
    break;

  case 229:
#line 2159 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 230:
#line 2161 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 231:
#line 2165 "gram1.y"
    {(yyval.ll_node)= make_llnd(fi, DATA_IMPL_DO, (yyvsp[(2) - (7)].ll_node), (yyvsp[(6) - (7)].ll_node), (yyvsp[(4) - (7)].symbol)); ;}
    break;

  case 232:
#line 2169 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 233:
#line 2171 "gram1.y"
    { (yyval.ll_node) = add_to_lowLevelList((yyvsp[(3) - (3)].ll_node), (yyvsp[(1) - (3)].ll_node)); ;}
    break;

  case 234:
#line 2175 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DATA_ELT, (yyvsp[(2) - (2)].ll_node), LLNULL, (yyvsp[(1) - (2)].symbol)); ;}
    break;

  case 235:
#line 2177 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DATA_ELT, (yyvsp[(2) - (2)].ll_node), LLNULL, (yyvsp[(1) - (2)].symbol)); ;}
    break;

  case 236:
#line 2179 "gram1.y"
    {
              (yyvsp[(2) - (3)].ll_node)->entry.Template.ll_ptr2 = (yyvsp[(3) - (3)].ll_node);
              (yyval.ll_node) = make_llnd(fi, DATA_ELT, (yyvsp[(2) - (3)].ll_node), LLNULL, (yyvsp[(1) - (3)].symbol)); 
            ;}
    break;

  case 237:
#line 2184 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DATA_ELT, (yyvsp[(1) - (1)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 251:
#line 2208 "gram1.y"
    {if((yyvsp[(2) - (6)].ll_node)->entry.Template.symbol->variant != TYPE_NAME)
               errstr("Undefined type %s",(yyvsp[(2) - (6)].ll_node)->entry.Template.symbol->ident,319); 
           ;}
    break;

  case 268:
#line 2253 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ICON_EXPR, (yyvsp[(1) - (1)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 269:
#line 2255 "gram1.y"
    {
              PTR_LLND p;

              p = intrinsic_op_node("+", UNARY_ADD_OP, (yyvsp[(2) - (2)].ll_node), LLNULL);
              (yyval.ll_node) = make_llnd(fi,ICON_EXPR, p, LLNULL, SMNULL);
            ;}
    break;

  case 270:
#line 2262 "gram1.y"
    {
              PTR_LLND p;
 
              p = intrinsic_op_node("-", MINUS_OP, (yyvsp[(2) - (2)].ll_node), LLNULL);
              (yyval.ll_node) = make_llnd(fi,ICON_EXPR, p, LLNULL, SMNULL);
            ;}
    break;

  case 271:
#line 2269 "gram1.y"
    {
              PTR_LLND p;
 
              p = intrinsic_op_node("+", ADD_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node));
              (yyval.ll_node) = make_llnd(fi,ICON_EXPR, p, LLNULL, SMNULL);
            ;}
    break;

  case 272:
#line 2276 "gram1.y"
    {
              PTR_LLND p;
 
              p = intrinsic_op_node("-", SUBT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node));
              (yyval.ll_node) = make_llnd(fi,ICON_EXPR, p, LLNULL, SMNULL);
            ;}
    break;

  case 273:
#line 2285 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 274:
#line 2287 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("*", MULT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 275:
#line 2289 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("/", DIV_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 276:
#line 2293 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 277:
#line 2295 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("**", EXP_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 278:
#line 2299 "gram1.y"
    {
              PTR_LLND p;

              p = make_llnd(fi,INT_VAL, LLNULL, LLNULL, SMNULL);
              p->entry.ival = atoi(yytext);
              p->type = global_int;
              (yyval.ll_node) = make_llnd(fi,EXPR_LIST, p, LLNULL, SMNULL);
            ;}
    break;

  case 279:
#line 2308 "gram1.y"
    {
              PTR_LLND p;
 
              p = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(1) - (1)].symbol));
              (yyval.ll_node) = make_llnd(fi,EXPR_LIST, p, LLNULL, SMNULL);
            ;}
    break;

  case 280:
#line 2315 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,EXPR_LIST, (yyvsp[(2) - (3)].ll_node), LLNULL, SMNULL);
            ;}
    break;

  case 281:
#line 2322 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,EXPR_LIST, (yyvsp[(1) - (1)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 282:
#line 2324 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 283:
#line 2328 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);
             (yyval.ll_node)->entry.Template.symbol->attr = (yyval.ll_node)->entry.Template.symbol->attr | SAVE_BIT;
           ;}
    break;

  case 284:
#line 2332 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,COMM_LIST, LLNULL, LLNULL, (yyvsp[(1) - (1)].symbol)); 
            (yyval.ll_node)->entry.Template.symbol->attr = (yyval.ll_node)->entry.Template.symbol->attr | SAVE_BIT;
          ;}
    break;

  case 285:
#line 2338 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(2) - (3)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 286:
#line 2340 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node), (yyvsp[(4) - (5)].ll_node), EXPR_LIST); ;}
    break;

  case 287:
#line 2344 "gram1.y"
    { as_op_kwd_ = YES; ;}
    break;

  case 288:
#line 2348 "gram1.y"
    { as_op_kwd_ = NO; ;}
    break;

  case 289:
#line 2353 "gram1.y"
    { 
             PTR_SYMB s; 
             s = make_scalar((yyvsp[(1) - (1)].hash_entry), TYNULL, LOCAL);	
	     s->attr = s->attr | type_opt;
	     (yyval.ll_node) = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
            ;}
    break;

  case 290:
#line 2360 "gram1.y"
    { PTR_SYMB s;
	      s = make_function((yyvsp[(3) - (4)].hash_entry), global_default, LOCAL);
	      s->variant = INTERFACE_NAME;
              s->attr = s->attr | type_opt;
              (yyval.ll_node) = make_llnd(fi,OPERATOR_OP, LLNULL, LLNULL, s);
	    ;}
    break;

  case 291:
#line 2367 "gram1.y"
    { PTR_SYMB s;
	      s = make_procedure(look_up_sym("="), LOCAL);
	      s->variant = INTERFACE_NAME;
              s->attr = s->attr | type_opt;
              (yyval.ll_node) = make_llnd(fi,ASSIGNMENT_OP, LLNULL, LLNULL, s);
	    ;}
    break;

  case 292:
#line 2377 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 293:
#line 2379 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 294:
#line 2383 "gram1.y"
    { PTR_SYMB p;

                /* The check if name and expr have compatible types has
                   not been done yet. */ 
		p = make_constant((yyvsp[(1) - (3)].hash_entry), TYNULL);
 	        p->attr = p->attr | PARAMETER_BIT;
                p->entry.const_value = (yyvsp[(3) - (3)].ll_node);
		(yyval.ll_node) = make_llnd(fi,CONST_REF, LLNULL, LLNULL, p);
	    ;}
    break;

  case 295:
#line 2395 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, MODULE_PROC_STMT, SMNULL, (yyvsp[(2) - (2)].ll_node), LLNULL, LLNULL); ;}
    break;

  case 296:
#line 2398 "gram1.y"
    { PTR_SYMB s;
 	      PTR_LLND q;

	      s = make_function((yyvsp[(1) - (1)].hash_entry), TYNULL, LOCAL);
	      s->variant = ROUTINE_NAME;
              q = make_llnd(fi, VAR_REF, LLNULL, LLNULL, s);
	      (yyval.ll_node) = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	    ;}
    break;

  case 297:
#line 2407 "gram1.y"
    { PTR_LLND p, q;
              PTR_SYMB s;

	      s = make_function((yyvsp[(3) - (3)].hash_entry), TYNULL, LOCAL);
	      s->variant = ROUTINE_NAME;
              p = make_llnd(fi, VAR_REF, LLNULL, LLNULL, s);
	      q = make_llnd(fi,EXPR_LIST, p, LLNULL, SMNULL);
	      add_to_lowLevelList(q, (yyvsp[(1) - (3)].ll_node));
	    ;}
    break;

  case 298:
#line 2420 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, USE_STMT, (yyvsp[(3) - (3)].symbol), LLNULL, LLNULL, LLNULL);
              /*add_scope_level($3->entry.Template.func_hedr, YES);*/ /*17.06.01*/
              copy_module_scope((yyvsp[(3) - (3)].symbol),LLNULL); /*17.03.03*/
              colon_flag = NO;
            ;}
    break;

  case 299:
#line 2426 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, USE_STMT, (yyvsp[(3) - (6)].symbol), (yyvsp[(6) - (6)].ll_node), LLNULL, LLNULL); 
              /*add_scope_level(module_scope, YES); *//* 17.06.01*/
              copy_module_scope((yyvsp[(3) - (6)].symbol),(yyvsp[(6) - (6)].ll_node)); /*17.03.03 */
              colon_flag = NO;
            ;}
    break;

  case 300:
#line 2432 "gram1.y"
    { PTR_LLND l;

	      l = make_llnd(fi, ONLY_NODE, LLNULL, LLNULL, SMNULL);
              (yyval.bf_node) = get_bfnd(fi, USE_STMT, (yyvsp[(3) - (6)].symbol), l, LLNULL, LLNULL);
            ;}
    break;

  case 301:
#line 2438 "gram1.y"
    { PTR_LLND l;

	      l = make_llnd(fi, ONLY_NODE, (yyvsp[(7) - (7)].ll_node), LLNULL, SMNULL);
              (yyval.bf_node) = get_bfnd(fi, USE_STMT, (yyvsp[(3) - (7)].symbol), l, LLNULL, LLNULL);
            ;}
    break;

  case 302:
#line 2446 "gram1.y"
    {
              if ((yyvsp[(1) - (1)].hash_entry)->id_attr == SMNULL)
	         warn1("Unknown module %s", (yyvsp[(1) - (1)].hash_entry)->ident,308);
              (yyval.symbol) = make_global_entity((yyvsp[(1) - (1)].hash_entry), MODULE_NAME, global_default, NO);
	      module_scope = (yyval.symbol)->entry.Template.func_hedr;
           
            ;}
    break;

  case 303:
#line 2456 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 304:
#line 2458 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 305:
#line 2462 "gram1.y"
    {  PTR_HASH oldhash;
	       PTR_SYMB oldsym, newsym;
	       PTR_LLND l, m;

	       oldhash = just_look_up_sym_in_scope(module_scope, (yyvsp[(3) - (3)].hash_entry)->ident);
	       if (oldhash == HSNULL) {
                  errstr("Unknown identifier %s", (yyvsp[(3) - (3)].hash_entry)->ident,309);
	          (yyval.ll_node)= LLNULL;
	       }
	       else {
	         oldsym = oldhash->id_attr;
	         newsym = make_local_entity((yyvsp[(1) - (3)].hash_entry), oldsym->variant, oldsym->type, LOCAL);
	         /* copies data in entry.Template structure and attr */
	         copy_sym_data(oldsym, newsym);	         
	           /*newsym->entry.Template.base_name = oldsym;*//*19.03.03*/
	  	 l = make_llnd(fi, VAR_REF, LLNULL, LLNULL, oldsym);
		 m = make_llnd(fi, VAR_REF, LLNULL, LLNULL, newsym);
		 (yyval.ll_node) = make_llnd(fi, RENAME_NODE, m, l, SMNULL);
 	      }
            ;}
    break;

  case 306:
#line 2483 "gram1.y"
    {  PTR_HASH oldhash;
	       PTR_SYMB oldsym, newsym;
	       PTR_LLND m;

	       oldhash = just_look_up_sym_in_scope(module_scope, (yyvsp[(1) - (1)].hash_entry)->ident);
	       if (oldhash == HSNULL) {
                  errstr("Unknown identifier %s.", (yyvsp[(1) - (1)].hash_entry)->ident,309);
	          (yyval.ll_node)= LLNULL;
	       }
	       else {
	         oldsym = oldhash->id_attr;
	         newsym = make_local_entity((yyvsp[(1) - (1)].hash_entry), oldsym->variant, oldsym->type,
LOCAL);
	         /* copies data in entry.Template structure and attr */
	         copy_sym_data(oldsym, newsym);	         
	           /*newsym->entry.Template.base_name = oldsym;*//*19.03.03*/
	  	/* l = make_llnd(fi, VAR_REF, LLNULL, LLNULL, oldsym);*/
		 m = make_llnd(fi, VAR_REF, LLNULL, LLNULL, newsym);
		 (yyval.ll_node) = make_llnd(fi, RENAME_NODE, m, LLNULL, oldsym);
 	      }
            ;}
    break;

  case 307:
#line 2508 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 308:
#line 2510 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 309:
#line 2514 "gram1.y"
    {  PTR_HASH oldhash;
	       PTR_SYMB oldsym, newsym;
	       PTR_LLND l, m;

	       oldhash = just_look_up_sym_in_scope(module_scope, (yyvsp[(3) - (3)].hash_entry)->ident);
	       if (oldhash == HSNULL) {
                  errstr("Unknown identifier %s", (yyvsp[(3) - (3)].hash_entry)->ident,309);
	          (yyval.ll_node)= LLNULL;
	       }
	       else {
	         oldsym = oldhash->id_attr;
	         newsym = make_local_entity((yyvsp[(1) - (3)].hash_entry), oldsym->variant, oldsym->type,
LOCAL);
	         /* copies data in entry.Template structure and attr */
	         copy_sym_data(oldsym, newsym);	         
	           /*newsym->entry.Template.base_name = oldsym;*//*19.03.03*/
	  	 l = make_llnd(fi, VAR_REF, LLNULL, LLNULL, oldsym);
		 m = make_llnd(fi, VAR_REF, LLNULL, LLNULL, newsym);
		 (yyval.ll_node) = make_llnd(fi, RENAME_NODE, m, l, SMNULL);
 	      }
            ;}
    break;

  case 310:
#line 2545 "gram1.y"
    { ndim = 0;	explicit_shape = 1; (yyval.ll_node) = LLNULL; ;}
    break;

  case 311:
#line 2547 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node); ;}
    break;

  case 312:
#line 2550 "gram1.y"
    { ndim = 0; explicit_shape = 1;;}
    break;

  case 313:
#line 2551 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,EXPR_LIST, (yyvsp[(2) - (2)].ll_node), LLNULL, SMNULL);
	      (yyval.ll_node)->type = global_default;
	    ;}
    break;

  case 314:
#line 2556 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 315:
#line 2560 "gram1.y"
    {
	      if(ndim == maxdim)
		err("Too many dimensions", 43);
	      else if(ndim < maxdim)
		(yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);
	      ++ndim;
	    ;}
    break;

  case 316:
#line 2568 "gram1.y"
    {
	      if(ndim == maxdim)
		err("Too many dimensions", 43);
	      else if(ndim < maxdim)
		(yyval.ll_node) = make_llnd(fi, DDOT, LLNULL, LLNULL, SMNULL);
	      ++ndim;
              explicit_shape = 0;
	    ;}
    break;

  case 317:
#line 2577 "gram1.y"
    {
	      if(ndim == maxdim)
		err("Too many dimensions", 43);
	      else if(ndim < maxdim)
		(yyval.ll_node) = make_llnd(fi,DDOT, (yyvsp[(1) - (2)].ll_node), LLNULL, SMNULL);
	      ++ndim;
              explicit_shape = 0;
	    ;}
    break;

  case 318:
#line 2586 "gram1.y"
    {
	      if(ndim == maxdim)
		err("Too many dimensions", 43);
	      else if(ndim < maxdim)
		(yyval.ll_node) = make_llnd(fi,DDOT, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);
	      ++ndim;
	    ;}
    break;

  case 319:
#line 2596 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,STAR_RANGE, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->type = global_default;
              explicit_shape = 0;
	    ;}
    break;

  case 321:
#line 2605 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 322:
#line 2607 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 323:
#line 2611 "gram1.y"
    {PTR_LABEL p;
	     p = make_label_node(fi,convci(yyleng, yytext));
	     p->scope = cur_scope();
	     (yyval.ll_node) = make_llnd_label(fi,LABEL_REF, p);
	  ;}
    break;

  case 324:
#line 2619 "gram1.y"
    { /*PTR_LLND l;*/

          /*   l = make_llnd(fi, EXPR_LIST, $3, LLNULL, SMNULL);*/
             (yyval.bf_node) = get_bfnd(fi,IMPL_DECL, SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL);
             redefine_func_arg_type();
           ;}
    break;

  case 325:
#line 2634 "gram1.y"
    { /*undeftype = YES;
	    setimpl(TYNULL, (int)'a', (int)'z'); FB COMMENTED---> NOT QUITE RIGHT BUT AVOID PB WITH COMMON*/
	    (yyval.bf_node) = get_bfnd(fi,IMPL_DECL, SMNULL, LLNULL, LLNULL, LLNULL);
	  ;}
    break;

  case 326:
#line 2641 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 327:
#line 2643 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 328:
#line 2647 "gram1.y"
    { 

            (yyval.ll_node) = make_llnd(fi, IMPL_TYPE, (yyvsp[(3) - (4)].ll_node), LLNULL, SMNULL);
            (yyval.ll_node)->type = vartype;
          ;}
    break;

  case 329:
#line 2662 "gram1.y"
    { implkwd = YES; ;}
    break;

  case 330:
#line 2663 "gram1.y"
    { vartype = (yyvsp[(2) - (2)].data_type); ;}
    break;

  case 331:
#line 2667 "gram1.y"
    { (yyval.data_type) = (yyvsp[(2) - (2)].data_type); ;}
    break;

  case 332:
#line 2669 "gram1.y"
    { (yyval.data_type) = (yyvsp[(1) - (1)].data_type);;}
    break;

  case 333:
#line 2681 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 334:
#line 2683 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 335:
#line 2687 "gram1.y"
    {
	      setimpl(vartype, (int)(yyvsp[(1) - (1)].charv), (int)(yyvsp[(1) - (1)].charv));
	      (yyval.ll_node) = make_llnd(fi,CHAR_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.cval = (yyvsp[(1) - (1)].charv);
	    ;}
    break;

  case 336:
#line 2693 "gram1.y"
    { PTR_LLND p,q;
	      
	      setimpl(vartype, (int)(yyvsp[(1) - (3)].charv), (int)(yyvsp[(3) - (3)].charv));
	      p = make_llnd(fi,CHAR_VAL, LLNULL, LLNULL, SMNULL);
	      p->entry.cval = (yyvsp[(1) - (3)].charv);
	      q = make_llnd(fi,CHAR_VAL, LLNULL, LLNULL, SMNULL);
	      q->entry.cval = (yyvsp[(3) - (3)].charv);
	      (yyval.ll_node)= make_llnd(fi,DDOT, p, q, SMNULL);
	    ;}
    break;

  case 337:
#line 2705 "gram1.y"
    {
	      if(yyleng!=1 || yytext[0]<'a' || yytext[0]>'z')
		{
		  err("IMPLICIT item must be single letter", 37);
		  (yyval.charv) = '\0';
		}
	      else (yyval.charv) = yytext[0];
	    ;}
    break;

  case 338:
#line 2716 "gram1.y"
    {
	      if (parstate == OUTSIDE)
	         { PTR_BFND p;

		   p = get_bfnd(fi,PROG_HEDR,
                                make_program(look_up_sym("_MAIN")),
                                LLNULL, LLNULL, LLNULL);
		   set_blobs(p, global_bfnd, NEW_GROUP1);
	           add_scope_level(p, NO);
		   position = IN_PROC; 
	  	   parstate = INSIDE;
                 }
	  
	    ;}
    break;

  case 339:
#line 2733 "gram1.y"
    { switch(parstate)
		{
                case OUTSIDE:  
			{ PTR_BFND p;

			  p = get_bfnd(fi,PROG_HEDR,
                                       make_program(look_up_sym("_MAIN")),
                                       LLNULL, LLNULL, LLNULL);
			  set_blobs(p, global_bfnd, NEW_GROUP1);
			  add_scope_level(p, NO);
			  position = IN_PROC; 
	  		  parstate = INDCL; }
	                  break;
                case INSIDE:    parstate = INDCL;
                case INDCL:     break;

                case INDATA:
                         /*  err(
                     "Statement order error: declaration after DATA or function statement", 
                                 29);*/
                              break;

                default:
                           err("Declaration among executables", 30);
                }
        ;}
    break;

  case 342:
#line 2771 "gram1.y"
    { (yyval.ll_node) = LLNULL; endioctl(); ;}
    break;

  case 343:
#line 2773 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);  endioctl();;}
    break;

  case 344:
#line 2777 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 345:
#line 2779 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 346:
#line 2781 "gram1.y"
    { PTR_LLND l;
	      l = make_llnd(fi, KEYWORD_ARG, (yyvsp[(1) - (2)].ll_node), (yyvsp[(2) - (2)].ll_node), SMNULL);
	      l->type = (yyvsp[(2) - (2)].ll_node)->type;
              (yyval.ll_node) = l; 
	    ;}
    break;

  case 347:
#line 2792 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(2) - (2)].ll_node), LLNULL, EXPR_LIST);
              endioctl(); 
            ;}
    break;

  case 348:
#line 2796 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node), EXPR_LIST);
              endioctl(); 
            ;}
    break;

  case 349:
#line 2802 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 350:
#line 2804 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 351:
#line 2808 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 352:
#line 2810 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node); ;}
    break;

  case 353:
#line 2812 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 354:
#line 2816 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 355:
#line 2818 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 356:
#line 2822 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 357:
#line 2824 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("+", ADD_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 358:
#line 2826 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("-", SUBT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 359:
#line 2828 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("*", MULT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 360:
#line 2830 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("/", DIV_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 361:
#line 2832 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("**", EXP_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 362:
#line 2834 "gram1.y"
    { (yyval.ll_node) = defined_op_node((yyvsp[(1) - (2)].hash_entry), (yyvsp[(2) - (2)].ll_node), LLNULL); ;}
    break;

  case 363:
#line 2836 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("+", UNARY_ADD_OP, (yyvsp[(2) - (2)].ll_node), LLNULL); ;}
    break;

  case 364:
#line 2838 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("-", MINUS_OP, (yyvsp[(2) - (2)].ll_node), LLNULL); ;}
    break;

  case 365:
#line 2840 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".eq.", EQ_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 366:
#line 2842 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".gt.", GT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 367:
#line 2844 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".lt.", LT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 368:
#line 2846 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".ge.", GTEQL_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 369:
#line 2848 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".ge.", LTEQL_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 370:
#line 2850 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".ne.", NOTEQL_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 371:
#line 2852 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".eqv.", EQV_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 372:
#line 2854 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".neqv.", NEQV_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 373:
#line 2856 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".xor.", XOR_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 374:
#line 2858 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".or.", OR_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 375:
#line 2860 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".and.", AND_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 376:
#line 2862 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".not.", NOT_OP, (yyvsp[(2) - (2)].ll_node), LLNULL); ;}
    break;

  case 377:
#line 2864 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("//", CONCAT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 378:
#line 2866 "gram1.y"
    { (yyval.ll_node) = defined_op_node((yyvsp[(2) - (3)].hash_entry), (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 379:
#line 2869 "gram1.y"
    { (yyval.token) = ADD_OP; ;}
    break;

  case 380:
#line 2870 "gram1.y"
    { (yyval.token) = SUBT_OP; ;}
    break;

  case 381:
#line 2882 "gram1.y"
    { PTR_SYMB s;
	      PTR_TYPE t;
	     /* PTR_LLND l;*/

       	      if (!(s = (yyvsp[(1) - (1)].hash_entry)->id_attr))
              {
	         s = make_scalar((yyvsp[(1) - (1)].hash_entry), TYNULL, LOCAL);
	     	 s->decl = SOFT;
	      } 
	
	      switch (s->variant)
              {
	      case CONST_NAME:
		   (yyval.ll_node) = make_llnd(fi,CONST_REF,LLNULL,LLNULL, s);
		   t = s->type;
	           if ((t != TYNULL) &&
                       ((t->variant == T_ARRAY) ||  (t->variant == T_STRING) ))
                                 (yyval.ll_node)->variant = ARRAY_REF;

                   (yyval.ll_node)->type = t;
	           break;
	      case DEFAULT:   /* if common region with same name has been
                                 declared. */
		   s = make_scalar((yyvsp[(1) - (1)].hash_entry), TYNULL, LOCAL);
	     	   s->decl = SOFT;

	      case VARIABLE_NAME:
                   (yyval.ll_node) = make_llnd(fi,VAR_REF,LLNULL,LLNULL, s);
	           t = s->type;
	           if (t != TYNULL) {
                     if ((t->variant == T_ARRAY) ||  (t->variant == T_STRING) ||
                         ((t->variant == T_POINTER) && (t->entry.Template.base_type->variant == T_ARRAY) ) )
                         (yyval.ll_node)->variant = ARRAY_REF;

/*  	              if (t->variant == T_DERIVED_TYPE)
                         $$->variant = RECORD_REF; */
	           }
                   (yyval.ll_node)->type = t;
	           break;
	      case TYPE_NAME:
  	           (yyval.ll_node) = make_llnd(fi,TYPE_REF,LLNULL,LLNULL, s);
	           (yyval.ll_node)->type = s->type;
	           break;
	      case INTERFACE_NAME:
  	           (yyval.ll_node) = make_llnd(fi, INTERFACE_REF,LLNULL,LLNULL, s);
	           (yyval.ll_node)->type = s->type;
	           break;
              case FUNCTION_NAME:
                   if(isResultVar(s)) {
                     (yyval.ll_node) = make_llnd(fi,VAR_REF,LLNULL,LLNULL, s);
	             t = s->type;
	             if (t != TYNULL) {
                       if ((t->variant == T_ARRAY) ||  (t->variant == T_STRING) ||
                         ((t->variant == T_POINTER) && (t->entry.Template.base_type->variant == T_ARRAY) ) )
                         (yyval.ll_node)->variant = ARRAY_REF;
	             }
                     (yyval.ll_node)->type = t;
	             break;
                   }                                        
	      default:
  	           (yyval.ll_node) = make_llnd(fi,VAR_REF,LLNULL,LLNULL, s);
	           (yyval.ll_node)->type = s->type;
	           break;
	      }
             /* if ($$->variant == T_POINTER) {
	         l = $$;
	         $$ = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	         $$->type = l->type->entry.Template.base_type;
	      }
              */ /*11.02.03*/
           ;}
    break;

  case 382:
#line 2956 "gram1.y"
    { PTR_SYMB  s;
	      (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); 
              s= (yyval.ll_node)->entry.Template.symbol;
              if ((((yyvsp[(1) - (1)].ll_node)->variant == VAR_REF) || ((yyvsp[(1) - (1)].ll_node)->variant == ARRAY_REF))  && (s->scope !=cur_scope()))  /*global_bfnd*/
              {
	          if(((s->variant == FUNCTION_NAME) && (!isResultVar(s))) || (s->variant == PROCEDURE_NAME) || (s->variant == ROUTINE_NAME))
                  { s = (yyval.ll_node)->entry.Template.symbol =  make_scalar(s->parent, TYNULL, LOCAL);
		    (yyval.ll_node)->type = s->type;  
		  }
              }
            ;}
    break;

  case 383:
#line 2968 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 384:
#line 2970 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 385:
#line 2974 "gram1.y"
    { int num_triplets;
	      PTR_SYMB s;  /*, sym;*/
	      PTR_LLND l;
	
	      l = (yyvsp[(1) - (5)].ll_node);
	      s = (yyvsp[(1) - (5)].ll_node)->entry.Template.symbol;
            
	      /* Handle variable to function conversion. */
	      if (((yyvsp[(1) - (5)].ll_node)->variant == VAR_REF) && 
	          (((s->variant == VARIABLE_NAME) && (s->type) &&
                    (s->type->variant != T_ARRAY)) ||
  	            (s->variant == ROUTINE_NAME))) {
	        s = (yyvsp[(1) - (5)].ll_node)->entry.Template.symbol =  make_function(s->parent, TYNULL, NO);
	        (yyvsp[(1) - (5)].ll_node)->variant = FUNC_CALL;
              }
	      if (((yyvsp[(1) - (5)].ll_node)->variant == VAR_REF) && (s->variant == FUNCTION_NAME)) { 
                if(isResultVar(s))
	          (yyvsp[(1) - (5)].ll_node)->variant = ARRAY_REF;
                else
                  (yyvsp[(1) - (5)].ll_node)->variant = FUNC_CALL;
              }
	      if (((yyvsp[(1) - (5)].ll_node)->variant == VAR_REF) && (s->variant == PROGRAM_NAME)) {
                 errstr("The name '%s' is invalid in this context",s->ident,285);
                 (yyvsp[(1) - (5)].ll_node)->variant = FUNC_CALL;
              }
              l = (yyvsp[(1) - (5)].ll_node);
	      num_triplets = is_array_section_ref((yyvsp[(4) - (5)].ll_node));
	      switch ((yyvsp[(1) - (5)].ll_node)->variant)
              {
	      case TYPE_REF:
                   (yyvsp[(1) - (5)].ll_node)->variant = STRUCTURE_CONSTRUCTOR;                  
                   (yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
                   (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node);
                   (yyval.ll_node)->type =  lookup_type(s->parent); 
	          /* $$ = make_llnd(fi, STRUCTURE_CONSTRUCTOR, $1, $4, SMNULL);
	           $$->type = $1->type;*//*18.02.03*/
	           break;
	      case INTERFACE_REF:
	       /*  sym = resolve_overloading(s, $4);
	           if (sym != SMNULL)
	  	   {
	              l = make_llnd(fi, FUNC_CALL, $4, LLNULL, sym);
	              l->type = sym->type;
	              $$ = $1; $$->variant = OVERLOADED_CALL;
	              $$->entry.Template.ll_ptr1 = l;
	              $$->type = sym->type;
	           }
	           else {
	             errstr("can't resolve call %s", s->ident,310);
	           }
	           break;
                 */ /*podd 01.02.03*/

                   (yyvsp[(1) - (5)].ll_node)->variant = FUNC_CALL;

	      case FUNC_CALL:
                   (yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
                   (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node);
                   if(s->type) 
                     (yyval.ll_node)->type = s->type;
                   else
                     (yyval.ll_node)->type = global_default;
	           late_bind_if_needed((yyval.ll_node));
	           break;
	      case DEREF_OP:
              case ARRAY_REF:
	           /* array element */
	           if (num_triplets == 0) {
                       if ((yyvsp[(4) - (5)].ll_node) == LLNULL) {
                           s = (yyvsp[(1) - (5)].ll_node)->entry.Template.symbol = make_function(s->parent, TYNULL, NO);
                           s->entry.func_decl.num_output = 1;
                           (yyvsp[(1) - (5)].ll_node)->variant = FUNC_CALL;
                           (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node);
                       } else if ((yyvsp[(1) - (5)].ll_node)->type->variant == T_STRING) {
                           PTR_LLND temp = (yyvsp[(4) - (5)].ll_node);
                           int num_input = 0;

                           while (temp) {
                             ++num_input;
                             temp = temp->entry.Template.ll_ptr2;
                           }
                           (yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
                           s = (yyvsp[(1) - (5)].ll_node)->entry.Template.symbol = make_function(s->parent, TYNULL, NO);
                           s->entry.func_decl.num_output = 1;
                           s->entry.func_decl.num_input = num_input;
                           (yyvsp[(1) - (5)].ll_node)->variant = FUNC_CALL;
                           (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node);
                       } else {
       	                   (yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
	                   (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node);
                           (yyval.ll_node)->type = (yyvsp[(1) - (5)].ll_node)->type->entry.ar_decl.base_type;
                       }
                   }
                   /* substring */
	           else if ((num_triplets == 1) && 
                            ((yyvsp[(1) - (5)].ll_node)->type->variant == T_STRING)) {
    	           /*
                     $1->entry.Template.ll_ptr1 = $4;
	             $$ = $1; $$->type = global_string;
                   */
	                  (yyval.ll_node) = make_llnd(fi, 
			  ARRAY_OP, LLNULL, LLNULL, SMNULL);
    	                  (yyval.ll_node)->entry.Template.ll_ptr1 = (yyvsp[(1) - (5)].ll_node);
       	                  (yyval.ll_node)->entry.Template.ll_ptr2 = (yyvsp[(4) - (5)].ll_node)->entry.Template.ll_ptr1;
	                  (yyval.ll_node)->type = global_string;
                   }           
                   /* array section */
                   else {
    	             (yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
	             (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node); (yyval.ll_node)->type = make_type(fi, T_ARRAY);
                     (yyval.ll_node)->type->entry.ar_decl.base_type = (yyvsp[(1) - (5)].ll_node)->type;
	             (yyval.ll_node)->type->entry.ar_decl.num_dimensions = num_triplets;
                   }
	           break;
	      default:
                    if((yyvsp[(1) - (5)].ll_node)->entry.Template.symbol)
                      errstr("Can't subscript %s",(yyvsp[(1) - (5)].ll_node)->entry.Template.symbol->ident, 44);
                    else
	              err("Can't subscript",44);
             }
             /*if ($$->variant == T_POINTER) {
	        l = $$;
	        $$ = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	        $$->type = l->type->entry.Template.base_type;
	     }
              */  /*11.02.03*/

	     endioctl(); 
           ;}
    break;

  case 386:
#line 3104 "gram1.y"
    { int num_triplets;
	      PTR_SYMB s;
	      PTR_LLND l;

	      s = (yyvsp[(1) - (6)].ll_node)->entry.Template.symbol;
/*              if ($1->type->variant == T_POINTER) {
	         l = $1;
	         $1 = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	         $1->type = l->type->entry.Template.base_type;
	      } */
	      if (((yyvsp[(1) - (6)].ll_node)->type->variant != T_ARRAY) ||
                  ((yyvsp[(1) - (6)].ll_node)->type->entry.ar_decl.base_type->variant != T_STRING)) {
	         errstr("Can't take substring of %s", s->ident, 45);
              }
              else {
  	        num_triplets = is_array_section_ref((yyvsp[(4) - (6)].ll_node));
	           /* array element */
                if (num_triplets == 0) {
                   (yyvsp[(1) - (6)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (6)].ll_node);
                  /* $1->entry.Template.ll_ptr2 = $6;*/
	          /* $$ = $1;*/
                   l=(yyvsp[(1) - (6)].ll_node);
                   /*$$->type = $1->type->entry.ar_decl.base_type;*/
                   l->type = (yyvsp[(1) - (6)].ll_node)->type->entry.ar_decl.base_type;
                }
                /* array section */
                else {
    	           (yyvsp[(1) - (6)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (6)].ll_node);
    	           /*$1->entry.Template.ll_ptr2 = $6;
	           $$ = $1; $$->type = make_type(fi, T_ARRAY);
                   $$->type->entry.ar_decl.base_type = $1->type;
	           $$->type->entry.ar_decl.num_dimensions = num_triplets;
                  */
                   l = (yyvsp[(1) - (6)].ll_node); l->type = make_type(fi, T_ARRAY);
                   l->type->entry.ar_decl.base_type = (yyvsp[(1) - (6)].ll_node)->type;
	           l->type->entry.ar_decl.num_dimensions = num_triplets;
               }
                (yyval.ll_node) = make_llnd(fi, ARRAY_OP, l, (yyvsp[(6) - (6)].ll_node), SMNULL);
	        (yyval.ll_node)->type = l->type;
              
              /* if ($$->variant == T_POINTER) {
	          l = $$;
	          $$ = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	          $$->type = l->type->entry.Template.base_type;
	       }
               */  /*11.02.03*/
             }
             endioctl();
          ;}
    break;

  case 387:
#line 3154 "gram1.y"
    {  int num_triplets;
	      PTR_LLND l,l1,l2;
              PTR_TYPE tp;

         /*   if ($1->variant == T_POINTER) {
	         l = $1;
	         $1 = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	         $1->type = l->type->entry.Template.base_type;
	      } */

              num_triplets = is_array_section_ref((yyvsp[(3) - (4)].ll_node));
              (yyval.ll_node) = (yyvsp[(1) - (4)].ll_node);
              l2 = (yyvsp[(1) - (4)].ll_node)->entry.Template.ll_ptr2;  
              l1 = (yyvsp[(1) - (4)].ll_node)->entry.Template.ll_ptr1;                
              if(l2 && l2->type->variant == T_STRING)/*substring*/
                if(num_triplets == 1){
	           l = make_llnd(fi, ARRAY_OP, LLNULL, LLNULL, SMNULL);
    	           l->entry.Template.ll_ptr1 = l2;
       	           l->entry.Template.ll_ptr2 = (yyvsp[(3) - (4)].ll_node)->entry.Template.ll_ptr1;
	           l->type = global_string; 
                   (yyval.ll_node)->entry.Template.ll_ptr2 = l;                                          
                } else
                   err("Can't subscript",44);
              else if (l2 && l2->type->variant == T_ARRAY) {
                 if(num_triplets > 0) { /*array section*/
                   tp = make_type(fi,T_ARRAY);
                   tp->entry.ar_decl.base_type = (yyvsp[(1) - (4)].ll_node)->type->entry.ar_decl.base_type;
                   tp->entry.ar_decl.num_dimensions = num_triplets;
                   (yyval.ll_node)->type = tp;
                   l2->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);
                   l2->type = (yyval.ll_node)->type;   
                  }                 
                 else {  /*array element*/
                   l2->type = l2->type->entry.ar_decl.base_type;
                   l2->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);   
                   if(l1->type->variant != T_ARRAY)  
                     (yyval.ll_node)->type = l2->type;
                 }
              } else 
                   {err("Can't subscript",44); /*fprintf(stderr,"%d  %d",$1->variant,l2);*/}
                   /*errstr("Can't subscript %s",l2->entry.Template.symbol->ident,441);*/
         ;}
    break;

  case 388:
#line 3198 "gram1.y"
    { int num_triplets;
	      PTR_LLND l,q;

          /*     if ($1->variant == T_POINTER) {
	         l = $1;
	         $1 = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	         $1->type = l->type->entry.Template.base_type;
	      } */

              (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node);
	      if (((yyvsp[(1) - (5)].ll_node)->type->variant != T_ARRAY) &&
                  ((yyvsp[(1) - (5)].ll_node)->type->entry.ar_decl.base_type->variant != T_STRING)) {
	         err("Can't take substring",45);
              }
              else {
  	        num_triplets = is_array_section_ref((yyvsp[(3) - (5)].ll_node));
                l = (yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr2;
                if(l) {
                /* array element */
	        if (num_triplets == 0) {
                   l->entry.Template.ll_ptr1 = (yyvsp[(3) - (5)].ll_node);       	           
                   l->type = global_string;
                }
                /* array section */
                else {	
    	             l->entry.Template.ll_ptr1 = (yyvsp[(3) - (5)].ll_node);
	             l->type = make_type(fi, T_ARRAY);
                     l->type->entry.ar_decl.base_type = global_string;
	             l->type->entry.ar_decl.num_dimensions = num_triplets;
                }
	        q = make_llnd(fi, ARRAY_OP, l, (yyvsp[(5) - (5)].ll_node), SMNULL);
	        q->type = l->type;
                (yyval.ll_node)->entry.Template.ll_ptr2 = q;
                if((yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr1->type->variant != T_ARRAY)  
                     (yyval.ll_node)->type = q->type;
               }
             }
          ;}
    break;

  case 389:
#line 3240 "gram1.y"
    { PTR_TYPE t;
	      PTR_SYMB  field;
	    /*  PTR_BFND at_scope;*/
              PTR_LLND l;


/*              if ($1->variant == T_POINTER) {
	         l = $1;
	         $1 = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	         $1->type = l->type->entry.Template.base_type;
	      } */

	      t = (yyvsp[(1) - (3)].ll_node)->type; 
	      
	      if (( ( ((yyvsp[(1) - (3)].ll_node)->variant == VAR_REF) 
	          ||  ((yyvsp[(1) - (3)].ll_node)->variant == CONST_REF) 
                  ||  ((yyvsp[(1) - (3)].ll_node)->variant == ARRAY_REF)
                  ||  ((yyvsp[(1) - (3)].ll_node)->variant == RECORD_REF)) && (t->variant == T_DERIVED_TYPE)) 
	          ||((((yyvsp[(1) - (3)].ll_node)->variant == ARRAY_REF) || ((yyvsp[(1) - (3)].ll_node)->variant == RECORD_REF)) && (t->variant == T_ARRAY) &&
                      (t = t->entry.ar_decl.base_type) && (t->variant == T_DERIVED_TYPE))) 
                {
	         if ((field = component(t->name, yytext))) {                   
	            l =  make_llnd(fi, VAR_REF, LLNULL, LLNULL, field);
                    l->type = field->type;
                    if(field->type->variant == T_ARRAY || field->type->variant == T_STRING)
                      l->variant = ARRAY_REF; 
                    (yyval.ll_node) = make_llnd(fi, RECORD_REF, (yyvsp[(1) - (3)].ll_node), l, SMNULL);
                    if((yyvsp[(1) - (3)].ll_node)->type->variant != T_ARRAY)
                       (yyval.ll_node)->type = field->type;
                    else {
                       (yyval.ll_node)->type = make_type(fi,T_ARRAY);
                       if(field->type->variant != T_ARRAY) 
	                 (yyval.ll_node)->type->entry.ar_decl.base_type = field->type;
                       else
                         (yyval.ll_node)->type->entry.ar_decl.base_type = field->type->entry.ar_decl.base_type;
	               (yyval.ll_node)->type->entry.ar_decl.num_dimensions = t->entry.ar_decl.num_dimensions;
                       }
                 }
                  else  
                    errstr("Illegal component  %s", yytext,311);
              }                     
               else 
                    errstr("Can't take component  %s", yytext,311);
             ;}
    break;

  case 390:
#line 3297 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 391:
#line 3299 "gram1.y"
    {(yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 392:
#line 3301 "gram1.y"
    {  int num_triplets;
               
              /* PTR_LLND l;*/
	      if ((yyvsp[(1) - (5)].ll_node)->type->variant == T_ARRAY)
              {
  	         num_triplets = is_array_section_ref((yyvsp[(4) - (5)].ll_node));
	         /* array element */
	         if (num_triplets == 0) {
       	            (yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
       	            (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node);
                    (yyval.ll_node)->type = (yyvsp[(1) - (5)].ll_node)->type->entry.ar_decl.base_type;
                 }
                 /* substring */
	       /*  else if ((num_triplets == 1) && 
                          ($1->type->variant == T_STRING)) {
    	                  $1->entry.Template.ll_ptr1 = $4;
	                  $$ = $1; $$->type = global_string;
                 }   */ /*podd*/        
                 /* array section */
                 else {
    	             (yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
	             (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node); (yyval.ll_node)->type = make_type(fi, T_ARRAY);
                     (yyval.ll_node)->type->entry.ar_decl.base_type = (yyvsp[(1) - (5)].ll_node)->type;
	             (yyval.ll_node)->type->entry.ar_decl.num_dimensions = num_triplets;
                 }
             } 
             else err("can't subscript",44);

            /* if ($$->variant == T_POINTER) {
	        l = $$;
	        $$ = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	        $$->type = l->type->entry.Template.base_type;
	     }
             */  /*11.02.03*/

            endioctl();
           ;}
    break;

  case 393:
#line 3340 "gram1.y"
    {  int num_triplets;
	      PTR_LLND l,l1,l2;

         /*   if ($1->variant == T_POINTER) {
	         l = $1;
	         $1 = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	         $1->type = l->type->entry.Template.base_type;
	      } */

              num_triplets = is_array_section_ref((yyvsp[(3) - (4)].ll_node));
              (yyval.ll_node) = (yyvsp[(1) - (4)].ll_node);
              l2 = (yyvsp[(1) - (4)].ll_node)->entry.Template.ll_ptr2;  
              l1 = (yyvsp[(1) - (4)].ll_node)->entry.Template.ll_ptr1;                
              if(l2 && l2->type->variant == T_STRING)/*substring*/
                if(num_triplets == 1){
	           l = make_llnd(fi, ARRAY_OP, LLNULL, LLNULL, SMNULL);
    	           l->entry.Template.ll_ptr1 = l2;
       	           l->entry.Template.ll_ptr2 = (yyvsp[(3) - (4)].ll_node)->entry.Template.ll_ptr1;
	           l->type = global_string; 
                   (yyval.ll_node)->entry.Template.ll_ptr2 = l;                                          
                } else
                   err("Can't subscript",44);
              else if (l2 && l2->type->variant == T_ARRAY) {
                 if(num_triplets > 0) { /*array section*/
                   (yyval.ll_node)->type = make_type(fi,T_ARRAY);
                   (yyval.ll_node)->type->entry.ar_decl.base_type = l2->type->entry.ar_decl.base_type;
                   (yyval.ll_node)->type->entry.ar_decl.num_dimensions = num_triplets;
                   l2->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);
                   l2->type = (yyval.ll_node)->type;   
                  }                 
                 else {  /*array element*/
                   l2->type = l2->type->entry.ar_decl.base_type;
                   l2->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);   
                   if(l1->type->variant != T_ARRAY)  
                     (yyval.ll_node)->type = l2->type;
                 }
              } else 
                   err("Can't subscript",44);
         ;}
    break;

  case 394:
#line 3382 "gram1.y"
    { 
	      if ((yyvsp[(1) - (2)].ll_node)->type->variant == T_STRING) {
                 (yyvsp[(1) - (2)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(2) - (2)].ll_node);
                 (yyval.ll_node) = (yyvsp[(1) - (2)].ll_node); (yyval.ll_node)->type = global_string;
              }
              else errstr("can't subscript of %s", (yyvsp[(1) - (2)].ll_node)->entry.Template.symbol->ident,44);
            ;}
    break;

  case 395:
#line 3392 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 396:
#line 3394 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 397:
#line 3398 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DDOT, (yyvsp[(2) - (5)].ll_node), (yyvsp[(4) - (5)].ll_node), SMNULL); ;}
    break;

  case 398:
#line 3402 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 399:
#line 3404 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 400:
#line 3408 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 401:
#line 3410 "gram1.y"
    { PTR_TYPE t;
               t = make_type_node((yyvsp[(1) - (3)].ll_node)->type, (yyvsp[(3) - (3)].ll_node));
               (yyval.ll_node) = (yyvsp[(1) - (3)].ll_node);
               (yyval.ll_node)->type = t;
             ;}
    break;

  case 402:
#line 3416 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 403:
#line 3418 "gram1.y"
    { PTR_TYPE t;
               t = make_type_node((yyvsp[(1) - (3)].ll_node)->type, (yyvsp[(3) - (3)].ll_node));
               (yyval.ll_node) = (yyvsp[(1) - (3)].ll_node);
               (yyval.ll_node)->type = t;
             ;}
    break;

  case 404:
#line 3424 "gram1.y"
    {
              if ((yyvsp[(2) - (2)].ll_node) != LLNULL)
		 (yyval.ll_node) = make_llnd(fi, ARRAY_OP, (yyvsp[(1) - (2)].ll_node), (yyvsp[(2) - (2)].ll_node), SMNULL); 
	      else 
                 (yyval.ll_node) = (yyvsp[(1) - (2)].ll_node);
             ;}
    break;

  case 405:
#line 3434 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,BOOL_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.bval = 1;
	      (yyval.ll_node)->type = global_bool;
	    ;}
    break;

  case 406:
#line 3440 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,BOOL_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.bval = 0;
	      (yyval.ll_node)->type = global_bool;
	    ;}
    break;

  case 407:
#line 3447 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,FLOAT_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.string_val = copys(yytext);
	      (yyval.ll_node)->type = global_float;
	    ;}
    break;

  case 408:
#line 3453 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,DOUBLE_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.string_val = copys(yytext);
	      (yyval.ll_node)->type = global_double;
	    ;}
    break;

  case 409:
#line 3461 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,INT_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.ival = atoi(yytext);
	      (yyval.ll_node)->type = global_int;
	    ;}
    break;

  case 410:
#line 3469 "gram1.y"
    { PTR_TYPE t;
	      PTR_LLND p,q;
	      (yyval.ll_node) = make_llnd(fi,STRING_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.string_val = copys(yytext);
              if(yyquote=='\"') 
	        t = global_string_2;
              else
	        t = global_string;

	      p = make_llnd(fi,INT_VAL, LLNULL, LLNULL, SMNULL);
	      p->entry.ival = yyleng;
	      p->type = global_int;
              q = make_llnd(fi, LEN_OP, p, LLNULL, SMNULL); 
              (yyval.ll_node)->type = make_type_node(t, q);
	    ;}
    break;

  case 411:
#line 3485 "gram1.y"
    { PTR_TYPE t;
	      (yyval.ll_node) = make_llnd(fi,STRING_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.string_val = copys(yytext);
              if(yyquote=='\"') 
	        t = global_string_2;
              else
	        t = global_string;
	      (yyval.ll_node)->type = make_type_node(t, (yyvsp[(1) - (3)].ll_node));
            ;}
    break;

  case 412:
#line 3495 "gram1.y"
    { PTR_TYPE t;
	      (yyval.ll_node) = make_llnd(fi,STRING_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.string_val = copys(yytext);
              if(yyquote=='\"') 
	        t = global_string_2;
              else
	        t = global_string;
	      (yyval.ll_node)->type = make_type_node(t, (yyvsp[(1) - (3)].ll_node));
            ;}
    break;

  case 413:
#line 3508 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,COMPLEX_VAL, (yyvsp[(2) - (5)].ll_node), (yyvsp[(4) - (5)].ll_node), SMNULL);
	      (yyval.ll_node)->type = global_complex;
	    ;}
    break;

  case 414:
#line 3515 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 415:
#line 3517 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 416:
#line 3540 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,(yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),SMNULL); ;}
    break;

  case 417:
#line 3542 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,(yyvsp[(1) - (2)].ll_node),LLNULL,SMNULL); ;}
    break;

  case 418:
#line 3544 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,make_llnd(fi,DDOT,(yyvsp[(1) - (5)].ll_node),(yyvsp[(3) - (5)].ll_node),SMNULL),(yyvsp[(5) - (5)].ll_node),SMNULL); ;}
    break;

  case 419:
#line 3546 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,make_llnd(fi,DDOT,(yyvsp[(1) - (4)].ll_node),LLNULL,SMNULL),(yyvsp[(4) - (4)].ll_node),SMNULL); ;}
    break;

  case 420:
#line 3548 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT, make_llnd(fi,DDOT,LLNULL,(yyvsp[(2) - (4)].ll_node),SMNULL),(yyvsp[(4) - (4)].ll_node),SMNULL); ;}
    break;

  case 421:
#line 3550 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,make_llnd(fi,DDOT,LLNULL,LLNULL,SMNULL),(yyvsp[(3) - (3)].ll_node),SMNULL); ;}
    break;

  case 422:
#line 3552 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,LLNULL,(yyvsp[(2) - (2)].ll_node),SMNULL); ;}
    break;

  case 423:
#line 3554 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,LLNULL,LLNULL,SMNULL); ;}
    break;

  case 424:
#line 3557 "gram1.y"
    {in_vec=YES;;}
    break;

  case 425:
#line 3557 "gram1.y"
    {in_vec=NO;;}
    break;

  case 426:
#line 3558 "gram1.y"
    { PTR_TYPE array_type;
             (yyval.ll_node) = make_llnd (fi,CONSTRUCTOR_REF,(yyvsp[(4) - (6)].ll_node),LLNULL,SMNULL); 
             /*$$->type = $2->type;*/ /*28.02.03*/
             array_type = make_type(fi, T_ARRAY);
	     array_type->entry.ar_decl.num_dimensions = 1;
             if((yyvsp[(4) - (6)].ll_node)->type->variant == T_ARRAY)
	       array_type->entry.ar_decl.base_type = (yyvsp[(4) - (6)].ll_node)->type->entry.ar_decl.base_type;
             else
               array_type->entry.ar_decl.base_type = (yyvsp[(4) - (6)].ll_node)->type;
             (yyval.ll_node)->type = array_type;
           ;}
    break;

  case 427:
#line 3572 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 428:
#line 3574 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 429:
#line 3597 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 430:
#line 3599 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node), EXPR_LIST); endioctl(); ;}
    break;

  case 431:
#line 3601 "gram1.y"
    { stat_alloc = make_llnd(fi, SPEC_PAIR, (yyvsp[(4) - (5)].ll_node), (yyvsp[(5) - (5)].ll_node), SMNULL);
                  endioctl();
                ;}
    break;

  case 432:
#line 3617 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 433:
#line 3619 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node), EXPR_LIST); endioctl(); ;}
    break;

  case 434:
#line 3621 "gram1.y"
    { stat_alloc = make_llnd(fi, SPEC_PAIR, (yyvsp[(4) - (5)].ll_node), (yyvsp[(5) - (5)].ll_node), SMNULL);
             endioctl();
           ;}
    break;

  case 435:
#line 3634 "gram1.y"
    {stat_alloc = LLNULL;;}
    break;

  case 436:
#line 3638 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 437:
#line 3640 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 438:
#line 3648 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 439:
#line 3650 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 440:
#line 3652 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 441:
#line 3654 "gram1.y"
    {
              (yyval.bf_node) = (yyvsp[(2) - (2)].bf_node);
              (yyval.bf_node)->entry.Template.ll_ptr3 = (yyvsp[(1) - (2)].ll_node);
            ;}
    break;

  case 442:
#line 3708 "gram1.y"
    { PTR_BFND biff;

	      (yyval.bf_node) = get_bfnd(fi,CONTROL_END, SMNULL, LLNULL, LLNULL, LLNULL); 
	      bind(); 
	      biff = cur_scope();
	      if ((biff->variant == FUNC_HEDR) || (biff->variant == PROC_HEDR)
		  || (biff->variant == PROS_HEDR) 
	          || (biff->variant == PROG_HEDR)
                  || (biff->variant == BLOCK_DATA)) {
                if(biff->control_parent == global_bfnd) position = IN_OUTSIDE;
		else if(!is_interface_stat(biff->control_parent)) position++;
              } else if  (biff->variant == MODULE_STMT)
                position = IN_OUTSIDE;
	      else err("Unexpected END statement read", 52);
             /* FB ADDED set the control parent so the empty function unparse right*/
              if ((yyval.bf_node))
                (yyval.bf_node)->control_parent = biff;
              delete_beyond_scope_level(pred_bfnd);
            ;}
    break;

  case 443:
#line 3730 "gram1.y"
    {
              make_extend((yyvsp[(3) - (3)].symbol));
              (yyval.bf_node) = BFNULL; 
              /* delete_beyond_scope_level(pred_bfnd); */
             ;}
    break;

  case 444:
#line 3743 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,CONTROL_END, SMNULL, LLNULL, LLNULL, LLNULL); 
	    bind(); 
	    delete_beyond_scope_level(pred_bfnd);
	    position = IN_OUTSIDE;
          ;}
    break;

  case 445:
#line 3752 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 446:
#line 3755 "gram1.y"
    {
              (yyval.bf_node) = (yyvsp[(2) - (2)].bf_node);
              (yyval.bf_node)->entry.Template.ll_ptr3 = (yyvsp[(1) - (2)].ll_node);
            ;}
    break;

  case 447:
#line 3805 "gram1.y"
    { thiswasbranch = NO;
              (yyvsp[(1) - (2)].bf_node)->variant = LOGIF_NODE;
              (yyval.bf_node) = make_logif((yyvsp[(1) - (2)].bf_node), (yyvsp[(2) - (2)].bf_node));
	      set_blobs((yyvsp[(1) - (2)].bf_node), pred_bfnd, SAME_GROUP);
	    ;}
    break;

  case 448:
#line 3811 "gram1.y"
    {
              (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node);
	      set_blobs((yyval.bf_node), pred_bfnd, NEW_GROUP1); 
            ;}
    break;

  case 449:
#line 3816 "gram1.y"
    {
              (yyval.bf_node) = (yyvsp[(2) - (3)].bf_node);
              (yyval.bf_node)->entry.Template.ll_ptr3 = (yyvsp[(1) - (3)].ll_node);
	      set_blobs((yyval.bf_node), pred_bfnd, NEW_GROUP1); 
            ;}
    break;

  case 450:
#line 3834 "gram1.y"
    { make_elseif((yyvsp[(4) - (7)].ll_node),(yyvsp[(7) - (7)].symbol)); lastwasbranch = NO; (yyval.bf_node) = BFNULL;;}
    break;

  case 451:
#line 3836 "gram1.y"
    { make_else((yyvsp[(3) - (3)].symbol)); lastwasbranch = NO; (yyval.bf_node) = BFNULL; ;}
    break;

  case 452:
#line 3838 "gram1.y"
    { make_endif((yyvsp[(3) - (3)].symbol)); (yyval.bf_node) = BFNULL; ;}
    break;

  case 453:
#line 3840 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 454:
#line 3842 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, CONTAINS_STMT, SMNULL, LLNULL, LLNULL, LLNULL); ;}
    break;

  case 455:
#line 3845 "gram1.y"
    { thiswasbranch = NO;
              (yyvsp[(1) - (2)].bf_node)->variant = FORALL_STAT;
              (yyval.bf_node) = make_logif((yyvsp[(1) - (2)].bf_node), (yyvsp[(2) - (2)].bf_node));
	      set_blobs((yyvsp[(1) - (2)].bf_node), pred_bfnd, SAME_GROUP);
	    ;}
    break;

  case 456:
#line 3851 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 457:
#line 3853 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(2) - (2)].bf_node); (yyval.bf_node)->entry.Template.ll_ptr3 = (yyvsp[(1) - (2)].ll_node);;}
    break;

  case 458:
#line 3855 "gram1.y"
    { make_endforall((yyvsp[(3) - (3)].symbol)); (yyval.bf_node) = BFNULL; ;}
    break;

  case 459:
#line 3858 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 460:
#line 3860 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 461:
#line 3887 "gram1.y"
    { 	     
	     /*  if($5 && $5->labdefined)
		 execerr("no backward DO loops", (char *)NULL); */
	       (yyval.bf_node) = make_do(WHILE_NODE, LBNULL, SMNULL, (yyvsp[(4) - (5)].ll_node), LLNULL, LLNULL);
	       /*$$->entry.Template.ll_ptr3 = $1;*/	     
           ;}
    break;

  case 462:
#line 3896 "gram1.y"
    {
               if( (yyvsp[(4) - (7)].label) && (yyvsp[(4) - (7)].label)->labdefined)
		  err("No backward DO loops", 46);
	        (yyval.bf_node) = make_do(WHILE_NODE, (yyvsp[(4) - (7)].label), SMNULL, (yyvsp[(7) - (7)].ll_node), LLNULL, LLNULL);            
	    ;}
    break;

  case 463:
#line 3904 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 464:
#line 3906 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(5) - (6)].ll_node);;}
    break;

  case 465:
#line 3908 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(3) - (4)].ll_node);;}
    break;

  case 466:
#line 3913 "gram1.y"
    {  
               if( (yyvsp[(4) - (11)].label) && (yyvsp[(4) - (11)].label)->labdefined)
		  err("No backward DO loops", 46);
	        (yyval.bf_node) = make_do(FOR_NODE, (yyvsp[(4) - (11)].label), (yyvsp[(7) - (11)].symbol), (yyvsp[(9) - (11)].ll_node), (yyvsp[(11) - (11)].ll_node), LLNULL);            
	    ;}
    break;

  case 467:
#line 3920 "gram1.y"
    {
               if( (yyvsp[(4) - (13)].label) && (yyvsp[(4) - (13)].label)->labdefined)
		  err("No backward DO loops", 46);
	        (yyval.bf_node) = make_do(FOR_NODE, (yyvsp[(4) - (13)].label), (yyvsp[(7) - (13)].symbol), (yyvsp[(9) - (13)].ll_node), (yyvsp[(11) - (13)].ll_node), (yyvsp[(13) - (13)].ll_node));            
	    ;}
    break;

  case 468:
#line 3928 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, CASE_NODE, (yyvsp[(4) - (4)].symbol), (yyvsp[(3) - (4)].ll_node), LLNULL, LLNULL); ;}
    break;

  case 469:
#line 3930 "gram1.y"
    { /*PTR_LLND p;*/
	     /* p = make_llnd(fi, DEFAULT, LLNULL, LLNULL, SMNULL); */
	      (yyval.bf_node) = get_bfnd(fi, DEFAULT_NODE, (yyvsp[(3) - (3)].symbol), LLNULL, LLNULL, LLNULL); ;}
    break;

  case 470:
#line 3934 "gram1.y"
    { make_endselect((yyvsp[(3) - (3)].symbol)); (yyval.bf_node) = BFNULL; ;}
    break;

  case 471:
#line 3937 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, SWITCH_NODE, SMNULL, (yyvsp[(6) - (7)].ll_node), LLNULL, LLNULL) ; ;}
    break;

  case 472:
#line 3939 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, SWITCH_NODE, SMNULL, (yyvsp[(7) - (8)].ll_node), LLNULL, (yyvsp[(1) - (8)].ll_node)) ; ;}
    break;

  case 473:
#line 3943 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node); ;}
    break;

  case 474:
#line 3949 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 475:
#line 3951 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DDOT, (yyvsp[(1) - (2)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 476:
#line 3953 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DDOT, LLNULL, (yyvsp[(2) - (2)].ll_node), SMNULL); ;}
    break;

  case 477:
#line 3955 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DDOT, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL); ;}
    break;

  case 478:
#line 3959 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, EXPR_LIST, (yyvsp[(1) - (1)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 479:
#line 3961 "gram1.y"
    { PTR_LLND p;
	      
	      p = make_llnd(fi, EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      add_to_lowLevelList(p, (yyvsp[(1) - (3)].ll_node));
	    ;}
    break;

  case 480:
#line 3969 "gram1.y"
    { (yyval.symbol) = SMNULL; ;}
    break;

  case 481:
#line 3971 "gram1.y"
    { (yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry), CONSTRUCT_NAME, global_default,
                                     LOCAL); ;}
    break;

  case 482:
#line 3977 "gram1.y"
    {(yyval.hash_entry) = HSNULL;;}
    break;

  case 483:
#line 3979 "gram1.y"
    { (yyval.hash_entry) = (yyvsp[(1) - (1)].hash_entry);;}
    break;

  case 484:
#line 3983 "gram1.y"
    {(yyval.hash_entry) = look_up_sym(yytext);;}
    break;

  case 485:
#line 3987 "gram1.y"
    { PTR_SYMB s;
	             s = make_local_entity( (yyvsp[(1) - (2)].hash_entry), CONSTRUCT_NAME, global_default, LOCAL);             
                    (yyval.ll_node) = make_llnd(fi, VAR_REF, LLNULL, LLNULL, s);
                   ;}
    break;

  case 486:
#line 4008 "gram1.y"
    { (yyval.bf_node) = make_if((yyvsp[(4) - (5)].ll_node)); ;}
    break;

  case 487:
#line 4011 "gram1.y"
    { (yyval.bf_node) = make_forall((yyvsp[(4) - (6)].ll_node),(yyvsp[(5) - (6)].ll_node)); ;}
    break;

  case 488:
#line 4015 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, EXPR_LIST, (yyvsp[(1) - (1)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 489:
#line 4017 "gram1.y"
    { PTR_LLND p;	      
	      p = make_llnd(fi, EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      add_to_lowLevelList(p, (yyvsp[(1) - (3)].ll_node));
	    ;}
    break;

  case 490:
#line 4024 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi, FORALL_OP, (yyvsp[(3) - (3)].ll_node), LLNULL, (yyvsp[(1) - (3)].symbol)); ;}
    break;

  case 491:
#line 4028 "gram1.y"
    { (yyval.ll_node)=LLNULL;;}
    break;

  case 492:
#line 4030 "gram1.y"
    { (yyval.ll_node)=(yyvsp[(2) - (2)].ll_node);;}
    break;

  case 493:
#line 4046 "gram1.y"
    { PTR_SYMB  s;
      	      if (!(s = (yyvsp[(1) - (1)].hash_entry)->id_attr))
              {
	         s = make_scalar((yyvsp[(1) - (1)].hash_entry), TYNULL, LOCAL);
	     	 s->decl = SOFT;
	      }
              (yyval.symbol) = s; 
	 ;}
    break;

  case 494:
#line 4059 "gram1.y"
    { PTR_SYMB s;
              PTR_LLND l;
              int vrnt;

            /*  s = make_scalar($1, TYNULL, LOCAL);*/ /*16.02.03*/
              s = (yyvsp[(1) - (5)].symbol);
	      if (s->variant != CONST_NAME) {
                if(in_vec) 
                   vrnt=SEQ;
                else
                   vrnt=DDOT;     
                l = make_llnd(fi, SEQ, make_llnd(fi, vrnt, (yyvsp[(3) - (5)].ll_node), (yyvsp[(5) - (5)].ll_node), SMNULL),
                              LLNULL, SMNULL);
		(yyval.ll_node) = make_llnd(fi,IOACCESS, LLNULL, l, s);
		do_name_err = NO;
	      }
	      else {
		err("Symbolic constant not allowed as DO variable", 47);
		do_name_err = YES;
	      }
	    ;}
    break;

  case 495:
#line 4082 "gram1.y"
    { PTR_SYMB s;
              PTR_LLND l;
              int vrnt;
              /*s = make_scalar($1, TYNULL, LOCAL);*/ /*16.02.03*/
              s = (yyvsp[(1) - (7)].symbol);
	      if( s->variant != CONST_NAME ) {
                if(in_vec) 
                   vrnt=SEQ;
                else
                   vrnt=DDOT;     
                l = make_llnd(fi, SEQ, make_llnd(fi, vrnt, (yyvsp[(3) - (7)].ll_node), (yyvsp[(5) - (7)].ll_node), SMNULL), (yyvsp[(7) - (7)].ll_node),
                              SMNULL);
		(yyval.ll_node) = make_llnd(fi,IOACCESS, LLNULL, l, s);
		do_name_err = NO;
	      }
	      else {
		err("Symbolic constant not allowed as DO variable", 47);
		do_name_err = YES;
	      }
	    ;}
    break;

  case 496:
#line 4105 "gram1.y"
    { (yyval.label) = LBNULL; ;}
    break;

  case 497:
#line 4107 "gram1.y"
    {
	       (yyval.label)  = make_label_node(fi,convci(yyleng, yytext));
	       (yyval.label)->scope = cur_scope();
	    ;}
    break;

  case 498:
#line 4114 "gram1.y"
    { make_endwhere((yyvsp[(3) - (3)].symbol)); (yyval.bf_node) = BFNULL; ;}
    break;

  case 499:
#line 4116 "gram1.y"
    { make_elsewhere((yyvsp[(3) - (3)].symbol)); lastwasbranch = NO; (yyval.bf_node) = BFNULL; ;}
    break;

  case 500:
#line 4118 "gram1.y"
    { make_elsewhere_mask((yyvsp[(4) - (6)].ll_node),(yyvsp[(6) - (6)].symbol)); lastwasbranch = NO; (yyval.bf_node) = BFNULL; ;}
    break;

  case 501:
#line 4120 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, WHERE_BLOCK_STMT, SMNULL, (yyvsp[(4) - (5)].ll_node), LLNULL, LLNULL); ;}
    break;

  case 502:
#line 4122 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, WHERE_BLOCK_STMT, SMNULL, (yyvsp[(5) - (6)].ll_node), LLNULL, (yyvsp[(1) - (6)].ll_node)); ;}
    break;

  case 503:
#line 4127 "gram1.y"
    { PTR_LLND p, q = LLNULL, r;
             PTR_SYMB s1, s2 = SMNULL, s3, arg_list;
	     PTR_HASH hash_entry;

	   /*  if (just_look_up_sym("=") != HSNULL) {
	        p = intrinsic_op_node("=", EQUAL, $2, $4);
   	        $$ = get_bfnd(fi, OVERLOADED_ASSIGN_STAT, SMNULL, p, $2, $4);
             }	      
             else */ if ((yyvsp[(2) - (4)].ll_node)->variant == FUNC_CALL) {
                if(parstate==INEXEC){
                  	  err("Declaration among executables", 30);
                 /*   $$=BFNULL;*/
 	         (yyval.bf_node) = get_bfnd(fi,STMTFN_STAT, SMNULL, (yyvsp[(2) - (4)].ll_node), LLNULL, LLNULL);
                } 
                else {
	         q = (yyvsp[(2) - (4)].ll_node);
  	         (yyvsp[(2) - (4)].ll_node)->variant = STMTFN_DECL;
		 /* $2->entry.Template.ll_ptr2 = $4; */
                 if( (yyvsp[(2) - (4)].ll_node)->entry.Template.ll_ptr1) {
		   r = (yyvsp[(2) - (4)].ll_node)->entry.Template.ll_ptr1->entry.Template.ll_ptr1;
                   if(r->variant != VAR_REF && r->variant != ARRAY_REF){
                     err("A dummy argument of a statement function must be a scalar identifier", 333);
                     s1 = SMNULL;
                   }
                   else                       
		     s1 = r ->entry.Template.symbol;
                 } else
                   s1 = SMNULL;
		 if (s1)
	            s1->scope = cur_scope();
 	         (yyval.bf_node) = get_bfnd(fi,STMTFN_STAT, SMNULL, (yyvsp[(2) - (4)].ll_node), LLNULL, LLNULL);
	         add_scope_level((yyval.bf_node), NO);
                 arg_list = SMNULL;
		 if (s1) 
                 {
	            /*arg_list = SMNULL;*/
                    p = (yyvsp[(2) - (4)].ll_node)->entry.Template.ll_ptr1;
                    while (p != LLNULL)
                    {
		    /*   if (p->entry.Template.ll_ptr1->variant != VAR_REF) {
			  errstr("cftn.gram:1: illegal statement function %s.", $2->entry.Template.symbol->ident);
			  break;
		       } 
                    */
                       r = p->entry.Template.ll_ptr1;
                       if(r->variant != VAR_REF && r->variant != ARRAY_REF){
                         err("A dummy argument of a statement function must be a scalar identifier", 333);
                         break;
                       }
	               hash_entry = look_up_sym(r->entry.Template.symbol->parent->ident);
	               s3 = make_scalar(hash_entry, s1->type, IO);
	               if (arg_list == SMNULL) 
                          s2 = arg_list = s3;
             	       else 
                       {
                          s2->id_list = s3;
                          s2 = s3;
                       }
                       p = p->entry.Template.ll_ptr2;
                    }
                 }
  		    (yyvsp[(2) - (4)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (4)].ll_node);
		    install_param_list((yyvsp[(2) - (4)].ll_node)->entry.Template.symbol,
				       arg_list, LLNULL, FUNCTION_NAME);
	            delete_beyond_scope_level((yyval.bf_node));
		 
		/* else
		    errstr("cftn.gram: Illegal statement function declaration %s.", $2->entry.Template.symbol->ident); */
               }
	     }
	     else {
		(yyval.bf_node) = get_bfnd(fi,ASSIGN_STAT,SMNULL, (yyvsp[(2) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node), LLNULL);
                 parstate = INEXEC;
             }
	  ;}
    break;

  case 504:
#line 4203 "gram1.y"
    { /*PTR_SYMB s;*/
	
	      /*s = make_scalar($2, TYNULL, LOCAL);*/
  	      (yyval.bf_node) = get_bfnd(fi, POINTER_ASSIGN_STAT, SMNULL, (yyvsp[(3) - (5)].ll_node), (yyvsp[(5) - (5)].ll_node), LLNULL);
	    ;}
    break;

  case 505:
#line 4215 "gram1.y"
    { PTR_SYMB p;

	      p = make_scalar((yyvsp[(5) - (5)].hash_entry), TYNULL, LOCAL);
	      p->variant = LABEL_VAR;
  	      (yyval.bf_node) = get_bfnd(fi,ASSLAB_STAT, p, (yyvsp[(3) - (5)].ll_node),LLNULL,LLNULL);
            ;}
    break;

  case 506:
#line 4222 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,CONT_STAT,SMNULL,LLNULL,LLNULL,LLNULL); ;}
    break;

  case 508:
#line 4225 "gram1.y"
    { inioctl = NO; ;}
    break;

  case 509:
#line 4227 "gram1.y"
    { PTR_LLND	p;

	      p = make_llnd(fi,EXPR_LIST, (yyvsp[(10) - (10)].ll_node), LLNULL, SMNULL);
	      p = make_llnd(fi,EXPR_LIST, (yyvsp[(8) - (10)].ll_node), p, SMNULL);
	      (yyval.bf_node)= get_bfnd(fi,ARITHIF_NODE, SMNULL, (yyvsp[(4) - (10)].ll_node),
			    make_llnd(fi,EXPR_LIST, (yyvsp[(6) - (10)].ll_node), p, SMNULL), LLNULL);
	      thiswasbranch = YES;
            ;}
    break;

  case 510:
#line 4236 "gram1.y"
    {
	      (yyval.bf_node) = subroutine_call((yyvsp[(1) - (1)].symbol), LLNULL, LLNULL, PLAIN);
/*	      match_parameters($1, LLNULL);
	      $$= get_bfnd(fi,PROC_STAT, $1, LLNULL, LLNULL, LLNULL);
*/	      endioctl(); 
            ;}
    break;

  case 511:
#line 4243 "gram1.y"
    {
	      (yyval.bf_node) = subroutine_call((yyvsp[(1) - (3)].symbol), LLNULL, LLNULL, PLAIN);
/*	      match_parameters($1, LLNULL);
	      $$= get_bfnd(fi,PROC_STAT,$1,LLNULL,LLNULL,LLNULL);
*/	      endioctl(); 
	    ;}
    break;

  case 512:
#line 4250 "gram1.y"
    {
	      (yyval.bf_node) = subroutine_call((yyvsp[(1) - (4)].symbol), (yyvsp[(3) - (4)].ll_node), LLNULL, PLAIN);
/*	      match_parameters($1, $3);
	      $$= get_bfnd(fi,PROC_STAT,$1,$3,LLNULL,LLNULL);
*/	      endioctl(); 
	    ;}
    break;

  case 513:
#line 4258 "gram1.y"
    {
	      (yyval.bf_node) = get_bfnd(fi,RETURN_STAT,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);
	      thiswasbranch = YES;
	    ;}
    break;

  case 514:
#line 4263 "gram1.y"
    {
	      (yyval.bf_node) = get_bfnd(fi,(yyvsp[(1) - (3)].token),SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);
	      thiswasbranch = ((yyvsp[(1) - (3)].token) == STOP_STAT);
	    ;}
    break;

  case 515:
#line 4268 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, CYCLE_STMT, (yyvsp[(3) - (3)].symbol), LLNULL, LLNULL, LLNULL); ;}
    break;

  case 516:
#line 4271 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, EXIT_STMT, (yyvsp[(3) - (3)].symbol), LLNULL, LLNULL, LLNULL); ;}
    break;

  case 517:
#line 4274 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, ALLOCATE_STMT,  SMNULL, (yyvsp[(5) - (6)].ll_node), stat_alloc, LLNULL); ;}
    break;

  case 518:
#line 4277 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, DEALLOCATE_STMT, SMNULL, (yyvsp[(5) - (6)].ll_node), stat_alloc , LLNULL); ;}
    break;

  case 519:
#line 4280 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, NULLIFY_STMT, SMNULL, (yyvsp[(4) - (5)].ll_node), LLNULL, LLNULL); ;}
    break;

  case 520:
#line 4283 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, WHERE_NODE, SMNULL, (yyvsp[(4) - (8)].ll_node), (yyvsp[(6) - (8)].ll_node), (yyvsp[(8) - (8)].ll_node)); ;}
    break;

  case 521:
#line 4301 "gram1.y"
    {(yyval.ll_node) = LLNULL;;}
    break;

  case 522:
#line 4305 "gram1.y"
    {
	      (yyval.bf_node)=get_bfnd(fi,GOTO_NODE,SMNULL,LLNULL,LLNULL,(PTR_LLND)(yyvsp[(3) - (3)].ll_node));
	      thiswasbranch = YES;
	    ;}
    break;

  case 523:
#line 4310 "gram1.y"
    { PTR_SYMB p;

	      if((yyvsp[(3) - (3)].hash_entry)->id_attr)
		p = (yyvsp[(3) - (3)].hash_entry)->id_attr;
	      else {
	        p = make_scalar((yyvsp[(3) - (3)].hash_entry), TYNULL, LOCAL);
		p->variant = LABEL_VAR;
	      }

	      if(p->variant == LABEL_VAR) {
		  (yyval.bf_node) = get_bfnd(fi,ASSGOTO_NODE,p,LLNULL,LLNULL,LLNULL);
		  thiswasbranch = YES;
	      }
	      else {
		  err("Must go to assigned variable", 48);
		  (yyval.bf_node) = BFNULL;
	      }
	    ;}
    break;

  case 524:
#line 4329 "gram1.y"
    { PTR_SYMB p;

	      if((yyvsp[(3) - (7)].hash_entry)->id_attr)
		p = (yyvsp[(3) - (7)].hash_entry)->id_attr;
	      else {
	        p = make_scalar((yyvsp[(3) - (7)].hash_entry), TYNULL, LOCAL);
		p->variant = LABEL_VAR;
	      }

	      if (p->variant == LABEL_VAR) {
		 (yyval.bf_node) = get_bfnd(fi,ASSGOTO_NODE,p,(yyvsp[(6) - (7)].ll_node),LLNULL,LLNULL);
		 thiswasbranch = YES;
	      }
	      else {
		err("Must go to assigned variable",48);
		(yyval.bf_node) = BFNULL;
	      }
	    ;}
    break;

  case 525:
#line 4348 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,COMGOTO_NODE, SMNULL, (yyvsp[(4) - (7)].ll_node), (yyvsp[(7) - (7)].ll_node), LLNULL); ;}
    break;

  case 528:
#line 4356 "gram1.y"
    { (yyval.symbol) = make_procedure((yyvsp[(3) - (4)].hash_entry), YES); ;}
    break;

  case 529:
#line 4360 "gram1.y"
    { 
              (yyval.ll_node) = set_ll_list((yyvsp[(2) - (2)].ll_node), LLNULL, EXPR_LIST);
              endioctl();
            ;}
    break;

  case 530:
#line 4365 "gram1.y"
    { 
               (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node), EXPR_LIST);
               endioctl();
            ;}
    break;

  case 531:
#line 4372 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 532:
#line 4374 "gram1.y"
    { (yyval.ll_node)  = make_llnd(fi, KEYWORD_ARG, (yyvsp[(1) - (2)].ll_node), (yyvsp[(2) - (2)].ll_node), SMNULL); ;}
    break;

  case 533:
#line 4376 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,LABEL_REF,LLNULL,LLNULL,(PTR_SYMB)(yyvsp[(2) - (2)].ll_node)); ;}
    break;

  case 534:
#line 4379 "gram1.y"
    { (yyval.token) = PAUSE_NODE; ;}
    break;

  case 535:
#line 4380 "gram1.y"
    { (yyval.token) = STOP_STAT; ;}
    break;

  case 536:
#line 4391 "gram1.y"
    { if(parstate == OUTSIDE)
		{ PTR_BFND p;

		  p = get_bfnd(fi,PROG_HEDR, make_program(look_up_sym("_MAIN")), LLNULL, LLNULL, LLNULL);
		  set_blobs(p, global_bfnd, NEW_GROUP1);
		  add_scope_level(p, NO);
		  position = IN_PROC; 
		}
		if(parstate < INDATA) enddcl();
		parstate = INEXEC;
		yystno = 0;
	      ;}
    break;

  case 537:
#line 4406 "gram1.y"
    { intonly = YES; ;}
    break;

  case 538:
#line 4410 "gram1.y"
    { intonly = NO; ;}
    break;

  case 539:
#line 4418 "gram1.y"
    { (yyvsp[(1) - (2)].bf_node)->entry.Template.ll_ptr2 = (yyvsp[(2) - (2)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node); ;}
    break;

  case 540:
#line 4421 "gram1.y"
    { PTR_LLND p, q = LLNULL;

		  q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  q->entry.string_val = (char *)"unit";
		  q->type = global_string;
		  p = make_llnd(fi, SPEC_PAIR, q, (yyvsp[(2) - (2)].ll_node), SMNULL);
		  (yyvsp[(1) - (2)].bf_node)->entry.Template.ll_ptr2 = p;
		  endioctl();
		  (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node); ;}
    break;

  case 541:
#line 4431 "gram1.y"
    { PTR_LLND p, q, r;

		  p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  p->entry.string_val = (char *)"*";
		  p->type = global_string;
		  q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  q->entry.string_val = (char *)"unit";
		  q->type = global_string;
		  r = make_llnd(fi, SPEC_PAIR, p, q, SMNULL);
		  (yyvsp[(1) - (2)].bf_node)->entry.Template.ll_ptr2 = r;
		  endioctl();
		  (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node); ;}
    break;

  case 542:
#line 4444 "gram1.y"
    { PTR_LLND p, q, r;

		  p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  p->entry.string_val = (char *)"**";
		  p->type = global_string;
		  q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  q->entry.string_val = (char *)"unit";
		  q->type = global_string;
		  r = make_llnd(fi, SPEC_PAIR, p, q, SMNULL);
		  (yyvsp[(1) - (2)].bf_node)->entry.Template.ll_ptr2 = r;
		  endioctl();
		  (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node); ;}
    break;

  case 543:
#line 4457 "gram1.y"
    { (yyvsp[(1) - (2)].bf_node)->entry.Template.ll_ptr2 = (yyvsp[(2) - (2)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node); ;}
    break;

  case 544:
#line 4461 "gram1.y"
    { (yyvsp[(1) - (2)].bf_node)->entry.Template.ll_ptr2 = (yyvsp[(2) - (2)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node); ;}
    break;

  case 545:
#line 4464 "gram1.y"
    { (yyvsp[(1) - (2)].bf_node)->entry.Template.ll_ptr2 = (yyvsp[(2) - (2)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node); ;}
    break;

  case 546:
#line 4467 "gram1.y"
    { (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr2 = (yyvsp[(2) - (3)].ll_node);
		  (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1 = (yyvsp[(3) - (3)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (3)].bf_node); ;}
    break;

  case 547:
#line 4471 "gram1.y"
    { (yyvsp[(1) - (4)].bf_node)->entry.Template.ll_ptr2 = (yyvsp[(2) - (4)].ll_node);
		  (yyvsp[(1) - (4)].bf_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (4)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (4)].bf_node); ;}
    break;

  case 548:
#line 4480 "gram1.y"
    { (yyvsp[(1) - (2)].bf_node)->entry.Template.ll_ptr2 = (yyvsp[(2) - (2)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node); ;}
    break;

  case 549:
#line 4483 "gram1.y"
    { (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr2 = (yyvsp[(2) - (3)].ll_node);
		  (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1 = (yyvsp[(3) - (3)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (3)].bf_node); ;}
    break;

  case 550:
#line 4487 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 551:
#line 4489 "gram1.y"
    { (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1 = (yyvsp[(3) - (3)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (3)].bf_node); ;}
    break;

  case 552:
#line 4495 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (3)].bf_node); ;}
    break;

  case 553:
#line 4499 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi, BACKSPACE_STAT, SMNULL, LLNULL, LLNULL, LLNULL);;}
    break;

  case 554:
#line 4501 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi, REWIND_STAT, SMNULL, LLNULL, LLNULL, LLNULL);;}
    break;

  case 555:
#line 4503 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi, ENDFILE_STAT, SMNULL, LLNULL, LLNULL, LLNULL);;}
    break;

  case 556:
#line 4510 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (3)].bf_node); ;}
    break;

  case 557:
#line 4514 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi, INQUIRE_STAT, SMNULL, LLNULL, LLNULL, LLNULL);;}
    break;

  case 558:
#line 4516 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi, OPEN_STAT, SMNULL, LLNULL, LLNULL, LLNULL);;}
    break;

  case 559:
#line 4518 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi, CLOSE_STAT, SMNULL, LLNULL, LLNULL, LLNULL);;}
    break;

  case 560:
#line 4522 "gram1.y"
    { PTR_LLND p;
		  PTR_LLND q = LLNULL;

		  if ((yyvsp[(1) - (1)].ll_node)->variant == INT_VAL)
 	          {
		        PTR_LABEL r;

			r = make_label_node(fi, (long) (yyvsp[(1) - (1)].ll_node)->entry.ival);
			r->scope = cur_scope();
			p = make_llnd_label(fi, LABEL_REF, r);
		  }
		  else p = (yyvsp[(1) - (1)].ll_node); 
		  q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  q->entry.string_val = (char *)"fmt";
		  q->type = global_string;
		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, q, p, SMNULL);
		  endioctl();
		;}
    break;

  case 561:
#line 4541 "gram1.y"
    { PTR_LLND p;
		  PTR_LLND q;

		  p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  p->entry.string_val = (char *)"*";
		  p->type = global_string;
		  q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  q->entry.string_val = (char *)"fmt";
		  q->type = global_string;
		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, q, p, SMNULL);
		  endioctl();
		;}
    break;

  case 562:
#line 4557 "gram1.y"
    { PTR_LLND p;

		  p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  p->entry.string_val = (char *)"unit";
		  p->type = global_string;
		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, p, (yyvsp[(2) - (3)].ll_node), SMNULL);
		  endioctl();
		;}
    break;

  case 563:
#line 4568 "gram1.y"
    { 
		  (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node);
		  endioctl();
		 ;}
    break;

  case 564:
#line 4575 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); endioctl();;}
    break;

  case 565:
#line 4577 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node), EXPR_LIST); endioctl();;}
    break;

  case 566:
#line 4581 "gram1.y"
    { PTR_LLND p;
		  PTR_LLND q;
 
		  nioctl++;
		  if ((nioctl == 2) && ((yyvsp[(1) - (1)].ll_node)->variant == INT_VAL))
 	          {
		        PTR_LABEL r;

			r = make_label_node(fi, (long) (yyvsp[(1) - (1)].ll_node)->entry.ival);
			r->scope = cur_scope();
			p = make_llnd_label(fi, LABEL_REF, r);
		  }
		  else p = (yyvsp[(1) - (1)].ll_node); 
		  q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  if (nioctl == 1)
		        q->entry.string_val = (char *)"unit"; 
		  else {
                     if(((yyvsp[(1) - (1)].ll_node)->variant == VAR_REF) && (yyvsp[(1) - (1)].ll_node)->entry.Template.symbol->variant == NAMELIST_NAME)
                       q->entry.string_val = (char *)"nml";
                     else
                       q->entry.string_val = (char *)"fmt";
                  }
		  q->type = global_string;
		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, q, p, SMNULL);
		;}
    break;

  case 567:
#line 4607 "gram1.y"
    { PTR_LLND p;
		  PTR_LLND q;

		  nioctl++;
		  p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  p->entry.string_val = (char *)"*";
		  p->type = global_string;
		  q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  if (nioctl == 1)
		        q->entry.string_val = (char *)"unit"; 
		  else  q->entry.string_val = (char *)"fmt";
		  q->type = global_string;
		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, q, p, SMNULL);
		;}
    break;

  case 568:
#line 4622 "gram1.y"
    { PTR_LLND p;
		  PTR_LLND q;

		  nioctl++;
		  p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  p->entry.string_val = (char *)"**";
		  p->type = global_string;
		  q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  if (nioctl == 1)
		        q->entry.string_val = (char *)"unit"; 
		  else  q->entry.string_val = (char *)"fmt";
		  q->type = global_string;
		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, q, p, SMNULL);
		;}
    break;

  case 569:
#line 4637 "gram1.y"
    { 
		  PTR_LLND p;
		  char *q;

		  q = (yyvsp[(1) - (2)].ll_node)->entry.string_val;
  		  if ((strcmp(q, "end") == 0) || (strcmp(q, "err") == 0) || (strcmp(q, "eor") == 0) || ((strcmp(q,"fmt") == 0) && ((yyvsp[(2) - (2)].ll_node)->variant == INT_VAL)))
 	          {
		        PTR_LABEL r;

			r = make_label_node(fi, (long) (yyvsp[(2) - (2)].ll_node)->entry.ival);
			r->scope = cur_scope();
			p = make_llnd_label(fi, LABEL_REF, r);
		  }
		  else p = (yyvsp[(2) - (2)].ll_node);

		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, (yyvsp[(1) - (2)].ll_node), p, SMNULL); ;}
    break;

  case 570:
#line 4654 "gram1.y"
    { PTR_LLND p;
                  
		  p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  p->entry.string_val = (char *)"*";
		  p->type = global_string;
		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, (yyvsp[(1) - (2)].ll_node), p, SMNULL);
		;}
    break;

  case 571:
#line 4662 "gram1.y"
    { PTR_LLND p;
		  p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  p->entry.string_val = (char *)"*";
		  p->type = global_string;
		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, (yyvsp[(1) - (2)].ll_node), p, SMNULL);
		;}
    break;

  case 572:
#line 4671 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  (yyval.ll_node)->entry.string_val = copys(yytext);
		  (yyval.ll_node)->type = global_string;
	        ;}
    break;

  case 573:
#line 4679 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi, READ_STAT, SMNULL, LLNULL, LLNULL, LLNULL);;}
    break;

  case 574:
#line 4684 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi, WRITE_STAT, SMNULL, LLNULL, LLNULL, LLNULL);;}
    break;

  case 575:
#line 4689 "gram1.y"
    {
	    PTR_LLND p, q, l;

	    if ((yyvsp[(3) - (4)].ll_node)->variant == INT_VAL)
		{
		        PTR_LABEL r;

			r = make_label_node(fi, (long) (yyvsp[(3) - (4)].ll_node)->entry.ival);
			r->scope = cur_scope();
			p = make_llnd_label(fi, LABEL_REF, r);
		}
	    else p = (yyvsp[(3) - (4)].ll_node);
	    
            q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
	    q->entry.string_val = (char *)"fmt";
            q->type = global_string;
            l = make_llnd(fi, SPEC_PAIR, q, p, SMNULL);

            (yyval.bf_node) = get_bfnd(fi, PRINT_STAT, SMNULL, LLNULL, l, LLNULL);
	    endioctl();
	   ;}
    break;

  case 576:
#line 4711 "gram1.y"
    { PTR_LLND p, q, r;
		
	     p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
	     p->entry.string_val = (char *)"*";
	     p->type = global_string;
	     q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
	     q->entry.string_val = (char *)"fmt";
             q->type = global_string;
             r = make_llnd(fi, SPEC_PAIR, q, p, SMNULL);
	     (yyval.bf_node) = get_bfnd(fi, PRINT_STAT, SMNULL, LLNULL, r, LLNULL);
	     endioctl();
           ;}
    break;

  case 577:
#line 4727 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST);;}
    break;

  case 578:
#line 4729 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST);;}
    break;

  case 579:
#line 4733 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 580:
#line 4735 "gram1.y"
    {
		  (yyvsp[(4) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(2) - (5)].ll_node);
		  (yyval.ll_node) = (yyvsp[(4) - (5)].ll_node);
		;}
    break;

  case 581:
#line 4742 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST);  (yyval.ll_node)->type = (yyvsp[(1) - (1)].ll_node)->type;;}
    break;

  case 582:
#line 4744 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 583:
#line 4746 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 584:
#line 4750 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); (yyval.ll_node)->type = (yyvsp[(1) - (3)].ll_node)->type;;}
    break;

  case 585:
#line 4752 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); (yyval.ll_node)->type = (yyvsp[(1) - (3)].ll_node)->type;;}
    break;

  case 586:
#line 4754 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); (yyval.ll_node)->type = (yyvsp[(1) - (3)].ll_node)->type;;}
    break;

  case 587:
#line 4756 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); (yyval.ll_node)->type = (yyvsp[(1) - (3)].ll_node)->type;;}
    break;

  case 588:
#line 4758 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); (yyval.ll_node)->type = (yyvsp[(1) - (3)].ll_node)->type;;}
    break;

  case 589:
#line 4760 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); (yyval.ll_node)->type = (yyvsp[(1) - (3)].ll_node)->type;;}
    break;

  case 590:
#line 4764 "gram1.y"
    { (yyval.ll_node) =  set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST);
	          (yyval.ll_node)->type = global_complex; ;}
    break;

  case 591:
#line 4767 "gram1.y"
    { (yyval.ll_node) =  set_ll_list((yyvsp[(2) - (3)].ll_node), LLNULL, EXPR_LIST);
                  (yyval.ll_node)->type = (yyvsp[(2) - (3)].ll_node)->type; ;}
    break;

  case 592:
#line 4770 "gram1.y"
    {
		  (yyvsp[(4) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(2) - (5)].ll_node);
		  (yyval.ll_node) =  set_ll_list((yyvsp[(4) - (5)].ll_node), LLNULL, EXPR_LIST);
                  (yyval.ll_node)->type = (yyvsp[(2) - (5)].ll_node)->type; 
		;}
    break;

  case 593:
#line 4776 "gram1.y"
    {
		  (yyvsp[(4) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(2) - (5)].ll_node);
		  (yyval.ll_node) =  set_ll_list((yyvsp[(4) - (5)].ll_node), LLNULL, EXPR_LIST);
                  (yyval.ll_node)->type = (yyvsp[(2) - (5)].ll_node)->type; 
		;}
    break;

  case 594:
#line 4782 "gram1.y"
    {
		  (yyvsp[(4) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(2) - (5)].ll_node);
		  (yyval.ll_node) =  set_ll_list((yyvsp[(4) - (5)].ll_node), LLNULL, EXPR_LIST);
                  (yyval.ll_node)->type = (yyvsp[(2) - (5)].ll_node)->type; 
		;}
    break;

  case 595:
#line 4790 "gram1.y"
    { inioctl = YES; ;}
    break;

  case 596:
#line 4794 "gram1.y"
    { startioctl();;}
    break;

  case 597:
#line 4802 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 598:
#line 4804 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node); ;}
    break;

  case 599:
#line 4808 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 600:
#line 4810 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 601:
#line 4812 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,(yyvsp[(2) - (3)].token), (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);
	      set_expr_type((yyval.ll_node));
	    ;}
    break;

  case 602:
#line 4817 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,MULT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);
	      set_expr_type((yyval.ll_node));
	    ;}
    break;

  case 603:
#line 4822 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,DIV_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);
	      set_expr_type((yyval.ll_node));
	    ;}
    break;

  case 604:
#line 4827 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,EXP_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);
	      set_expr_type((yyval.ll_node));
	    ;}
    break;

  case 605:
#line 4832 "gram1.y"
    {
	      if((yyvsp[(1) - (2)].token) == SUBT_OP)
		{
		  (yyval.ll_node) = make_llnd(fi,SUBT_OP, (yyvsp[(2) - (2)].ll_node), LLNULL, SMNULL);
		  set_expr_type((yyval.ll_node));
		}
	      else	(yyval.ll_node) = (yyvsp[(2) - (2)].ll_node);
	    ;}
    break;

  case 606:
#line 4841 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,CONCAT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);
	      set_expr_type((yyval.ll_node));
	    ;}
    break;

  case 607:
#line 4846 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 608:
#line 4851 "gram1.y"
    { comments = cur_comment = CMNULL; ;}
    break;

  case 609:
#line 4853 "gram1.y"
    { PTR_CMNT p;
	    p = make_comment(fi,*commentbuf, HALF);
	    if (cur_comment)
               cur_comment->next = p;
            else {
	       if ((pred_bfnd->control_parent->variant == LOGIF_NODE) ||(pred_bfnd->control_parent->variant == FORALL_STAT))

	           pred_bfnd->control_parent->entry.Template.cmnt_ptr = p;

	       else last_bfnd->entry.Template.cmnt_ptr = p;
            }
	    comments = cur_comment = CMNULL;
          ;}
    break;

  case 664:
#line 4927 "gram1.y"
    { PTR_SYMB s;
	      PTR_LLND q, r;
	      if(! explicit_shape) {
              err("Explicit shape specification is required", 50);
		/*$$ = BFNULL;*/
	      }
	      s = make_array((yyvsp[(3) - (4)].hash_entry), TYNULL, (yyvsp[(4) - (4)].ll_node), ndim, LOCAL);
              if(s->attr & TEMPLATE_BIT)
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
              if((s->attr & PROCESSORS_BIT) || (s->attr & TASK_BIT) || (s->attr & DVM_POINTER_BIT))
                errstr( "Inconsistent declaration of identifier  %s ", s->ident, 16);
              else
	        s->attr = s->attr | TEMPLATE_BIT;
              if((yyvsp[(4) - (4)].ll_node)) s->attr = s->attr | DIMENSION_BIT;  
	      q = make_llnd(fi,ARRAY_REF, (yyvsp[(4) - (4)].ll_node), LLNULL, s);
	      s->type->entry.ar_decl.ranges = (yyvsp[(4) - (4)].ll_node);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi,HPF_TEMPLATE_STAT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 665:
#line 4947 "gram1.y"
    {  PTR_SYMB s;
	      PTR_LLND q, r;
	      if(! explicit_shape) {
		err("Explicit shape specification is required", 50);
		/* $$ = BFNULL;*/
	      }
	      s = make_array((yyvsp[(3) - (4)].hash_entry), TYNULL, (yyvsp[(4) - (4)].ll_node), ndim, LOCAL);
              if(s->attr & TEMPLATE_BIT)
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
              if((s->attr & PROCESSORS_BIT) || (s->attr & TASK_BIT) || (s->attr & DVM_POINTER_BIT))
                errstr( "Inconsistent declaration of identifier  %s ", s->ident, 16);
              else
	        s->attr = s->attr | TEMPLATE_BIT;
              if((yyvsp[(4) - (4)].ll_node)) s->attr = s->attr | DIMENSION_BIT;
	      q = make_llnd(fi,ARRAY_REF, (yyvsp[(4) - (4)].ll_node), LLNULL, s);
	      s->type->entry.ar_decl.ranges = (yyvsp[(4) - (4)].ll_node);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (4)].bf_node)->entry.Template.ll_ptr1);
	;}
    break;

  case 666:
#line 4969 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_DYNAMIC_DIR, SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL);;}
    break;

  case 667:
#line 4973 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 668:
#line 4975 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 669:
#line 4979 "gram1.y"
    {  PTR_SYMB s;
	      s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
              if(s->attr &  DYNAMIC_BIT)
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
              if((s->attr & PROCESSORS_BIT) || (s->attr & TASK_BIT) || (s->attr & HEAP_BIT)) 
                errstr("Inconsistent declaration of identifier  %s", s->ident, 16); 
              else
                s->attr = s->attr | DYNAMIC_BIT;        
	      (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, s);
	   ;}
    break;

  case 670:
#line 4992 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_INHERIT_DIR, SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL);;}
    break;

  case 671:
#line 4996 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 672:
#line 4998 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 673:
#line 5002 "gram1.y"
    {  PTR_SYMB s;
	      s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
              if((s->attr & PROCESSORS_BIT) ||(s->attr & TASK_BIT)  || (s->attr & TEMPLATE_BIT) || (s->attr & ALIGN_BIT) || (s->attr & DISTRIBUTE_BIT)) 
                errstr("Inconsistent declaration of identifier  %s", s->ident, 16); 
              else
                s->attr = s->attr | INHERIT_BIT;        
	      (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, s);
	   ;}
    break;

  case 674:
#line 5013 "gram1.y"
    { PTR_LLND q;
             q = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
              /* (void)fprintf(stderr,"hpf.gram: shadow\n");*/ 
	     (yyval.bf_node) = get_bfnd(fi,DVM_SHADOW_DIR,SMNULL,q,(yyvsp[(4) - (4)].ll_node),LLNULL);
            ;}
    break;

  case 675:
#line 5024 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node);;}
    break;

  case 676:
#line 5028 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 677:
#line 5030 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 678:
#line 5034 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 679:
#line 5036 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);;}
    break;

  case 680:
#line 5046 "gram1.y"
    {  PTR_SYMB s;
	      s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
              if(s->attr & SHADOW_BIT)
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
              if((s->attr & PROCESSORS_BIT) ||(s->attr & TASK_BIT)  || (s->attr & TEMPLATE_BIT) || (s->attr & HEAP_BIT)) 
                      errstr( "Inconsistent declaration of identifier %s", s->ident, 16); 
              else
        	      s->attr = s->attr | SHADOW_BIT;  
	      (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, s);
	   ;}
    break;

  case 681:
#line 5058 "gram1.y"
    { PTR_SYMB s;
	      PTR_LLND q, r;
	      if(! explicit_shape) {
              err("Explicit shape specification is required", 50);
		/* $$ = BFNULL;*/
	      }
	      s = make_array((yyvsp[(3) - (4)].hash_entry), TYNULL, (yyvsp[(4) - (4)].ll_node), ndim, LOCAL);
              if(s->attr &  PROCESSORS_BIT)
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
              if((s->attr & ALIGN_BIT) ||(s->attr & DISTRIBUTE_BIT) ||(s->attr & TEMPLATE_BIT) || (s->attr & DYNAMIC_BIT) ||(s->attr & SHADOW_BIT) || (s->attr & TASK_BIT) || (s->attr & DVM_POINTER_BIT) || (s->attr & INHERIT_BIT))
                errstr("Inconsistent declaration of identifier %s", s->ident, 16);
              else
	        s->attr = s->attr | PROCESSORS_BIT;
              if((yyvsp[(4) - (4)].ll_node)) s->attr = s->attr | DIMENSION_BIT;
	      q = make_llnd(fi,ARRAY_REF, (yyvsp[(4) - (4)].ll_node), LLNULL, s);
	      s->type->entry.ar_decl.ranges = (yyvsp[(4) - (4)].ll_node);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi,HPF_PROCESSORS_STAT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 682:
#line 5078 "gram1.y"
    { PTR_SYMB s;
	      PTR_LLND q, r;
	      if(! explicit_shape) {
              err("Explicit shape specification is required", 50);
		/* $$ = BFNULL;*/
	      }
	      s = make_array((yyvsp[(3) - (4)].hash_entry), TYNULL, (yyvsp[(4) - (4)].ll_node), ndim, LOCAL);
              if(s->attr &  PROCESSORS_BIT)
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
              if((s->attr & ALIGN_BIT) ||(s->attr & DISTRIBUTE_BIT) ||(s->attr & TEMPLATE_BIT) || (s->attr & DYNAMIC_BIT) ||(s->attr & SHADOW_BIT) || (s->attr & TASK_BIT) || (s->attr & DVM_POINTER_BIT) || (s->attr & INHERIT_BIT))
                errstr("Inconsistent declaration of identifier %s", s->ident, 16);
              else
	        s->attr = s->attr | PROCESSORS_BIT;
              if((yyvsp[(4) - (4)].ll_node)) s->attr = s->attr | DIMENSION_BIT;
	      q = make_llnd(fi,ARRAY_REF, (yyvsp[(4) - (4)].ll_node), LLNULL, s);
	      s->type->entry.ar_decl.ranges = (yyvsp[(4) - (4)].ll_node);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi,HPF_PROCESSORS_STAT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 683:
#line 5098 "gram1.y"
    {  PTR_SYMB s;
	      PTR_LLND q, r;
	      if(! explicit_shape) {
		err("Explicit shape specification is required", 50);
		/*$$ = BFNULL;*/
	      }
	      s = make_array((yyvsp[(3) - (4)].hash_entry), TYNULL, (yyvsp[(4) - (4)].ll_node), ndim, LOCAL);
              if(s->attr &  PROCESSORS_BIT)
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
              if((s->attr & ALIGN_BIT) ||(s->attr & DISTRIBUTE_BIT) ||(s->attr & TEMPLATE_BIT) || (s->attr & DYNAMIC_BIT) ||(s->attr & SHADOW_BIT) || (s->attr & TASK_BIT) || (s->attr &  DVM_POINTER_BIT) || (s->attr & INHERIT_BIT) )
                errstr("Inconsistent declaration of identifier  %s", s->ident, 16);
              else
	        s->attr = s->attr | PROCESSORS_BIT;
              if((yyvsp[(4) - (4)].ll_node)) s->attr = s->attr | DIMENSION_BIT;
	      q = make_llnd(fi,ARRAY_REF, (yyvsp[(4) - (4)].ll_node), LLNULL, s);
	      s->type->entry.ar_decl.ranges = (yyvsp[(4) - (4)].ll_node);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (4)].bf_node)->entry.Template.ll_ptr1);
	;}
    break;

  case 684:
#line 5120 "gram1.y"
    {  PTR_LLND q,r;
                   q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].symbol));
                   r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	           (yyval.bf_node) = get_bfnd(fi,DVM_INDIRECT_GROUP_DIR, SMNULL, r, LLNULL, LLNULL);
                ;}
    break;

  case 685:
#line 5126 "gram1.y"
    {  PTR_LLND q,r;
                   q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].symbol));
                   r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
                   add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	           ;
                ;}
    break;

  case 686:
#line 5135 "gram1.y"
    {(yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry), REF_GROUP_NAME,global_default,LOCAL);
          if((yyval.symbol)->attr &  INDIRECT_BIT)
                errstr( "Multiple declaration of identifier  %s ", (yyval.symbol)->ident, 73);
           (yyval.symbol)->attr = (yyval.symbol)->attr | INDIRECT_BIT;
          ;}
    break;

  case 687:
#line 5143 "gram1.y"
    {  PTR_LLND q,r;
                   q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].symbol));
                   r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	           (yyval.bf_node) = get_bfnd(fi,DVM_REMOTE_GROUP_DIR, SMNULL, r, LLNULL, LLNULL);
                ;}
    break;

  case 688:
#line 5149 "gram1.y"
    {  PTR_LLND q,r;
                   q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].symbol));
                   r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
                   add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
                ;}
    break;

  case 689:
#line 5157 "gram1.y"
    {(yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry), REF_GROUP_NAME,global_default,LOCAL);
           if((yyval.symbol)->attr &  INDIRECT_BIT)
                errstr( "Inconsistent declaration of identifier  %s ", (yyval.symbol)->ident, 16);
          ;}
    break;

  case 690:
#line 5164 "gram1.y"
    {  PTR_LLND q,r;
                   q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].symbol));
                   r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	           (yyval.bf_node) = get_bfnd(fi,DVM_REDUCTION_GROUP_DIR, SMNULL, r, LLNULL, LLNULL);
                ;}
    break;

  case 691:
#line 5170 "gram1.y"
    {  PTR_LLND q,r;
                   q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].symbol));
                   r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
                   add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	           ;
                ;}
    break;

  case 692:
#line 5179 "gram1.y"
    {(yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry), REDUCTION_GROUP_NAME,global_default,LOCAL);;}
    break;

  case 693:
#line 5183 "gram1.y"
    {  PTR_LLND q,r;
                   q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].symbol));
                   r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	           (yyval.bf_node) = get_bfnd(fi,DVM_CONSISTENT_GROUP_DIR, SMNULL, r, LLNULL, LLNULL);
                ;}
    break;

  case 694:
#line 5189 "gram1.y"
    {  PTR_LLND q,r;
                   q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].symbol));
                   r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
                   add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);	           
                ;}
    break;

  case 695:
#line 5197 "gram1.y"
    {(yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry), CONSISTENT_GROUP_NAME,global_default,LOCAL);;}
    break;

  case 696:
#line 5211 "gram1.y"
    { PTR_SYMB s;
            if(parstate == INEXEC){
              if (!(s = (yyvsp[(2) - (3)].hash_entry)->id_attr))
              {
	         s = make_array((yyvsp[(2) - (3)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
	     	 s->decl = SOFT;
	      } 
            } else
              s = make_array((yyvsp[(2) - (3)].hash_entry), TYNULL, LLNULL, 0, LOCAL);

              (yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(3) - (3)].ll_node), LLNULL, s);
            ;}
    break;

  case 697:
#line 5224 "gram1.y"
    { (yyval.ll_node) = LLNULL; opt_kwd_ = NO;;}
    break;

  case 698:
#line 5230 "gram1.y"
    { PTR_LLND q;
             if(!(yyvsp[(4) - (5)].ll_node))
               err("Distribution format list is omitted", 51);
            /* if($6)
               err("NEW_VALUE specification in DISTRIBUTE directive");*/
             q = set_ll_list((yyvsp[(3) - (5)].ll_node),LLNULL,EXPR_LIST);
	     (yyval.bf_node) = get_bfnd(fi,DVM_DISTRIBUTE_DIR,SMNULL,q,(yyvsp[(4) - (5)].ll_node),(yyvsp[(5) - (5)].ll_node));
            ;}
    break;

  case 699:
#line 5246 "gram1.y"
    { PTR_LLND q;
                /*  if(!$4)
                  {err("Distribution format is omitted", 51); errcnt--;}
                 */
              q = set_ll_list((yyvsp[(3) - (6)].ll_node),LLNULL,EXPR_LIST);
                 /* r = LLNULL;
                   if($6){
                     r = set_ll_list($6,LLNULL,EXPR_LIST);
                     if($7) r = set_ll_list(r,$7,EXPR_LIST);
                   } else
                     if($7) r = set_ll_list(r,$7,EXPR_LIST);
                 */
	      (yyval.bf_node) = get_bfnd(fi,DVM_REDISTRIBUTE_DIR,SMNULL,q,(yyvsp[(4) - (6)].ll_node),(yyvsp[(6) - (6)].ll_node));;}
    break;

  case 700:
#line 5261 "gram1.y"
    {
                 /* r = LLNULL;
                    if($5){
                      r = set_ll_list($5,LLNULL,EXPR_LIST);
                      if($6) r = set_ll_list(r,$6,EXPR_LIST);
                    } else
                      if($6) r = set_ll_list(r,$6,EXPR_LIST);
                  */
	      (yyval.bf_node) = get_bfnd(fi,DVM_REDISTRIBUTE_DIR,SMNULL,(yyvsp[(8) - (8)].ll_node) ,(yyvsp[(3) - (8)].ll_node),(yyvsp[(5) - (8)].ll_node) );
             ;}
    break;

  case 701:
#line 5289 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 702:
#line 5291 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 703:
#line 5295 "gram1.y"
    {(yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 704:
#line 5297 "gram1.y"
    {(yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 705:
#line 5301 "gram1.y"
    {  PTR_SYMB s;
 
          if(parstate == INEXEC){
            if (!(s = (yyvsp[(1) - (1)].hash_entry)->id_attr))
              {
	         s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
	     	 s->decl = SOFT;
	      } 
            if(s->attr & PROCESSORS_BIT)
              errstr( "Illegal use of PROCESSORS name %s ", s->ident, 53);
            if(s->attr & TASK_BIT)
              errstr( "Illegal use of task array name %s ", s->ident, 71);

          } else {
            s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
            if(s->attr & DISTRIBUTE_BIT)
              errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
            else if( (s->attr & PROCESSORS_BIT) || (s->attr & TASK_BIT) || (s->attr & INHERIT_BIT))
              errstr("Inconsistent declaration of identifier  %s",s->ident, 16);
            else
              s->attr = s->attr | DISTRIBUTE_BIT;
          } 
         if(s->attr & ALIGN_BIT)
               errstr("A distributee may not have the ALIGN attribute:%s",s->ident, 54);
          (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, s);               	  
	;}
    break;

  case 706:
#line 5330 "gram1.y"
    {  PTR_SYMB s;
          s = make_array((yyvsp[(1) - (4)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
        
          if(parstate != INEXEC) 
               errstr( "Illegal distributee:%s", s->ident, 312);
          else {
            if(s->attr & PROCESSORS_BIT)
               errstr( "Illegal use of PROCESSORS name %s ", s->ident, 53);  
            if(s->attr & TASK_BIT)
               errstr( "Illegal use of task array name %s ", s->ident, 71);        
            if(s->attr & ALIGN_BIT)
               errstr("A distributee may not have the ALIGN attribute:%s",s->ident, 54);
            if(!(s->attr & DVM_POINTER_BIT))
               errstr("Illegal distributee:%s", s->ident, 312);
          /*s->attr = s->attr | DISTRIBUTE_BIT;*/
	  (yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(3) - (4)].ll_node), LLNULL, s); 
          }
        
	;}
    break;

  case 707:
#line 5353 "gram1.y"
    {  PTR_SYMB s;
          if((s=(yyvsp[(1) - (1)].hash_entry)->id_attr) == SMNULL)
            s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
          if((parstate == INEXEC) && !(s->attr & PROCESSORS_BIT))
               errstr( "'%s' is not processor array ", s->ident, 67);
	  (yyval.symbol) = s
	;}
    break;

  case 708:
#line 5373 "gram1.y"
    { (yyval.ll_node) = LLNULL;  ;}
    break;

  case 709:
#line 5375 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (2)].ll_node);;}
    break;

  case 710:
#line 5379 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node);;}
    break;

  case 711:
#line 5400 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(2) - (2)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 712:
#line 5402 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node),(yyvsp[(4) - (4)].ll_node),EXPR_LIST); ;}
    break;

  case 713:
#line 5405 "gram1.y"
    { opt_kwd_ = YES; ;}
    break;

  case 714:
#line 5414 "gram1.y"
    {  
               (yyval.ll_node) = make_llnd(fi,BLOCK_OP, LLNULL, LLNULL, SMNULL);
        ;}
    break;

  case 715:
#line 5418 "gram1.y"
    {  err("Distribution format BLOCK(n) is not permitted in FDVM", 55);
          (yyval.ll_node) = make_llnd(fi,BLOCK_OP, (yyvsp[(4) - (5)].ll_node), LLNULL, SMNULL);
          endioctl();
        ;}
    break;

  case 716:
#line 5423 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,BLOCK_OP, LLNULL, LLNULL, (yyvsp[(3) - (4)].symbol)); ;}
    break;

  case 717:
#line 5425 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,BLOCK_OP,  (yyvsp[(5) - (6)].ll_node),  LLNULL,  (yyvsp[(3) - (6)].symbol)); ;}
    break;

  case 718:
#line 5427 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,BLOCK_OP,  LLNULL, (yyvsp[(3) - (4)].ll_node),  SMNULL); ;}
    break;

  case 719:
#line 5429 "gram1.y"
    { 
          (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
          (yyval.ll_node)->entry.string_val = (char *) "*";
          (yyval.ll_node)->type = global_string;
        ;}
    break;

  case 720:
#line 5437 "gram1.y"
    {  PTR_SYMB s;
	      s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
              if((s->attr & PROCESSORS_BIT) ||(s->attr & TASK_BIT)  || (s->attr & TEMPLATE_BIT)) 
                errstr("Inconsistent declaration of identifier  %s", s->ident, 16); 
       
	      (yyval.symbol) = s;
	   ;}
    break;

  case 721:
#line 5447 "gram1.y"
    { char *q;
          nioctl = 1;
          q = (yyvsp[(1) - (2)].ll_node)->entry.string_val;
          if((!strcmp(q,"shadow")) && ((yyvsp[(2) - (2)].ll_node)->variant == INT_VAL))                          (yyval.ll_node) = make_llnd(fi,SPEC_PAIR, (yyvsp[(1) - (2)].ll_node), (yyvsp[(2) - (2)].ll_node), SMNULL);
          else
          {  err("Illegal shadow width specification", 56);
             (yyval.ll_node) = LLNULL;
          }
        ;}
    break;

  case 722:
#line 5457 "gram1.y"
    { char *ql, *qh;
          PTR_LLND p1, p2;
          nioctl = 2;
          ql = (yyvsp[(1) - (5)].ll_node)->entry.string_val;
          qh = (yyvsp[(4) - (5)].ll_node)->entry.string_val;
          if((!strcmp(ql,"low_shadow")) && ((yyvsp[(2) - (5)].ll_node)->variant == INT_VAL) && (!strcmp(qh,"high_shadow")) && ((yyvsp[(5) - (5)].ll_node)->variant == INT_VAL)) 
              {
                 p1 = make_llnd(fi,SPEC_PAIR, (yyvsp[(1) - (5)].ll_node), (yyvsp[(2) - (5)].ll_node), SMNULL);
                 p2 = make_llnd(fi,SPEC_PAIR, (yyvsp[(4) - (5)].ll_node), (yyvsp[(5) - (5)].ll_node), SMNULL);
                 (yyval.ll_node) = make_llnd(fi,CONS, p1, p2, SMNULL);
              } 
          else
          {  err("Illegal shadow width specification", 56);
             (yyval.ll_node) = LLNULL;
          }
        ;}
    break;

  case 723:
#line 5486 "gram1.y"
    { PTR_LLND q;
              q = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
              (yyval.bf_node) = (yyvsp[(4) - (4)].bf_node);
              (yyval.bf_node)->entry.Template.ll_ptr1 = q;
            ;}
    break;

  case 724:
#line 5501 "gram1.y"
    { PTR_LLND q;
              q = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
              (yyval.bf_node) = (yyvsp[(4) - (4)].bf_node);
              (yyval.bf_node)->variant = DVM_REALIGN_DIR; 
              (yyval.bf_node)->entry.Template.ll_ptr1 = q;
            ;}
    break;

  case 725:
#line 5508 "gram1.y"
    {
              (yyval.bf_node) = (yyvsp[(3) - (6)].bf_node);
              (yyval.bf_node)->variant = DVM_REALIGN_DIR; 
              (yyval.bf_node)->entry.Template.ll_ptr1 = (yyvsp[(6) - (6)].ll_node);
            ;}
    break;

  case 726:
#line 5526 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 727:
#line 5528 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 728:
#line 5532 "gram1.y"
    {  PTR_SYMB s;
          s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
          if((s->attr & ALIGN_BIT)) 
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
          if((s->attr & PROCESSORS_BIT) || (s->attr & TASK_BIT) || (s->attr & INHERIT_BIT)) 
                errstr( "Inconsistent declaration of identifier  %s", s->ident, 16); 
          else  if(s->attr & DISTRIBUTE_BIT)
               errstr( "An alignee may not have the DISTRIBUTE attribute:'%s'", s->ident,57);             else
                s->attr = s->attr | ALIGN_BIT;     
	  (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, s);
	;}
    break;

  case 729:
#line 5546 "gram1.y"
    {PTR_SYMB s;
        s = (yyvsp[(1) - (1)].ll_node)->entry.Template.symbol;
        if(s->attr & PROCESSORS_BIT)
               errstr( "Illegal use of PROCESSORS name %s ", s->ident, 53);
        else  if(s->attr & TASK_BIT)
              errstr( "Illegal use of task array name %s ", s->ident, 71);
        else if( !(s->attr & DIMENSION_BIT) && !(s->attr & DVM_POINTER_BIT))
            errstr("The alignee %s isn't an array", s->ident, 58);
        else {
            /*  if(!(s->attr & DYNAMIC_BIT))
                 errstr("'%s' hasn't the DYNAMIC attribute", s->ident, 59);
             */
              if(!(s->attr & ALIGN_BIT) && !(s->attr & INHERIT_BIT))
                 errstr("'%s' hasn't the ALIGN attribute", s->ident, 60);
              if(s->attr & DISTRIBUTE_BIT)
                 errstr("An alignee may not have the DISTRIBUTE attribute: %s", s->ident, 57);

/*               if(s->entry.var_decl.local == IO)
 *                 errstr("An alignee may not be the dummy argument");
*/
          }
	  (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, s);
	;}
    break;

  case 730:
#line 5572 "gram1.y"
    { /* PTR_LLND r;
              if($7) {
                r = set_ll_list($6,LLNULL,EXPR_LIST);
                r = set_ll_list(r,$7,EXPR_LIST);
              }
              else
                r = $6;
              */
            (yyval.bf_node) = get_bfnd(fi,DVM_ALIGN_DIR,SMNULL,LLNULL,(yyvsp[(2) - (6)].ll_node),(yyvsp[(6) - (6)].ll_node));
           ;}
    break;

  case 731:
#line 5585 "gram1.y"
    {
           (yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(3) - (4)].ll_node), LLNULL, (yyvsp[(1) - (4)].symbol));        
          ;}
    break;

  case 732:
#line 5601 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 733:
#line 5603 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 734:
#line 5606 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 735:
#line 5608 "gram1.y"
    {
                  (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
                  (yyval.ll_node)->entry.string_val = (char *) "*";
                  (yyval.ll_node)->type = global_string;
                 ;}
    break;

  case 736:
#line 5614 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 737:
#line 5618 "gram1.y"
    { 
         /* if(parstate == INEXEC){ *for REALIGN directive*
              if (!($$ = $1->id_attr))
              {
	         $$ = make_array($1, TYNULL, LLNULL,0,LOCAL);
	     	 $$->decl = SOFT;
	      } 
          } else
             $$ = make_array($1, TYNULL, LLNULL, 0, LOCAL);
          */
          if (!((yyval.symbol) = (yyvsp[(1) - (1)].hash_entry)->id_attr))
          {
	       (yyval.symbol) = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL,0,LOCAL);
	       (yyval.symbol)->decl = SOFT;
	  } 
          (yyval.symbol)->attr = (yyval.symbol)->attr | ALIGN_BASE_BIT;
          if((yyval.symbol)->attr & PROCESSORS_BIT)
               errstr( "Illegal use of PROCESSORS name %s ", (yyval.symbol)->ident, 53);
          else  if((yyval.symbol)->attr & TASK_BIT)
               errstr( "Illegal use of task array name %s ", (yyval.symbol)->ident, 71);
          else
          if((parstate == INEXEC) /* for  REALIGN directive */
             &&   !((yyval.symbol)->attr & DIMENSION_BIT) && !((yyval.symbol)->attr & DVM_POINTER_BIT))
            errstr("The align-target %s isn't declared as array", (yyval.symbol)->ident, 61); 
         ;}
    break;

  case 738:
#line 5646 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 739:
#line 5648 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 740:
#line 5652 "gram1.y"
    { PTR_SYMB s;
            s = make_scalar((yyvsp[(1) - (1)].hash_entry),TYNULL,LOCAL);
            if(s->type->variant != T_INT || s->attr & PARAMETER_BIT)             
              errstr("The align-dummy %s isn't a scalar integer variable", s->ident, 62); 
	   (yyval.ll_node) = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
           (yyval.ll_node)->type = global_int;
         ;}
    break;

  case 741:
#line 5660 "gram1.y"
    {  
          (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
          (yyval.ll_node)->entry.string_val = (char *) "*";
          (yyval.ll_node)->type = global_string;
        ;}
    break;

  case 742:
#line 5666 "gram1.y"
    {   (yyval.ll_node) = make_llnd(fi,DDOT, LLNULL, LLNULL, SMNULL); ;}
    break;

  case 743:
#line 5669 "gram1.y"
    { PTR_SYMB s;
	             PTR_LLND q, r, p;
                     int numdim;
                     if((type_options & PROCESSORS_BIT) || (type_options & TEMPLATE_BIT)){
                       if(! explicit_shape) {
                         err("Explicit shape specification is required", 50);
		         /*$$ = BFNULL;*/
	               }
                     } 
                   /*  else {
                       if($6)
                         err("Shape specification is not permitted", 263);
                     } */
                     if(type_options & DIMENSION_BIT)
                       { p = attr_dims; numdim = attr_ndim;}
                     else
                       { p = LLNULL; numdim = 0; }
                     if((yyvsp[(6) - (6)].ll_node))          /*dimension information after the object name*/
                     { p = (yyvsp[(6) - (6)].ll_node); numdim = ndim;} /*overrides the DIMENSION attribute */
	             s = make_array((yyvsp[(5) - (6)].hash_entry), TYNULL, p, numdim, LOCAL);

                     if((type_options & PROCESSORS_BIT) &&((type_options & ALIGN_BIT) ||(type_options & DISTRIBUTE_BIT) ||(type_options & TEMPLATE_BIT) || (type_options & DYNAMIC_BIT) ||(type_options & SHADOW_BIT) ))
                       err("Illegal combination of attributes", 63);
                     else  if((type_options & PROCESSORS_BIT) && ((s->attr & ALIGN_BIT) ||(s->attr & DISTRIBUTE_BIT) ||(s->attr & TEMPLATE_BIT) || (s->attr & DYNAMIC_BIT) ||(s->attr & SHADOW_BIT)) )
                     {  errstr("Inconsistent declaration of  %s", s->ident, 16);
                        type_options = type_options & (~PROCESSORS_BIT);
                     }
                     else if ((s->attr & PROCESSORS_BIT) && ((type_options & ALIGN_BIT) ||(type_options & DISTRIBUTE_BIT) ||(type_options & TEMPLATE_BIT) || (type_options & DYNAMIC_BIT) ||(type_options & SHADOW_BIT))) 
                        errstr("Inconsistent declaration of  %s", s->ident, 16);
                     else if ((s->attr & INHERIT_BIT) && ((type_options & ALIGN_BIT) ||(type_options & DISTRIBUTE_BIT)))
                        errstr("Inconsistent declaration of  %s", s->ident, 16);
                     if(( s->attr & DISTRIBUTE_BIT) &&  (type_options & DISTRIBUTE_BIT))
                           errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
                     if(( s->attr & ALIGN_BIT) &&  (type_options & ALIGN_BIT))
                           errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
                     if(( s->attr & SHADOW_BIT) &&  (type_options & SHADOW_BIT))
                           errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
                     if(( s->attr & TEMPLATE_BIT) &&  (type_options & TEMPLATE_BIT))
                           errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
                     if(( s->attr & PROCESSORS_BIT) &&  (type_options & PROCESSORS_BIT))
                           errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
	             s->attr = s->attr | type_options;
                     if((yyvsp[(6) - (6)].ll_node)) s->attr = s->attr | DIMENSION_BIT;
                     if((s->attr & DISTRIBUTE_BIT) && (s->attr & ALIGN_BIT))
                       errstr("%s has the DISTRIBUTE and ALIGN attribute",s->ident, 64);
	             q = make_llnd(fi,ARRAY_REF, (yyvsp[(6) - (6)].ll_node), LLNULL, s);
	             if(p) s->type->entry.ar_decl.ranges = p;
	             r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	             (yyval.bf_node) = get_bfnd(fi,DVM_VAR_DECL, SMNULL, r, LLNULL,(yyvsp[(1) - (6)].ll_node));
	            ;}
    break;

  case 744:
#line 5720 "gram1.y"
    { PTR_SYMB s;
	             PTR_LLND q, r, p;
                     int numdim;
                    if((type_options & PROCESSORS_BIT) || (type_options & TEMPLATE_BIT)){
                       if(! explicit_shape) {
                         err("Explicit shape specification is required", 50);
		         /*$$ = BFNULL;*/
	               }
                     } 
                    /* else {
                       if($4)
                         err("Shape specification is not permitted", 263);
                     } */
                     if(type_options & DIMENSION_BIT)
                       { p = attr_dims; numdim = attr_ndim;}
                     else
                       { p = LLNULL; numdim = 0; }
                     if((yyvsp[(4) - (4)].ll_node))                   /*dimension information after the object name*/
                     { p = (yyvsp[(4) - (4)].ll_node); numdim = ndim;}/*overrides the DIMENSION attribute */
	             s = make_array((yyvsp[(3) - (4)].hash_entry), TYNULL, p, numdim, LOCAL);

                     if((type_options & PROCESSORS_BIT) &&((type_options & ALIGN_BIT) ||(type_options & DISTRIBUTE_BIT) ||(type_options & TEMPLATE_BIT) || (type_options & DYNAMIC_BIT) ||(type_options & SHADOW_BIT) ))
                       err("Illegal combination of attributes", 63);
                     else  if((type_options & PROCESSORS_BIT) && ((s->attr & ALIGN_BIT) ||(s->attr & DISTRIBUTE_BIT) ||(s->attr & TEMPLATE_BIT) || (s->attr & DYNAMIC_BIT) ||(s->attr & SHADOW_BIT)) )
                     {  errstr("Inconsistent declaration of identifier %s", s->ident, 16);
                        type_options = type_options & (~PROCESSORS_BIT);
                     }
                     else if ((s->attr & PROCESSORS_BIT) && ((type_options & ALIGN_BIT) ||(type_options & DISTRIBUTE_BIT) ||(type_options & TEMPLATE_BIT) || (type_options & DYNAMIC_BIT) ||(type_options & SHADOW_BIT))) 
                          errstr("Inconsistent declaration of identifier  %s", s->ident,16);
                     else if ((s->attr & INHERIT_BIT) && ((type_options & ALIGN_BIT) ||(type_options & DISTRIBUTE_BIT)))
                          errstr("Inconsistent declaration of identifier %s", s->ident, 16);
                     if(( s->attr & DISTRIBUTE_BIT) &&  (type_options & DISTRIBUTE_BIT))
                          errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
                     if(( s->attr & ALIGN_BIT) &&  (type_options & ALIGN_BIT))
                          errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
                     if(( s->attr & SHADOW_BIT) &&  (type_options & SHADOW_BIT))
                          errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
                     if(( s->attr & TEMPLATE_BIT) &&  (type_options & TEMPLATE_BIT))
                          errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
                     if(( s->attr & PROCESSORS_BIT) &&  (type_options & PROCESSORS_BIT))
                          errstr( "Multiple declaration of identifier  %s ", s->ident, 73);   
	             s->attr = s->attr | type_options;
                     if((yyvsp[(4) - (4)].ll_node)) s->attr = s->attr | DIMENSION_BIT;
                     if((s->attr & DISTRIBUTE_BIT) && (s->attr & ALIGN_BIT))
                           errstr("%s has the DISTRIBUTE and ALIGN attribute",s->ident, 64);
	             q = make_llnd(fi,ARRAY_REF, (yyvsp[(4) - (4)].ll_node), LLNULL, s);
	             if(p) s->type->entry.ar_decl.ranges = p;
	             r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	             add_to_lowLevelList(r, (yyvsp[(1) - (4)].bf_node)->entry.Template.ll_ptr1);
	            ;}
    break;

  case 745:
#line 5779 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); type_options = type_opt; ;}
    break;

  case 746:
#line 5781 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node),(yyvsp[(4) - (4)].ll_node),EXPR_LIST); type_options = type_options | type_opt;;}
    break;

  case 747:
#line 5784 "gram1.y"
    { type_opt = TEMPLATE_BIT;
               (yyval.ll_node) = make_llnd(fi,TEMPLATE_OP,LLNULL,LLNULL,SMNULL);
               ;}
    break;

  case 748:
#line 5788 "gram1.y"
    { type_opt = PROCESSORS_BIT;
                (yyval.ll_node) = make_llnd(fi,PROCESSORS_OP,LLNULL,LLNULL,SMNULL);
               ;}
    break;

  case 749:
#line 5792 "gram1.y"
    { type_opt = PROCESSORS_BIT;
                (yyval.ll_node) = make_llnd(fi,PROCESSORS_OP,LLNULL,LLNULL,SMNULL);
               ;}
    break;

  case 750:
#line 5796 "gram1.y"
    { type_opt = DYNAMIC_BIT;
                (yyval.ll_node) = make_llnd(fi,DYNAMIC_OP,LLNULL,LLNULL,SMNULL);
               ;}
    break;

  case 751:
#line 5813 "gram1.y"
    {
                if(! explicit_shape) {
                  err("Explicit shape specification is required", 50);
                }
                if(! (yyvsp[(3) - (4)].ll_node)) {
                  err("No shape specification", 65);
	        }
                type_opt = DIMENSION_BIT;
                attr_ndim = ndim; attr_dims = (yyvsp[(3) - (4)].ll_node);
                (yyval.ll_node) = make_llnd(fi,DIMENSION_OP,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);
	       ;}
    break;

  case 752:
#line 5825 "gram1.y"
    { type_opt = SHADOW_BIT;
                  (yyval.ll_node) = make_llnd(fi,SHADOW_OP,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);
                 ;}
    break;

  case 753:
#line 5829 "gram1.y"
    { type_opt = ALIGN_BIT;
                  (yyval.ll_node) = make_llnd(fi,ALIGN_OP,(yyvsp[(3) - (7)].ll_node),(yyvsp[(7) - (7)].ll_node),SMNULL);
                 ;}
    break;

  case 754:
#line 5833 "gram1.y"
    { type_opt = ALIGN_BIT;
                  (yyval.ll_node) = make_llnd(fi,ALIGN_OP,LLNULL,SMNULL,SMNULL);
                ;}
    break;

  case 755:
#line 5843 "gram1.y"
    { 
                 type_opt = DISTRIBUTE_BIT;
                 (yyval.ll_node) = make_llnd(fi,DISTRIBUTE_OP,(yyvsp[(2) - (4)].ll_node),(yyvsp[(4) - (4)].ll_node),SMNULL);
                ;}
    break;

  case 756:
#line 5848 "gram1.y"
    { 
                 type_opt = DISTRIBUTE_BIT;
                 (yyval.ll_node) = make_llnd(fi,DISTRIBUTE_OP,LLNULL,LLNULL,SMNULL);
                ;}
    break;

  case 757:
#line 5855 "gram1.y"
    { 
	      PTR_LLND  l;
	      l = make_llnd(fi, TYPE_OP, LLNULL, LLNULL, SMNULL);
	      l->type = (yyvsp[(1) - (11)].data_type);
	      (yyval.bf_node) = get_bfnd(fi,DVM_POINTER_DIR, SMNULL, (yyvsp[(11) - (11)].ll_node),(yyvsp[(7) - (11)].ll_node), l);
	    ;}
    break;

  case 758:
#line 5863 "gram1.y"
    {ndim = 0;;}
    break;

  case 759:
#line 5864 "gram1.y"
    { PTR_LLND  q;
             if(ndim == maxdim)
		err("Too many dimensions", 43);
	      else if(ndim < maxdim)
		q = make_llnd(fi,DDOT,LLNULL,LLNULL,SMNULL);
	      ++ndim;
              (yyval.ll_node) = set_ll_list(q, LLNULL, EXPR_LIST);
	       /*$$ = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);*/
	       /*$$->type = global_default;*/
	    ;}
    break;

  case 760:
#line 5875 "gram1.y"
    { PTR_LLND  q;
             if(ndim == maxdim)
		err("Too many dimensions", 43);
	      else if(ndim < maxdim)
		q = make_llnd(fi,DDOT,LLNULL,LLNULL,SMNULL);
	      ++ndim;
              (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), q, EXPR_LIST);
            ;}
    break;

  case 761:
#line 5886 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 762:
#line 5888 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 763:
#line 5892 "gram1.y"
    {PTR_SYMB s;
           /* s = make_scalar($1,TYNULL,LOCAL);*/
            s = make_array((yyvsp[(1) - (1)].hash_entry),TYNULL,LLNULL,0,LOCAL);
            s->attr = s->attr | DVM_POINTER_BIT;
            if((s->attr & PROCESSORS_BIT) || (s->attr & TASK_BIT) || (s->attr & INHERIT_BIT))
               errstr( "Inconsistent declaration of identifier %s", s->ident, 16);     
            (yyval.ll_node) = make_llnd(fi,VAR_REF,LLNULL,LLNULL,s);
            ;}
    break;

  case 764:
#line 5903 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_HEAP_DIR, SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL);;}
    break;

  case 765:
#line 5907 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 766:
#line 5909 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 767:
#line 5913 "gram1.y"
    {  PTR_SYMB s;
	      s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
              s->attr = s->attr | HEAP_BIT;
              if((s->attr & PROCESSORS_BIT) ||(s->attr & TASK_BIT)  || (s->attr & TEMPLATE_BIT) || (s->attr & ALIGN_BIT) || (s->attr & DISTRIBUTE_BIT) || (s->attr & INHERIT_BIT) || (s->attr & DYNAMIC_BIT) || (s->attr & SHADOW_BIT) || (s->attr & DVM_POINTER_BIT)) 
                errstr("Inconsistent declaration of identifier  %s", s->ident, 16); 
      
	      (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, s);
	   ;}
    break;

  case 768:
#line 5924 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_CONSISTENT_DIR, SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL);;}
    break;

  case 769:
#line 5928 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 770:
#line 5930 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 771:
#line 5934 "gram1.y"
    {  PTR_SYMB s;
	      s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
              s->attr = s->attr | CONSISTENT_BIT;
              if((s->attr & PROCESSORS_BIT) ||(s->attr & TASK_BIT)  || (s->attr & TEMPLATE_BIT) || (s->attr & ALIGN_BIT) || (s->attr & DISTRIBUTE_BIT) || (s->attr & INHERIT_BIT) || (s->attr & DYNAMIC_BIT) || (s->attr & SHADOW_BIT) || (s->attr & DVM_POINTER_BIT)) 
                errstr("Inconsistent declaration of identifier  %s", s->ident, 16); 
      
	      (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, s);
	   ;}
    break;

  case 772:
#line 5946 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_ASYNCID_DIR, SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL);;}
    break;

  case 773:
#line 5948 "gram1.y"
    { PTR_LLND p;
              p = make_llnd(fi,COMM_LIST, LLNULL, LLNULL, SMNULL);              
              (yyval.bf_node) = get_bfnd(fi,DVM_ASYNCID_DIR, SMNULL, (yyvsp[(8) - (8)].ll_node), p, LLNULL);
            ;}
    break;

  case 774:
#line 5955 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 775:
#line 5957 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 776:
#line 5961 "gram1.y"
    {  PTR_SYMB s;
              if((yyvsp[(2) - (2)].ll_node)){
                  s = make_array((yyvsp[(1) - (2)].hash_entry), global_default, (yyvsp[(2) - (2)].ll_node), ndim, LOCAL);
		  s->variant = ASYNC_ID;
                  s->attr = s->attr | DIMENSION_BIT;
                  s->type->entry.ar_decl.ranges = (yyvsp[(2) - (2)].ll_node);
                  (yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(2) - (2)].ll_node), LLNULL, s);
              } else {
              s = make_local_entity((yyvsp[(1) - (2)].hash_entry), ASYNC_ID, global_default, LOCAL);
	      (yyval.ll_node) = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
              }
	   ;}
    break;

  case 777:
#line 5977 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_NEW_VALUE_DIR,SMNULL, LLNULL, LLNULL,LLNULL);;}
    break;

  case 778:
#line 5999 "gram1.y"
    {  if( (yyvsp[(8) - (9)].ll_node)->entry.Template.symbol->attr & TASK_BIT)
           (yyval.bf_node) = get_bfnd(fi,DVM_PARALLEL_TASK_DIR,SMNULL,(yyvsp[(8) - (9)].ll_node),(yyvsp[(9) - (9)].ll_node),(yyvsp[(4) - (9)].ll_node));
         else
           (yyval.bf_node) = get_bfnd(fi,DVM_PARALLEL_ON_DIR,SMNULL,(yyvsp[(8) - (9)].ll_node),(yyvsp[(9) - (9)].ll_node),(yyvsp[(4) - (9)].ll_node));
      ;}
    break;

  case 779:
#line 6007 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 780:
#line 6009 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 782:
#line 6016 "gram1.y"
    {
          if((yyvsp[(1) - (4)].ll_node)->type->variant != T_ARRAY) 
           errstr("'%s' isn't array", (yyvsp[(1) - (4)].ll_node)->entry.Template.symbol->ident, 66);
                 (yyvsp[(1) - (4)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);
                 (yyval.ll_node) = (yyvsp[(1) - (4)].ll_node);
                 (yyval.ll_node)->type = (yyvsp[(1) - (4)].ll_node)->type->entry.ar_decl.base_type;
         ;}
    break;

  case 783:
#line 6026 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 784:
#line 6028 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 785:
#line 6032 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 786:
#line 6034 "gram1.y"
    {
             (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
             (yyval.ll_node)->entry.string_val = (char *) "*";
             (yyval.ll_node)->type = global_string;
            ;}
    break;

  case 787:
#line 6042 "gram1.y"
    {  (yyval.ll_node) = LLNULL;;}
    break;

  case 788:
#line 6044 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 789:
#line 6048 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 790:
#line 6050 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (2)].ll_node),(yyvsp[(2) - (2)].ll_node),EXPR_LIST); ;}
    break;

  case 801:
#line 6067 "gram1.y"
    { if((yyvsp[(5) - (8)].symbol)->attr & INDIRECT_BIT)
                            errstr("'%s' is not remote group name", (yyvsp[(5) - (8)].symbol)->ident, 68);
                          (yyval.ll_node) = make_llnd(fi,REMOTE_ACCESS_OP,(yyvsp[(7) - (8)].ll_node),LLNULL,(yyvsp[(5) - (8)].symbol));
                        ;}
    break;

  case 802:
#line 6072 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,REMOTE_ACCESS_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 803:
#line 6076 "gram1.y"
    {
                          (yyval.ll_node) = make_llnd(fi,CONSISTENT_OP,(yyvsp[(7) - (8)].ll_node),LLNULL,(yyvsp[(5) - (8)].symbol));
                        ;}
    break;

  case 804:
#line 6080 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,CONSISTENT_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 805:
#line 6084 "gram1.y"
    {  
            if(((yyval.symbol)=(yyvsp[(1) - (1)].hash_entry)->id_attr) == SMNULL){
                errstr("'%s' is not declared as group", (yyvsp[(1) - (1)].hash_entry)->ident, 74);
                (yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry),CONSISTENT_GROUP_NAME,global_default,LOCAL);
            } else {
                if((yyval.symbol)->variant != CONSISTENT_GROUP_NAME)
                   errstr("'%s' is not declared as group", (yyvsp[(1) - (1)].hash_entry)->ident, 74);
            }
          ;}
    break;

  case 806:
#line 6097 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi,NEW_SPEC_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 807:
#line 6101 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi,NEW_SPEC_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 808:
#line 6105 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_PRIVATE_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 809:
#line 6109 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_CUDA_BLOCK_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 810:
#line 6112 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);;}
    break;

  case 811:
#line 6114 "gram1.y"
    {(yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);;}
    break;

  case 812:
#line 6116 "gram1.y"
    {(yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node),(yyvsp[(3) - (5)].ll_node),EXPR_LIST); (yyval.ll_node) = set_ll_list((yyval.ll_node),(yyvsp[(5) - (5)].ll_node),EXPR_LIST);;}
    break;

  case 813:
#line 6120 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);;}
    break;

  case 814:
#line 6122 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);;}
    break;

  case 815:
#line 6126 "gram1.y"
    { if(!((yyvsp[(5) - (8)].symbol)->attr & INDIRECT_BIT))
                         errstr("'%s' is not indirect group name", (yyvsp[(5) - (8)].symbol)->ident, 313);
                      (yyval.ll_node) = make_llnd(fi,INDIRECT_ACCESS_OP,(yyvsp[(7) - (8)].ll_node),LLNULL,(yyvsp[(5) - (8)].symbol));
                    ;}
    break;

  case 816:
#line 6131 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,INDIRECT_ACCESS_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 817:
#line 6135 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi,STAGE_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 818:
#line 6139 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi,ACROSS_OP,(yyvsp[(4) - (4)].ll_node),LLNULL,SMNULL);;}
    break;

  case 819:
#line 6141 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi,ACROSS_OP,(yyvsp[(4) - (5)].ll_node),(yyvsp[(5) - (5)].ll_node),SMNULL);;}
    break;

  case 820:
#line 6145 "gram1.y"
    {  if((yyvsp[(3) - (5)].ll_node))
                     (yyval.ll_node) = make_llnd(fi,DDOT,(yyvsp[(3) - (5)].ll_node),(yyvsp[(4) - (5)].ll_node),SMNULL);
                   else
                     (yyval.ll_node) = (yyvsp[(4) - (5)].ll_node);
                ;}
    break;

  case 821:
#line 6153 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "in";
              (yyval.ll_node)->type = global_string;
            ;}
    break;

  case 822:
#line 6159 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "out";
              (yyval.ll_node)->type = global_string;
            ;}
    break;

  case 823:
#line 6165 "gram1.y"
    {  (yyval.ll_node) = LLNULL; opt_kwd_ = NO;;}
    break;

  case 824:
#line 6169 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);;}
    break;

  case 825:
#line 6171 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);;}
    break;

  case 826:
#line 6175 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 827:
#line 6177 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (4)].ll_node);
                    (yyval.ll_node)-> entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);  
                  ;}
    break;

  case 828:
#line 6181 "gram1.y"
    { /*  PTR_LLND p;
                       p = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
                       p->entry.string_val = (char *) "corner";
                       p->type = global_string;
                   */
                   (yyvsp[(1) - (7)].ll_node)-> entry.Template.ll_ptr1 = (yyvsp[(3) - (7)].ll_node);  
                   (yyval.ll_node) = make_llnd(fi,ARRAY_OP,(yyvsp[(1) - (7)].ll_node),(yyvsp[(6) - (7)].ll_node),SMNULL);
                 ;}
    break;

  case 829:
#line 6193 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);;}
    break;

  case 830:
#line 6195 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);;}
    break;

  case 831:
#line 6199 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);;}
    break;

  case 832:
#line 6203 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);;}
    break;

  case 833:
#line 6205 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);;}
    break;

  case 834:
#line 6209 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,(yyvsp[(1) - (5)].ll_node),make_llnd(fi,DDOT,(yyvsp[(3) - (5)].ll_node),(yyvsp[(5) - (5)].ll_node),SMNULL),SMNULL); ;}
    break;

  case 835:
#line 6211 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,(yyvsp[(1) - (3)].ll_node),make_llnd(fi,DDOT,(yyvsp[(3) - (3)].ll_node),LLNULL,SMNULL),SMNULL); ;}
    break;

  case 836:
#line 6213 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,(yyvsp[(1) - (3)].ll_node),make_llnd(fi,DDOT,LLNULL,(yyvsp[(3) - (3)].ll_node),SMNULL),SMNULL); ;}
    break;

  case 837:
#line 6215 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,(yyvsp[(1) - (1)].ll_node),LLNULL,SMNULL); ;}
    break;

  case 838:
#line 6217 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,LLNULL,make_llnd(fi,DDOT,(yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),SMNULL),SMNULL); ;}
    break;

  case 839:
#line 6219 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,LLNULL,make_llnd(fi,DDOT,(yyvsp[(1) - (1)].ll_node),LLNULL,SMNULL),SMNULL); ;}
    break;

  case 840:
#line 6221 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,LLNULL,make_llnd(fi,DDOT,LLNULL,(yyvsp[(1) - (1)].ll_node),SMNULL),SMNULL); ;}
    break;

  case 841:
#line 6225 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(3) - (3)].ll_node);;}
    break;

  case 842:
#line 6229 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(3) - (3)].ll_node);;}
    break;

  case 843:
#line 6233 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(3) - (3)].ll_node);;}
    break;

  case 844:
#line 6237 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node);;}
    break;

  case 845:
#line 6241 "gram1.y"
    {PTR_LLND q;
                /* q = set_ll_list($9,$6,EXPR_LIST); */
                 q = set_ll_list((yyvsp[(6) - (10)].ll_node),LLNULL,EXPR_LIST); /*podd 11.10.01*/
                 q = add_to_lowLevelList((yyvsp[(9) - (10)].ll_node),q);        /*podd 11.10.01*/
                 (yyval.ll_node) = make_llnd(fi,REDUCTION_OP,q,LLNULL,SMNULL);
                ;}
    break;

  case 846:
#line 6248 "gram1.y"
    {PTR_LLND q;
                 q = set_ll_list((yyvsp[(6) - (8)].ll_node),LLNULL,EXPR_LIST);
                 (yyval.ll_node) = make_llnd(fi,REDUCTION_OP,q,LLNULL,SMNULL);
                ;}
    break;

  case 847:
#line 6254 "gram1.y"
    {  (yyval.ll_node) = make_llnd(fi,REDUCTION_OP,(yyvsp[(9) - (10)].ll_node),LLNULL,(yyvsp[(6) - (10)].symbol)); ;}
    break;

  case 848:
#line 6258 "gram1.y"
    { opt_kwd_r = YES; ;}
    break;

  case 849:
#line 6261 "gram1.y"
    { opt_kwd_r = NO; ;}
    break;

  case 850:
#line 6265 "gram1.y"
    { 
                  if(((yyval.symbol)=(yyvsp[(1) - (1)].hash_entry)->id_attr) == SMNULL) {
                      errstr("'%s' is not declared as reduction group", (yyvsp[(1) - (1)].hash_entry)->ident, 69);
                      (yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry),REDUCTION_GROUP_NAME,global_default,LOCAL);
                  } else {
                    if((yyval.symbol)->variant != REDUCTION_GROUP_NAME)
                      errstr("'%s' is not declared as reduction group", (yyvsp[(1) - (1)].hash_entry)->ident, 69);
                  }
                ;}
    break;

  case 851:
#line 6278 "gram1.y"
    {(yyval.ll_node) = set_ll_list((yyvsp[(2) - (2)].ll_node),LLNULL,EXPR_LIST);;}
    break;

  case 852:
#line 6280 "gram1.y"
    {(yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node),(yyvsp[(4) - (4)].ll_node),EXPR_LIST);;}
    break;

  case 853:
#line 6284 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi,ARRAY_OP,(yyvsp[(1) - (4)].ll_node),(yyvsp[(3) - (4)].ll_node),SMNULL);;}
    break;

  case 854:
#line 6286 "gram1.y"
    {(yyvsp[(3) - (6)].ll_node) = set_ll_list((yyvsp[(3) - (6)].ll_node),(yyvsp[(5) - (6)].ll_node),EXPR_LIST);
            (yyval.ll_node) = make_llnd(fi,ARRAY_OP,(yyvsp[(1) - (6)].ll_node),(yyvsp[(3) - (6)].ll_node),SMNULL);;}
    break;

  case 855:
#line 6291 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "sum";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 856:
#line 6297 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "product";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 857:
#line 6303 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "min";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 858:
#line 6309 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "max";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 859:
#line 6315 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "or";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 860:
#line 6321 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "and";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 861:
#line 6327 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "eqv";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 862:
#line 6333 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "neqv";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 863:
#line 6339 "gram1.y"
    { err("Illegal reduction operation name", 70);
               errcnt--;
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "unknown";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 864:
#line 6348 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "maxloc";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 865:
#line 6354 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "minloc";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 866:
#line 6371 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SHADOW_RENEW_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 867:
#line 6379 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SHADOW_START_OP,LLNULL,LLNULL,(yyvsp[(4) - (4)].symbol));;}
    break;

  case 868:
#line 6387 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SHADOW_WAIT_OP,LLNULL,LLNULL,(yyvsp[(4) - (4)].symbol));;}
    break;

  case 869:
#line 6389 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SHADOW_COMP_OP,LLNULL,LLNULL,SMNULL);;}
    break;

  case 870:
#line 6391 "gram1.y"
    {  (yyvsp[(5) - (9)].ll_node)-> entry.Template.ll_ptr1 = (yyvsp[(7) - (9)].ll_node); (yyval.ll_node) = make_llnd(fi,SHADOW_COMP_OP,(yyvsp[(5) - (9)].ll_node),LLNULL,SMNULL);;}
    break;

  case 871:
#line 6395 "gram1.y"
    {(yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry), SHADOW_GROUP_NAME,global_default,LOCAL);;}
    break;

  case 872:
#line 6399 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);;}
    break;

  case 873:
#line 6401 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);;}
    break;

  case 874:
#line 6405 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 875:
#line 6407 "gram1.y"
    { PTR_LLND p;
          p = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
          p->entry.string_val = (char *) "corner";
          p->type = global_string;
          (yyval.ll_node) = make_llnd(fi,ARRAY_OP,(yyvsp[(1) - (5)].ll_node),p,SMNULL);
         ;}
    break;

  case 876:
#line 6415 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node);
          (yyval.ll_node)-> entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);  
        ;}
    break;

  case 877:
#line 6419 "gram1.y"
    { PTR_LLND p;
          p = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
          p->entry.string_val = (char *) "corner";
          p->type = global_string;
          (yyvsp[(1) - (9)].ll_node)-> entry.Template.ll_ptr1 = (yyvsp[(4) - (9)].ll_node);  
          (yyval.ll_node) = make_llnd(fi,ARRAY_OP,(yyvsp[(1) - (9)].ll_node),p,SMNULL);
       ;}
    break;

  case 878:
#line 6430 "gram1.y"
    { optcorner = YES; ;}
    break;

  case 879:
#line 6434 "gram1.y"
    { PTR_SYMB s;
         s = (yyvsp[(1) - (1)].ll_node)->entry.Template.symbol;
         if(s->attr & PROCESSORS_BIT)
             errstr( "Illegal use of PROCESSORS name %s ", s->ident, 53);
         else if(s->attr & TASK_BIT)
             errstr( "Illegal use of task array name %s ", s->ident, 71);
         else
           if(s->type->variant != T_ARRAY) 
             errstr("'%s' isn't array", s->ident, 66);
           else 
              if((!(s->attr & DISTRIBUTE_BIT)) && (!(s->attr & ALIGN_BIT)))
               ; /*errstr("hpf.gram: %s is not distributed array", s->ident);*/
                
         (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);
        ;}
    break;

  case 880:
#line 6460 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_SHADOW_START_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 881:
#line 6462 "gram1.y"
    {errstr("Missing DVM directive prefix", 49);;}
    break;

  case 882:
#line 6466 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_SHADOW_WAIT_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 883:
#line 6468 "gram1.y"
    {errstr("Missing DVM directive prefix", 49);;}
    break;

  case 884:
#line 6472 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_SHADOW_GROUP_DIR,(yyvsp[(3) - (6)].symbol),(yyvsp[(5) - (6)].ll_node),LLNULL,LLNULL);;}
    break;

  case 885:
#line 6476 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_REDUCTION_START_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 886:
#line 6480 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_REDUCTION_WAIT_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 887:
#line 6489 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_CONSISTENT_START_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 888:
#line 6493 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_CONSISTENT_WAIT_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 889:
#line 6497 "gram1.y"
    { if(((yyvsp[(4) - (7)].symbol)->attr & INDIRECT_BIT))
                errstr("'%s' is not remote group name", (yyvsp[(4) - (7)].symbol)->ident, 68);
           (yyval.bf_node) = get_bfnd(fi,DVM_REMOTE_ACCESS_DIR,(yyvsp[(4) - (7)].symbol),(yyvsp[(6) - (7)].ll_node),LLNULL,LLNULL);
         ;}
    break;

  case 890:
#line 6502 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_REMOTE_ACCESS_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL);;}
    break;

  case 891:
#line 6506 "gram1.y"
    {  
            if(((yyval.symbol)=(yyvsp[(1) - (1)].hash_entry)->id_attr) == SMNULL){
                errstr("'%s' is not declared as group", (yyvsp[(1) - (1)].hash_entry)->ident, 74);
                (yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry),REF_GROUP_NAME,global_default,LOCAL);
            } else {
              if((yyval.symbol)->variant != REF_GROUP_NAME)
                errstr("'%s' is not declared as group", (yyvsp[(1) - (1)].hash_entry)->ident, 74);
            }
          ;}
    break;

  case 892:
#line 6518 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 893:
#line 6520 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 894:
#line 6524 "gram1.y"
    {
              (yyval.ll_node) = (yyvsp[(1) - (4)].ll_node);
              (yyval.ll_node)->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);
            ;}
    break;

  case 895:
#line 6529 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 896:
#line 6533 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 897:
#line 6535 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 898:
#line 6539 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 899:
#line 6541 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT, LLNULL, LLNULL, SMNULL);;}
    break;

  case 900:
#line 6545 "gram1.y"
    {  PTR_LLND q;
             q = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
             (yyval.bf_node) = get_bfnd(fi,DVM_TASK_DIR,SMNULL,q,LLNULL,LLNULL);
          ;}
    break;

  case 901:
#line 6550 "gram1.y"
    {   PTR_LLND q;
              q = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      add_to_lowLevelList(q, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
          ;}
    break;

  case 902:
#line 6557 "gram1.y"
    { 
             PTR_SYMB s;
	      s = make_array((yyvsp[(1) - (2)].hash_entry), global_int, (yyvsp[(2) - (2)].ll_node), ndim, LOCAL);
              if((yyvsp[(2) - (2)].ll_node)){
                  s->attr = s->attr | DIMENSION_BIT;
                  s->type->entry.ar_decl.ranges = (yyvsp[(2) - (2)].ll_node);
              }
              else
                  err("No dimensions in TASK directive", 75);
              if(ndim > 1)
                  errstr("Illegal rank of '%s'", s->ident, 76);
              if(s->attr & TASK_BIT)
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
              if((s->attr & ALIGN_BIT) ||(s->attr & DISTRIBUTE_BIT) ||(s->attr & TEMPLATE_BIT) || (s->attr & DYNAMIC_BIT) ||(s->attr & SHADOW_BIT) || (s->attr & PROCESSORS_BIT)  || (s->attr & DVM_POINTER_BIT) || (s->attr & INHERIT_BIT))
                errstr("Inconsistent declaration of identifier  %s", s->ident, 16);
              else
	        s->attr = s->attr | TASK_BIT;
    
	      (yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(2) - (2)].ll_node), LLNULL, s);	  
	    ;}
    break;

  case 903:
#line 6580 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi,DVM_TASK_REGION_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 904:
#line 6582 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi,DVM_TASK_REGION_DIR,(yyvsp[(3) - (4)].symbol),(yyvsp[(4) - (4)].ll_node),LLNULL,LLNULL);;}
    break;

  case 905:
#line 6584 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi,DVM_TASK_REGION_DIR,(yyvsp[(3) - (4)].symbol),LLNULL,(yyvsp[(4) - (4)].ll_node),LLNULL);;}
    break;

  case 906:
#line 6586 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi,DVM_TASK_REGION_DIR,(yyvsp[(3) - (5)].symbol),(yyvsp[(4) - (5)].ll_node),(yyvsp[(5) - (5)].ll_node),LLNULL);;}
    break;

  case 907:
#line 6588 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi,DVM_TASK_REGION_DIR,(yyvsp[(3) - (5)].symbol),(yyvsp[(5) - (5)].ll_node),(yyvsp[(4) - (5)].ll_node),LLNULL);;}
    break;

  case 908:
#line 6592 "gram1.y"
    { PTR_SYMB s;
              if((s=(yyvsp[(1) - (1)].hash_entry)->id_attr) == SMNULL)
                s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
              
              if(!(s->attr & TASK_BIT))
                 errstr("'%s' is not task array", s->ident, 77);
              (yyval.symbol) = s;
              ;}
    break;

  case 909:
#line 6603 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_END_TASK_REGION_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 910:
#line 6607 "gram1.y"
    {  PTR_SYMB s;
              PTR_LLND q;
             /*
              s = make_array($1, TYNULL, LLNULL, 0, LOCAL);                           
	      if((parstate == INEXEC) && !(s->attr & TASK_BIT))
                 errstr("'%s' is not task array", s->ident, 77);  
              q =  set_ll_list($3,LLNULL,EXPR_LIST);
	      $$ = make_llnd(fi,ARRAY_REF, q, LLNULL, s);
              */

              s = (yyvsp[(1) - (4)].symbol);
              q =  set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
	      (yyval.ll_node) = make_llnd(fi,ARRAY_REF, q, LLNULL, s);
	   ;}
    break;

  case 911:
#line 6622 "gram1.y"
    {  PTR_LLND q; 
              q =  set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
	      (yyval.ll_node) = make_llnd(fi,ARRAY_REF, q, LLNULL, (yyvsp[(1) - (4)].symbol));
	   ;}
    break;

  case 912:
#line 6629 "gram1.y"
    {              
         (yyval.bf_node) = get_bfnd(fi,DVM_ON_DIR,SMNULL,(yyvsp[(3) - (4)].ll_node),(yyvsp[(4) - (4)].ll_node),LLNULL);
    ;}
    break;

  case 913:
#line 6635 "gram1.y"
    {(yyval.ll_node) = LLNULL;;}
    break;

  case 914:
#line 6637 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 915:
#line 6641 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi,DVM_END_ON_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 916:
#line 6645 "gram1.y"
    { PTR_LLND q;
        /* if(!($6->attr & PROCESSORS_BIT))
           errstr("'%s' is not processor array", $6->ident, 67);
         */
        q = make_llnd(fi,ARRAY_REF, (yyvsp[(7) - (7)].ll_node), LLNULL, (yyvsp[(6) - (7)].symbol));
        (yyval.bf_node) = get_bfnd(fi,DVM_MAP_DIR,SMNULL,(yyvsp[(3) - (7)].ll_node),q,LLNULL);
      ;}
    break;

  case 917:
#line 6653 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_MAP_DIR,SMNULL,(yyvsp[(3) - (6)].ll_node),LLNULL,(yyvsp[(6) - (6)].ll_node)); ;}
    break;

  case 918:
#line 6657 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_PREFETCH_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 919:
#line 6661 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_RESET_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 920:
#line 6669 "gram1.y"
    { if(!((yyvsp[(4) - (7)].symbol)->attr & INDIRECT_BIT))
                         errstr("'%s' is not indirect group name", (yyvsp[(4) - (7)].symbol)->ident, 313);
                      (yyval.bf_node) = get_bfnd(fi,DVM_INDIRECT_ACCESS_DIR,(yyvsp[(4) - (7)].symbol),(yyvsp[(6) - (7)].ll_node),LLNULL,LLNULL);
                    ;}
    break;

  case 921:
#line 6674 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_INDIRECT_ACCESS_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL);;}
    break;

  case 922:
#line 6688 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 923:
#line 6690 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 924:
#line 6694 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 925:
#line 6696 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (4)].ll_node); (yyval.ll_node)->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);;}
    break;

  case 926:
#line 6705 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,HPF_INDEPENDENT_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 927:
#line 6707 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,HPF_INDEPENDENT_DIR,SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL);;}
    break;

  case 928:
#line 6709 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,HPF_INDEPENDENT_DIR,SMNULL, LLNULL, (yyvsp[(3) - (3)].ll_node), LLNULL);;}
    break;

  case 929:
#line 6711 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,HPF_INDEPENDENT_DIR,SMNULL, (yyvsp[(3) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node),LLNULL);;}
    break;

  case 930:
#line 6747 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi,REDUCTION_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 931:
#line 6751 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_ASYNCHRONOUS_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);;}
    break;

  case 932:
#line 6755 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_ENDASYNCHRONOUS_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 933:
#line 6759 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_ASYNCWAIT_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);;}
    break;

  case 934:
#line 6763 "gram1.y"
    {  
            if(((yyval.symbol)=(yyvsp[(1) - (1)].hash_entry)->id_attr) == SMNULL) {
                errstr("'%s' is not declared as ASYNCID", (yyvsp[(1) - (1)].hash_entry)->ident, 115);
                (yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry),ASYNC_ID,global_default,LOCAL);
            } else {
              if((yyval.symbol)->variant != ASYNC_ID)
                errstr("'%s' is not declared as ASYNCID", (yyvsp[(1) - (1)].hash_entry)->ident, 115);
            }
     ;}
    break;

  case 935:
#line 6775 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(1) - (1)].symbol));;}
    break;

  case 936:
#line 6777 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(3) - (4)].ll_node), LLNULL, (yyvsp[(1) - (4)].symbol));;}
    break;

  case 937:
#line 6781 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_F90_DIR,SMNULL,(yyvsp[(3) - (5)].ll_node),(yyvsp[(5) - (5)].ll_node),LLNULL);;}
    break;

  case 938:
#line 6784 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_DEBUG_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);;}
    break;

  case 939:
#line 6786 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_DEBUG_DIR,SMNULL,(yyvsp[(3) - (6)].ll_node),(yyvsp[(5) - (6)].ll_node),LLNULL);;}
    break;

  case 940:
#line 6790 "gram1.y"
    { 
              (yyval.ll_node) = set_ll_list((yyvsp[(2) - (2)].ll_node), LLNULL, EXPR_LIST);
              endioctl();
            ;}
    break;

  case 941:
#line 6795 "gram1.y"
    { 
              (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node), EXPR_LIST);
              endioctl();
            ;}
    break;

  case 942:
#line 6802 "gram1.y"
    { (yyval.ll_node)  = make_llnd(fi, KEYWORD_ARG, (yyvsp[(1) - (2)].ll_node), (yyvsp[(2) - (2)].ll_node), SMNULL); ;}
    break;

  case 943:
#line 6805 "gram1.y"
    {
	         (yyval.ll_node) = make_llnd(fi,INT_VAL, LLNULL, LLNULL, SMNULL);
	         (yyval.ll_node)->entry.ival = atoi(yytext);
	         (yyval.ll_node)->type = global_int;
	        ;}
    break;

  case 944:
#line 6813 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_ENDDEBUG_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);;}
    break;

  case 945:
#line 6817 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_INTERVAL_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);;}
    break;

  case 946:
#line 6821 "gram1.y"
    { (yyval.ll_node) = LLNULL;;}
    break;

  case 947:
#line 6824 "gram1.y"
    { if((yyvsp[(1) - (1)].ll_node)->type->variant != T_INT)             
                    err("Illegal interval number", 78);
                  (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);
                 ;}
    break;

  case 948:
#line 6831 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_ENDINTERVAL_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 949:
#line 6835 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_TRACEON_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 950:
#line 6839 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_TRACEOFF_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 951:
#line 6843 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_BARRIER_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 952:
#line 6847 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_CHECK_DIR,SMNULL,(yyvsp[(9) - (9)].ll_node),(yyvsp[(5) - (9)].ll_node),LLNULL); ;}
    break;

  case 953:
#line 6851 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_IO_MODE_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL);;}
    break;

  case 954:
#line 6854 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 955:
#line 6856 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 956:
#line 6860 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_ASYNC_OP,LLNULL,LLNULL,SMNULL);;}
    break;

  case 957:
#line 6862 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_LOCAL_OP, LLNULL,LLNULL,SMNULL);;}
    break;

  case 958:
#line 6864 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,PARALLEL_OP, LLNULL,LLNULL,SMNULL);;}
    break;

  case 986:
#line 6897 "gram1.y"
    {
          (yyval.bf_node) = get_bfnd(fi,OMP_ONETHREAD_DIR,SMNULL,LLNULL,LLNULL,LLNULL);
	;}
    break;

  case 987:
#line 6903 "gram1.y"
    {
  	   (yyval.bf_node) = make_endparallel();
	;}
    break;

  case 988:
#line 6909 "gram1.y"
    {
  	   (yyval.bf_node) = make_parallel();
           (yyval.bf_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (4)].ll_node);
	   opt_kwd_ = NO;
	;}
    break;

  case 989:
#line 6915 "gram1.y"
    {
  	   (yyval.bf_node) = make_parallel();
	   opt_kwd_ = NO;
	;}
    break;

  case 990:
#line 6921 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
	;}
    break;

  case 991:
#line 6925 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node),(yyvsp[(4) - (5)].ll_node),EXPR_LIST);	
	;}
    break;

  case 1001:
#line 6942 "gram1.y"
    {
		(yyval.ll_node) = (yyvsp[(4) - (5)].ll_node);
        ;}
    break;

  case 1002:
#line 6947 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_PRIVATE,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1003:
#line 6952 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_FIRSTPRIVATE,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1004:
#line 6958 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_LASTPRIVATE,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1005:
#line 6964 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_COPYIN,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1006:
#line 6970 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_SHARED,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1007:
#line 6975 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_DEFAULT,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1008:
#line 6981 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
		(yyval.ll_node)->entry.string_val = (char *) "private";
		(yyval.ll_node)->type = global_string;
	;}
    break;

  case 1009:
#line 6987 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
		(yyval.ll_node)->entry.string_val = (char *) "shared";
		(yyval.ll_node)->type = global_string;
	;}
    break;

  case 1010:
#line 6993 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
		(yyval.ll_node)->entry.string_val = (char *) "none";
		(yyval.ll_node)->type = global_string;
	;}
    break;

  case 1011:
#line 7000 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_IF,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1012:
#line 7006 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_NUM_THREADS,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1013:
#line 7012 "gram1.y"
    {
		PTR_LLND q;
		q = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
		(yyval.ll_node) = make_llnd(fi,OMP_REDUCTION,q,LLNULL,SMNULL);
	;}
    break;

  case 1014:
#line 7019 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi,DDOT,(yyvsp[(2) - (4)].ll_node),(yyvsp[(4) - (4)].ll_node),SMNULL);;}
    break;

  case 1016:
#line 7025 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "+";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1017:
#line 7031 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "-";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1018:
#line 7038 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "*";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1019:
#line 7044 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "/";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1020:
#line 7050 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "min";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1021:
#line 7056 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "max";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1022:
#line 7062 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) ".or.";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1023:
#line 7068 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) ".and.";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1024:
#line 7074 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) ".eqv.";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1025:
#line 7080 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) ".neqv.";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1026:
#line 7086 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "iand";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1027:
#line 7092 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "ieor";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1028:
#line 7098 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "ior";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1029:
#line 7104 "gram1.y"
    { err("Illegal reduction operation name", 70);
               errcnt--;
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "unknown";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1030:
#line 7114 "gram1.y"
    {
  	   (yyval.bf_node) = make_sections((yyvsp[(4) - (4)].ll_node));
	   opt_kwd_ = NO;
	;}
    break;

  case 1031:
#line 7119 "gram1.y"
    {
  	   (yyval.bf_node) = make_sections(LLNULL);
	   opt_kwd_ = NO;
	;}
    break;

  case 1032:
#line 7125 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
	;}
    break;

  case 1033:
#line 7129 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node),(yyvsp[(4) - (5)].ll_node),EXPR_LIST);
	;}
    break;

  case 1038:
#line 7141 "gram1.y"
    {
		PTR_LLND q;
   	        (yyval.bf_node) = make_endsections();
		q = set_ll_list((yyvsp[(4) - (4)].ll_node),LLNULL,EXPR_LIST);
                (yyval.bf_node)->entry.Template.ll_ptr1 = q;
                opt_kwd_ = NO;
	;}
    break;

  case 1039:
#line 7149 "gram1.y"
    {
   	        (yyval.bf_node) = make_endsections();
	        opt_kwd_ = NO; 
	;}
    break;

  case 1040:
#line 7155 "gram1.y"
    {
           (yyval.bf_node) = make_ompsection();
	;}
    break;

  case 1041:
#line 7161 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_DO_DIR,SMNULL,(yyvsp[(4) - (4)].ll_node),LLNULL,LLNULL);
	   opt_kwd_ = NO;
	;}
    break;

  case 1042:
#line 7166 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_DO_DIR,SMNULL,LLNULL,LLNULL,LLNULL);
	   opt_kwd_ = NO;
	;}
    break;

  case 1043:
#line 7172 "gram1.y"
    {
		PTR_LLND q;
		q = set_ll_list((yyvsp[(4) - (4)].ll_node),LLNULL,EXPR_LIST);
	        (yyval.bf_node) = get_bfnd(fi,OMP_END_DO_DIR,SMNULL,q,LLNULL,LLNULL);
      	        opt_kwd_ = NO;
	;}
    break;

  case 1044:
#line 7179 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_END_DO_DIR,SMNULL,LLNULL,LLNULL,LLNULL);
	   opt_kwd_ = NO;
	;}
    break;

  case 1045:
#line 7185 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
	;}
    break;

  case 1046:
#line 7189 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node),(yyvsp[(4) - (5)].ll_node),EXPR_LIST);
	;}
    break;

  case 1053:
#line 7203 "gram1.y"
    {
		/*$$ = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
		$$->entry.string_val = (char *) "ORDERED";
		$$->type = global_string;*/
                (yyval.ll_node) = make_llnd(fi,OMP_ORDERED,LLNULL,LLNULL,SMNULL);
	;}
    break;

  case 1054:
#line 7212 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_SCHEDULE,(yyvsp[(4) - (7)].ll_node),(yyvsp[(6) - (7)].ll_node),SMNULL);
	;}
    break;

  case 1055:
#line 7216 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_SCHEDULE,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1056:
#line 7222 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
		(yyval.ll_node)->entry.string_val = (char *) "STATIC";
		(yyval.ll_node)->type = global_string;
		
	;}
    break;

  case 1057:
#line 7229 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
		(yyval.ll_node)->entry.string_val = (char *) "DYNAMIC";
		(yyval.ll_node)->type = global_string;
		
	;}
    break;

  case 1058:
#line 7236 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
		(yyval.ll_node)->entry.string_val = (char *) "GUIDED";
		(yyval.ll_node)->type = global_string;
		
	;}
    break;

  case 1059:
#line 7243 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
		(yyval.ll_node)->entry.string_val = (char *) "RUNTIME";
		(yyval.ll_node)->type = global_string;
		
	;}
    break;

  case 1060:
#line 7252 "gram1.y"
    {
  	   (yyval.bf_node) = make_single();
           (yyval.bf_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (4)].ll_node);
	   opt_kwd_ = NO;
	;}
    break;

  case 1061:
#line 7258 "gram1.y"
    {
  	   (yyval.bf_node) = make_single();
	   opt_kwd_ = NO;
	;}
    break;

  case 1062:
#line 7264 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
	;}
    break;

  case 1063:
#line 7268 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node),(yyvsp[(4) - (5)].ll_node),EXPR_LIST);
	;}
    break;

  case 1066:
#line 7278 "gram1.y"
    {
  	   (yyval.bf_node) = make_endsingle();
           (yyval.bf_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (4)].ll_node);
	   opt_kwd_ = NO;
	;}
    break;

  case 1067:
#line 7284 "gram1.y"
    {
  	   (yyval.bf_node) = make_endsingle();
	   opt_kwd_ = NO;
	;}
    break;

  case 1068:
#line 7290 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
	;}
    break;

  case 1069:
#line 7294 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node),(yyvsp[(4) - (5)].ll_node),EXPR_LIST);
	;}
    break;

  case 1072:
#line 7305 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_COPYPRIVATE,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1073:
#line 7311 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_NOWAIT,LLNULL,LLNULL,SMNULL);
	;}
    break;

  case 1074:
#line 7317 "gram1.y"
    {
           (yyval.bf_node) = make_workshare();
	;}
    break;

  case 1075:
#line 7322 "gram1.y"
    {
		PTR_LLND q;
   	        (yyval.bf_node) = make_endworkshare();
		q = set_ll_list((yyvsp[(4) - (4)].ll_node),LLNULL,EXPR_LIST);
                (yyval.bf_node)->entry.Template.ll_ptr1 = q;
  	        opt_kwd_ = NO;
	;}
    break;

  case 1076:
#line 7330 "gram1.y"
    {
   	        (yyval.bf_node) = make_endworkshare();
                opt_kwd_ = NO;
	;}
    break;

  case 1077:
#line 7336 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_PARALLEL_DO_DIR,SMNULL,(yyvsp[(4) - (4)].ll_node),LLNULL,LLNULL);
	   opt_kwd_ = NO;
	;}
    break;

  case 1078:
#line 7341 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_PARALLEL_DO_DIR,SMNULL,LLNULL,LLNULL,LLNULL);
	   opt_kwd_ = NO;
	;}
    break;

  case 1079:
#line 7348 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
	;}
    break;

  case 1080:
#line 7352 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node),(yyvsp[(4) - (5)].ll_node),EXPR_LIST);
	;}
    break;

  case 1092:
#line 7372 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_END_PARALLEL_DO_DIR,SMNULL,LLNULL,LLNULL,LLNULL);
	;}
    break;

  case 1093:
#line 7377 "gram1.y"
    {
           (yyval.bf_node) = make_parallelsections((yyvsp[(4) - (4)].ll_node));
	   opt_kwd_ = NO;
	;}
    break;

  case 1094:
#line 7382 "gram1.y"
    {
           (yyval.bf_node) = make_parallelsections(LLNULL);
	   opt_kwd_ = NO;
	;}
    break;

  case 1095:
#line 7389 "gram1.y"
    {
           (yyval.bf_node) = make_endparallelsections();
	;}
    break;

  case 1096:
#line 7394 "gram1.y"
    {
           (yyval.bf_node) = make_parallelworkshare();
           (yyval.bf_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (4)].ll_node);
	   opt_kwd_ = NO;
	;}
    break;

  case 1097:
#line 7400 "gram1.y"
    {
           (yyval.bf_node) = make_parallelworkshare();
	   opt_kwd_ = NO;
	;}
    break;

  case 1098:
#line 7406 "gram1.y"
    {
           (yyval.bf_node) = make_endparallelworkshare();
	;}
    break;

  case 1099:
#line 7411 "gram1.y"
    { 
	   (yyval.bf_node) = get_bfnd(fi,OMP_THREADPRIVATE_DIR, SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL);
	;}
    break;

  case 1100:
#line 7416 "gram1.y"
    {
  	   (yyval.bf_node) = make_master();
	;}
    break;

  case 1101:
#line 7421 "gram1.y"
    {
  	   (yyval.bf_node) = make_endmaster();
	;}
    break;

  case 1102:
#line 7425 "gram1.y"
    {
  	   (yyval.bf_node) = make_ordered();
	;}
    break;

  case 1103:
#line 7430 "gram1.y"
    {
  	   (yyval.bf_node) = make_endordered();
	;}
    break;

  case 1104:
#line 7435 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_BARRIER_DIR,SMNULL,LLNULL,LLNULL,LLNULL);
	;}
    break;

  case 1105:
#line 7439 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_ATOMIC_DIR,SMNULL,LLNULL,LLNULL,LLNULL);
	;}
    break;

  case 1106:
#line 7444 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_FLUSH_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);
	;}
    break;

  case 1107:
#line 7448 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_FLUSH_DIR,SMNULL,LLNULL,LLNULL,LLNULL);
	;}
    break;

  case 1108:
#line 7454 "gram1.y"
    {
  	   (yyval.bf_node) = make_critical();
           (yyval.bf_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
	;}
    break;

  case 1109:
#line 7459 "gram1.y"
    {
  	   (yyval.bf_node) = make_critical();
	;}
    break;

  case 1110:
#line 7465 "gram1.y"
    {
  	   (yyval.bf_node) = make_endcritical();
           (yyval.bf_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
	;}
    break;

  case 1111:
#line 7470 "gram1.y"
    {
  	   (yyval.bf_node) = make_endcritical();
	;}
    break;

  case 1112:
#line 7476 "gram1.y"
    { 
		PTR_SYMB s;
		PTR_LLND l;
		s = make_common((yyvsp[(2) - (5)].hash_entry)); 
		l = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
		(yyval.ll_node) = make_llnd(fi,OMP_THREADPRIVATE, l, LLNULL, SMNULL);
	;}
    break;

  case 1113:
#line 7486 "gram1.y"
    {
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);
	;}
    break;

  case 1114:
#line 7490 "gram1.y"
    {	
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);
	;}
    break;

  case 1115:
#line 7494 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);
	;}
    break;

  case 1116:
#line 7498 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);
	;}
    break;

  case 1117:
#line 7503 "gram1.y"
    {
		operator_slash = 1;
	;}
    break;

  case 1118:
#line 7506 "gram1.y"
    {
		operator_slash = 0;
	;}
    break;

  case 1125:
#line 7519 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_REGION_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);;}
    break;

  case 1126:
#line 7523 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_CHECKSECTION_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 1127:
#line 7527 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_GET_ACTUAL_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL);;}
    break;

  case 1128:
#line 7529 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_GET_ACTUAL_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 1129:
#line 7531 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_GET_ACTUAL_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 1130:
#line 7535 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_ACTUAL_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL);;}
    break;

  case 1131:
#line 7537 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_ACTUAL_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 1132:
#line 7539 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_ACTUAL_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 1133:
#line 7543 "gram1.y"
    { (yyval.ll_node) = LLNULL;;}
    break;

  case 1134:
#line 7545 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 1135:
#line 7549 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 1136:
#line 7551 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 1137:
#line 7555 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (2)].ll_node);;}
    break;

  case 1138:
#line 7558 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (2)].ll_node);;}
    break;

  case 1139:
#line 7561 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (2)].ll_node);;}
    break;

  case 1140:
#line 7566 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_INOUT_OP,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1141:
#line 7568 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_IN_OP,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1142:
#line 7570 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_OUT_OP,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1143:
#line 7572 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_LOCAL_OP,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1144:
#line 7574 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_INLOCAL_OP,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1145:
#line 7578 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_TARGETS_OP,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1146:
#line 7582 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_ASYNC_OP,LLNULL,LLNULL,SMNULL);;}
    break;

  case 1147:
#line 7587 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 1148:
#line 7591 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 1149:
#line 7593 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 1150:
#line 7597 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_HOST_OP, LLNULL,LLNULL,SMNULL);;}
    break;

  case 1151:
#line 7599 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_CUDA_OP, LLNULL,LLNULL,SMNULL);;}
    break;

  case 1152:
#line 7603 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_END_REGION_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 1153:
#line 7607 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_END_CHECKSECTION_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;


/* Line 1267 of yacc.c.  */
#line 12961 "gram1.tab.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEOF && yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}



